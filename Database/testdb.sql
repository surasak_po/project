-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 10, 2020 at 08:06 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_no` int(11) NOT NULL,
  `admin_code` varchar(20) NOT NULL,
  `admin_email` text NOT NULL,
  `admin_tel` char(10) NOT NULL,
  `admin_password` varchar(20) NOT NULL,
  `admin_fullname` varchar(100) NOT NULL,
  `admin_cardpicture` text NOT NULL,
  `admin_address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_no`, `admin_code`, `admin_email`, `admin_tel`, `admin_password`, `admin_fullname`, `admin_cardpicture`, `admin_address`) VALUES
(1, 'admin0001', 'prayhuth@gmail.com', '0834523215', 'prayhuth044', 'นายประหยัด  ซันโทรลา', '', '143/2 กรุงเทพมหานคร 10000'),
(2, 'ADXXXX', 'ADXXXX', 'ADXXXX', 'ADXXXX', 'ADXXXX', 'ADXXXX', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart`
--

CREATE TABLE `tbl_cart` (
  `cart_no` int(11) NOT NULL,
  `cart_code` varchar(20) NOT NULL,
  `customer_code` varchar(20) NOT NULL,
  `store_code` varchar(20) NOT NULL,
  `shipping_code` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_cart`
--

INSERT INTO `tbl_cart` (`cart_no`, `cart_code`, `customer_code`, `store_code`, `shipping_code`) VALUES
(71, 'CART0000071-3', 'CUS0000001-8', 'STORE0000001-9', 'SHIP0000006-9');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart_detail`
--

CREATE TABLE `tbl_cart_detail` (
  `cart_detail_no` int(11) NOT NULL,
  `cart_detail_code` varchar(20) NOT NULL,
  `cart_detail_status` char(1) NOT NULL DEFAULT '1',
  `cart_detail_num` int(7) NOT NULL DEFAULT 1,
  `cart_code` varchar(20) NOT NULL,
  `product_group_code` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

CREATE TABLE `tbl_customer` (
  `customer_no` int(11) NOT NULL,
  `customer_code` varchar(20) NOT NULL,
  `customer_email` text NOT NULL,
  `customer_tel` char(10) NOT NULL,
  `customer_password` varchar(20) NOT NULL,
  `customer_fullname` varchar(100) NOT NULL,
  `customer_profile` text NOT NULL DEFAULT 'guest.jpg',
  `customer_banned` char(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_customer`
--

INSERT INTO `tbl_customer` (`customer_no`, `customer_code`, `customer_email`, `customer_tel`, `customer_password`, `customer_fullname`, `customer_profile`, `customer_banned`) VALUES
(1, 'CUS0000001-8', 'surasak@gmail.com', '0832521523', 'surasak052', 'นายสุรศักดิ์  พงษ์ธานี', 'CUS0000001-8.png', '0'),
(2, 'CUS0000002-7', 'chaiyantanik@gmail.com', '0809319794', 'chaiyan016', 'นายชัยญาณธนิก  วงศ์บุญ', 'guest.jpg', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer_address`
--

CREATE TABLE `tbl_customer_address` (
  `customer_address_no` int(11) NOT NULL,
  `customer_address_code` varchar(20) NOT NULL,
  `customer_address_fullname` varchar(100) NOT NULL DEFAULT '-',
  `customer_address_tel` char(10) NOT NULL DEFAULT '-',
  `customer_address_province` varchar(50) NOT NULL DEFAULT '-',
  `customer_address_district` varchar(50) NOT NULL DEFAULT '-',
  `customer_address_subdistrict` text NOT NULL,
  `customer_address_postcode` char(5) NOT NULL DEFAULT '-',
  `customer_address_detail` text NOT NULL DEFAULT '-',
  `customer_address_status` char(1) NOT NULL DEFAULT '0',
  `customer_code` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_customer_address`
--

INSERT INTO `tbl_customer_address` (`customer_address_no`, `customer_address_code`, `customer_address_fullname`, `customer_address_tel`, `customer_address_province`, `customer_address_district`, `customer_address_subdistrict`, `customer_address_postcode`, `customer_address_detail`, `customer_address_status`, `customer_code`) VALUES
(2, 'CUSADDRESS0000002-9', 'นายชัยญาณธนิก  วงศ์บุญ', '0809319794', 'กรุงเทพมหานคร', 'คลองสามวา', 'สามวาตะวันตก', '10510', '22/405 เลียบคลองสอง 23', '1', 'CUS0000002-7'),
(3, 'CUSADDRESS0000003-5', 'ไพลิน', '0684528545', 'กำแพงเพชร', 'คลองขลุง', 'คลองสมบูรณ์', '62120', '143/2', '1', 'CUS0000001-8');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_image_product`
--

CREATE TABLE `tbl_image_product` (
  `image_product_no` int(11) NOT NULL,
  `image_product_code` varchar(20) NOT NULL,
  `image_product_name` text NOT NULL,
  `product_code` varchar(20) NOT NULL,
  `image_product_banned` char(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_image_product`
--

INSERT INTO `tbl_image_product` (`image_product_no`, `image_product_code`, `image_product_name`, `product_code`, `image_product_banned`) VALUES
(2, 'IMGPRO0000002-3', 'IMGPRO0000002-3.png', 'PRO0000002-4', '0'),
(3, 'IMGPRO0000003-4', 'IMGPRO0000003-4.png', 'PRO0000003-4', '0'),
(5, 'IMGPRO0000005-7', 'IMGPRO0000005-7.png', 'PRO0000001-9', '0'),
(7, 'IMGPRO0000007-7', 'IMGPRO0000007-7.png', 'PRO0000002-4', '0'),
(8, 'IMGPRO0000008-0', 'IMGPRO0000008-0.png', 'PRO0000002-4', '0'),
(9, 'IMGPRO0000009-3', 'IMGPRO0000009-3.png', 'PRO0000002-4', '0'),
(10, 'IMGPRO0000010-6', 'IMGPRO0000010-6.png', 'PRO0000004-3', '0'),
(11, 'IMGPRO0000011-6', 'IMGPRO0000011-6.png', 'PRO0000005-3', '0'),
(12, 'IMGPRO0000012-1', 'IMGPRO0000012-1.png', 'PRO0000005-3', '0'),
(13, 'IMGPRO0000013-3', 'IMGPRO0000013-3.png', 'PRO0000005-3', '0'),
(14, 'IMGPRO0000014-0', 'IMGPRO0000014-0.png', 'PRO0000005-3', '0'),
(15, 'IMGPRO0000015-4', 'IMGPRO0000015-4.png', 'PRO0000005-3', '0'),
(16, 'IMGPRO0000016-6', 'IMGPRO0000016-6.png', 'PRO0000006-6', '0'),
(17, 'IMGPRO0000017-9', 'IMGPRO0000017-9.png', 'PRO0000007-2', '0'),
(18, 'IMGPRO0000018-5', 'IMGPRO0000018-5.png', 'PRO0000008-9', '0'),
(21, 'IMGPRO0000019-4', 'IMGPRO0000019-4.png', 'PRO0000008-9', '0'),
(22, 'IMGPRO0000022-6', 'IMGPRO0000022-6.png', 'PRO0000008-9', '0'),
(23, 'IMGPRO0000023-3', 'IMGPRO0000023-3.png', 'PRO0000008-9', '0'),
(24, 'IMGPRO0000024-4', 'IMGPRO0000024-4.png', 'PRO0000009-1', '0'),
(25, 'IMGPRO0000025-0', 'IMGPRO0000025-0.png', 'PRO0000009-1', '0'),
(26, 'IMGPRO0000026-4', 'IMGPRO0000026-4.png', 'PRO0000010-2', '0'),
(27, 'IMGPRO0000027-2', 'IMGPRO0000027-2.png', 'PRO0000011-7', '0'),
(28, 'IMGPRO0000028-3', 'IMGPRO0000028-3.png', 'PRO0000011-7', '0'),
(29, 'IMGPRO0000029-1', 'IMGPRO0000029-1.png', 'PRO0000011-7', '0'),
(30, 'IMGPRO0000030-8', 'IMGPRO0000030-8.png', 'PRO0000012-3', '0'),
(31, 'IMGPRO0000031-8', 'IMGPRO0000031-8.png', 'PRO0000012-3', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nametype`
--

CREATE TABLE `tbl_nametype` (
  `nametype_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_nametype`
--

INSERT INTO `tbl_nametype` (`nametype_name`) VALUES
('คัน'),
('ต้น'),
('เล่ม'),
('ลูก'),
('ผืน'),
('ตัว'),
('ถุง'),
('ข้าง'),
('คู่'),
('อัน'),
('แผ่น'),
('ก้อน'),
('เม็ด'),
('ดอก'),
('กรวย'),
('พวง'),
('ฝัก'),
('ชุด'),
('ชิ้น'),
('กล่อง'),
('กระบอก'),
('ขวด'),
('วง');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `order_no` int(11) NOT NULL,
  `order_code` varchar(20) NOT NULL,
  `order_date` datetime NOT NULL,
  `order_status` char(1) NOT NULL DEFAULT '0',
  `tracking_number` text NOT NULL DEFAULT '-',
  `order_shipping_price` double(9,2) NOT NULL DEFAULT 0.00,
  `shipping_code` varchar(20) NOT NULL,
  `customer_code` varchar(20) NOT NULL,
  `store_code` varchar(20) NOT NULL,
  `customer_address_code` varchar(20) NOT NULL,
  `order_paystatus` char(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`order_no`, `order_code`, `order_date`, `order_status`, `tracking_number`, `order_shipping_price`, `shipping_code`, `customer_code`, `store_code`, `customer_address_code`, `order_paystatus`) VALUES
(139, 'ORDER0000001-0', '2020-10-10 20:14:41', '3', '-', 50.00, 'SHIP0000006-9', 'CUS0000001-8', 'STORE0000001-9', 'CUSADDRESS0000003-5', '1'),
(140, 'ORDER0000140-7', '2020-10-10 20:59:46', '3', '-', 50.00, 'SHIP0000006-9', 'CUS0000001-8', 'STORE0000001-9', 'CUSADDRESS0000003-5', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_detail`
--

CREATE TABLE `tbl_order_detail` (
  `order_detail_no` int(11) NOT NULL,
  `order_detail_code` varchar(20) NOT NULL,
  `order_detail_num` int(7) NOT NULL,
  `order_detail_price` double(11,2) NOT NULL,
  `product_group_code` varchar(20) NOT NULL,
  `order_code` varchar(20) NOT NULL,
  `order_detail_review` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_order_detail`
--

INSERT INTO `tbl_order_detail` (`order_detail_no`, `order_detail_code`, `order_detail_num`, `order_detail_price`, `product_group_code`, `order_code`, `order_detail_review`) VALUES
(122, 'ORDERDETAIL0000001-3', 1, 1500.00, 'PROGROUP0000004-4', 'ORDER0000001-0', 'สินค้าแย่มากๆค่ะ'),
(123, 'ORDERDETAIL0000123-1', 1, 1500.00, 'PROGROUP0000004-4', 'ORDER0000140-7', 'เนื้อผ้าดีมาก'),
(124, 'ORDERDETAIL0000124-9', 1, 18000.00, 'PROGROUP0000002-6', 'ORDER0000140-7', 'ดีมากค่ะ'),
(125, 'ORDERDETAIL0000125-5', 1, 18000.00, 'PROGROUP0000001-1', 'ORDER0000140-7', 'ดีค่ะ'),
(126, 'ORDERDETAIL0000126-7', 2, 18000.00, 'PROGROUP0000003-3', 'ORDER0000140-7', 'ดีสุดๆ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment`
--

CREATE TABLE `tbl_payment` (
  `payment_no` int(11) NOT NULL,
  `payment_code` varchar(20) NOT NULL,
  `payment_token` text NOT NULL,
  `payment_status` varchar(100) NOT NULL,
  `payment_type` varchar(50) NOT NULL,
  `customer_code` varchar(20) NOT NULL,
  `order_code` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_payment`
--

INSERT INTO `tbl_payment` (`payment_no`, `payment_code`, `payment_token`, `payment_status`, `payment_type`, `customer_code`, `order_code`) VALUES
(101, 'PAYMENT0000001-8', 'chrg_test_5lhrei7j9wz9rqkdlh7', '0', 'promptpay', 'CUS0000001-8', 'ORDER0000001-0'),
(102, 'PAYMENT0000102-4', 'chrg_test_5lhrudgpx2qzmjif8tw', '0', 'promptpay', 'CUS0000001-8', 'ORDER0000140-7');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `product_no` int(11) NOT NULL,
  `product_code` varchar(20) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_detail` text NOT NULL,
  `store_code` varchar(20) NOT NULL,
  `product_type_code` varchar(20) NOT NULL,
  `product_banned` char(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`product_no`, `product_code`, `product_name`, `product_detail`, `store_code`, `product_type_code`, `product_banned`) VALUES
(1, 'PRO0000001-9', 'ผ้าไหมตีนจก', 'ผ้าไหมตีนจก ลายพริกไทย ตีนลายปีกไก่', 'STORE0000001-9', 'prodtype0001', '0'),
(2, 'PRO0000002-4', 'ผ้าไหมยกดอกต่อตีนจก', 'ผ้าไหมยกดอกต่อตีนจก', 'STORE0000001-9', 'prodtype0001', '0'),
(3, 'PRO0000003-4', 'เสื้อผ้าฝ้ายเย็บมือ', 'เสื้อผ้าฝ้ายเย็บมือ', 'STORE0000001-9', 'prodtype0001', '0'),
(4, 'PRO0000004-3', 'เสื้อบาติก', 'เสื้อบาติก', 'STORE0000002-7', 'prodtype0001', '0'),
(5, 'PRO0000005-3', 'ออเจ้าซอส', 'ซอสหลากหลายรสจากร้านออเจ้าซอส', 'STORE0000003-5', 'prodtype0002', '0'),
(6, 'PRO0000006-6', 'นมถั่วเหลืองสูตรผสมน', 'นมถั่วเหลืองสูตรผสมนมแพะ', 'STORE0000004-0', 'prodtype0003', '0'),
(7, 'PRO0000007-2', 'นมถั่วเหลืองผสมนมแพะ', 'แบบกระเช้า', 'STORE0000004-0', 'prodtype0003', '0'),
(8, 'PRO0000008-9', 'งานกล่องหวาย', 'งานกล่องหวาย', 'STORE0000006-6', 'prodtype0004', '0'),
(9, 'PRO0000009-1', 'ถาดหวาย', 'ถาดหวาย', 'STORE0000006-6', 'prodtype0004', '0'),
(10, 'PRO0000010-2', 'ชุดถาดหวาย', '5 ใบ ขนาด 20x30x8', 'STORE0000006-6', 'prodtype0004', '0'),
(11, 'PRO0000011-7', 'แหวน', 'แหวนอัญมณี', 'STORE0000007-6', 'prodtype0005', '0'),
(12, 'PRO0000012-3', 'กำไลข้อมือ', 'กำไลข้อมือ', 'STORE0000007-6', 'prodtype0005', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_group`
--

CREATE TABLE `tbl_product_group` (
  `product_group_no` int(11) NOT NULL,
  `product_group_code` varchar(20) NOT NULL,
  `product_group_name` varchar(100) NOT NULL,
  `product_group_picture` text NOT NULL,
  `product_group_wide` double(9,2) NOT NULL DEFAULT 0.00,
  `product_group_long` double(9,2) NOT NULL DEFAULT 0.00,
  `product_group_high` double(9,2) NOT NULL DEFAULT 0.00,
  `product_group_unit_name` varchar(20) NOT NULL DEFAULT '-',
  `product_group_weight` double(9,2) NOT NULL DEFAULT 0.00,
  `product_group_price` double(11,2) NOT NULL DEFAULT 0.00,
  `product_group_num` int(7) NOT NULL DEFAULT 0,
  `product_code` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_product_group`
--

INSERT INTO `tbl_product_group` (`product_group_no`, `product_group_code`, `product_group_name`, `product_group_picture`, `product_group_wide`, `product_group_long`, `product_group_high`, `product_group_unit_name`, `product_group_weight`, `product_group_price`, `product_group_num`, `product_code`) VALUES
(1, 'PROGROUP0000001-1', 'ลายพริกไทย', 'PROGROUP0000001-1.png', 50.00, 100.00, 0.00, 'ชิ้น', 0.10, 18000.00, 6, 'PRO0000001-9'),
(2, 'PROGROUP0000002-6', 'ตีนลายขอขื่อ', 'PROGROUP0000002-6.png', 50.00, 100.00, 0.00, 'ชิ้น', 0.10, 18000.00, 7, 'PRO0000002-4'),
(3, 'PROGROUP0000003-3', 'ลายดอกแก้ว', 'PROGROUP0000003-3.png', 50.00, 100.00, 0.00, 'ชิ้น', 0.10, 18000.00, 6, 'PRO0000002-4'),
(4, 'PROGROUP0000004-4', 'เสื้อผ้าฝ้ายเย็บมือ', 'PROGROUP0000004-4.png', 40.00, 100.00, 0.00, 'ชิ้น', 0.10, 1500.00, 2, 'PRO0000003-4'),
(5, 'PROGROUP0000005-1', 'เสื้อบาติก', 'PROGROUP0000005-1.png', 50.00, 60.00, 0.00, 'ชิ้น', 0.10, 750.00, 5, 'PRO0000004-3'),
(7, 'PROGROUP0000006-8', 'ซอสน้ำจิ้มซีฟู้ด', 'PROGROUP0000006-8.png', 8.00, 0.00, 20.00, 'ชิ้น', 0.50, 45.00, 100, 'PRO0000005-3'),
(8, 'PROGROUP0000008-0', 'ซอสปิ้งย่าง', 'PROGROUP0000008-0.png', 8.00, 0.00, 20.00, 'ชิ้น', 0.50, 45.00, 80, 'PRO0000005-3'),
(9, 'PROGROUP0000009-6', 'ซอสผัดไทย', 'PROGROUP0000009-6.png', 8.00, 0.00, 20.00, 'ชิ้น', 0.50, 45.00, 45, 'PRO0000005-3'),
(10, 'PROGROUP0000010-9', 'ซอสน้ำยำ', 'PROGROUP0000010-9.png', 8.00, 0.00, 20.00, 'ชิ้น', 0.50, 45.00, 120, 'PRO0000005-3'),
(11, 'PROGROUP0000011-6', 'สูตรผสมนมแพะ 25%', 'PROGROUP0000011-6.png', 5.00, 0.00, 13.00, 'ชิ้น', 0.50, 18.00, 176, 'PRO0000006-6'),
(12, 'PROGROUP0000012-9', 'แบบกระเช้า', 'PROGROUP0000012-9.png', 25.00, 15.00, 18.00, 'ชิ้น', 2.00, 1200.00, 20, 'PRO0000007-2'),
(13, 'PROGROUP0000013-1', 'สีธรรมชาติ เบอร์ 4', 'PROGROUP0000013-1.png', 15.00, 23.00, 8.00, 'ชิ้น', 80.00, 90.00, 20, 'PRO0000008-9'),
(14, 'PROGROUP0000014-0', 'สีธรรมชาติ เบอร์ 5', 'PROGROUP0000014-0.png', 13.00, 19.00, 8.00, 'ชิ้น', 70.00, 70.00, 15, 'PRO0000008-9'),
(15, 'PROGROUP0000015-6', 'ผสมกก เบอร์ 4', 'PROGROUP0000015-6.png', 15.00, 23.00, 8.00, 'ชิ้น', 80.00, 80.00, 5, 'PRO0000009-1'),
(16, 'PROGROUP0000016-8', 'ผสมกก เบอร์ 5', 'PROGROUP0000016-8.png', 23.00, 30.00, 8.00, 'ชิ้น', 80.00, 130.00, 15, 'PRO0000009-1'),
(17, 'PROGROUP0000017-2', 'ผสมกก 5 ใบ', 'PROGROUP0000017-2.png', 20.00, 30.00, 8.00, 'ชิ้น', 565.00, 450.00, 5, 'PRO0000010-2'),
(18, 'PROGROUP0000018-0', 'แหวนบุษราคัม', 'PROGROUP0000018-0.png', 3.00, 3.00, 3.00, 'ชิ้น', 10.00, 90000.00, 3, 'PRO0000011-7'),
(19, 'PROGROUP0000019-8', 'แหวนไพลิน', 'PROGROUP0000019-8.png', 2.50, 3.00, 3.00, 'ชิ้น', 5.00, 38000.00, 7, 'PRO0000011-7'),
(20, 'PROGROUP0000020-9', 'แหวนไพลินชาย', 'PROGROUP0000020-9.png', 2.50, 2.50, 3.00, 'ชิ้น', 15.00, 90000.00, 6, 'PRO0000011-7'),
(21, 'PROGROUP0000021-9', 'ข้อมือแฟนซี', 'PROGROUP0000021-9.png', 2.00, 17.00, 0.00, 'ชิ้น', 40.00, 52000.00, 2, 'PRO0000012-3'),
(22, 'PROGROUP0000022-0', 'ข้อมือทับทิมชาย', 'PROGROUP0000022-0.png', 6.50, 0.00, 0.00, 'ชิ้น', 55.00, 90000.00, 1, 'PRO0000012-3');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_type`
--

CREATE TABLE `tbl_product_type` (
  `product_type_no` int(11) NOT NULL,
  `product_type_code` varchar(20) NOT NULL,
  `product_type_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_product_type`
--

INSERT INTO `tbl_product_type` (`product_type_no`, `product_type_code`, `product_type_name`) VALUES
(1, 'prodtype0001', 'เสื้อผ้า'),
(2, 'prodtype0002', 'อาหาร'),
(3, 'prodtype0003', 'เครื่องดื่ม'),
(4, 'prodtype0004', 'ของใช้'),
(5, 'prodtype0005', 'เครื่องประดับ'),
(6, 'prodtype0006', 'สมุนไพร');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shipping`
--

CREATE TABLE `tbl_shipping` (
  `shipping_no` int(11) NOT NULL,
  `shipping_code` varchar(20) NOT NULL,
  `shipping_price` double(9,2) NOT NULL DEFAULT 0.00,
  `transport_code` varchar(20) NOT NULL,
  `store_code` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_shipping`
--

INSERT INTO `tbl_shipping` (`shipping_no`, `shipping_code`, `shipping_price`, `transport_code`, `store_code`) VALUES
(7, 'SHIP0000006-9', 50.00, 'TRAN0000001-3', 'STORE0000001-9'),
(8, 'SHIP0000008-4', 20.00, 'TRAN0000009-9', 'STORE0000007-6'),
(9, 'SHIP0000009-4', 100.00, 'TRAN0000009-9', 'STORE0000006-6'),
(10, 'SHIP0000010-1', 88.00, 'TRAN0000001-3', 'STORE0000005-5'),
(11, 'SHIP0000011-3', 200.00, 'TRAN0000001-3', 'STORE0000004-0'),
(12, 'SHIP0000012-7', 100.00, 'TRAN0000001-3', 'STORE0000003-5'),
(14, 'SHIP0000013-5', 20.00, 'TRAN0000009-9', 'STORE0000002-7');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_store`
--

CREATE TABLE `tbl_store` (
  `store_no` int(11) NOT NULL,
  `store_code` varchar(20) NOT NULL,
  `store_fullname` varchar(50) NOT NULL,
  `store_namestore` varchar(50) NOT NULL,
  `store_address` text NOT NULL,
  `store_email` text NOT NULL,
  `store_tel` char(10) NOT NULL,
  `store_password` varchar(20) NOT NULL,
  `store_cardpicture` text NOT NULL,
  `store_picture` text NOT NULL DEFAULT '\'gueststore.jpg\'',
  `store_bank_code` char(10) NOT NULL,
  `store_bank_brand` varchar(50) NOT NULL,
  `store_nameinbank` varchar(100) NOT NULL,
  `store_income` double(7,2) NOT NULL DEFAULT 0.00,
  `store_omise_receive_code` text NOT NULL,
  `store_status` char(1) NOT NULL DEFAULT '0',
  `admin_code` varchar(20) NOT NULL DEFAULT 'ADXXXX'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_store`
--

INSERT INTO `tbl_store` (`store_no`, `store_code`, `store_fullname`, `store_namestore`, `store_address`, `store_email`, `store_tel`, `store_password`, `store_cardpicture`, `store_picture`, `store_bank_code`, `store_bank_brand`, `store_nameinbank`, `store_income`, `store_omise_receive_code`, `store_status`, `admin_code`) VALUES
(1, 'STORE0000001-9', 'อุษา บุยสาพิพัฒน์', 'กลุ่มทอผ้าโบราณ', '2 หมู่ 1 บ้านเนินขาม ตำบลเนินขาม กิ่งอำเภอเนินขาม จังหวัดชัยนาท 17130', 'store1@gmail.com', '0810364720', 'store001', 'STORE0000001-9card.png', 'STORE0000001-9store.png', '2601423525', 'kbank', 'อุษา บุยสาพิพัฒน์', 99999.99, 'recp_test_5lcn909ypxsnt95al2m', '1', 'admin0001'),
(2, 'STORE0000002-7', 'สายใจ บูระเพ็ง', 'กลุ่มสายใจบาติก', '32/1 หมู่ 5 ถนนกระบี่ - เขาทอง ตำบลปากน้ำ อำเภอเมือง จังหวัดกระบี่ 81000', 'store2@gmail.com', '0831066577', 'store002', 'STORE0000002-7card.png', 'STORE0000002-7store.png', '6250622121', 'ktb', 'สายใจ บูระเพ็ง', 2270.00, 'recp_test_5ld03ljn81cny05c3lb', '1', 'ADXXXX'),
(3, 'STORE0000003-5', 'รมิดา วัชระพันธ์', 'ออเจ้าซอส', '277 ซอยประชาราษฎร์บำเพ็ญ 5 ถนนประชาราษฎร์บำเพ็ญ แขวงห้วยขวาง เขตห้วยขวาง กทม. 10310', 'store3@gmail.com', '0616614549', 'store003', 'STORE0000003-5card.png', 'STORE0000003-5store.png', '4537323474', 'kbank', 'รมิดา วัชระพันธ์', 0.00, 'recp_test_5ld0f2bqfmdpo3b77xw', '1', 'ADXXXX'),
(4, 'STORE0000004-0', 'สกล พงษ์นุ่มกุล', 'บ.ตอยยีบันฟู๊ดส์จำกัด', '163/15 หมู่1 ตำบลท่าสาป อำเภอเมือง จังหวัดยะลา 95000', 'store4@gmail.com', '0824262502', 'store004', 'STORE0000004-0card.png', 'STORE0000004-0store.png', '2346234689', 'ktb', 'สกล พงษ์นุ่มกุล', 0.00, 'recp_test_5ld0ff2q6g50kvo8mx7', '1', 'ADXXXX'),
(5, 'STORE0000005-5', 'บุญพา รอดดี', 'น้ำมันเขียวนายเสริม ', '147 หมู่ 8 ต.ท่ามะกา อ.ท่ามะกา จ.กาญจนบุรี 71120', 'store5@gmail.com', '0961673702', 'store005', 'STORE0000005-5card.png', 'gueststore.jpg', '7429429883', 'scb', 'บุญพา รอดดี', 0.00, 'recp_test_5ld0fs3drgpamu5u4ct', '1', 'ADXXXX'),
(6, 'STORE0000006-6', 'เสาวนีย์ เสวกวิหารี', 'กลุ่มจักสานหวายบ้านโคกพร้าว (Bankokprao)', '15/4 หมู่ 5 หมู่บ้านโคกพร้าว ถนนสิงห์บุรี-สุพรรณบุรี ตำบลบางระจัน อำเภอค่ายบางระจัน จังหวัดสิงห์บุรี 16150', 'store6@gmail.com', '0809562177', 'store006', 'STORE0000006-6card.png', 'STORE0000006-6store.png', '4725890432', 'kbank', 'เสาวนีย์ เสวกวิหารี', 0.00, 'recp_test_5ld0g7iep8061icwwzm', '1', 'ADXXXX'),
(7, 'STORE0000007-6', 'โชคชัย วิวิธจินดา', 'กลุ่มเจียระไนพลอย', '163 หมู่ 3 บ้านบ่อไร่ ตำบลบ่อพลอย อำเภอบ่อไร่ จังหวัดตราด 23140', 'store7@gmail.com', '0815910803', 'store007', 'STORE0000007-6card.png', 'STORE0000007-6store.png', '7203684323', 'ktb', 'โชคชัย วิวิธจินดา', 99999.99, 'recp_test_5ld0gm4jot4w6knlysc', '1', 'ADXXXX');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transport`
--

CREATE TABLE `tbl_transport` (
  `transport_no` int(11) NOT NULL,
  `transport_code` varchar(20) NOT NULL,
  `transport_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_transport`
--

INSERT INTO `tbl_transport` (`transport_no`, `transport_code`, `transport_name`) VALUES
(8, 'TRAN0000001-3', 'Kerry-Express-th'),
(9, 'TRAN0000009-9', 'J&T-Express-th');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_withdraw_money`
--

CREATE TABLE `tbl_withdraw_money` (
  `withdraw_money_no` int(11) NOT NULL,
  `withdraw_money_code` varchar(20) NOT NULL,
  `withdraw_money_num` double(9,2) NOT NULL DEFAULT 20.00,
  `withdraw_money_key` text NOT NULL,
  `withdraw_money_status` char(1) NOT NULL DEFAULT '0',
  `withdraw_money_date` datetime NOT NULL,
  `store_code` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_code`),
  ADD KEY `admin_no` (`admin_no`);

--
-- Indexes for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  ADD PRIMARY KEY (`cart_code`),
  ADD KEY `cart_no` (`cart_no`);

--
-- Indexes for table `tbl_cart_detail`
--
ALTER TABLE `tbl_cart_detail`
  ADD PRIMARY KEY (`cart_detail_code`),
  ADD KEY `cart_detail_no` (`cart_detail_no`),
  ADD KEY `cart_code` (`cart_code`);

--
-- Indexes for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  ADD PRIMARY KEY (`customer_code`),
  ADD KEY `customer_no` (`customer_no`);

--
-- Indexes for table `tbl_customer_address`
--
ALTER TABLE `tbl_customer_address`
  ADD PRIMARY KEY (`customer_address_code`),
  ADD KEY `customer_address_no` (`customer_address_no`);

--
-- Indexes for table `tbl_image_product`
--
ALTER TABLE `tbl_image_product`
  ADD PRIMARY KEY (`image_product_code`),
  ADD KEY `image_product_no` (`image_product_no`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`order_code`),
  ADD KEY `order_no` (`order_no`);

--
-- Indexes for table `tbl_order_detail`
--
ALTER TABLE `tbl_order_detail`
  ADD PRIMARY KEY (`order_detail_code`),
  ADD KEY `order_detail_no` (`order_detail_no`),
  ADD KEY `tbl_order_detail_ibfk_1` (`order_code`);

--
-- Indexes for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  ADD PRIMARY KEY (`payment_code`),
  ADD KEY `payment_no` (`payment_no`),
  ADD KEY `customer_code` (`customer_code`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`product_code`),
  ADD KEY `product_no` (`product_no`);

--
-- Indexes for table `tbl_product_group`
--
ALTER TABLE `tbl_product_group`
  ADD PRIMARY KEY (`product_group_code`),
  ADD KEY `product_group_no` (`product_group_no`),
  ADD KEY `product_code` (`product_code`);

--
-- Indexes for table `tbl_product_type`
--
ALTER TABLE `tbl_product_type`
  ADD PRIMARY KEY (`product_type_code`),
  ADD KEY `product_type_no` (`product_type_no`);

--
-- Indexes for table `tbl_shipping`
--
ALTER TABLE `tbl_shipping`
  ADD PRIMARY KEY (`shipping_code`),
  ADD KEY `shipping_no` (`shipping_no`),
  ADD KEY `store_code` (`store_code`);

--
-- Indexes for table `tbl_store`
--
ALTER TABLE `tbl_store`
  ADD PRIMARY KEY (`store_code`),
  ADD KEY `store_no` (`store_no`);

--
-- Indexes for table `tbl_transport`
--
ALTER TABLE `tbl_transport`
  ADD PRIMARY KEY (`transport_code`),
  ADD KEY `transport_no` (`transport_no`);

--
-- Indexes for table `tbl_withdraw_money`
--
ALTER TABLE `tbl_withdraw_money`
  ADD PRIMARY KEY (`withdraw_money_code`),
  ADD KEY `withdraw_money_no` (`withdraw_money_no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  MODIFY `cart_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `tbl_cart_detail`
--
ALTER TABLE `tbl_cart_detail`
  MODIFY `cart_detail_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;

--
-- AUTO_INCREMENT for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  MODIFY `customer_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_customer_address`
--
ALTER TABLE `tbl_customer_address`
  MODIFY `customer_address_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_image_product`
--
ALTER TABLE `tbl_image_product`
  MODIFY `image_product_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `order_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT for table `tbl_order_detail`
--
ALTER TABLE `tbl_order_detail`
  MODIFY `order_detail_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  MODIFY `payment_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `product_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_product_group`
--
ALTER TABLE `tbl_product_group`
  MODIFY `product_group_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tbl_product_type`
--
ALTER TABLE `tbl_product_type`
  MODIFY `product_type_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_shipping`
--
ALTER TABLE `tbl_shipping`
  MODIFY `shipping_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_store`
--
ALTER TABLE `tbl_store`
  MODIFY `store_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_transport`
--
ALTER TABLE `tbl_transport`
  MODIFY `transport_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_withdraw_money`
--
ALTER TABLE `tbl_withdraw_money`
  MODIFY `withdraw_money_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_cart_detail`
--
ALTER TABLE `tbl_cart_detail`
  ADD CONSTRAINT `tbl_cart_detail_ibfk_1` FOREIGN KEY (`cart_code`) REFERENCES `tbl_cart` (`cart_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_order_detail`
--
ALTER TABLE `tbl_order_detail`
  ADD CONSTRAINT `tbl_order_detail_ibfk_1` FOREIGN KEY (`order_code`) REFERENCES `tbl_order` (`order_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  ADD CONSTRAINT `tbl_payment_ibfk_1` FOREIGN KEY (`customer_code`) REFERENCES `tbl_customer` (`customer_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_product_group`
--
ALTER TABLE `tbl_product_group`
  ADD CONSTRAINT `tbl_product_group_ibfk_1` FOREIGN KEY (`product_code`) REFERENCES `tbl_product` (`product_code`);

--
-- Constraints for table `tbl_shipping`
--
ALTER TABLE `tbl_shipping`
  ADD CONSTRAINT `tbl_shipping_ibfk_1` FOREIGN KEY (`store_code`) REFERENCES `tbl_store` (`store_code`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
