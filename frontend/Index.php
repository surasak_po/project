<?php
    include "../restapi/setting/config.php";
    @session_start();
    @session_cache_expire(30);

    if(@$_SESSION['role'] == "admin"){
        echo "<meta  http-equiv='refresh' content='0;URL=1AdminCustomer.php'>";
    }
    if(@$_SESSION['role'] == "store"){
        echo "<meta  http-equiv='refresh' content='0;URL=Store.php'>";
    }

    @$producttypecode = $_GET['producttypecode'];
    @$productname = $_GET['productname'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>หน้าหลัก</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="./css/styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <style>
        @font-face {
            font-family: 'Opun-Regular.ttf';
            src: url('fonts/Opun-Regular.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }

        .card {
            background-color: #F88360;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }

        .card:hover {
            background-color: #F88360;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
            border: 2px solid #FF5F0F;
        }


        .cardproducttype {
            background-color: White;

            text-align: center;
        }

        .cardproducttype:hover {
            background-color: White;

            text-align: center;
            border: 2px solid #FF5F0F;
        }

        .modal-header .close {
            display: none;
        }
    </style>
</head>

<script>
    setInterval(function () {
        var customer_code = "<?php echo @$_SESSION['customer_code'];?>";

        if (customer_code != "") { //ถ้า login
            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async: false,
                url: "../restapi/getcartnum.php",
                data: JSON.stringify({
                    customer_code: customer_code
                }),
                success: function (response) {
                    var json_data = response;
                    var getdata = json_data;
                    document.getElementById("badge_cart_num").innerHTML = getdata.cartnum;
                }
            });

            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async: false,
                url: "../restapi/getcartinfologin.php",
                data: JSON.stringify({
                    customer_code: customer_code
                }),
                success: function (response) {
                    var json_data = response;
                    var apistatus = json_data.result;

                    if (apistatus == "empty") {

                        strHTML = "<div class=\"container\">"
                        strHTML += "<div class=\"row\">"

                        strHTML += "<div class=\"col-md-1\">"
                        strHTML += "</div>"

                        strHTML += "<div class=\"col-md-8\">"
                        strHTML +=
                            "<img src=\"icons/empty-cart.png\"  width=\"250px\" height=\"200px\" >";
                        strHTML +=
                            "<p style=\"font-size:16px;text-align:center;font-family:'Opun-Regular.ttf'!important;Padding-left:40px;\" >ยังไม่มีสินค้าในตะกร้า</p>"
                        strHTML += "</div>"

                        strHTML += "<div class=\"col-md-2\">"
                        strHTML += "</div>"

                        strHTML += "</div>"
                        strHTML += "</div>"
                        $('#list-cart').html(strHTML);
                    } else {

                        strHTML =
                            " <table class=\"table table-bordered\" style=\"font-size:10px;font-family:'Opun-Regular.ttf'!important;\">";
                        strHTML += "<tbody id=\"cart_list_table\">";
                        strHTML += "<tr>";
                        strHTML += "<th style=\"text-align:center;\"></th>";
                        strHTML += "<th style=\"text-align:center;\"> ชื่อสินค้า</th>";
                        strHTML += "<th style=\"text-align:center;\" width=\"5%\">จำนวน</th>";
                        strHTML +=
                        "<th style=\"text-align:center;\" width=\"20%\">ราคาต่อชิ้น</th>";
                        strHTML += "<th style=\"text-align:center;\" width=\"10%\">ราคา</th>";
                        strHTML += "<th style=\"text-align:center;\" width=\"5%\"></th>";
                        strHTML += " </tr>";
                        $.each(response, function (index) {
                            strHTML += "<tr>";
                            strHTML += "<td align=center>" +
                                "<img src=\" ../restapi/product/" + response[index]
                                .product_group_picture +
                                "\"  width=\"50px\" height=\"50px\" >" + "</td>";
                            strHTML += "<td align=center>" + response[index]
                                .product_group_name + "</td>";
                            strHTML += "<td align=center>" + response[index]
                                .cart_detail_num + "</td>";
                            strHTML += "<td align=center>" + response[index]
                                .product_group_price + "</td>";
                            strHTML += "<td align=center>" + response[index].totalprice +
                                ".00" + "</td>";
                            strHTML += "<td align=center>"+" <a  style=\"cursor:pointer;font-size:20px;\" onclick='deletecartlogin(this.id)' id=\""+response[index].product_group_code+"\" >ลบ</a>"+"</td>";
                            strHTML += "</tr>";
                        });
                        strHTML += "</tbody>";
                        strHTML += "</table>";
                        strHTML +=
                            "<div class=\"row\">  <div class=\"col-md-6\"></div> <div class=\"col-md-6\" style=\"cursor:pointer;\" onclick=\"opencart();\"><div align=\"center\"  align=\"right\" style=\"background-color:#D5AEAE;font-size:16px;font-family:'Opun-Regular.ttf'!important;\"><br>ดูตะกร้าสินค้าของคุณ<br></div></div> </div>";


                        $('#list-cart').html(strHTML);
                    }
                }
            });

        } else { //ไม่ล้อคอิน
            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async: false,
                url: "../restapi/getcartnumnologin.php",
                data: JSON.stringify({}),
                success: function (response) {
                    var json_data = response;
                    var getdata = json_data;
                    if (getdata.cartnum == "0") {
                        document.getElementById("badge_cart_num").innerHTML = "";
                    } else {

                        document.getElementById("badge_cart_num").innerHTML = getdata.cartnum;
                    }

                }
            });

            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async: false,
                url: "../restapi/getcartinfonologin.php",
                data: JSON.stringify({}),
                success: function (response) {
                    var json_data = response;
                    var apistatus = json_data.result;

                    if (apistatus == "empty") {

                        strHTML = "<div class=\"container\">"
                        strHTML += "<div class=\"row\">"

                        strHTML += "<div class=\"col-md-1\">"
                        strHTML += "</div>"

                        strHTML += "<div class=\"col-md-8\">"
                        strHTML +=
                            "<img src=\"icons/empty-cart.png\"  width=\"250px\" height=\"200px\" >";
                        strHTML +=
                            "<p style=\"font-size:16px;text-align:center;font-family:'Opun-Regular.ttf'!important;Padding-left:40px;\" >ยังไม่มีสินค้าในตะกร้า</p>"
                        strHTML += "</div>"

                        strHTML += "<div class=\"col-md-2\">"
                        strHTML += "</div>"

                        strHTML += "</div>"
                        strHTML += "</div>"
                        $('#list-cart').html(strHTML);
                    } else {

                        strHTML =
                            " <table class=\"table table-bordered\" style=\"font-size:10px;font-family:'Opun-Regular.ttf'!important;\">";
                        strHTML += "<tbody id=\"cart_list_table\">";
                        strHTML += "<tr>";
                        strHTML += "<th style=\"text-align:center;\"></th>";
                        strHTML += "<th style=\"text-align:center;\"> ชื่อสินค้า</th>";
                        strHTML += "<th style=\"text-align:center;\" width=\"5%\">จำนวน</th>";
                        strHTML +=
                        "<th style=\"text-align:center;\" width=\"20%\">ราคาต่อชิ้น</th>";
                        strHTML += "<th style=\"text-align:center;\" width=\"10%\">ราคา</th>";
                        strHTML += "<th style=\"text-align:center;\" width=\"5%\"></th>";
                        strHTML += " </tr>";
                        $.each(response, function (index) {
                            strHTML += "<tr>";
                            strHTML += "<td align=center>" +
                                "<img src=\" ../restapi/product/" + response[index]
                                .product_group_picture +
                                "\"  width=\"50px\" height=\"50px\" >" + "</td>";
                            strHTML += "<td align=center>" + response[index]
                                .product_group_name + "</td>";
                            strHTML += "<td align=center>" + response[index]
                                .cart_detail_num + "</td>";
                            strHTML += "<td align=center>" + response[index]
                                .product_group_price + "</td>";
                            strHTML += "<td align=center>" + response[index].totalprice +
                                ".00" + "</td>";
                            strHTML += "<td align=center>"+" <a style=\"cursor:pointer;\" onclick='deletecartnologin(this.id)' id=\""+response[index].product_group_code+"\" >ลบ</a>"+"</td>";
                            strHTML += "</tr>";
                        });
                        strHTML += "</tbody>";
                        strHTML += "</table>";
                        strHTML +=
                            "<div class=\"row\">  <div class=\"col-md-6\"></div> <div class=\"col-md-6\" style=\"cursor:pointer;\" onclick=\"opencart();\"><div align=\"center\"  align=\"right\" style=\"background-color:#D5AEAE;font-size:16px;font-family:'Opun-Regular.ttf'!important;\"><br>ดูตะกร้าสินค้าของคุณ<br></div></div> </div>";


                        $('#list-cart').html(strHTML);
                    }
                }
            });
        }

    }, 1000);

    function deletecartnologin(id) {

        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async: false,
            url: "../restapi/deletecartnologin.php",
            data: JSON.stringify({
                id: id
            }),
            success: function (response) {

            }
        });
    }

    function deletecartlogin(id) {

        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async: false,
            url: "../restapi/deletecartlogin.php",
            data: JSON.stringify({
                id: id
            }),
            success: function (response) {

            }
        });
    }

    function hasbanned() {
        $("#hasbanned").modal({
            backdrop: 'static',
            keyboard: false
        })
        $("#hasbanned").modal('show');
        setTimeout(function () {
            $('#hasbanned').modal('hide')
        }, 3000);
    }

    function loginfail() {
        $("#loginfail").modal({
            backdrop: 'static',
            keyboard: false
        })
        $("#loginfail").modal('show');
        setTimeout(function () {
            $('#loginfail').modal('hide')
        }, 3000);
    }

    function loginsuccess() {
        $("#loginsuccess").modal({
            backdrop: 'static',
            keyboard: false
        })
        $("#loginsuccess").modal('show');
        setTimeout(function () {
            $('#loginsuccess').modal('hide')
        }, 3000);
    }

    function logoutsuccess() {
        $("#logoutsuccess").modal({
            backdrop: 'static',
            keyboard: false
        })
        $("#logoutsuccess").modal('show');
        setTimeout(function () {
            $('#logoutsuccess').modal('hide')
        }, 2000);
    }

    function customerLoginFunction() {
        var customerUsername = document.getElementById("customerUsername").value;
        var customerPassword = document.getElementById("customerPassword").value;

        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async: false,
            url: "../restapi/logincustomer.php",
            data: JSON.stringify({
                customerUsername: customerUsername,
                customerPassword: customerPassword
            }),
            success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;

                if (apistatus == "Success") {
                    loginsuccess();
                    setTimeout(function () {
                        location.reload();
                    }, 3600);
                } else if (apistatus == "Banned") {

                    hasbanned();
                    setTimeout(function () {
                        location.reload();
                    }, 3600);
                } else {
                    loginfail();
                    document.getElementById("customerUsername").value = ""
                    document.getElementById("customerPassword").value = ""
                }
            }
        });
    }

    function storeLoginFunction() {
        var storeUsername = document.getElementById("storeUsername").value;
        var storePassword = document.getElementById("storePassword").value;

        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async: false,
            url: "../restapi/LoginStore.php",
            data: JSON.stringify({
                storeUsername: storeUsername,
                storePassword: storePassword
            }),
            success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;
                if (apistatus == "Success") {
                    loginsuccess();
                    setTimeout(function () {
                        location.replace("Store.php");
                    }, 3600);
                } else if (apistatus == "Banned") {
                    hasbanned();
                    setTimeout(function () {
                        location.reload();
                    }, 3600);
                } else {
                    loginfail();
                    document.getElementById("storeUsername").value = ""
                    document.getElementById("storePassword").value = ""
                }
            }
        });
    }

    function adminLoginFunction() {
        var adminUsername = document.getElementById("adminUsername").value;
        var adminPassword = document.getElementById("adminPassword").value;

        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async: false,
            url: "../restapi/LoginAdmin.php",
            data: JSON.stringify({
                adminUsername: adminUsername,
                adminPassword: adminPassword
            }),
            success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;


                if (apistatus == "Success") {
                    loginsuccess();
                    setTimeout(function () {
                        location.replace("1AdminCustomer.php");
                    }, 3600);
                } else if (apistatus == "Banned") {
                    hasbanned();
                    setTimeout(function () {
                        location.reload();
                    }, 3600);
                } else {
                    loginfail();
                    document.getElementById("adminUsername").value = ""
                    document.getElementById("adminPassword").value = ""
                }


            }
        });
    }

    function logoutFunction() {
        logoutsuccess();
        setTimeout(function () {
            document.forms["formLogout"].action = "../restapi/logout.php";
            document.forms["formLogout"].submit();
        }, 2600);
    }
</script>

<body>

    <!-- Modal Zone -->
    <!-- 1. Register Group Modal -->
    <div class="modal" id="registerGroupModal">
        <div class="modal-dialog">
            <div class="modal-content" align="center">

                <!-- Modal Header -->
                <div class="modal-header d-block">
                    <h2 class="modal-title" style="font-weight: bold;">เลือกประเภทการสมัครสมาชิก</h2>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div row align="center" style="margin-bottom: -25px;">
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 50px;">
                            <p style="font-size: 60px; color: #17a2b8; text-shadow: 2px 2px 2px #000000;"><i
                                    class="fas fa-users"></i></p>
                        </div>
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 10px;">
                            <p style="font-size: 60px; color: #007bff; text-shadow: 2px 2px 2px #000000;"><i
                                    class="fas fa-store"></i></p>
                        </div>
                    </div>
                    <div row align="center">
                        <div class="col-sm-5 d-inline-flex">
                            <a class="btn btn-info" type="button" href="CustomerRegister.php">สำหรับลูกค้า</a>
                        </div>
                        <div class="col-sm-5 d-inline-flex">

                            <a class="btn btn-primary" type="button" href="StoreRegister.php">สำหรับร้านค้า</a>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <br>
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-danger" type="button" data-dismiss="modal">ปิดหน้านี้</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- 1. End of Register Group Modal -->

    <!-- 2. Login Group Modal -->
    <div class="modal" id="loginGroupModal">
        <div class="modal-dialog">
            <div class="modal-content" align="center">

                <!-- Modal Header -->
                <div class="modal-header d-block">
                    <h2 class="modal-title" style="font-weight: bold;">เลือกประเภทการเข้าสู่ระบบ</h2>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div row align="center" style="margin-bottom: -25px;">
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 50px;">
                            <p style="font-size: 60px; color: #17a2b8; text-shadow: 2px 2px 2px #000000;"><i
                                    class="fas fa-users"></i></p>
                        </div>
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 10px;">
                            <p style="font-size: 60px; color: #007bff; text-shadow: 2px 2px 2px #000000;"><i
                                    class="fas fa-store"></i></p>
                        </div>
                    </div>
                    <div row align="center" style="margin-bottom: 30px;">
                        <div class="col-sm-5 d-inline-flex">
                            <button data-toggle="modal" data-target="#loginCustomerModal" class="btn btn-info"
                                type="button" data-dismiss="modal">สำหรับลูกค้า</button>
                        </div>
                        <div class="col-sm-5 d-inline-flex">
                            <button data-toggle="modal" data-target="#loginStoreModal" class="btn btn-primary"
                                type="button" data-dismiss="modal">สำหรับร้านค้า</button>
                        </div>
                    </div>
                    <div row align="center" style="margin-bottom: -25px;">
                        <p style="font-size: 60px; color: #343a40; text-shadow: 2px 2px 2px #000000;"><i
                                class="fas fa-crown"></i></p>
                    </div>
                    <div row align="center">
                        <button data-toggle="modal" data-target="#loginAdminModal" class="btn btn-dark" type="button"
                            data-dismiss="modal">สำหรับแอดมิน</button>
                    </div>
                </div>

                <!-- Modal footer -->
                <br>
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-danger" type="button" data-dismiss="modal">ปิดหน้านี้</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- 2. End of Login Group Modal -->

    <!-- 2.1 Login Customer Modal -->
    <div class="modal" id="loginCustomerModal">
        <div class="modal-dialog">
            <div class="modal-content" align="center">

                <!-- Modal Header -->
                <div class="modal-header d-block">
                    <h2 class="modal-title" style="font-weight: bold;">เข้าสู่ระบบ (สำหรับลูกค้า)</h2>
                </div>

                <!-- Modal body -->
                <form id="formLoginCustomer" name="formLoginCustomer" method="POST">
                    <div class="modal-body">
                        <img src="./icons/loginlogo.png" width="150px" height="150px">
                        <div class="form-group col-sm-9">
                            <label for="customerUsername" style="font-weight: regular;">ชื่อผู้ใช้</label>
                            <input type="text" placeholder="เบอร์โทรศัพท์หรืออีเมลล์" class="form-control"
                                id="customerUsername" name="customerUsername">
                        </div>
                        <div class="form-group col-sm-9">
                            <label for="customerPassword" style="font-weight: regular;">รหัสผ่าน</label>
                            <input type="password" placeholder="รหัสผ่าน" class="form-control" id="customerPassword"
                                name="customerPassword">
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <br>
                    <div class="modal-footer d-block" align="center">
                        <button class="btn btn-success" type="button"
                            onclick="customerLoginFunction()">เข้าสู่ระบบ</button>
                        <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#loginGroupModal"
                            data-dismiss="modal">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- 2.1 End of Login Customer Modal -->

    <!-- 2.2 Login Store Modal -->
    <div class="modal" id="loginStoreModal">
        <div class="modal-dialog">
            <div class="modal-content" align="center">

                <!-- Modal Header -->
                <div class="modal-header d-block">
                    <h2 class="modal-title" style="font-weight: bold;">เข้าสู่ระบบ (สำหรับร้านค้า)</h2>
                </div>

                <!-- Modal body -->
                <form id="formLoginStore" name="formLoginStore" method="POST">
                    <div class="modal-body">
                        <img src="./icons/loginlogo.png" width="150px" height="150px">
                        <div class="form-group col-sm-9">
                            <label for="storeUsername" style="font-weight: regular;">ชื่อผู้ใช้</label>
                            <input type="text" placeholder="เบอร์โทรศัพท์หรืออีเมลล์" class="form-control"
                                id="storeUsername" name="storeUsername">
                        </div>
                        <div class="form-group col-sm-9">
                            <label for="storePassword" style="font-weight: regular;">รหัสผ่าน</label>
                            <input type="password" placeholder="รหัสผ่าน" class="form-control" id="storePassword"
                                name="storePassword">
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer d-block" align="center">
                        <button class="btn btn-success" type="button"
                            onclick="storeLoginFunction()">เข้าสู่ระบบ</button>
                        <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#loginGroupModal"
                            data-dismiss="modal">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- 2.2 End of Login Store Modal -->

    <!-- 2.3 Admin Login Modal -->
    <div class="modal" id="loginAdminModal">
        <div class="modal-dialog">
            <div class="modal-content" align="center">

                <!-- Modal Header -->
                <div class="modal-header d-block">
                    <h2 class="modal-title" style="font-weight: bold;">เข้าสู่ระบบ (สำหรับผู้ดูแลระบบ)</h2>
                </div>
                <form id="formAdminLogin" name="formAdminLogin" method="POST">

                    <!-- Modal body -->
                    <div class="modal-body">
                        <img src="./icons/loginlogo.png" width="150px" height="150px">
                        <div class="form-group col-sm-9">
                            <label for="adminUsername" style="font-weight: regular;">ชื่อผู้ใช้</label>
                            <input type="text" placeholder="ชื่อผู้ใช้งานผู้ดูแล" class="form-control"
                                id="adminUsername" name="adminUsername">
                        </div>
                        <div class="form-group col-sm-9">
                            <label for="adminPassword" style="font-weight: regular;">รหัสผ่าน</label>
                            <input type="password" placeholder="รหัสผ่าน" class="form-control" id="adminPassword"
                                name="adminPassword">
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer d-block" align="center">
                        <button class="btn btn-success" type="button"
                            onclick="adminLoginFunction()">เข้าสู่ระบบ</button>
                        <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#loginGroupModal"
                            data-dismiss="modal">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- 2.3 End of Admin Login Modal -->
    <!-- End of Modal Zone -->

    <nav class="navbar navbar-expand-sm">
        <a class="navbar-brand" style="font-family:'Opun-Regular.ttf'!important; color: #FFA200;font-size:35px;"
            href="index.php">
            <i class="fas fa-dolly"></i>ผลิตภัณฑ์ชุมชน
        </a>
        <ul class="navbar-nav mr-auto"></ul>
        <ul class="navbar-nav">
            <?php
                if (@$_SESSION['role'] == "customer") {
                    $strSQL = "SELECT * FROM tbl_customer WHERE customer_code = '".$_SESSION['customer_code']."' ";
                    $result = @$conn->query($strSQL);
                    while($row = $result->fetch_assoc()){
                        ?>

            <li class="nav-item">
                <a href="CustomerInfoNewForm.php" class="nav-link"><i class="fas fa-user"></i>ข้อมูลส่วนตัว</a>
            </li>
            <li class="nav-item">
                <a style="color: #FFA200 !important;,font-size: 14px !important;\" class="nav-link">
                    <?php echo "ยินดีต้อนรับ&nbspคุณ&nbsp :&nbsp " . @$row['customer_fullname'] . ""; ?> </a>
            </li>
            <form id="formLogout\" name="formLogout">
                <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="logoutFunction()"><i
                            class="fas fa-sign-out-alt"></i>ออกจากระบบ</a>
                </li>
            </form>
            <li class="nav-item">
                <a class="nav-link"><i style="font-size:25px;" class="cart fas fa-shopping-cart">
                        <div class="list-cart" id="list-cart"></div>
                    </i><span id="badge_cart_num" class="badge badge-light"></span></a>

            </li>

            <?php }
                } else {   ?>

            <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#registerGroupModal" style="cursor:pointer"><i
                        class="fas fa-user-plus"></i>สมัครสมาชิก</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#loginGroupModal" style="cursor:pointer"><i
                        class="fas fa-sign-in-alt"></i>เข้าสู่ระบบ</a>
            </li>
            <li class="nav-item\">
                <a class="nav-link"><i style="font-size:25px;" class="cart fas fa-shopping-cart">
                        <div class="list-cart" id="list-cart"></div>
                    </i>

                    <span id="badge_cart_num" class="badge badge-light"></span></a>
            </li>

            <?php    }    ?>

        </ul>
    </nav>

    <style>
        .list-cart {

            background-color: #E4E4E4;
            /* height: 250px;
  width: 300px; 
  border: 2px solid red;*/
            right: 0;
            position: absolute;
            transition: 2s all;
            transform: scale(0);
            transform-origin: 100% 0%;
        }

        .cart:hover>.list-cart {
            transform: scale(0.7);
        }
    </style>


    <script>
        function opencart() {
            var customer_code = "<?php echo @$_SESSION['customer_code'];?>";

            if (customer_code == "") {
                location.replace("LoginRegCus.php");
            } else {

                location.replace("ShoppingBasket.php");
            }

        }
    </script>

    <div style="padding-bottom: 10px; background-color: #D8D8D4;">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="input-group md-form form-sm form-2 pl-0">
                    <input id="searchname" class="form-control my-0 py-1 red-border" type="text" placeholder="ค้นหา"
                        aria-label="ค้นหา">
                    <div class="input-group-append">
                        <span class="input-group-text red lighten-3" id="basic-text1">
                            <i class="fas fa-search text-grey" aria-hidden="true"
                                onclick="searchnameproduct()"></i></span>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
    <script>
        function searchnameproduct() {
            var productname = document.getElementById("searchname").value;
            window.location.href = "index.php?productname=" + productname;
        }

        function producttypeserch(obj) {
            var producttypecode = obj.getAttribute("producttypecode");
            window.location.href = "index.php?producttypecode=" + producttypecode;
        }
    </script>
    <div>
        <div class="container" style="margin-top: -16px;" align="center">
            <div id="rcorners" style="margin-top: 30px; margin-bottom: 30px;">
                <h4>หมวดหมู่สินค้า</h4>
                <div class="row">
                    <div class="col-sm-3"></div>

                    <div class="col-sm-2 cardproducttype" style="cursor:pointer;" producttypecode="prodtype0001"
                        onclick="producttypeserch(this)">
                        <a><img id="category" src="./icons/menshirt.png" alt="Avatar" class="image"></a>
                        <div class="middle">
                            <a>เสื้อ</a>
                        </div>
                    </div>
                    <div class="col-sm-2 cardproducttype" style="cursor:pointer;" producttypecode="prodtype0002"
                        onclick="producttypeserch(this)">
                        <a><img id="category" src="./icons/food.png" alt="Avatar" class="image"></a>
                        <div class="middle">
                            <a>อาหาร</a>
                        </div>
                    </div>
                    <div class="col-sm-2 cardproducttype" style="cursor:pointer;" producttypecode="prodtype0003"
                        onclick="producttypeserch(this)">
                        <a><img id="category" src="./icons/drink.png" alt="Avatar" class="image"></a>
                        <div class="middle">
                            <a>เครื่องดื่ม</a>
                        </div>
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-3"></div>

                    <div class="col-sm-2 cardproducttype" style="cursor:pointer;" producttypecode="prodtype0004"
                        onclick="producttypeserch(this)">
                        <a><img id="category" src="./icons/bag.png" alt="Avatar" class="image"></a>
                        <div class="middle">
                            <a>ของใช้</a>
                        </div>
                    </div>
                    <div class="col-sm-2 cardproducttype" style="cursor:pointer;" producttypecode="prodtype0005"
                        onclick="producttypeserch(this)">
                        <a><img id="category" src="./icons/jewelry.png" alt="Avatar" class="image"></a>
                        <div class="middle">
                            <a>เครื่องประดับ</a>
                        </div>
                    </div>
                    <div class="col-sm-2 cardproducttype" style="cursor:pointer;" producttypecode="prodtype0006"
                        onclick="producttypeserch(this)">
                        <a><img id="category" src="./icons/green.png" alt="Avatar" class="image"></a>
                        <div class="middle">
                            <a>สมุนไพร</a>
                        </div>
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
        </div>
    </div>

    <div align="center">
        <div id="rcorners2">

            <div class="container">
                <div class="row">
                    <?php
             
                            $strgetproduct="SELECT * FROM tbl_product 
                            INNER JOIN tbl_store ON tbl_product.store_code = tbl_store.store_code
                            WHERE product_banned != '1'  
                            AND store_status !='3'
                            AND product_type_code LIKE '%$producttypecode%' AND product_name LIKE '%$productname%' ";
                            $resultstrgetproduct = @$conn->query($strgetproduct);
                            if(@$resultstrgetproduct->num_rows >0){
                                while($rowstrgetproduct = $resultstrgetproduct->fetch_assoc()){

                ?>
                    <div class="col-sm-3" id="cardhov" style="padding-bottom:20px;">
                        <div class="card" style="width:250px; background-color:#FFF3ED;"
                            product_code="<?php echo $rowstrgetproduct['product_code']; ?>"
                            onclick="productdetail(this)">
                            <a style="cursor:pointer; font-size:18px; font-weight:bold; color:white; padding-top:10px;">
                                <?php
      $strgetproductimage="SELECT * FROM tbl_image_product WHERE image_product_banned != '1' AND product_code = '".$rowstrgetproduct['product_code']."' ORDER BY image_product_no ASC LIMIT 1";
      $resultstrgetproductimage = @$conn->query($strgetproductimage);
      if(@$resultstrgetproductimage->num_rows >0){
        while($rowstrgetproductimage = $resultstrgetproductimage->fetch_assoc()){
         

?>
                                <img src="../restapi/product/<?php echo $rowstrgetproductimage['image_product_name'] ?>"
                                    style="width:160px; height:160px;">
                                <?php
  }}else{
?>
                                <img src="../restapi/product/productimgempty.png" style="width:160px; height:160px;">
                                <?php
  }
?>
                            </a>
                            <div class="card-body">
                                <h6 class="card-title"><?php echo $rowstrgetproduct['product_name'];?></h6>
                                <p style="color:#FF671B;">
                                    <?php 
                                $strgetmin="SELECT MIN(product_group_price) AS product_group_price FROM tbl_product_group WHERE product_code = '".$rowstrgetproduct['product_code']."' ";
                                $resultstrgetmin = @$conn->query($strgetmin);
                                while($rowstrgetmin = $resultstrgetmin->fetch_assoc()){
                                    $strmin=$rowstrgetmin['product_group_price'];
                                    echo $strmin;
                                }
                                $strgetmax="SELECT MAX(product_group_price) AS product_group_price FROM tbl_product_group WHERE product_code = '".$rowstrgetproduct['product_code']."' ";
                                $resultstrgetmax = @$conn->query($strgetmax);
                                while($rowstrgetmax = $resultstrgetmax->fetch_assoc()){
                                    $strmax=$rowstrgetmax['product_group_price'];
                                    if($strmin==$strmax){
                                                //NOT SHOW
                                    }else{
                                        echo "-".$strmax;
                                    }
                                }
                            ?>
                                    บาท

                                </p>
                                <form id="formgetproductdetail" name="formgetproductdetail" method="post">

                                </form>
                            </div>
                        </div>
                    </div>
                    <?php  }}else{   $haveproduct ="0";   } ?>
                </div>
            </div>
        </div>
    </div>

    <?php if(@$haveproduct=="0"){ ?>
    <div align="center">
        <img src="icons/empty_box.png" width="100px;"><br>
        ไม่มีสินค้า
    </div>
    <?php  } ?>
    <script>
        function productdetail(obj) {
            product_code = obj.getAttribute("product_code");

            document.forms["formgetproductdetail"].action = "ProductDetail.php?product_code=" + product_code;
            document.forms["formgetproductdetail"].submit();

        }
    </script>
    <!-- Modal ERROR-->
    <div class="modal fade" id="loginfail" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="font-size:30px;font-weight:bold;"></h4>
                </div>
                <div class="modal-body" align="center" style="font-size:25px;">
                    <img src="image/wrong.png" width="150px" height="150px">
                    <br><br>
                    <p>ชื่อผู้ใช้หรือรหัสผ่านของคุณผิดพลาด</p>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
                </div>
            </div>

        </div>
    </div>
    <!-- Modal -->

    <!-- Modal SUCCESS-->
    <div class="modal fade" id="loginsuccess" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="font-size:30px;font-weight:bold;"></h4>
                </div>
                <div class="modal-body" align="center" style="font-size:25px;">
                    <img src="image/correct.png" width="150px" height="150px">
                    <br><br>
                    <p>เข้าสู่ระบบสำเร็จ</p>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
                </div>
            </div>

        </div>
    </div>
    <!-- Modal -->

    <!-- Modal LOGOUT-->
    <div class="modal fade" id="logoutsuccess" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
                </div>
                <div class="modal-body" align="center" style="font-size:25px;">
                    <img src="image/logout.png" width="150px" height="150px">
                    <br><br>
                    <p>กำลังออกจากระบบ</p>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
                </div>
            </div>

        </div>
    </div>
    <!-- Modal -->


    <!-- Modal BANNED-->
    <div class="modal fade" id="hasbanned" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="font-size:30px;font-weight:bold;"></h4>
                </div>
                <div class="modal-body" align="center" style="font-size:25px;">
                    <img src="image/banned.png" width="150px" height="150px">
                    <br><br>
                    <p>รหัสผู้ใช้ของท่านโดน แบน กรุณาติดต่อ ผู้ดูแลระบบ ด้วยค่ะ</p>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
                </div>
            </div>

        </div>
    </div>
    <!-- Modal -->
</body>

<footer style="background-color: #575757;">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-sm" style="display: flex;justify-content: center;">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <label style="color: #FFA200;font-size: 14px;">&copy;&nbsp;2020 localshopcenter.com</label>
                </li>
            </ul>
        </nav>
    </div>
</footer>

</html>