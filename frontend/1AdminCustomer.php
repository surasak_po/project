<?php
   include "../restapi/setting/config.php";

    @session_start();
    @session_cache_expire(30);

    if(@$_SESSION['role'] == "customer"){
        echo "<meta  http-equiv='refresh' content='0;URL=Index.php'>";
    }
    if(@$_SESSION['role'] == "store"){
        echo "<meta  http-equiv='refresh' content='0;URL=Store.php'>";
    }
    if(@$_SESSION['role'] == ""){
        echo "<meta  http-equiv='refresh' content='0;URL=Index.php'>";
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="./css/styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }

        .card {
            background-color: #F88360;  
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }

        table, td {
            border: 2px solid grey;
        }

        th {
            border: 2px solid pink;
        }

        #left {
            text-align: left;
        }

        #size {
            width: 1px;
        }

        #element1 {
            display: flex;
            justify-content: space-between;
            margin-bottom: -10px;
        }

        #element2 {
            display: flex;
            justify-content: space-between;
            margin-bottom: -10px;
            margin-left: -15px;
        }

        #b :hover {
            color: black!important;
        }
        .modal-header .close {
        display:none;
        }
        #menuleft{
            color: black !important;
        }
        #menuleft:hover{
            color: #FF8811 !important;
        }
    </style>
</head>

<script>
    
    var globalCustomerCode;
    

    function cantdelete(){
        $("#cantdelete").modal({backdrop: 'static', keyboard: false})  
      $("#cantdelete").modal('show');
      setTimeout(function(){$('#cantdelete').modal('hide')},2000);
    }
    function deletesuccess(){
        $("#deletesuccess").modal({backdrop: 'static', keyboard: false})  
      $("#deletesuccess").modal('show');
      setTimeout(function(){$('#deletesuccess').modal('hide')},2000);
    }
    function insertsuccess(){
        $("#insertsuccess").modal({backdrop: 'static', keyboard: false})  
      $("#insertsuccess").modal('show');
      setTimeout(function(){$('#insertsuccess').modal('hide')},1000);
    }
    function updatesuccess(){
        $("#updatesuccess").modal({backdrop: 'static', keyboard: false})  
      $("#updatesuccess").modal('show');
      setTimeout(function(){$('#updatesuccess').modal('hide')},1000);
    }

    function logoutsuccess(){
        $("#logoutsuccess").modal({backdrop: 'static', keyboard: false})  
      $("#logoutsuccess").modal('show');
      setTimeout(function(){$('#logoutsuccess').modal('hide')},2000);
    }

    var loadFile = function(event) {
        var output = document.getElementById('showImageUpload');
        output.src = URL.createObjectURL(event.target.files[0]);
    };
    var loadFile = function(event) {
        var output = document.getElementById('showImageCardUpload');
        output.src = URL.createObjectURL(event.target.files[0]);
    };

    function editCustomerInfoModal(obj) {

        var customer_code = obj.getAttribute("customer_code");

        $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/getcustomerdetail.php",
                data:JSON.stringify({
                    customer_code:customer_code
                }),
                success: function (response) {
                var json_data = response;
                var getdata = json_data;
             

        document.getElementById("showImageUploadcustomer").src = "../restapi/profile/"+getdata.customer_profile;
        document.getElementById("customer_code").value = customer_code;
        document.getElementById("customer_fullname").value = getdata.customer_fullname;
        document.getElementById("customer_tel").value = getdata.customer_tel;
        document.getElementById("customer_email").value = getdata.customer_email;
        document.getElementById("customer_password").value = getdata.customer_password;
        document.getElementById("customer_profile").value = getdata.customer_profile;
                }
            });
        
    }

    function deleteCustomerModal(obj) {
        var customerCode = obj.getAttribute("customerCode");
        var customerFullname = obj.getAttribute("customerFullname");
        globalCustomerCode = customerCode;
        document.getElementById("customerFullname").innerHTML = customerFullname;
    }
    
    function updatestorestatus(obj) {
        var store_code = obj.getAttribute("store_codeupdatestatus");
        var store_status =document.getElementById("select_store_status"+store_code).value;
        console.log(store_status+store_code);
        var admin_code ="<?php echo $_SESSION['admin_code'];?>";

        $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/updatestorestatusfromadmin.php",
                data:JSON.stringify({
                    store_status:store_status,
                    store_code:store_code,
                    admin_code:admin_code
                }),
                success: function (response) {
                var json_data = response;
                var getdata = json_data;
                console.log(getdata);
                if(getdata.result=="Success"){
                    updatesuccess();
                    setTimeout(function(){ 
                    location.reload();
                    },1600);
                }

                }
            });

    }
    function viewStoreInfoModal(obj) {
        var store_code = obj.getAttribute("store_code");
      
            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/getstoredetail.php",
                data:JSON.stringify({
                    store_code:store_code
                }),
                success: function (response) {
                var json_data = response;
                var getdata = json_data;
                    console.log(getdata);
        document.getElementById("store_namestore").value = getdata.store_namestore;
        document.getElementById("store_fullname").value = getdata.store_fullname;
        document.getElementById("store_email").value = getdata.store_email;
        document.getElementById("store_tel").value = getdata.store_tel;
        document.getElementById("store_password").value = getdata.store_password;
        document.getElementById("store_nameinbank").value = getdata.store_nameinbank;
        document.getElementById("store_bank_code").value = getdata.store_bank_code;
       document.getElementById("store_bank_brand").value = getdata.store_bank_brand;
        document.getElementById("showImageUpload").src = "../restapi/profilestore/"+getdata.store_picture;
        document.getElementById("showImagecardUpload").src = "../restapi/cardstore/"+getdata.store_cardpicture;

                }
            });
    }

    function logoutFunction() {

        logoutsuccess();
                   setTimeout(function(){ 
                    document.forms["formLogout"].action = "webservice/Logout.php";
                    document.forms["formLogout"].submit();
                   },2600);
        
    }
</script>

<body>
    <nav class="navbar navbar-expand-sm">
        <a class="navbar-brand" style="font-family:'KRR_AengAei.ttf'!important; color: #FFA200;font-size:35px;">
            <i class="fas fa-dolly"></i>ผู้ดูแลระบบ
        </a>
        <ul class="navbar-nav mr-auto"></ul>
        <ul class="navbar-nav">
            <?php
                echo "
                    <form id=\"formLogout\" name=\"formLogout\">
                        <a class=\"nav-link\" style=\"cursor:pointer\" onclick=\"logoutFunction()\"><i class=\"fas fa-sign-out-alt\"></i>ออกจากระบบ</a>
                    </form>
                ";
            ?>
        </ul>
    </nav>
<!-- เปิด -->
    <div class="container-fluid" style="background-color:#ECECEC ;">
            <div class="row">
            <div class="col-sm-2" align="left" style="background-color:#ECECEC ;"><br>
                <ul style=" list-style-type: none;">
                <li style="margin-bottom:15px;"> <i class="fas fa-user" style="color:#3AE100;"></i><span style="font-weight:bold;">จัดการข้อมูล</span>
                <ul style=" list-style-type: none;">
                <li style="margin-bottom:5px;margin-top:5px;color:#FF8811;"><a href="1AdminCustomer.php" >ลูกค้า</a></li>
                <li style="margin-bottom:5px;"><a href="1AdminStore.php" id="menuleft">ร้านค้า</a></li>   
                <li style="margin-bottom:5px;"><a href="1AdminProductType.php" id="menuleft">ประเภทสินค้า</a></li>
                <li style="margin-bottom:5px;"><a href="1AdminProduct.php" id="menuleft">สินค้า</a></li>
                <li style="margin-bottom:5px;"><a href="1AdminProductImage.php" id="menuleft">รูปสินค้า</a></li>
                <li style="margin-bottom:5px;"><a href="1AdminProductGroup.php" id="menuleft">ตัวเลือกสินค้า</a></li>
                <li style="margin-bottom:5px;"><a href="1AdminTransport.php" id="menuleft">ขนส่ง</a></li>
                <li style="margin-bottom:5px;"><a href="1AdminCustomerAddress.php" id="menuleft">สถานที่จัดส่ง</a></li>
                </ul>
                </li>

                <li style="margin-bottom:15px;"> <i class="fas fa-money-check-alt" style="color:Blue;"></i><span style="font-weight:bold;">การขาย</span>
                <ul style=" list-style-type: none;">
                    <li style="margin-bottom:5px;"><a href="AdminIncome.php" id="menuleft">รายรับของระบบ</a></li>
                    <li style="margin-bottom:5px;"><a href="AdminSaleReport.php" id="menuleft">รายงานการขาย</a></li>
                </ul>
                </li>

                </ul>
            </div>
             <div class="col-sm-10" align="center" style="background-color:white;"> 

    <div class="container">
        <br>
    
        <br>
            <div id="customer"  ><br>
                <div class="row"> 
                    <div class="col-md-12"> 
                    <h3>จัดการข้อมูลลูกค้า</h3>
                    </div> 
                </div>
                <br>
                <div class="container" style="width: 850px">
                <table class="table table-lg" >
                    <thead class="thead-dark" align="center">
                        <tr>
                            <th width="10px">ลำดับ</th>
                            <th>ชื่อ - นามสกุล ลูกค้า</th>
                            <th id="size">ดูข้อมูล</th>
                            <th id="size">แบน</th>
                        </tr>
                    </thead>
                    <tbody> 
                        <?php
                            $no = 0;
                            $getcustomerlist = "SELECT * FROM tbl_customer";
                            $resultgetcustomerlist = @$conn->query($getcustomerlist);
                            if($resultgetcustomerlist->num_rows > 0){
                                while($rowgetcustomerlist = $resultgetcustomerlist->fetch_assoc()){
                                    $no++;
                        ?>
                        <tr>
                            <td align="center"><?php echo $no?></td>
                            <td><?php echo $rowgetcustomerlist['customer_fullname']; ?> </td>
                            <td>
                                <ul id="element2">
                                    <a id="b" class="nav-link" data-toggle="modal" data-target="#updateCustomerModal"  style="cursor:pointer; color: blue;" 
                                        customer_code="<?php echo $rowgetcustomerlist['customer_code'];?>"
                                        onclick="editCustomerInfoModal(this)"
                                    ><i class="fas fa-eye"></i></a>
                                </ul>
                            </td>
                                 <td>
                                        <?php
                                            if($rowgetcustomerlist['customer_banned']=="0"){
                                                echo "<input type=\"checkbox\" id=\"customer_banned\" name=\"customer_banned\" class=\"form-control\"   onclick=\"bannedcustomer(this)\" customer_code= ".$rowgetcustomerlist['customer_code'];
                                            }else{
                                                echo "<input type=\"checkbox\" id=\"customer_banned\" name=\"customer_banned\" class=\"form-control\"  onclick=\"bannedcustomer(this)\" checked customer_code= ".$rowgetcustomerlist['customer_code'];
                                            }
                                        ?>
                                </td>
                        </tr>
                        <?php
                                }
                            }
                        ?>
                    </tbody>
                </table>
                </div>
            </div>
<script>
function bannedcustomer(obj){
customer_code = obj.getAttribute("customer_code");

$.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/updatecustomerbanned.php",
                data:JSON.stringify({
                    customer_code:customer_code
                }),
                success: function (response) {
                var json_data = response;
                var getdata = json_data;
                console.log(getdata);
                if(getdata.result=="Success"){
                    updatesuccess();
                    setTimeout(function(){ 
                    location.reload();
                    },1600);
                }
                }
            });

}
</script>
</div> </div> </div> </div>   
 <!-- Modal AddataSUCCESS-->
 <div class="modal fade" id="insertsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <span>เพิ่มข้อมูลสำเร็จ</span><span id="insertsuccesshtml"></span>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

          <!-- Modal LOGOUT-->
          <div class="modal fade" id="logoutsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/logout.png" width="150px" height="150px">
        <br><br>
        <p>กำลังออกจากระบบ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

 <!-- Modal UPDATESUCCESS-->
 <div class="modal fade" id="updatesuccess" role="dialog" data-backdrop="static" data-keyboard="false"> 
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p>แก้ไขข้อมูลเรียบร้อย</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

   <!-- Modal DELETESUCCESS-->
 <div class="modal fade" id="deletesuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p>ลบข้อมูลสำเร็จ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

           <!-- Modal CANTDELETE-->
           <div class="modal fade" id="cantdelete" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p>ไม่สามารถลบข้อมูลได้เนื่องจากมีการใช้ข้อมูลอยู่</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

     <!-- Modal Zone -->
    <!-- 1.1 Update Customer Modal -->
    <div class="modal" id="updateCustomerModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">ดูข้อมูลลูกค้า</h2>
            </div>
         

            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">
                        <div class="row" style="margin-left: 200px;">
                         <div class="col-sm-4">โปรไฟล์ลูกค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                            <img  id="showImageUploadcustomer" width="110px" height="100px" disabled>
                             </div>
                        </div>
                        <div class="row" style="margin-left: 200px;">
                            <div class="col-sm-4">รหัสผู้ใช้งาน : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="customer_code" name="customer_code" value="<?php echo $row['customer_code']; ?>" readonly >
                            </div>
                        </div>
                        <div class="row" style="margin-left: 200px;">
                            <div class="col-sm-4">ชื่อ - นามสกุล : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="customer_fullname" name="customer_fullname" value="<?php echo $row['customer_fullname']; ?>" disabled>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 200px;">
                            <div class="col-sm-4">เบอร์โทรศัพท์ <br>(ชื่อบัญชีผู้ใช้งาน) : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="customer_tel" name="customer_tel" value="<?php echo $row['customer_tel']; ?>" disabled >
                            </div>
                        </div>
                        <div class="row" style="margin-left: 200px;">
                            <div class="col-sm-4">E-mail : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="email" class="form-control form-control-sm" id="customer_email" name="customer_email" value="<?php echo $row['customer_email']; ?>" disabled>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 200px;">
                            <div class="col-sm-4">รหัสผ่าน : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="password" class="form-control form-control-sm" id="customer_password" name="customer_password" value="<?php echo $row['customer_password']; ?>" disabled>
                            </div>
                        </div>

                        </div>
                        
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                    </div>
                </div>
            
        </div>
    </div>
    </div>
    <!-- 1.1 End of Update Customer Modal -->
    
    <!-- 2.1 View Store Info Modal -->
    <div class="modal" id="viewStoreInfoModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">ข้อมูลร้านค้า</h2>
            </div>
            <form id="formViewStoreInfo" name="formViewStoreInfo" method="POST" enctype="multipart/form-data">

            <!-- Modal body -->
            <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">
                        <div class="row" style="margin-left: 100px;">
                            <div class="col-sm-4">รูปประจำตัวร้านค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <img src="" id="showImageUpload" width="150px" height="150px" disabled><br>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 100px;">
                            <div class="col-sm-4">รูปประจำตัวเจ้าของร้านค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <img src="" id="showImagecardUpload" width="150px" height="150px" disabled><br>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 100px;">
                            <div class="col-sm-4">ชื่อร้านค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="store_namestore" name="store_namestore" disabled >
                            </div>
                        </div>
                        <div class="row" style="margin-left: 100px;">
                            <div class="col-sm-4">ชื่อเจ้าของร้านค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="store_fullname" name="store_fullname"  disabled >
                            </div>
                        </div>
                        <div class="row" style="margin-left: 100px;">
                            <div class="col-sm-4">เบอร์โทรศัพท์ :</div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="store_tel" name="store_tel"  disabled>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 100px;">
                            <div class="col-sm-4">E-mail : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="email" class="form-control form-control-sm" id="store_email" name="store_email" disabled>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 100px;">
                            <div class="col-sm-4">รหัสผ่าน : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="password" class="form-control form-control-sm" id="store_password" name="store_password"  disabled>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 100px;">
                            <div class="col-sm-4">ชื่อในสมุดบัญชีธนาคาร : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="store_nameinbank" name="store_nameinbank" disabled>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 100px;">
                            <div class="col-sm-4">เลขบัญชีธนาคาร : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="store_bank_code" name="store_bank_code" disabled>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 100px;">
                            <div class="col-sm-4">ธนาคารที่ใช้ : </div>
                            <div class="col-sm-6 d-inline-flex">
                            <input type="text" class="form-control form-control-sm" id="store_bank_brand" name="store_bank_brand"  disabled>
                            </div>
                        </div>

                        </div>
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่างนี้</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2.1 End of Update Store Modal -->
    
    <!-- 2.2 Activate Store Modal -->
    <div class="modal" id="activateStoreModal">
    <div class="modal-dialog">
        <div class="modal-content">
        
            <!-- Modal body -->
            <div class="modal-body d-block" align="center">
                <img src="icons/activate.png" width="100px" height="100px"><br><br><br>
                <h2><b>ยืนยันร้านค้า</b></h2><br>
                <p>ท่านต้องการยืนยันร้านค้า <b id="storeNameStore"></b> <b id="storeNameStore"></b> หรือไม่?? </p><br>
                <form id="formStoreActive" name="formStoreActive" method="POST">
                    <button type="button" class="btn btn-success" data-dismiss="modal"onclick="activateStore()">ยืนยัน</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
                </form>
            </div>
        </div>
    </div>
    </div>
    <!-- 2.2 End of Delete Customer Modal -->
    <!-- End of Modal Zone -->


<!-- 4.1 add producttype Modal -->
<div class="modal" id="addproducttype" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">เพิ่มข้อมูลประเภทสินค้า</h2>
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">
                        <div class="row" >
                            <div class="col-sm-4">ชื่อประเภทสินค้า : </div><br>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="product_type_name" name="product_type_name"  >
                            </div>
                        </div>
                        </div>   
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-success btn-sm" data-dismiss="modal" onclick="addproducttypename()">ยืนยัน</a>
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    <!-- 4.1 End ofadd producttype Modal -->
   <script>
   function addproducttypename(){
        product_type_name=document.getElementById("product_type_name").value;
        $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/addproducttype.php",
                data:JSON.stringify({
                    product_type_name:product_type_name
                }),
                success: function (response) {
                var json_data = response;
                var getdata = json_data;
                console.log(getdata);
                if(getdata.result=="Success"){
                    insertsuccess();
                    setTimeout(function(){ 
                    location.reload();
                    },1600);
                }
                }
            });
   }
   </script> 
<!-- 4.2 updateproducttype Modal -->
<div class="modal" id="updateproducttype" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">แก้ไขข้อมูลประเภทสินค้า</h2>
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">ชื่อประเภทสินค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input hidden type="text" class="form-control form-control-sm" id="product_type_code_update" name="product_type_code_update"  >
                                <input type="text" class="form-control form-control-sm" id="product_type_name_update" name="product_type_name_update"  >
                            </div>
                        </div>
                        </div>
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-success btn-sm" data-dismiss="modal" onclick="updateproducttypename()">ยืนยัน</a>
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    <!-- 4.2 End of updateproducttype Modal -->
    <script>
   function updateproducttypename(){
    product_type_name_update=document.getElementById("product_type_name_update").value;
    product_type_code_update=document.getElementById("product_type_code_update").value;
        $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/updateproducttype.php",
                data:JSON.stringify({
                    product_type_code:product_type_code_update,
                    product_type_name:product_type_name_update
                }),
                success: function (response) {
                var json_data = response;
                var getdata = json_data;
                console.log(getdata);
                if(getdata.result=="Success"){
                    updatesuccess();
                    setTimeout(function(){ 
                    location.reload();
                    },1600);
                }
                }
            });
   }
   </script> 

<!--  viewproduct Modal -->
<div class="modal" id="viewproduct" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">ดูข้อมูลสินค้า</h2>
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">ชื่อสินค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="product_name" name="product_name"  Readonly>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">รายละเอียด : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="textarea" class="form-control form-control-sm" id="product_detail" name="product_detail"  Readonly>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">ประเภทสินค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="product_product_type_name" name="product_product_type_name"  Readonly>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">สินค้าจากร้าน : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="product_store_namestore" name="product_store_namestore"  Readonly>
                            </div>
                        </div>
                        </div>
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    <!--  End of viewproduct Modal -->

    <!--  viewproductgroup Modal -->
<div class="modal" id="viewproductgroup">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">ดูข้อมูลตัวเลือกสินค้า</h2>
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">ชื่อตัวเลือกสินค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="productgroup_name" name="productgroup_name"  Readonly>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">ของสินค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="textarea" class="form-control form-control-sm" id="productgroup_productname" name="productgroup_productname"  Readonly>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">รูปตัวเลือกสินค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                            <img id="productgroup_image" name="productgroup_image" width="100px" height="100px">
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">กว้าง : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="productgroup_width" name="productgroup_width"  Readonly>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">ยาว : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="productgroup_long" name="productgroup_long"  Readonly>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">สูง : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="productgroup_high" name="productgroup_high"  Readonly>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">น้ำหนัก : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="productgroup_weight" name="productgroup_weight"  Readonly>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">หน่วยนับ : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="productgroup_unit" name="productgroup_unit"  Readonly>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">ราคา : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="productgroup_price" name="productgroup_price"  Readonly>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">คงเหลือในคลัง : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="productgroup_num" name="productgroup_num"  Readonly>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">จากร้านค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="productgroup_namestore" name="productgroup_namestore"  Readonly>
                            </div>
                        </div>

                        </div>
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    <!--  End of viewproductgroup Modal -->
    
<!--  updatetransport Modal -->
<div class="modal" id="edittransport" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">แก้ไขข้อมูลขนส่ง</h2>
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">ชื่อขนส่ง : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input hidden type="text" class="form-control form-control-sm" id="edittransport_code" name="edittransport_code"  >
                                <input type="text" class="form-control form-control-sm" id="edittransport_name" name="edittransport_name"  >
                            </div>
                        </div>


                        </div></div>
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-success btn-sm" data-dismiss="modal" onclick="updatetransport()">ยืนยัน</a>
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    <!-- End of updatetransport Modal -->
<script>
function updatetransport(){
    var edittransport_code = document.getElementById("edittransport_code").value;
    var edittransport_name = document.getElementById("edittransport_name").value;

    $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/updatetransport.php",
                data:JSON.stringify({
                    transport_code:edittransport_code,
                    transport_name:edittransport_name
                }),
                success: function (response) {
                var json_data = response;
                if(json_data.result=="Success"){
                    updatesuccess();
                    setTimeout(function(){ 
                    location.reload();
                    },1600);
                }
                }
            });
   

}
</script>
<!--  insertransport Modal -->
<div class="modal" id="inserttransport" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">เพิ่มข้อมูลขนส่ง</h2>
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">ชื่อขนส่ง : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="inserttransport_name" name="inserttransport_name"  >
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">ราคา : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="inserttransport_price" name="inserttransport_price"  >
                            </div>
                        </div>

                        </div></div>
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-success btn-sm" data-dismiss="modal" onclick="inserttransport()">ยืนยัน</a>
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    <!-- End of insertransport Modal -->
    <script>
function inserttransport(){
    var inserttransport_name = document.getElementById("inserttransport_name").value;
    var inserttransport_price = document.getElementById("inserttransport_price").value;
    $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/addtransport.php",
                data:JSON.stringify({
                    transport_name:inserttransport_name,
                    transport_price:inserttransport_price
                }),
                success: function (response) {
                var json_data = response;
                if(json_data.result=="Success"){
                    insertsuccess();
                    setTimeout(function(){ 
                    location.reload();
                    },1600);
                }
                }
            });
   

}
</script>
</body>

<footer style="background-color: #575757;">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-sm" style="display: flex;justify-content: center;">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <label style="color: #FFA200;font-size: 14px;">&copy;&nbsp;2020 localshopcenter.com</label>
                </li>
            </ul>
        </nav>
    </div>
</footer>
</html>