<?php
     include "../restapi/setting/config.php";
    @session_start();
    @session_cache_expire(30);
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>ข้อมูลส่วนตัวลูกค้า</title>
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="bootstrap/css/animate.css" rel="stylesheet">
  <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
  <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
  <script src="bootstrap/js/popper.min"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="bootstrap/js/holder.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
  <link href="./css/styles.css" rel="stylesheet">
  <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
  <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
  <link href="./bootstrap/css/all.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="https://cdn.omise.co/omise.js"></script>
  <!-- Toggle Switch-->

  <style>
    .switch {
      position: relative;
      display: inline-block;
      width: 60px;
      height: 34px;
    }

    .switch input {
      opacity: 0;
      width: 0;
      height: 0;
    }

    .slider {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #ccc;
      -webkit-transition: .4s;
      transition: .4s;
    }

    .slider:before {
      position: absolute;
      content: "";
      height: 26px;
      width: 26px;
      left: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
    }

    input:checked+.slider {
      background-color: #2196F3;
    }

    input:focus+.slider {
      box-shadow: 0 0 1px #2196F3;
    }

    input:checked+.slider:before {
      -webkit-transform: translateX(26px);
      -ms-transform: translateX(26px);
      transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
      border-radius: 34px;
    }

    .slider.round:before {
      border-radius: 50%;
    }
  </style>
  <!-- Toggle Switch -->
  <style>
    @font-face {
      font-family: 'KRR_AengAei.ttf';
      src: url('fonts/KRR_AengAei.ttf') format('truetype');
      font-weight: normal;
      font-style: normal;
    }

    .row {
      margin-top: 8px;
    }

    p.round {
      border: 3px solid orangered;
      border-radius: 8px;
      height: 40px;
      padding-top: 5px;
      padding-left: 3px;
    }

    div ul#uif li a:hover {
      color: rgb(196, 46, 0) !important;
    }

    #menuleft {
      color: black !important;
    }

    #menuleft:hover {
      color: #FF8811 !important;
    }

    .modal-header .close {
      display: none;
    }
  </style>
</head>
<script>
  function ordersuccessbycreditcard() {
    $("#ordersuccessbycreditcard").modal({
      backdrop: 'static',
      keyboard: false
    })
    $("#ordersuccessbycreditcard").modal('show');
    setTimeout(function () {
      $('#ordersuccessbycreditcard').modal('hide')
    }, 3000);
  }

  function Failedpayment() {
    $("#Failedpayment").modal({
      backdrop: 'static',
      keyboard: false
    })
    $("#Failedpayment").modal('show');
    setTimeout(function () {
      $('#Failedpayment').modal('hide')
    }, 3000);
  }

  function alertexception() {
    $("#alertexception").modal({
      backdrop: 'static',
      keyboard: false
    })
    $("#alertexception").modal('show');
    setTimeout(function () {
      $('#alertexception').modal('hide')
    }, 2000);
  }

  function alerttel() {
    $("#alerttel").modal({
      backdrop: 'static',
      keyboard: false
    })
    $("#alerttel").modal('show');
    setTimeout(function () {
      $('#alerttel').modal('hide')
    }, 2000);
  }

  function alertmail() {
    $("#alertmail").modal({
      backdrop: 'static',
      keyboard: false
    })
    $("#alertmail").modal('show');
    setTimeout(function () {
      $('#alertmail').modal('hide')
    }, 2000);
  }

  function deletesuccess() {
    $("#deletesuccess").modal({
      backdrop: 'static',
      keyboard: false
    })
    $("#deletesuccess").modal('show');
    setTimeout(function () {
      $('#deletesuccess').modal('hide')
    }, 2000);
  }

  function cantdelete() {
    $("#cantdelete").modal({
      backdrop: 'static',
      keyboard: false
    })
    $("#cantdelete").modal('show');
    setTimeout(function () {
      $('#cantdelete').modal('hide')
    }, 2000);
  }

  function insertsuccess() {
    $("#insertsuccess").modal({
      backdrop: 'static',
      keyboard: false
    })
    $("#insertsuccess").modal('show');
    setTimeout(function () {
      $('#insertsuccess').modal('hide')
    }, 1000);
  }

  function updatesuccess() {
    $("#updatesuccess").modal({
      backdrop: 'static',
      keyboard: false
    })
    $("#updatesuccess").modal('show');
    setTimeout(function () {
      $('#updatesuccess').modal('hide')
    }, 1000);
  }

  function logoutsuccess() {
    $("#logoutsuccess").modal({
      backdrop: 'static',
      keyboard: false
    })
    $("#logoutsuccess").modal('show');
    setTimeout(function () {
      $('#logoutsuccess').modal('hide')
    }, 2000);
  }

  var loadFile = function (event) {
    var output = document.getElementById('showuploadimage');
    output.src = URL.createObjectURL(event.target.files[0]);
  };

  function logoutFunction() {
    logoutsuccess();
    setTimeout(function () {
      document.forms["formLogout"].action = "webservice/Logout.php";
      document.forms["formLogout"].submit();
    }, 2600);
  }
</script>

<body style="background-color:#ECECEC ;">
  <nav class="navbar navbar-expand-sm">
    <a class="navbar-brand" href="index.php"
      style="font-family: 'KRR_AengAei.ttf' !important; color: #FFA200;font-size:35px;">
      <i class="fas fa-dolly"></i>ผลิตภัณฑ์ชุมชน
    </a>
    <ul class="navbar-nav mr-auto"></ul>
    <ul class="navbar-nav">
      <?php
            if(@$_SESSION['role'] == "customer") {
                $customerCode = $_SESSION['customer_code'];
                $strSQL = "SELECT * FROM tbl_customer WHERE customer_code = '".$customerCode."' ";
                $result = @$conn->query($strSQL);
                while($row = $result->fetch_assoc()){
                    echo "
                        <li class=\"nav-item\">
                            <a style=\"color: White !important;,font-size: 14px !important;\" class=\"nav-link\"><i class=\"fas fa-user\"></i>ข้อมูลส่วนตัว</a>
                        </li>
                        <li class=\"nav-item\">
                            <a style=\"color: #FFA200 !important;,font-size: 14px !important;\" class=\"nav-link\">ยินดีต้อนรับ&nbspคุณ&nbsp :&nbsp " . @$row['customer_fullname'] . "</a>
                        </li>
                        <form id=\"formLogout\" name=\"formLogout\">
                            <li class=\"nav-item\">
                                <a class=\"nav-link\" style=\"cursor:pointer\" onclick=\"logoutFunction()\"><i class=\"fas fa-sign-out-alt\"></i>ออกจากระบบ</a>
                            </li>
                        </form>
                    ";
                }
            }
            ?>
    </ul>
  </nav>




  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-1" align="center" style="background-color:#ECECEC ;">
      </div>
      <div class="col-sm-2">
        <ul style=" list-style-type: none;">
          <li style="margin-bottom:15px;"> <i class="fas fa-user" style="color:#3AE100;"></i><span
              style="font-weight:bold;">ข้อมูลส่วนตัว</span>
            <ul style=" list-style-type: none;">
              <li style="margin-bottom:5px;margin-top:5px;"><a href="CustomerInfoNewForm.php" id="menuleft">ประวัติ</a>
              </li>
              <li style="margin-bottom:5px;"><a href="CustomerInfoNewFormAddress.php" id="menuleft">สถานที่จัดส่ง</a>
              </li>
            </ul>
          </li>
          <li style="margin-bottom:15px;"> <i class="fas fa-file-invoice" style="color:#1A52E7;"></i><span
              style="font-weight:bold;">การซื้อของฉัน</span>
            <ul style=" list-style-type: none;">
              <li style="margin-bottom:5px;margin-top:5px;"><a href="CustomerInfoNewFormOrderAll.php"
                  id="menuleft">ทั้งหมด</a></li>
              <li style="margin-bottom:5px;color:#FF8811;">ที่ต้องชำระ</li>
              <li style="margin-bottom:5px;"><a href="CustomerInfoNewFormWaitTracking.php"
                  id="menuleft">ที่ต้องจัดส่ง</a></li>
              <li style="margin-bottom:5px;"><a href="CustomerInfoNewFormWaitReceives.php"
                  id="menuleft">ที่ต้องได้รับ</a></li>
              <li style="margin-bottom:5px;"><a href="CustomerInfoNewFormWaitReceivesSuccess.php"
                  id="menuleft">สำเร็จแล้ว</a></li>
            </ul>
          </li>
        </ul>
      </div>


      <div class="col-sm-8" align="center" style="background-color:white;">

        <!-- เปิด -->
        <div class="container">
          <div id="rcorners">
            <!-- เริ่ม -->
            <?php 
$strgetorder="SELECT * FROM tbl_payment
INNER JOIN tbl_order ON tbl_payment.order_code = tbl_order.order_code
INNER JOIN tbl_store ON tbl_order.store_code = tbl_store.store_code
INNER JOIN tbl_shipping ON tbl_order.shipping_code = tbl_shipping.shipping_code
INNER JOIN tbl_transport ON tbl_shipping.transport_code = tbl_transport.transport_code
WHERE tbl_order.customer_code = '".$_SESSION['customer_code']."' AND order_status = '0'
AND  tbl_payment.customer_code = '".$_SESSION['customer_code']."' ";
     $resultstrgetorder = @$conn->query($strgetorder);
     if(@$resultstrgetorder->num_rows >0){
         while($rowstrgetorder = $resultstrgetorder->fetch_assoc()){

            $totalpricepayment = 0;
            $order_code = $rowstrgetorder['order_code'];
            $payment_code = $rowstrgetorder['payment_code'];
            $payment_type = $rowstrgetorder['payment_type'];
            $payment_token = $rowstrgetorder['payment_token'];
           
?>
            <!-- แบ่งหัว -->
            <div class="row">
              <br><br>
              <div class="col-sm-12" style="background-color:#C4C4C4;"></div>
            </div>
            <!-- แบ่งหัว -->
            <div class="row">
              <div class="col-sm-4" align="left">

                <label style="font-size:14px; margin-left:7px;">
                  ชื่อร้านค้า : <span
                    style="font-weight:bold;"><?php echo $rowstrgetorder['store_namestore'];?></span><br>
                  หมายเลขใบสั่งซื้อ : <span style="font-weight:bold;"><?php echo $order_code;?></span>
                </label>

              </div>
              <div class="col-sm-4" align="center">
                <label style="font-size:14px; margin-left:7px;">การชำระเงิน : <span
                    style="font-weight:bold;"><?php  if($payment_type=="promptpay"){echo "พร้อมเพย์";}else{echo "บัตรเครดิต";}?></span></label>
              </div>
              <div class="col-sm-4" align="right">
                การจัดส่ง : <span
                  style="font-weight:bold;"><?php echo $rowstrgetorder['transport_name']." ค่าจัดส่ง  ฿".$rowstrgetorder['order_shipping_price'] ?></span>
              </div>
            </div>
            <div class="row" style="margin-top:-10px; margin-bottom:-10px;">
              <hr width=90% color=#F2F2F2>
            </div>

            <!-- productloop -->
            <?php 
$totalprice = $rowstrgetorder['order_shipping_price'];
$strgetorderdetail="SELECT * FROM tbl_order_detail 
INNER JOIN tbl_product_group ON tbl_order_detail.product_group_code = tbl_product_group.product_group_code
INNER JOIN tbl_product ON tbl_product_group.product_code = tbl_product.product_code
WHERE order_code = '$order_code'  ";
     $resultstrgetorderdetail = @$conn->query($strgetorderdetail);
     if(@$resultstrgetorderdetail->num_rows >0){
         while($rowstrgetorderdetail = $resultstrgetorderdetail->fetch_assoc()){
?>
            <div class="row">
              <div class="col-sm-2" align="right">
                <img src="../restapi/product/<?php echo $rowstrgetorderdetail['product_group_picture']; ?>"
                  style="margin-left:50px;" width="120px" height="120px">
              </div>
              <div class="col-sm-6" align="left" style="margin-left:20px;">
                <label style="font-size:20px">ชื่อสินค้า :
                  <?php echo $rowstrgetorderdetail['product_name']; ?></label><br>
                <label style="font-size:12px">ชื่อหมวด :
                  <?php echo $rowstrgetorderdetail['product_group_name']; ?></label><br>
                <label style="font-size:12px">จำนวน :
                  <?php echo $rowstrgetorderdetail['order_detail_num']." ".$rowstrgetorderdetail['product_group_unit_name']; ?>
                </label>
              </div>
              <div class="col-sm-3" align="right">
                <label> ฿
                  <?php 
                            $totalprice = $totalprice + ($rowstrgetorderdetail['order_detail_price']*$rowstrgetorderdetail['order_detail_num']);
                            echo $rowstrgetorderdetail['order_detail_price']*$rowstrgetorderdetail['order_detail_num']; ?></label>.00
              </div>
            </div>

            <?php }}?>
            <!-- productloop -->

            <div id="rcorners2">
              <div class="row">
                <div class="col-sm-12" align="right">
                  <label style="font-size:14px;">ยอดรวมคำสั่งซื้อทั้งหมด&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                  <label style="font-size:24px; color:orange;">฿ <?php echo $totalprice?>.00</label>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6" align="left">
                  <label style="font-size:12px; margin-left:15px;">ยืนยันการรับสินค้าภายในวันที่
                    <?php echo date('Y-m-d H:m:s',strtotime($rowstrgetorder['order_date']. ' + 7 days')) ?></label>
                </div>
                <div class="col-sm-6" align="right">
                  <?php if($payment_type=="promptpay"){ ?>
                  <a class="btn btn-lg btn-warning " style="font-size:13px;" onclick="paymentpromptpay(this)"
                    total_pricepromptpay="<?php echo $totalprice; ?>" order_code="<?php echo $order_code; ?>"
                    payment_token="<?php echo $payment_token; ?>">ชำระเงิน</a>
                  <?php } ?>

                  <?php if($payment_type=="credit_card"){ ?>
                  <form id="checkoutForm<?php echo $order_code; ?>" method="POST">
                    <input type="hidden" name="omiseToken">
                    <input type="hidden" name="omiseSource">
                    <button type="submit" id="checkoutButton<?php echo $order_code; ?>" class="btn btn-lg btn-warning "
                      style="font-size:13px;" onclick="paymentcreditcard(this)" order_code="<?php echo $order_code;?>"
                      total_price="<?php echo $totalprice."00";?>"
                      payment_code="<?php echo $payment_code; ?>">ชำระเงิน</button>
                  </form>
                  <?php } ?>


                </div>
              </div>
            </div>
            <script>
              OmiseCard.configure({
                publicKey: "pkey_test_5kwbj3tbv09itjm0p8m"
              });

              var button = document.querySelector("#checkoutButton" + "<?php echo $order_code; ?>");
              var form = document.querySelector("#checkoutForm" + "<?php echo $order_code; ?>");
              button.addEventListener("click", (event) => {
                event.preventDefault();
                OmiseCard.open({
                  amount: total_price,
                  currency: "THB",
                  defaultPaymentMethod: "credit_card",
                  onCreateTokenSuccess: (nonce) => {
                    $.ajax({
                      type: "POST",
                      dataType: 'json',
                      contentType: 'application/json',
                      async: false,
                      url: "../restapi/1getaymentcreditcard.php",
                      data: JSON.stringify({
                        omise_token: nonce,
                        totalprice: total_price,
                        payment_code: payment_code
                      }),
                      success: function (response) {

                        var json_data = response.result;

                        if (json_data == "Success") {
                          ordersuccessbycreditcard()
                          setTimeout(function () {
                            location.reload();
                          }, 3600);
                        } else {
                          Failedpayment()
                          setTimeout(function () {
                            location.reload();
                          }, 3600);
                        }


                      }
                    });

                  }
                });
              });
            </script>
            <?php }} ?>
            <script>
              var payment_code;
              var total_price;
              var order_code;

              function paymentcreditcard(obj) {
                payment_code = obj.getAttribute("payment_code");
                total_price = obj.getAttribute("total_price");
                order_code = obj.getAttribute("order_code");
                console.log(order_code);
              }
            </script>

            <script>
              function paymentpromptpay(obj) {
                payment_token = obj.getAttribute("payment_token");
                totalprice = obj.getAttribute("total_pricepromptpay");
                order_code = obj.getAttribute("order_code");
                var ImgPromptpay = "";

                $.ajax({
                  type: "POST",
                  dataType: 'json',
                  contentType: 'application/json',
                  async: false,
                  url: "../restapi/1getpaymentpromptpay.php",
                  data: JSON.stringify({
                    payment_token: payment_token
                  }),
                  success: function (response) {
                    var json_data = response.result;
                    ImgPromptpay += "<div style=\"background-color:#CAD0C7\" >";
                    ImgPromptpay += "<p>ใบสั่งซื้อหมายเลข : " + order_code + "</p>";
                    ImgPromptpay += "<p>ราคาทั้งสิ้น : " + "฿" + totalprice + ".00" + "</p>";
                    ImgPromptpay += "<img src=" + "  '" + json_data + "' " + ">";
                    ImgPromptpay += "<br><br>";
                    ImgPromptpay += "</div>"
                    ImgPromptpay += "<br>";
                    $('#ImgPromptpay').html(ImgPromptpay);
                    promptpay1();
                  }
                });


              }
            </script>
            <!-- แบ่งท้าย -->
            <div class="row">
              <br><br>
              <div class="col-sm-12" style="background-color:#C4C4C4;"></div>
            </div>
            <!-- แบ่งท้าย -->
            <!-- สุดสิ้นลูป -->
            <br><br>
          </div>
        </div>

        <!-- ปิด -->

      </div>
      <div class="col-sm-1" align="center" style="background-color:#ECECEC  ;">
      </div>
    </div>
  </div>


  <!-- Modal LOGOUT-->
  <div class="modal fade" id="logoutsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
          <img src="image/logout.png" width="150px" height="150px">
          <br><br>
          <p>กำลังออกจากระบบ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->



  <!-- Modal AddataSUCCESS-->
  <div class="modal fade" id="insertsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
          <img src="image/correct.png" width="150px" height="150px">
          <br><br>
          <span>เพิ่มข้อมูลสำเร็จ</span><span id="insertsuccesshtml"></span>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

  <!-- Modal CANTDELETE-->
  <div class="modal fade" id="cantdelete" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
          <img src="image/alert.png" width="150px" height="150px">
          <br><br>
          <p>ไม่สามารถลบข้อมูลได้เนื่องจากมีการใช้ข้อมูลอยู่</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

  <!-- Modal DELETESUCCESS-->
  <div class="modal fade" id="deletesuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
          <img src="image/correct.png" width="150px" height="150px">
          <br><br>
          <p>ลบข้อมูลสำเร็จ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

  <!-- Modal ALERTTEL-->
  <div class="modal fade" id="alerttel" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
          <img src="image/alert.png" width="150px" height="150px">
          <br><br>
          <p>เบอร์โทรศัพท์ของท่าน ซ้ำ!! ในระบบ กรุณากรอก เบอร์โทรศัพท์ อื่น</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

  <!-- Modal ALERTMAIL-->
  <div class="modal fade" id="alertmail" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
          <img src="image/alert.png" width="150px" height="150px">
          <br><br>
          <p>E-Mail ของท่าน ซ้ำ!! ในระบบ กรุณากรอก E-Mail อื่น</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

  <!-- Modal alertexception-->
  <div class="modal fade" id="alertexception" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
          <img src="image/alert.png" width="150px" height="150px">
          <br><br>
          <p>เกิดข้อผิดพลาดกรุณาลองอีกครั้ง</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>

  <script>
    function promptpay1() {
      $("#promptpay1").modal({
        backdrop: 'static',
        keyboard: false
      });
      $("#promptpay1").modal('show');
      setTimeout(function () {
        $('#promptpay1').modal('hide')
      }, 100000);
    }
  </script>
  <!-- Modal -->
  <div class="modal fade" id="promptpay1" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
          <div class="row">
            <div class="col-md-12">
              <img src="icons/promptpay.png" width="150px" height="100px">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div id="ImgPromptpay"> </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal" id="btnOK"
            onclick="promptpayok()">ตกลง</button>
        </div>
      </div>
    </div>
  </div>

  <script>
    function promptpayok() {
      location.reload();
    }
  </script>


  <!-- Modal SUCCESSCreditcard-->
  <div class="modal fade" id="ordersuccessbycreditcard" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
          <img src="image/correct.png" width="150px" height="150px">
          <br><br>
          <p>สั่งซื้อสำเร็จ ขอบคุณที่ใช้บริการค่ะ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- ModalSUCCESSCreditcard -->

  <!-- Modal FAILEDPAYMENT-->
  <div class="modal fade" id="Failedpayment" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
          <img src="image/alert.png" width="150px" height="150px">
          <br><br>
          <p>ชำระเงินล้มเหลว กรุณาชำระเงินอีกครั้ง ทีหน้า "สินค้าที่ยังไม่ได้ชำระเงิน"</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal FAILEDPAYMENT-->
</body>
<footer style="background-color: #575757; ">
  <div class="container-fluid">
    <nav class="navbar navbar-expand-sm" style="display: flex;justify-content: center;">
      <ul class="navbar-nav">
        <li class="nav-item">
          <label style="color: #FFA200;font-size: 14px;">&copy;&nbsp;2020 localshopcenter.com</label>
        </li>
      </ul>
    </nav>
  </div>
</footer>

</html>