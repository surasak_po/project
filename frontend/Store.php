<?php
    include "webservice/setting/Config.php";

    @session_start();
    @session_cache_expire(30);

    if(@$_SESSION['role'] == "admin"){
        echo "<meta  http-equiv='refresh' content='0;URL=1AdminCustomer.php'>";
    }
    if(@$_SESSION['role'] == "customer"){
        echo "<meta  http-equiv='refresh' content='0;URL=Index.php'>";
    }
    if(@$_SESSION['role'] == ""){
        echo "<meta  http-equiv='refresh' content='0;URL=Index.php'>";
    }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>จัดการข้อมูลร้านค้า</title>
    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="./css/sidebar.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="./css/styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }

        @font-face {
            font-family: 'Opun-Regular.ttf';
            src: url('fonts/Opun-Regular.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }

        body {
            font-family: 'Opun-Regular.ttf' !important;
        }

        .navbar {
            background-color: #575757;
            font-weight: bold;
            font-size: 18px;
            margin-bottom: 0px;
        }

        .navbar a {
            color: #FFA200;
            margin-top: -6px;
        }

        nav ul li a {
            font-size: 14px !important;
            text-decoration: underline;
        }

        #margin {
            margin-top: 10px;
            margin-left: 30px;
        }

        ul#menu li {
            display: inline;
            text-align: center;
            margin-left: 20px;
            margin-right: 20px;
            font-size: 18px;
            color: #575757;
        }

        ul#menu li a {
            color: #575757;
        }

        ul#menu li a:hover {
            color: #FFA200;
            text-decoration: none;
        }

        ul#label li {
            display: inline;
            text-align: center;
            margin-left: 20px;
            margin-right: 20px;
            font-size: 16px;
            color: #575757;
        }

        #hline {
            color: #FFA200 !important;
        }

        /* div a {
            font-size: 14px;
        }

        div a:hover {
            color: black;
            text-decoration: none;
        } */

        #bgblack {
            background-color: rgb(255, 94, 0);
        }

        .nav-link {
            text-decoration: none;
        }

        .nav-link:hover {
            color: black !important;
        }

        .nav-item {
            margin-top: 10px !important;
        }

        .row {
            padding-top: 10px;
        }

        .fas {
            padding-right: 5px;
            margin-right: 5px;
        }
        .modal-header .close {
        display:none;
        }
    </style>
</head>

<script>

setTimeout(function(){
    var store_code ="<?php echo @$_SESSION['store_code'];?>";

    if(store_code!=""){
    $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/1updatewithdrawstatus.php",
            data:JSON.stringify({
                store_code:store_code
            }),
            success: function (response) {
            var json_data = response;
            var getdata = json_daFta;   
     document.getElementById("badge_order_num").innerHTML = getdata.ordernum;
            }
        }); 
    }
}, 100);
function alertexception() {
    $("#alertexception").modal({backdrop: 'static', keyboard: false})  
        $("#alertexception").modal('show');
        setTimeout(function(){$('#alertexception').modal('hide')},2000);
    }
   function alerttel() {
    $("#alerttel").modal({backdrop: 'static', keyboard: false})  
        $("#alerttel").modal('show');
        setTimeout(function(){$('#alerttel').modal('hide')},2000);
    }
    function alertmail() {
        $("#alertmail").modal({backdrop: 'static', keyboard: false})  
        $("#alertmail").modal('show');
        setTimeout(function(){$('#alertmail').modal('hide')},2000);
    }
 function updatesuccess(){
    $("#updatesuccess").modal({backdrop: 'static', keyboard: false})  
      $("#updatesuccess").modal('show');
      setTimeout(function(){$('#updatesuccess').modal('hide')},1000);
    }

  function logoutsuccess(){
    $("#logoutsuccess").modal({backdrop: 'static', keyboard: false})  
      $("#logoutsuccess").modal('show');
      setTimeout(function(){$('#logoutsuccess').modal('hide')},2000);
    }

    function updateStoreInfo() {
        var emailFilter= /^.+@.+\..{2,3}$/ ;
        var button = document.getElementById("updateStoreInfoBtn").innerHTML;

        if(button == "แก้ไข"){
            document.getElementById("store_picture").disabled = false;
            document.getElementById("store_namestore").disabled = false;
            document.getElementById("store_fullname").disabled = false;
            document.getElementById("store_email").disabled = false;
            document.getElementById("store_address").disabled = false;
            document.getElementById("store_tel").disabled = false;
            document.getElementById("store_password").disabled = false;
            document.getElementById('updateStoreInfoBtn').classList.remove('btn-outline-warning');
            document.getElementById('updateStoreInfoBtn').classList.add('btn-outline-success');
            document.getElementById("updateStoreInfoBtn").innerHTML = "ยืนยัน";
        }
        if(button == "ยืนยัน"){
            if (!$.trim($("#store_namestore").val())) {
                document.getElementById("store_namestore_req").hidden=false;
            } else {
                document.getElementById("store_namestore_req").hidden=true;
            }

            if (!$.trim($("#store_fullname").val())) {
                document.getElementById("store_fullname_req").hidden=false;
            } else {
                document.getElementById("store_fullname_req").hidden=true;
            }

            if (!$.trim($("#store_email").val())) {
                document.getElementById("store_email_req").hidden=false;
            } else {
                document.getElementById("store_email_req").hidden=true;
            }

            if (!$.trim($("#store_tel").val())) {
                document.getElementById("store_tel_req").hidden=false;
            } else {
                document.getElementById("store_tel_req").hidden=true;
            }

            if (!$.trim($("#store_address").val())) {
                document.getElementById("store_address_req").hidden=false;
            } else {
                document.getElementById("store_address_req").hidden=true;
            }

            if (!$.trim($("#store_password").val())) {
                document.getElementById("store_password_req").hidden=false;
            } else {
                document.getElementById("store_password_req").hidden=true;
            }
            if($.trim($("#store_namestore").val()) && $.trim($("#store_fullname").val()) && $.trim($("#store_email").val()) && $.trim($("#store_address").val()) 
            && $.trim($("#store_tel").val()) && $.trim($("#store_password").val())) {
                if($.trim($("#store_tel").val()).length != 10 && !(emailFilter.test($.trim($("#store_email").val())))){
                    document.getElementById("store_tel_req").innerHTML="กรุณากรอกเบอร์โทรศัพท์ให้ครบ 10 หลัก";
                    document.getElementById("store_tel_req").hidden=false;
                    document.getElementById("store_email_req").innerHTML="ท่านกรอกรูปแบบอีเมลล์ไม่ถูกต้อง";
                    document.getElementById("store_email_req").hidden=false;
                } else if (document.getElementById("store_tel").value.length != 10 && (emailFilter.test($.trim($("#store_email").val())))) {
                    document.getElementById("store_tel_req").innerHTML="กรุณากรอกเบอร์โทรศัพท์ให้ครบ 10 หลัก";
                    document.getElementById("store_tel_req").hidden=false; 
                } else if (document.getElementById("store_tel").value.length == 10 && !(emailFilter.test($.trim($("#store_email").val())))) {
                    document.getElementById("store_email_req").innerHTML="ท่านกรอกรูปแบบอีเมลล์ไม่ถูกต้อง";
                    document.getElementById("store_email_req").hidden=false;
                } else {
            store_code="<?php echo $_SESSION['store_code']; ?>";
            store_picture=document.getElementById("store_picture").value;
            store_namestore=document.getElementById("store_namestore").value;
            store_fullname=document.getElementById("store_fullname").value;
            store_email=document.getElementById("store_email").value;
            store_address=document.getElementById("store_address").value;
            store_tel=document.getElementById("store_tel").value;
            store_password=document.getElementById("store_password").value;
                
                    if(store_picture == ""){

                            $.ajax({
                                            type: "POST",
                                            dataType: 'json',
                                            contentType: 'application/json',
                                            async : false,
                                            url: "../restapi/updatestore.php",
                                            data:JSON.stringify({
                                                store_code:store_code,
                                                store_namestore:store_namestore,
                                                store_fullname:store_fullname,
                                                store_email:store_email,
                                                store_address:store_address,
                                                store_tel:store_tel,
                                                store_password:store_password
                                            }),
                                            success: function (response) {
                                                var json_data = response;
                                                var apistatus = json_data.result;
                                                if(apistatus=="Success") {
                                                    updatesuccess();
                                                    setTimeout(function(){ 
                                                        location.reload();
                                                    },1600);
                                                }else if(apistatus=="EMAIL_DUPLICATE"){
                                                    alertmail();
                                                    setTimeout(function(){},2600);
                                                }else if(apistatus=="TEL_DUPLICATE"){
                                                    alerttel();
                                                    setTimeout(function(){ },2600);
                                                }else{
                                                    alertexception();
                                                    setTimeout(function(){ },2600);
                                                }

                                            }
                            });

                    }else{
                        var base64 = encodeImageFileAsURL(document.getElementById('store_picture'));
                            base64.onloadend = function() {
                            var imagebase64 = base64.result;
                           
                            $.ajax({
                                            type: "POST",
                                            dataType: 'json',
                                            contentType: 'application/json',
                                            async : false,
                                            url: "../restapi/updatestore.php",
                                            data:JSON.stringify({
                                                store_code:store_code,
                                                store_picture:imagebase64,
                                                store_namestore:store_namestore,
                                                store_fullname:store_fullname,
                                                store_email:store_email,
                                                store_tel:store_tel,
                                                store_password:store_password
                                            }),
                                            success: function (response) {
                                                var json_data = response;
                                                var apistatus = json_data.result;
                                                if(apistatus=="Success") {
                                                    updatesuccess();
                                                    setTimeout(function(){ 
                                                        location.reload();
                                                    },1600);
                                                }else if(apistatus=="EMAIL_DUPLICATE"){
                                                    alertmail();
                                                    setTimeout(function(){},2600);
                                                }else if(apistatus=="TEL_DUPLICATE"){
                                                    alerttel();
                                                    setTimeout(function(){ },2600);
                                                }else{
                                                    alertexception();
                                                    setTimeout(function(){ },2600);
                                                }
                                            }
                                            });

                            }

                    }
            }
        }
        }

    }
    

function encodeImageFileAsURL(element) {
  var file = element.files[0];
  var reader = new FileReader();
  reader.onloadend = function() {
   // console.log(reader.result);
  }
  reader.readAsDataURL(file);

  return reader;
}
    function cancelUpdateStoreInfo() {
      location.reload();
    }

    function logoutFunction() {
           logoutsuccess();
                   setTimeout(function(){ 
                    document.forms["formLogout"].action = "webservice/Logout.php";
                    document.forms["formLogout"].submit();
                   },2600);
    }
</script>

<body>
    <nav class="navbar navbar-expand-sm">
        <a class="navbar-brand" style="font-family:'KRR_AengAei.ttf'!important; color: #FFA200;font-size:35px;">
            <i class="fas fa-dolly"></i>ผลิตภัณฑ์ชุมชน
        </a>
        <ul class="navbar-nav mr-auto"></ul>
        <ul class="navbar-nav">
            <?php
                echo "
                    <form id=\"formLogout\" name=\"formLogout\">
                        <a class=\"nav-link\" style=\"cursor:pointer\" onclick=\"logoutFunction()\"><i class=\"fas fa-sign-out-alt\"></i>ออกจากระบบ</a>
                    </form>
                ";
            ?>
        </ul>
    </nav>

    <!-- Sidebar  -->
    <div class="wrapper">
        
        <nav id="sidebar">
            <div class="sidebar-header">
                <h4>หน้าจัดการร้านค้า</h4>
                <strong>BS</strong>
            </div>

            <ul class="list-unstyled components" >
                <li class="active">
                    <a href="Store.php">
                        <i class="fas fa-briefcase"></i>
                        ข้อมูลร้านค้า
                    </a>
                </li>
           
                <li >
                    <a href="StoreProductView.php">
                        <i class="fas fa-box"></i>
                        เพิ่มสินค้า
                    </a>
                </li>

                <li>
                    <a href="StoreMyproduct.php">
                        <i class="fas fa-box"></i>
                      สินค้าของฉัน
                    </a>
                </li>

                <li>
                    <a href="StoreTransportView.php">
                    <i class="fas fa-box"></i>
                        จัดการช่องทางการจัดส่ง
                    </a>
                </li>
                <li>
                    <a href="StoreOrderview.php">
                        <i class="fas fa-box"></i>
                       ส่งสินค้า
                        <span id="badge_order_num" class="badge badge-light"></span>
                    </a>
                </li>
                <li>
                    <a href="StoreSell_list.php">
                        <i class="fas fa-box"></i>
                        รายรับของฉัน
                        <span id="badge_order_num" class="badge badge-light"></span>
                    </a>
                </li>
                <li >
                <a href="StoreWithdraw.php">
                        <i class="fas fa-box"></i>
                        ถอนเงิน
                        <span id="badge_order_num" class="badge badge-light"></span>
                    </a>
                </li>    
            </ul>
        </nav>

        <script>

setInterval(function(){ 
    var store_code ="<?php echo @$_SESSION['store_code'];?>";

            if(store_code!=""){ //ถ้า login
                $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: 'application/json',
                        async : false,
                        url: "../restapi/getordernumstore.php",
                        data:JSON.stringify({
                            store_code:store_code
                        }),
                        success: function (response) {
                        var json_data = response;
                        var getdata = json_data;   
                document.getElementById("badge_order_num").innerHTML = getdata.ordernum;
                        }
                    }); 
                }
        }, 500);
        </script>
        <!-- Page Content  -->
        <div id="content">
            <!-- <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span>Toggle Sidebar</span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>
                </div>
            </nav> -->
<script>
    var loadFilestore_picture = function(event) {
        var output = document.getElementById('showImageUpload');
        output.src = URL.createObjectURL(event.target.files[0]);
    };
    function chkNumber(ele)
                            {
                            var vchar = String.fromCharCode(event.keyCode);
                            if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
                            ele.onKeyPress=vchar;
                            }
</script>
            <?php
                $strSQL = "SELECT * FROM tbl_store WHERE store_code = '".$_SESSION['store_code']."' ";
                $result = @$conn->query($strSQL);
                while($row = $result->fetch_assoc()){
            ?>
            <h3 style="margin-bottom: 30px;">ข้อมูลร้านค้า ร้าน : <?php echo $row['store_namestore']; ?></h3>
            <form id="formStoreInfo" name="formStoreInfo" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <div class="row" style="margin-left: 100px;">
                    <div class="col-sm-4">รูปประจำตัวร้านค้า : </div>
                    <div class="col-sm-4 d-inline-flex">
                        <img src="../restapi/profilestore/<?php echo $row['store_picture'];?>" id="showImageUpload" width="150px" height="150px" disabled><br>
                    </div>
                </div><br>
                <div class="row" style="margin-left: 500px; margin-top: -20px; margin-bottom: 30px;">
                    <input type="file" onchange="loadFilestore_picture(event)" accept="image/*" id="store_picture" name="store_picture" disabled>
                </div>

                <!-- <div class="row" style="margin-left: 100px;">
                    <div class="col-sm-4">รูปถ่ายหน้าคู่กับบัตรประชาชน : </div>
                    <div class="col-sm-4 d-inline-flex">
                        <img src="../restapi/cardstore/<?php echo $row['store_cardpicture'];?>" id="showImageCardUpload" width="200px" height="200px" disabled><br>
                    </div>
                </div><br>
                <div class="row" style="margin-left: 500px; margin-top: -20px; margin-bottom: 30px;">
                    <input type="file" onchange="loadFile(event)" accept="image/*" id="storeCardPicture" name="storeCardPicture" disabled>
                </div> -->

                <div class="row" style="margin-left: 100px;">
                    <div class="col-sm-4">ชื่อร้านค้า : </div>
                    <div class="col-sm-4 d-inline-flex">
                        <input type="text" class="form-control form-control-sm" id="store_namestore" name="store_namestore" value="<?php echo $row['store_namestore']; ?>" disabled >
                    </div>
                    <div class="col-sm-4" align="left">
                        <p hidden id="store_namestore_req" name="store_namestore_req" style="color:red; font-size:13px;">กรุณากรอกชื่อร้านด้วยค่ะ</p>
                    </div>
                </div>
                <div class="row" style="margin-left: 100px;">
                    <div class="col-sm-4">ชื่อเจ้าของร้านค้า : </div>
                    <div class="col-sm-4 d-inline-flex">
                        <input type="text" class="form-control form-control-sm" id="store_fullname" name="store_fullname" value="<?php echo $row['store_fullname']; ?>" disabled >
                    </div>
                    <div class="col-sm-4" align="left">
                        <p hidden id="store_fullname_req" name="store_fullname_req" style="color:red; font-size:13px;">กรุณากรอกชื่อ - นามสกุลด้วยค่ะ</p>
                    </div>
                </div>
                <div class="row" style="margin-left: 100px;">
                    <div class="col-sm-4">เบอร์โทรศัพท์ :</div>
                    <div class="col-sm-4 d-inline-flex">
                        <input OnKeyPress="return chkNumber(this)" maxlength="10" type="text" class="form-control form-control-sm" id="store_tel" name="store_tel" value="<?php echo $row['store_tel']; ?>" disabled>
                    </div>
                    <div class="col-sm-4" align="left">
                        <p hidden id="store_tel_req" name="store_tel_req" style="color:red; font-size:13px;">กรุณากรอกเบอร์โทรศัพท์ด้วยค่ะ</p>
                    </div>
                </div>
                <div class="row" style="margin-left: 100px;">
                    <div class="col-sm-4">E-mail : </div>
                    <div class="col-sm-4 d-inline-flex">
                        <input type="email" class="form-control form-control-sm" id="store_email" name="store_email" value="<?php echo $row['store_email']; ?>" disabled>
                    </div>
                    <div class="col-sm-4" align="left">
                        <p hidden id="store_email_req" name="store_email_req" style="color:red; font-size:13px;">กรุณากรอกอีเมลล์ด้วยค่ะ</p>
                    </div>
                </div>
                <div class="row" style="margin-left: 100px;">
                    <div class="col-sm-4">ที่อยู่ : </div>
                    <div class="col-sm-4 d-inline-flex">
                      <textarea class="form-control form-control-sm" id="store_address" name="store_address" disabled><?php echo $row['store_address']; ?></textarea>
                    </div>
                    <div class="col-sm-4" align="left">
                        <p hidden id="store_address_req" name="store_address_req" style="color:red; font-size:13px;">กรุณากรอกที่อยู่ร้านด้วยค่ะ</p>
                    </div>
                </div>
                <div class="row" style="margin-left: 100px;">
                    <div class="col-sm-4">รหัสผ่าน : </div>
                    <div class="col-sm-4 d-inline-flex">
                        <input type="password" class="form-control form-control-sm" id="store_password" name="store_password" value="<?php echo $row['store_password']; ?>" disabled>
                    </div>
                    <div class="col-sm-4" align="left">
                        <p hidden id="store_password_req" name="store_password_req" style="color:red; font-size:13px;">กรุณากรอกรหัสผ่านด้วยค่ะ</p>
                    </div>
                </div>
                <div class="row" style="margin-left: 100px;">
                    <div class="col-sm-4">ชื่อในสมุดบัญชีธนาคาร : </div>
                    <div class="col-sm-4 d-inline-flex">
                        <input type="email" class="form-control form-control-sm" id="store_nameinbank" name="store_nameinbank" value="<?php echo $row['store_nameinbank']; ?>" disabled>
                    </div>
                </div>
                <div class="row" style="margin-left: 100px;">
                    <div class="col-sm-4">เลขบัญชีธนาคาร : </div>
                    <div class="col-sm-4 d-inline-flex">
                        <input type="email" class="form-control form-control-sm" id="store_bank_code" name="store_bank_code" value="<?php echo $row['store_bank_code']; ?>" disabled>
                    </div>
                </div>
                <div class="row" style="margin-left: 100px;">
                    <div class="col-sm-4">ธนาคารที่ใช้ : </div>
                    <div class="col-sm-4 d-inline-flex">
                        <select name="store_bank_brand" id="store_bank_brand" disabled>
                            <?php
                            if( $row['store_bank_brand']=="bbl"){
                                echo "<option value=\"bbl\">ธนาคารกรุงเทพ (BBL)</option>";
                            }
                            if( $row['store_bank_brand']=="kbank"){
                                echo "<option value=\"kbank\">ธนาคารกสิกรไทย (KBANK)</option>";
                            }
                            if( $row['store_bank_brand']=="ktb"){
                                echo "<option value=\"ktb\">ธนาคารกรุงไทย (KTB)</option>";
                            }
                            if( $row['store_bank_brand']=="tmb"){
                                echo "<option value=\"tmb\">ธนาคารทหารไทย (TMB)</option>";
                            }
                            if( $row['store_bank_brand']=="scb"){
                                echo "<option value=\"scb\">ธนาคารไทยพาณิชย์ (SCB)</option>";
                            }
                            ?>                    
                        </select>
                    </div>
                </div>
                <div class="row" style="margin-left: 100px;">
                    <div class="col-sm-4">สถานะร้านค้า : </div>
                    <div class="col-sm-4 d-inline-flex">
                       <label
                    style="<?php if($row['store_status']=="0"){
                                echo  "background-color:#FAFF58 ;";
                             }
                             if($row['store_status']=="1"){
                                echo  "background-color:#A8FF70 ;";
                             }
                             if($row['store_status']=="3"){
                                echo  "background-color:#FF4600 ;";
                             }
                            ?>"
                    >
                    <?php if($row['store_status']=="0"){
                                echo   "ส่งคำขอ";
                             }
                             if($row['store_status']=="1"){
                                echo   "ยืนยัน";
                             }
                             if($row['store_status']=="3"){
                                echo   "แบนร้อง";
                             }
                            ?>
                    </label>
                    </div>
                </div>
                <div class="row" style="margin-left: 100px;">
                    <div class="col-sm-4">ยอดเงินของฉัน : </div>
                    <div class="col-sm-4 d-inline-flex">
                       <span>฿</span><label> <?php echo $row['store_income']; ?>
                       </label>
                    </div>
                </div>
                <div class="row" align="center" style="margin-left: 300px; padding-top: 20px;">
                    <div class="col-sm-4">
                        <a id="updateStoreInfoBtn" class="btn btn-outline-warning btn-sm" onclick="updateStoreInfo()">แก้ไข</a>
                    </div>
                    <div class="col-sm-2 d-inline-flex">
                        <a id="cancelUpdateStoreInfoBtn" style="margin-left: -50px;" class="btn btn-outline-danger btn-sm" onclick="cancelUpdateStoreInfo()">ยกเลิก</a>
                    </div>
                </div>
                <br><br><br>
            </div>
            </form>
            <?php
                }
            ?>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
    <!-- End of Sidebar  -->

            <!-- Modal LOGOUT-->
            <div class="modal fade" id="logoutsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/logout.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;font-family:'Opun-Regular.ttf'!important;font-size:20px;">กำลังออกจากระบบ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

      <!-- Modal UPDATESUCCESS-->
      <div class="modal fade" id="updatesuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;font-family:'Opun-Regular.ttf'!important;font-size:20px;">แก้ไขข้อมูลเรียบร้อย</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

      <!-- Modal ALERTTEL-->
      <div class="modal fade" id="alerttel" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;font-family:'Opun-Regular.ttf'!important;font-size:20px;">เบอร์โทรศัพท์ของท่าน ซ้ำ!! ในระบบ กรุณากรอก เบอร์โทรศัพท์ อื่น</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

         <!-- Modal ALERTMAIL-->
         <div class="modal fade" id="alertmail" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;font-family:'Opun-Regular.ttf'!important;font-size:20px;">E-Mail ของท่าน ซ้ำ!! ในระบบ กรุณากรอก E-Mail อื่น</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

           <!-- Modal alertexception-->
           <div class="modal fade" id="alertexception" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;font-family:'Opun-Regular.ttf'!important;font-size:20px;">เกิดข้อผิดพลาดกรุณาลองอีกครั้ง</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

</body>

</html>