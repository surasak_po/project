<?php
     include "../restapi/setting/config.php";
    @session_start();
    @session_cache_expire(30);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ข้อมูลส่วนตัวลูกค้า</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="./css/styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

<!-- Toggle Switch-->

<style>

.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<!-- Toggle Switch -->
    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }

        .row {
            margin-top: 8px;
        }

        p.round {
            border: 3px solid orangered;
            border-radius: 8px;
            height: 40px;
            padding-top: 5px;
            padding-left: 3px;
        }

        div ul#uif li a:hover {
            color: rgb(196, 46, 0) !important;
        }
        #menuleft{
            color: black !important;
        }
        #menuleft:hover{
            color: #FF8811 !important;
        }
        .modal-header .close {
        display:none;
        }
    </style>
</head>
<script>


  function alertexception() {
    $("#alertexception").modal({backdrop: 'static', keyboard: false})  
        $("#alertexception").modal('show');
        setTimeout(function(){$('#alertexception').modal('hide')},2000);
    }
   function alerttel() {
    $("#alerttel").modal({backdrop: 'static', keyboard: false})  
        $("#alerttel").modal('show');
        setTimeout(function(){$('#alerttel').modal('hide')},2000);
    }
    function alertmail() {
        $("#alertmail").modal({backdrop: 'static', keyboard: false})  
        $("#alertmail").modal('show');
        setTimeout(function(){$('#alertmail').modal('hide')},2000);
    }
 function deletesuccess(){
    $("#deletesuccess").modal({backdrop: 'static', keyboard: false})  
      $("#deletesuccess").modal('show');
      setTimeout(function(){$('#deletesuccess').modal('hide')},2000);
    }
 function cantdelete(){
    $("#cantdelete").modal({backdrop: 'static', keyboard: false})  
      $("#cantdelete").modal('show');
      setTimeout(function(){$('#cantdelete').modal('hide')},2000);
    }

 function insertsuccess(){
    $("#insertsuccess").modal({backdrop: 'static', keyboard: false})  
      $("#insertsuccess").modal('show');
      setTimeout(function(){$('#insertsuccess').modal('hide')},1000);
    }

function updatecustomeraddressstatus(obj){
    var customer_address_code =obj.getAttribute("customer_address_code");
    var customer_code ="<?php echo $_SESSION['customer_code'];?>";
    //console.log(customer_code+customer_address_code);
                     $.ajax({
                                            type: "POST",
                                            dataType: 'json',
                                            contentType: 'application/json',
                                            async : false,
                                            url: "../restapi/updatecustomeraddressstatus.php",
                                            data:JSON.stringify({
                                                customer_code:customer_code,
                                                customer_address_code:customer_address_code
                                            }),
                                            success: function (response) {
                                                var json_data = response;
                                                var apistatus = json_data.result;
                                                console.log(apistatus);
                                                if(apistatus=="Success") {
                                                    updatesuccess();
                                                    setTimeout(function(){ 
                                                        location.reload();
                                                    },1600);
                                                }
                                            }
                                            });
}

function encodeImageFileAsURL(element) {
  var file = element.files[0];
  var reader = new FileReader();
  reader.onloadend = function() {
   // console.log(reader.result);
  }
  reader.readAsDataURL(file);

  return reader;
}
function updatesuccess(){
    $("#updatesuccess").modal({backdrop: 'static', keyboard: false})  
      $("#updatesuccess").modal('show');
      setTimeout(function(){$('#updatesuccess').modal('hide')},1000);
    }
  function logoutsuccess(){
    $("#logoutsuccess").modal({backdrop: 'static', keyboard: false})  
      $("#logoutsuccess").modal('show');
      setTimeout(function(){$('#logoutsuccess').modal('hide')},2000);
    }

    var loadFile = function(event) {
        var output = document.getElementById('showuploadimage');
        output.src = URL.createObjectURL(event.target.files[0]);
    };

    function updateCustomerInfo() { 
        var button = document.getElementById("updateCustomerInfoBtn").innerHTML;
        if(button == "แก้ไข"){
            document.getElementById("customer_profile").disabled = false;
            document.getElementById("customer_tel").disabled = false;
            document.getElementById("customer_email").disabled = false;
            document.getElementById("customer_password").disabled = false;
            document.getElementById("customer_fullname").disabled = false;
            document.getElementById('updateCustomerInfoBtn').classList.remove('btn-outline-warning');
            document.getElementById('updateCustomerInfoBtn').classList.add('btn-outline-success');
            document.getElementById("updateCustomerInfoBtn").innerHTML = "ยืนยัน";
        }
        if(button == "ยืนยัน"){
            if(!$.trim($("#customer_tel").val())) {
                document.getElementById("customer_tel_req").hidden=false;
            } else {
                document.getElementById("customer_tel_req").hidden=true;
            } 
            
            if (!$.trim($("#customer_fullname").val())) {
                document.getElementById("customer_fullname_req").hidden=false;
            } else {
                document.getElementById("customer_fullname_req").hidden=true;
            }
            
            if (!$.trim($("#customer_password").val())) {
                document.getElementById("customer_password_req").hidden=false;
            } else {
                document.getElementById("customer_password_req").hidden=true;
            }

            if (!$.trim($("#customer_email").val())) {
                document.getElementById("customer_email_req").hidden=false;
            } else {
                document.getElementById("customer_email_req").hidden=true;
            }

            if($.trim($("#customer_tel").val()) && $.trim($("#customer_fullname").val()) && 
            $.trim($("#customer_password").val()) && $.trim($("#customer_email").val())) {
                var emailFilter= /^.+@.+\..{2,3}$/ ;
                if($.trim($("#customer_tel").val()).length != 10 && !(emailFilter.test($.trim($("#customer_email").val())))) {
                    document.getElementById("customer_tel_req").innerHTML="กรุณากรอกเบอร์โทรศัพท์ให้ครบ 10 หลัก";
                    document.getElementById("customer_tel_req").hidden=false; 
                    document.getElementById("customer_email_req").innerHTML="ท่านกรอกรูปแบบอีเมลล์ไม่ถูกต้อง";
                    document.getElementById("customer_email_req").hidden=false;
                } else if ($.trim($("#customer_tel").val()).length != 10 && (emailFilter.test($.trim($("#customer_email").val())))) {
                    document.getElementById("customer_tel_req").innerHTML="กรุณากรอกเบอร์โทรศัพท์ให้ครบ 10 หลัก";
                    document.getElementById("customer_tel_req").hidden=false; 
                } else if ($.trim($("#customer_tel").val()).length == 10 && !(emailFilter.test($.trim($("#customer_email").val())))) {
                    document.getElementById("customer_email_req").innerHTML="ท่านกรอกรูปแบบอีเมลล์ไม่ถูกต้อง";
                    document.getElementById("customer_email_req").hidden=false;
                } else {
                            customer_code="<?php echo $_SESSION['customer_code']; ?>";
                            customer_fullname=document.getElementById("customer_fullname").value;
                            customer_email=document.getElementById("customer_email").value;
                            customer_tel=document.getElementById("customer_tel").value;
                            customer_password=document.getElementById("customer_password").value;
                            customer_profile=document.getElementById("customer_profile").value;
                        
                            if(customer_profile == ""){
                                    $.ajax({
                                            type: "POST",
                                            dataType: 'json',
                                            contentType: 'application/json',
                                            async : false,
                                            url: "../restapi/updatecustomer.php",
                                            data:JSON.stringify({
                                                customer_code:customer_code,
                                                customer_fullname:customer_fullname,
                                                customer_email:customer_email,
                                                customer_tel:customer_tel,
                                                customer_password:customer_password
                                            }),
                                            success: function (response) {
                                                var json_data = response;
                                                var apistatus = json_data.result;
                                                if(apistatus=="Success") {
                                                    updatesuccess();
                                                    setTimeout(function(){ 
                                                        location.reload();
                                                    },1600);
                                                }else if(apistatus=="EMAIL_DUPLICATE"){
                                                    alertmail();
                                                    setTimeout(function(){},2600);
                                                }else if(apistatus=="TEL_DUPLICATE"){
                                                    alerttel();
                                                    setTimeout(function(){ },2600);
                                                }else{
                                                    alertexception();
                                                    setTimeout(function(){ },2600);
                                                }
                                            }
                                            });

                            }else{
                                var base64 = encodeImageFileAsURL(document.getElementById('customer_profile'));
                                base64.onloadend = function() {
                                var imagebase64 = base64.result;

                                        $.ajax({
                                            type: "POST",
                                            dataType: 'json',
                                            contentType: 'application/json',
                                            async : false,
                                            url: "../restapi/updatecustomer.php",
                                            data:JSON.stringify({
                                                customer_code:customer_code,
                                                customer_fullname:customer_fullname,
                                                customer_email:customer_email,
                                                customer_tel:customer_tel,
                                                customer_password:customer_password,
                                                customer_profile:imagebase64
                                            }),
                                            success: function (response) {
                                                var json_data = response;
                                                var apistatus = json_data.result;
                                                if(apistatus=="Success") {
                                                    updatesuccess();
                                                    setTimeout(function(){ 
                                                        location.reload();
                                                    },2600);
                                                }else if(apistatus=="EMAIL_DUPLICATE"){
                                                    alertmail();
                                                    setTimeout(function(){},2600);
                                                }else if(apistatus=="TEL_DUPLICATE"){
                                                    alerttel();
                                                    setTimeout(function(){ },2600);
                                                }else{
                                                    alertexception();
                                                    setTimeout(function(){ },2600);
                                                }
                                            }
                                        });
                                }

                            }

                }
            }
        }
    }

    function cancelUpdateCustomerInfo() {
        location.reload();
    }

    function updateCustomerAddress() { 
        var button = document.getElementById("updateCustomerAddressBtn").innerHTML;
        if(button == "แก้ไข"){
            document.getElementById("customerAddressTel").disabled = false;
            document.getElementById("customerAddressFullname").disabled = false;
            document.getElementById("customerAddressDistrict").disabled = false;
            document.getElementById("customerAddressPostcode").disabled = false;
            document.getElementById("customerAddressProvince").disabled = false;
            document.getElementById("customerAddressDetail").disabled = false;
            document.getElementById('updateCustomerAddressBtn').classList.remove('btn-outline-warning');
            document.getElementById('updateCustomerAddressBtn').classList.add('btn-outline-success');
            document.getElementById("updateCustomerAddressBtn").innerHTML = "ยืนยัน";
        }
        if(button == "ยืนยัน"){
            if(document.formCustomerInfo.customerTel.value == "") {
                alert('กรุณากรอกเบอร์โทรศัพท์ของท่าน');
                document.formCustomerInfo.customerTel.focus();		
                return false;
            } else if (document.formCustomerInfo.customerEmail.value == "") {
                alert('กรุณากรอกอีเมลล์ของท่าน');
                document.formCustomerInfo.customerEmail.focus();		
                return false;
            } else if (document.formCustomerInfo.customerPassword.value == "") {
                alert('กรุณากรอกรหัสผ่านของท่าน');
                document.formCustomerInfo.customerPassword.focus();		
                return false;
            } else if (document.customerInfo.customerFullname.value == "") {
                alert('กรุณากรอกชื่อ - นามสกุลของท่าน');
                document.formCustomerInfo.customerFullname.focus();		
                return false;
            } else {
                document.forms["formCustomerAddress"].action = "webservice/UpdateCustomerAddress.php";
                document.forms["formCustomerAddress"].submit();
            }
        }
    }

    function cancelUpdateCustomerAddress() {
        var button = document.getElementById("updateCustomerAddressBtn").innerHTML;
        if(button == "ยืนยัน"){
            document.getElementById("customerAddressTel").disabled = true;
            document.getElementById("customerAddressFullname").disabled = true;
            document.getElementById("customerAddressDistrict").disabled = true;
            document.getElementById("customerAddressPostcode").disabled = true;
            document.getElementById("customerAddressProvince").disabled = true;
            document.getElementById("customerAddressDetail").disabled = true;
            document.getElementById('updateCustomerAddressBtn').classList.remove('btn-outline-success');
            document.getElementById('updateCustomerAddressBtn').classList.add('btn-outline-warning');
            document.getElementById("updateCustomerAddressBtn").innerHTML = "แก้ไข";
        }
    }

    function logoutFunction() {
                 logoutsuccess();
                   setTimeout(function(){ 
                    document.forms["formLogout"].action = "webservice/Logout.php";
                    document.forms["formLogout"].submit();
                   },2600);
    }
</script>

<body style="background-color:#ECECEC ;">
    <nav class="navbar navbar-expand-sm">
        <a class="navbar-brand" href="index.php" style="font-family: 'KRR_AengAei.ttf' !important; color: #FFA200;font-size:35px;">
            <i class="fas fa-dolly"></i>ผลิตภัณฑ์ชุมชน
        </a>
        <ul class="navbar-nav mr-auto"></ul>
        <ul class="navbar-nav">
            <?php
            if(@$_SESSION['role'] == "customer") {
                $customerCode = $_SESSION['customer_code'];
                $strSQL = "SELECT * FROM tbl_customer WHERE customer_code = '".$customerCode."' ";
                $result = @$conn->query($strSQL);
                while($row = $result->fetch_assoc()){
                    echo "
                        <li class=\"nav-item\">
                            <a style=\"color: White !important;,font-size: 14px !important;\" class=\"nav-link\"><i class=\"fas fa-user\"></i>ข้อมูลส่วนตัว</a>
                        </li>
                        <li class=\"nav-item\">
                            <a style=\"color: #FFA200 !important;,font-size: 14px !important;\" class=\"nav-link\">ยินดีต้อนรับ&nbspคุณ&nbsp :&nbsp " . @$row['customer_fullname'] . "</a>
                        </li>
                        <form id=\"formLogout\" name=\"formLogout\">
                            <li class=\"nav-item\">
                                <a class=\"nav-link\" style=\"cursor:pointer\" onclick=\"logoutFunction()\"><i class=\"fas fa-sign-out-alt\"></i>ออกจากระบบ</a>
                            </li>
                        </form>
                    ";
                }
            }
            ?>
        </ul>
    </nav>



    <div class="container-fluid" >
            <div class="row">
            <div class="col-sm-1" align="center" style="background-color:#ECECEC ;"> 
            </div>

                <div class="col-sm-2">
                <ul style=" list-style-type: none;">
                <li style="margin-bottom:15px;"> <i class="fas fa-user" style="color:#3AE100;"></i><span style="font-weight:bold;">ข้อมูลส่วนตัว</span>
                <ul style=" list-style-type: none;">
                    <li style="margin-bottom:5px;margin-top:5px;color:#FF8811;">ประวัติ</li>
                    <li style="margin-bottom:5px;"  ><a href="CustomerInfoNewFormAddress.php" id="menuleft">สถานที่จัดส่ง</a></li>
                </ul>
                </li>
                <li style="margin-bottom:15px;"> <i class="fas fa-file-invoice" style="color:#1A52E7;"></i><span style="font-weight:bold;">การซื้อของฉัน</span>
                    <ul style=" list-style-type: none;">
                    <li style="margin-bottom:5px;margin-top:5px;" ><a href="CustomerInfoNewFormOrderAll.php" id="menuleft">ทั้งหมด</a></li>
                    <li style="margin-bottom:5px;" ><a href="CustomerInfoNewFormWaitPayment.php" id="menuleft">ที่ต้องชำระ</a></li>
                    <li style="margin-bottom:5px;" ><a href="CustomerInfoNewFormWaitTracking.php" id="menuleft">ที่ต้องจัดส่ง</a></li>
                    <li style="margin-bottom:5px;" ><a href="CustomerInfoNewFormWaitReceives.php" id="menuleft">ที่ต้องได้รับ</a></li>
                    <li style="margin-bottom:5px;" ><a href="CustomerInfoNewFormWaitReceivesSuccess.php" id="menuleft">สำเร็จแล้ว</a></li>
                    </ul>
                </li>
                </ul>
                </div>


                <div class="col-sm-8" align="center" style="background-color:white;"> 
                       <!-- Tab panes -->
        <?php
            $strSQL = "SELECT * FROM tbl_customer  WHERE customer_code = '".$_SESSION['customer_code']."' ";
            $result = @$conn->query($strSQL);
            while($row = $result->fetch_assoc()){
        ?>
        <div class="tab-content">
            <div id="customerInfo" class="container tab-pane active"><br>
                <h3>ข้อมูลส่วนตัว ของ : คุณ<?php $customerFullname = $row['customer_fullname']; echo $customerFullname;?></h3>
                <form method="POST" name="formCustomerInfo" id="formCustomerInfo" enctype="multipart/form-data">
                    <div class="row" style="margin-left: 450px;">
                        <img src="../restapi/profile/<?php echo $row['customer_profile'];?>" id="showuploadimage" width="150px" height="150px" disabled>
                    </div>
                    <div class="row" style="margin-left: 450px;">
                        <input type="file" onchange="loadFile(event)" accept="image/*" id="customer_profile" name="customer_profile" disabled>
                    </div>
                    <div class="row" style="margin-left: 100px;">
                        <div class="col-sm-3">เบอร์โทรศัพท์ : </div>
                        <div class="col-sm-5 d-inline-flex">
                            <input type="text" class="form-control form-control-sm" id="customer_tel" name="customer_tel" value="<?php echo $row['customer_tel']; ?>" disabled >
                        </div>
                        <div class="col-sm-4" align="left">
                            <p hidden id="customer_tel_req" name="customer_tel_req" style="color:red; font-size:13px;">กรุณากรอกเบอร์โทรศัพท์ด้วยค่ะ</p>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 100px;">
                        <div class="col-sm-3">E-mail : </div>
                        <div class="col-sm-5 d-inline-flex">
                            <input type="email" class="form-control form-control-sm" id="customer_email" name="customer_email" value="<?php echo $row['customer_email']; ?>" disabled>
                        </div>
                        <div class="col-sm-4" align="left">
                            <p hidden id="customer_email_req" name="customer_email_req" style="color:red; font-size:13px;">กรุณากรอกอีเมลด้วยค่ะ</p>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 100px;">
                        <div class="col-sm-3">รหัสผ่าน : </div>
                        <div class="col-sm-5 d-inline-flex">
                            <input type="password" class="form-control form-control-sm" id="customer_password" name="customer_password" value="<?php echo $row['customer_password']; ?>" disabled>
                        </div>
                        <div class="col-sm-4" align="left">
                            <p hidden id="customer_password_req" name="customer_password_req" style="color:red; font-size:13px;">กรุณากรอกรหัสผ่านด้วยค่ะ</p>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 100px;">
                        <div class="col-sm-3">ชื่อ - นามสกุล : </div>
                        <div class="col-sm-5 d-inline-flex">
                            <input type="text" class="form-control form-control-sm" id="customer_fullname" name="customer_fullname" value="<?php echo $row['customer_fullname']; ?>" disabled>
                        </div>
                        <div class="col-sm-4" align="left">
                            <p hidden id="customer_fullname_req" name="customer_fullname_req" style="color:red; font-size:13px;">กรุณากรอกชื่อ - นามสกุลด้วยค่ะ</p>
                        </div>
                    </div>
                    <div class="row" align="center" style="margin-left: 300px; padding-top: 20px;">
                        <div class="col-sm-4">
                            <a id="updateCustomerInfoBtn" class="btn btn-outline-warning btn-sm" onclick="updateCustomerInfo()">แก้ไข</a>
                        </div>
                        <div class="col-sm-2 d-inline-flex">
                            <a id="cancelUpdateCustomerInfoBtn" style="margin-left: -50px;" class="btn btn-outline-danger btn-sm" onclick="cancelUpdateCustomerInfo()">ยกเลิก</a>
                        </div>
                    </div>
                </form>
            </div>
            <?php
            }
        ?>
    <br>  <br>
                </div>
                <div class="col-sm-1" align="center" style="background-color:#ECECEC  ;"> 
                </div>
            </div>
    </div>


            <!-- Modal LOGOUT-->
            <div class="modal fade" id="logoutsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/logout.png" width="150px" height="150px">
        <br><br>
        <p>กำลังออกจากระบบ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->


    <!-- Modal UPDATESUCCESS-->
    <div class="modal fade" id="updatesuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p>แก้ไขข้อมูลเรียบร้อย</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

<!--  insertransport Modal -->
<div class="modal" id="customeraddaddress">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">เพิ่มข้อมูลที่อยู่</h2>
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">ชื่อ - นามสกุล : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="customer_address_fullname" name="customer_address_fullname"  >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">เบอรโทรศัพท์ : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" maxlenght="10" class="form-control form-control-sm" id="customer_address_tel" name="customer_address_tel"  >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">จังหวัด : </div>
                            <div class="col-sm-6 d-inline-flex">
                              
                              <select id="customer_address_province" name="customer_address_province" onchange="getprovincecode()">
                                   
                                    <option hidden>กรุณาเลือกจังหวัด</option>
                              <?php
                                   include "../restapi/setting/configthailand.php";
                                   $getprovince ="SELECT * FROM th_province ORDER BY name_th ASC";
                                   $resultgetprovince = @$conn->query($getprovince);
                                   if($resultgetprovince->num_rows > 0){
                                       while($rowgetprovince = $resultgetprovince->fetch_assoc()){
                              ?>
                                     <option value="<?php echo $rowgetprovince['name_th'].":".$rowgetprovince['id'] ?>">จังหวัด<?php echo $rowgetprovince['name_th'];  ?></option>
                              <?php
                                       }}
                              ?>
                              </select> 
                            
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">อำเภอ : </div>
                            <div class="col-sm-6 d-inline-flex">
                                   
                            <select id="customer_address_district" name="customer_address_district" Disabled onchange="getdistrictcode()">
                            <option hidden>กรุณาเลือกอำเภอ</option>
                            </select> 
                            
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">ตำบล : </div>
                            <div class="col-sm-6 d-inline-flex">
                                   
                            <select id="customer_address_subdistrict" name="customer_address_subdistrict" Disabled onchange="getamphurpostcode()">
                            <option hidden>กรุณาเลือกตำบล</option>
                            </select> 
                            
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">รหัสไปรษณีย์ : </div>
                            <div class="col-sm-6 d-inline-flex">
                               
                                <input type="text" class="form-control form-control-sm" id="customer_address_postcode" name="customer_address_postcode"  readonly>
                           
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">รายละเอียดที่อยู่ (บ้านเลขที่ หมู่บ้าน ถนน ซอย) : </div>
                            <div class="col-sm-6 d-inline-flex">
                               
                                <textarea  rows="4" class="form-control form-control-sm" id="customer_address_detail" name="customer_address_detail" >
                                </textarea>
                           
                            </div>
                        </div>

                        </div></div>
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-success btn-sm" data-dismiss="modal" onclick="insertaddress()">ยืนยัน</a>
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    <!-- End of insertransport Modal -->

    <script>

    function insertaddress(){
        var customer_code="<?php echo $_SESSION['customer_code']?>";
        var customer_address_fullname=document.getElementById("customer_address_fullname").value;
        var customer_address_tel=document.getElementById("customer_address_tel").value;
        
        var customer_address_province = document.getElementById("customer_address_province").value;
        var customer_address_province = customer_address_province.split(":");
        var customer_address_province = customer_address_province[0].trim();

        var customer_address_district = document.getElementById("customer_address_district").value;
        var customer_address_district = customer_address_district.split(":");
        var customer_address_district = customer_address_district[0].trim();

        var customer_address_subdistrict = document.getElementById("customer_address_subdistrict").value;
        var customer_address_subdistrict = customer_address_subdistrict.split(":");
        var customer_address_subdistrict = customer_address_subdistrict[0].trim();

        var customer_address_postcode = document.getElementById("customer_address_postcode").value;
        var customer_address_detail = document.getElementById("customer_address_detail").value.trim();
       

        $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/addcustomeraddress.php",
                data:JSON.stringify({
                    customer_code:customer_code,
                    customer_address_fullname:customer_address_fullname,
                    customer_address_tel:customer_address_tel,
                    customer_address_province:customer_address_province,
                    customer_address_district:customer_address_district,
                    customer_address_subdistrict:customer_address_subdistrict,
                    customer_address_postcode:customer_address_postcode,
                    customer_address_detail:customer_address_detail
                }),
                success: function (response) {
                var json_data = response;
                var getdata = json_data;
                console.log(getdata);
                if(getdata.result=="Success"){
                    insertsuccess();
                    setTimeout(function(){ 
                    location.reload();
                    },1600);
                }
                }
            });
    }
    
    function getdistrictcode(){
           document.getElementById("customer_address_postcode").value="";
           var districtcode=document.getElementById("customer_address_district").value;
           var districtcode = districtcode.split(":");
           console.log(districtcode[1]);
           customer_address_subdistrict="<option hidden>กรุณาเลือกตำบล</option>";
            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/getsubdistrictlist.php",
                data:JSON.stringify({
                    districtcode :districtcode[1] 
                }),
                success: function (response) {
    
                    $.each(response, function(index) {
                        customer_address_subdistrict += "<option value="+"  '"+response[index].TAMBON_NAME+":"+response[index].POST_CODE+"' "+">"+response[index].TAMBON_NAME+"</option>";
                    });
                    document.getElementById("customer_address_subdistrict").disabled = false;
                    $('#customer_address_subdistrict').html(customer_address_subdistrict);
                }
            });
    }

    function getprovincecode(){
            document.getElementById("customer_address_postcode").value="";
           var provincecode=document.getElementById("customer_address_province").value;
           var provincecode = provincecode.split(":");
           console.log(provincecode[1]);
           customer_address_district="<option hidden>กรุณาเลือกอำเภอ</option>";
           customer_address_subdistrict="<option hidden>กรุณาเลือกตำบล</option>";
           $('#customer_address_subdistrict').html(customer_address_subdistrict);
           document.getElementById("customer_address_subdistrict").disabled = true;
            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/getamphurlist.php",
                data:JSON.stringify({
                    PROVINCE_ID :provincecode[1] 
                }),
                success: function (response) {
    
                    $.each(response, function(index) {
                        customer_address_district += "<option value="+"  '"+response[index].AMPHUR_NAME+":"+response[index].AMPHUR_ID+"' "+">"+response[index].AMPHUR_NAME+"</option>";
                    });
                    document.getElementById("customer_address_district").disabled = false;
                    $('#customer_address_district').html(customer_address_district);
                }
            });

    }
    function getamphurpostcode(){
        var postcode=document.getElementById("customer_address_subdistrict").value;
        var postcode = postcode.split(":");
        var postcode = postcode[1];
        console.log(postcode);
        document.getElementById("customer_address_postcode").value=postcode;
    }
    </script>


    <!-- Modal AddataSUCCESS-->
    <div class="modal fade" id="insertsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <span>เพิ่มข้อมูลสำเร็จ</span><span id="insertsuccesshtml"></span>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

            <!-- Modal CANTDELETE-->
            <div class="modal fade" id="cantdelete" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p>ไม่สามารถลบข้อมูลได้เนื่องจากมีการใช้ข้อมูลอยู่</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

     <!-- Modal DELETESUCCESS-->
 <div class="modal fade" id="deletesuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p>ลบข้อมูลสำเร็จ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

         <!-- Modal ALERTTEL-->
         <div class="modal fade" id="alerttel" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p>เบอร์โทรศัพท์ของท่าน ซ้ำ!! ในระบบ กรุณากรอก เบอร์โทรศัพท์ อื่น</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

         <!-- Modal ALERTMAIL-->
         <div class="modal fade" id="alertmail" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p>E-Mail ของท่าน ซ้ำ!! ในระบบ กรุณากรอก E-Mail อื่น</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

           <!-- Modal alertexception-->
           <div class="modal fade" id="alertexception" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p>เกิดข้อผิดพลาดกรุณาลองอีกครั้ง</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

</body>
<footer style="background-color: #575757; ">
<div class="container-fluid">
        <nav class="navbar navbar-expand-sm" style="display: flex;justify-content: center;">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <label style="color: #FFA200;font-size: 14px;">&copy;&nbsp;2020 localshopcenter.com</label>
                </li>
            </ul>
        </nav>
    </div>
</footer>
</html>