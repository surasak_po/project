<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>สมัครสมาชิกสำหรับลูกค้า</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="./css/styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }

        .card {
            background-color: #F88360;  
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }
        #rcorners4 {
            border-radius: 20px;
            border: 1.5px solid #D1D1D1;
            padding: 20px;
            width: 65%;
        }
        a :hover{
            color:white;
        }
        .modal-header .close {
        display:none;
        }
    </style>
</head>
<script>
    function alerttel() {
        $("#alerttel").modal({backdrop: 'static', keyboard: false})  
        $("#alerttel").modal('show');
        setTimeout(function(){$('#alerttel').modal('hide')},2000);
    }
    function alertmail() {
        $("#alertmail").modal({backdrop: 'static', keyboard: false})  
        $("#alertmail").modal('show');
        setTimeout(function(){$('#alertmail').modal('hide')},2000);
    }
    function loginfail() {
        $("#loginfail").modal({backdrop: 'static', keyboard: false})  
        $("#loginfail").modal('show');
        setTimeout(function(){$('#loginfail').modal('hide')},3000);
    }

    function loginsuccess() {
        $("#loginsuccess").modal({backdrop: 'static', keyboard: false})  
        $("#loginsuccess").modal('show');
        setTimeout(function(){$('#loginsuccess').modal('hide')},3000);
    }

    function logoutsuccess() {
        $("#logoutsuccess").modal({backdrop: 'static', keyboard: false})  
        $("#logoutsuccess").modal('show');
        setTimeout(function(){$('#logoutsuccess').modal('hide')},2000);
    }
    function regissuccess() {
        $("#regissuccess").modal({backdrop: 'static', keyboard: false})  
        $("#regissuccess").modal('show');
        setTimeout(function(){$('#regissuccess').modal('hide')},3000);
    }
    function customerLoginFunction() {
        var customerUsername = document.getElementById("customerUsername").value;
        var customerPassword = document.getElementById("customerPassword").value;

        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/logincustomer.php",
            data:JSON.stringify({
                customerUsername:customerUsername,
                customerPassword:customerPassword
            }),
            success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;
                if(apistatus=="Success") {
              
                   loginsuccess();
                   setTimeout(function(){ 
                    location.replace("index.php");
                   },3600);

                }else{
                    loginfail();
                    document.getElementById("customerUsername").value="";
                    document.getElementById("customerPassword").value="";
                }
            }
        });
    }

    function storeLoginFunction() {
        var storeUsername = document.getElementById("storeUsername").value;
        var storePassword = document.getElementById("storePassword").value;

        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/LoginStore.php",
            data:JSON.stringify({
                storeUsername:storeUsername,
                storePassword:storePassword
            }),
            success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;
                if(apistatus=="Success") {
                loginsuccess();
                setTimeout(function(){ 
                    location.replace("Store.php");
                },3600);

                }else{
                    loginfail();
                    document.getElementById("storeUsername").value="";
                    document.getElementById("storePassword").value="";
                }
            }
        });
    }

    function adminLoginFunction() {
        var adminUsername = document.getElementById("adminUsername").value;
        var adminPassword = document.getElementById("adminPassword").value;
    
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/LoginAdmin.php",
            data:JSON.stringify({
                adminUsername:adminUsername,
                adminPassword:adminPassword
            }),
            success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;
                if(apistatus=="Success") {
                loginsuccess();
                setTimeout(function(){ 
                    location.replace("Admin.php");
                },3600);

                }else{
                    loginfail();
                    document.getElementById("adminUsername").value=""
                    document.getElementById("adminPassword").value=""
                }
            }
        });
    }

    function logoutFunction() {
        logoutsuccess();
            setTimeout(function(){ 
            document.forms["formLogout"].action = "../restapi/logout.php";
            document.forms["formLogout"].submit();
        },2600);
    }
</script>
<body>
    <!-- 2. Login Group Modal -->
    <div class="modal" id="loginGroupModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เลือกประเภทการเข้าสู่ระบบ</h2>
            </div>

            <!-- Modal body -->
                <div class="modal-body">
                    <div row align="center" style="margin-bottom: -25px;">
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 50px;">
                            <p style="font-size: 60px; color: #17a2b8; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-users"></i></p>
                        </div>
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 10px;">
                            <p style="font-size: 60px; color: #007bff; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-store"></i></p>
                        </div>
                    </div>
                    <div row align="center" style="margin-bottom: 30px;">
                        <div class="col-sm-5 d-inline-flex">
                            <button data-toggle="modal" data-target="#loginCustomerModal" class="btn btn-info" type="button" data-dismiss="modal">สำหรับลูกค้า</button>
                        </div>
                        <div class="col-sm-5 d-inline-flex">
                            <button data-toggle="modal" data-target="#loginStoreModal" class="btn btn-primary" type="button" data-dismiss="modal">สำหรับร้านค้า</button>
                        </div>
                    </div>
                    <div row align="center" style="margin-bottom: -25px;">
                            <p style="font-size: 60px; color: #343a40; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-crown"></i></p>
                    </div>
                    <div row align="center">
                        <button data-toggle="modal" data-target="#loginAdminModal" class="btn btn-dark" type="button" data-dismiss="modal">สำหรับแอดมิน</button>
                    </div>
                </div>

            <!-- Modal footer -->
            <br>
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-danger" type="button" data-dismiss="modal">ปิดหน้านี้</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2. End of Login Customer Modal -->

    <!-- 2.1 Login Customer Modal -->
    <div class="modal" id="loginCustomerModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เข้าสู่ระบบ (สำหรับลูกค้า)</h2>
            </div>

            <!-- Modal body -->
            <form id="formLoginCustomer" name="formLoginCustomer" method="POST">
                <div class="modal-body">
                    <img src="./icons/loginlogo.png" width="150px" height="150px">
                    <div class="form-group col-sm-9">
                        <label for="customerUsername" style="font-weight: regular;">ชื่อผู้ใช้</label>
                        <input type="text" placeholder="เบอร์โทรศัพท์หรืออีเมลล์" class="form-control" id="customerUsername" name="customerUsername">
                    </div>
                    <div class="form-group col-sm-9">
                        <label for="customerPassword" style="font-weight: regular;">รหัสผ่าน</label>
                        <input type="password" placeholder="รหัสผ่าน" class="form-control" id="customerPassword" name="customerPassword">
                    </div>
                </div>

            <!-- Modal footer -->
            <br>
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-success" type="button" onclick="customerLoginFunction()">เข้าสู่ระบบ</button>
                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#loginGroupModal" data-dismiss="modal">ยกเลิก</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2.1 End of Login Customer Modal -->

    <!-- 2.2 Login Store Modal -->
    <div class="modal" id="loginStoreModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เข้าสู่ระบบ (สำหรับร้านค้า)</h2>
            </div>

            <!-- Modal body -->
            <form id="formLoginStore" name="formLoginStore" method="POST">
                <div class="modal-body">
                    <img src="./icons/loginlogo.png" width="150px" height="150px">
                    <div class="form-group col-sm-9">
                        <label for="storeUsername" style="font-weight: regular;">ชื่อผู้ใช้</label>
                        <input type="text" placeholder="เบอร์โทรศัพท์หรืออีเมลล์" class="form-control" id="storeUsername" name="storeUsername">
                    </div>
                    <div class="form-group col-sm-9">
                        <label for="storePassword" style="font-weight: regular;">รหัสผ่าน</label>
                        <input type="password" placeholder="รหัสผ่าน" class="form-control" id="storePassword" name="storePassword">
                    </div>
                </div>

            <!-- Modal footer -->
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-success" type="button" onclick="storeLoginFunction()">เข้าสู่ระบบ</button>
                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#loginGroupModal" data-dismiss="modal">ยกเลิก</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2.2 End of Login Store Modal -->

    <!-- 2.3 Admin Login Modal -->
    <div class="modal" id="loginAdminModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">
                
            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เข้าสู่ระบบ (สำหรับผู้ดูแลระบบ)</h2>
            </div>
            <form id="formAdminLogin" name="formAdminLogin" method="POST">

            <!-- Modal body -->
                <div class="modal-body">
                    <img src="./icons/loginlogo.png" width="150px" height="150px">
                    <div class="form-group col-sm-9">
                        <label for="adminUsername" style="font-weight: regular;">ชื่อผู้ใช้</label>
                        <input type="text" placeholder="ชื่อผู้ใช้งานผู้ดูแล" class="form-control" id="adminUsername" name="adminUsername">
                    </div>
                    <div class="form-group col-sm-9">
                        <label for="adminPassword" style="font-weight: regular;">รหัสผ่าน</label>
                        <input type="password" placeholder="รหัสผ่าน" class="form-control" id="adminPassword" name="adminPassword">
                    </div>
                </div>

            <!-- Modal footer -->
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-success" type="button" onclick="adminLoginFunction()">เข้าสู่ระบบ</button>
                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#loginGroupModal" data-dismiss="modal">ยกเลิก</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2.3 End of Admin Login Modal -->
    <!-- End of Modal Zone -->
    <nav class="navbar navbar-expand-sm">
        <a class="navbar-brand nav-link" href="Index.php"  style="font-family:'KRR_AengAei.ttf'!important; color: #FFA200;font-size:35px;">
            <i class="fas fa-dolly"></i>ผลิตภัณฑ์ชุมชน
        </a>
        <ul class="navbar-nav mr-auto"></ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" style="color:white;"><i class="fas fa-user-plus"></i>สมัครสมาชิก</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#loginGroupModal" style="cursor:pointer"><i class="fas fa-sign-in-alt"></i>เข้าสู่ระบบ</a>
            </li>
        </ul>
    </nav>

    <div align="center">
        <div style="margin-top: 20px; margin-bottom: 20px;">
            <h3 align="left" style="margin-left:80px;">สมัครสมาชิกสำหรับลูกค้า</h3>
        </div>
        <div id="rcorners4" style="margin-bottom:40px;">
            <img src="icons/registerlogo.png" width= "150px"/>
            <form id="formCustomerRegister" name="formCustomerRegister" method="POST">
            <div class="form-group">
                <div class="row">
                    <div align="left" style="padding-left: 40px;" class="col-sm-3">
                        <label style="font-weight: regular;"><span style="color:red;">*</span>เบอร์โทรศัพท์ : </label>
                    </div>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="customer_tel" name="customer_tel" maxlength="10"
                        onkeyup="this.value=this.value.replace(/[^\d]/,'')">
                    </div>
                    <div class="col-sm-4" align="left">
                        <p hidden id="customer_tel_req" name="customer_tel_req" style="color:red;">กรุณากรอกเบอร์โทรศัพท์ด้วยค่ะ</p>
                    </div>
                </div>
                <div class="row">
                    <div align="left" style="padding-left: 40px;" class="col-sm-3">
                        <label style="font-weight: regular;"><span style="color:red;">*</span>อีเมลล์ : </label>
                    </div>
                    <div class="col-sm-5">
                        <input type="email" class="form-control" id="customer_email" name="customer_email" pattern="[^@\s]+@[^@\s]+\.[^@\s]+">
                    </div>
                    <div class="col-sm-4" align="left">
                        <p hidden id="customer_email_req" name="customer_email_req" style="color:red;">กรุณากรอกอีเมลล์ด้วยค่ะ</p>
                    </div>
                </div>
                <div class="row">
                    <div align="left" style="padding-left: 40px;" class="col-sm-3">
                        <label style="font-weight: regular;"><span style="color:red;">*</span>ชื่อ-นามสกุล : </label>
                    </div>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="customer_fullname" name="customer_fullname">
                    </div>
                    <div class="col-sm-4" align="left">
                        <p hidden id="customer_fullname_req" name="customer_fullname_req" style="color:red;">กรุณากรอกชื่อและนามสกุลด้วยค่ะ</p>
                    </div>
                </div>
                <div class="row">
                    <div align="left" style="padding-left: 40px;" class="col-sm-3">
                        <label style="font-weight: regular;"><span style="color:red;">*</span>รหัสผ่าน : </label>
                    </div>
                    <div class="col-sm-5">
                        <input type="password" class="form-control" id="customer_password" name="customer_password">
                    </div>
                    <div class="col-sm-4" align="left">
                        <p hidden id="customer_password_req" name="customer_password_req" style="color:red;">กรุณากรอกรหัสผ่านด้วยค่ะ</p>
                    </div>
                </div>
            </div>
            <button stlye="margin-bottom:20px;" class="btn btn-success" type="button" onclick="registerFunction()">สมัครสมาชิก</button>
            </form>
        </div>
    </div>
        <!-- Modal SUCCESS-->
  <div class="modal fade" id="loginsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p>เข้าสู่ระบบสำเร็จ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

     <!-- Modal REGISSUCCESS-->
     <div class="modal fade" id="regissuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p>สมัครสมาชิกสำเร็จ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

       <!-- Modal ALERTTEL-->
       <div class="modal fade" id="alerttel" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p>เบอร์โทรศัพท์ของท่าน ซ้ำ!! ในระบบ กรุณากรอก เบอร์โทรศัพท์ อื่น</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

         <!-- Modal ALERTMAIL-->
         <div class="modal fade" id="alertmail" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p>E-Mail ของท่าน ซ้ำ!! ในระบบ กรุณากรอก E-Mail อื่น</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->
</body>
<script>
    function registerFunction() {
        var customer_tel = document.getElementById("customer_tel").value;
        var customer_email = document.getElementById("customer_email").value;
        var customer_fullname = document.getElementById("customer_fullname").value;
        var customer_password = document.getElementById("customer_password").value;
        var emailFilter= /^.+@.+\..{2,3}$/ ;
        if(customer_tel == "") {
            document.getElementById("customer_tel_req").hidden=false;
        } else {
            document.getElementById("customer_tel_req").hidden=true;
        } 
        
        if (customer_fullname == "") {
            document.getElementById("customer_fullname_req").hidden=false;
        } else {
            document.getElementById("customer_fullname_req").hidden=true;
        }
        
        if (customer_password == "") {
            document.getElementById("customer_password_req").hidden=false;
        } else {
            document.getElementById("customer_password_req").hidden=true;
        }

        if (customer_email == "") {
            document.getElementById("customer_email_req").hidden=false;
        } else {
            document.getElementById("customer_email_req").hidden=true;
        }
        
        if(customer_tel != "" && customer_email != "" && customer_fullname != "" && customer_password != "") {
            if(customer_tel.length != 10 && !(emailFilter.test(customer_email))) {
                document.getElementById("customer_tel_req").innerHTML="กรุณากรอกเบอร์โทรศัพท์ให้ครบ 10 หลัก";
                document.getElementById("customer_tel_req").hidden=false; 
                document.getElementById("customer_email_req").innerHTML="ท่านกรอกรูปแบบอีเมลล์ไม่ถูกต้อง";
                document.getElementById("customer_email_req").hidden=false;
            } else if (customer_tel.length != 10 && (emailFilter.test(customer_email))) {
                document.getElementById("customer_tel_req").innerHTML="กรุณากรอกเบอร์โทรศัพท์ให้ครบ 10 หลัก";
                document.getElementById("customer_tel_req").hidden=false; 
            } else if (customer_tel.length == 10 &&!(emailFilter.test(customer_email))) {
                document.getElementById("customer_email_req").innerHTML="ท่านกรอกรูปแบบอีเมลล์ไม่ถูกต้อง";
                document.getElementById("customer_email_req").hidden=false;
            } else {
                $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/addcustomer.php",
                data:JSON.stringify({
                    customer_tel:customer_tel,
                    customer_email:customer_email,
                    customer_fullname:customer_fullname,
                    customer_password:customer_password
                }),
                success: function (response) {
                    var json_data = response.result;
                    if(json_data=="Success") {
                        regissuccess()
                        setTimeout(function(){ 
                            location.replace("LoginRegCus.php");
                        },3600);
                    }
                    if(json_data=="EMAIL_DUPLICATE") { alertmail()}
                    if(json_data=="TEL_DUPLICATE") { alerttel() }
                }
            });
            }
        }
    }
</script>

<footer style="background-color: #575757;">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-sm" style="display: flex;justify-content: center;">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <label style="color: #FFA200;font-size: 14px;">&copy;&nbsp;2020 localshopcenter.com</label>
                </li>
            </ul>
        </nav>
    </div>
</footer>
</html>