<?php
    include "../restapi/setting/config.php";
    @session_start();
    @session_cache_expire(30);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ตะกร้าสินค้า</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="./css/styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <style>
        body {
            background-color: #ECECEC;
        }

        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }

        #rcorners2 {
            border: 1.5px solid #D1D1D1;
            padding: 10px;
            width: 85%;
            -webkit-box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
            -moz-box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
            box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
            background-color: white;
        }
    
        .card {
            background-color: white;  
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }

        input.checkbox { 
            width: 23px; 
            height: 23px;
        }

        img {
            border: 0.5px solid #ddd;
            padding: 5px;
            width: 99px;
            background-color: white;
            -webkit-box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
            -moz-box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
            box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
        }

        #b :hover {
            color: #FF6969!important;
        }
    </style>
</head>

<script>
    var changeclass="";
    function proGroupSelect(id) {
        alert("change img price name")
        if(changeclass==""){
            document.getElementById(id).classList.add('btn-warning');
            document.getElementById(id).classList.remove('btn-outline-warning');
            changeclass=id;
        } else {
            document.getElementById(changeclass).classList.add('btn-outline-warning');
            document.getElementById(changeclass).classList.remove('btn-warning');
            document.getElementById(id).classList.add('btn-warning');
            document.getElementById(id).classList.remove('btn-outline-warning');
            changeclass=id;
        }
        
    }

    function buyBtn() {
        alert("buy");
    }
    
    function addCartBtn() {
        alert("addcart");
    }

    function loginfail() {
        $("#loginfail").modal('show');
        setTimeout(function(){$('#loginfail').modal('hide')},3000);
    }

    function loginsuccess() {
        $("#loginsuccess").modal('show');
        setTimeout(function(){$('#loginsuccess').modal('hide')},3000);
    }

    function logoutsuccess() {
        $("#logoutsuccess").modal('show');
        setTimeout(function(){$('#logoutsuccess').modal('hide')},2000);
    }
    
    function customerLoginFunction() {
        var customerUsername = document.getElementById("customerUsername").value;
        var customerPassword = document.getElementById("customerPassword").value;

        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/logincustomer.php",
            data:JSON.stringify({
                customerUsername:customerUsername,
                customerPassword:customerPassword
            }),
            success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;
                if(apistatus=="Success") {
              
                   loginsuccess();
                   setTimeout(function(){ 
                    location.reload();
                   },3600);

                }else{
                    loginfail();
                    document.getElementById("customerUsername").value="";
                    document.getElementById("customerPassword").value="";
                }
            }
        });
    }

    function storeLoginFunction() {
        var storeUsername = document.getElementById("storeUsername").value;
        var storePassword = document.getElementById("storePassword").value;

        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/LoginStore.php",
            data:JSON.stringify({
                storeUsername:storeUsername,
                storePassword:storePassword
            }),
            success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;
                if(apistatus=="Success") {
                loginsuccess();
                setTimeout(function(){ 
                    location.replace("Store.php");
                },3600);

                }else{
                    loginfail();
                    document.getElementById("storeUsername").value="";
                    document.getElementById("storePassword").value="";
                }
            }
        });
    }

    function adminLoginFunction() {
        var adminUsername = document.getElementById("adminUsername").value;
        var adminPassword = document.getElementById("adminPassword").value;
    
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/LoginAdmin.php",
            data:JSON.stringify({
                adminUsername:adminUsername,
                adminPassword:adminPassword
            }),
            success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;
                if(apistatus=="Success") {
                loginsuccess();
                setTimeout(function(){ 
                    location.replace("Admin.php");
                },3600);

                }else{
                    loginfail();
                    document.getElementById("adminUsername").value=""
                    document.getElementById("adminPassword").value=""
                }
            }
        });
    }

    function logoutFunction() {
        logoutsuccess();
            setTimeout(function(){ 
            document.forms["formLogout"].action = "../restapi/logout.php";
            document.forms["formLogout"].submit();
        },2600);
    }
</script>

<body>
    <!-- Modal Zone -->
    <!-- 1. Register Group Modal -->
    <div class="modal" id="registerGroupModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เลือกประเภทการสมัครสมาชิก</h2>
            </div>

            <!-- Modal body -->
                <div class="modal-body">
                    <div row align="center" style="margin-bottom: -25px;">
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 50px;">
                            <p style="font-size: 60px; color: #17a2b8; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-users"></i></p>
                        </div>
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 10px;">
                            <p style="font-size: 60px; color: #007bff; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-store"></i></p>
                        </div>
                    </div>
                    <div row align="center">
                        <div class="col-sm-5 d-inline-flex">
                            <a class="btn btn-info" type="button" href="CustomerRegister.php">สำหรับลูกค้า</a>
                        </div>
                        <div class="col-sm-5 d-inline-flex">
                            
                        <a class="btn btn-primary" type="button" href="StoreRegister.php">สำหรับร้านค้า</a>
                        </div>
                    </div>
                </div>

            <!-- Modal footer -->
            <br>
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-danger" type="button" data-dismiss="modal">ปิดหน้านี้</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 1. End of Register Group Modal -->

    <!-- 2. Login Group Modal -->
    <div class="modal" id="loginGroupModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เลือกประเภทการเข้าสู่ระบบ</h2>
            </div>

            <!-- Modal body -->
                <div class="modal-body">
                    <div row align="center" style="margin-bottom: -25px;">
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 50px;">
                            <p style="font-size: 60px; color: #17a2b8; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-users"></i></p>
                        </div>
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 10px;">
                            <p style="font-size: 60px; color: #007bff; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-store"></i></p>
                        </div>
                    </div>
                    <div row align="center" style="margin-bottom: 30px;">
                        <div class="col-sm-5 d-inline-flex">
                            <button data-toggle="modal" data-target="#loginCustomerModal" class="btn btn-info" type="button" data-dismiss="modal">สำหรับลูกค้า</button>
                        </div>
                        <div class="col-sm-5 d-inline-flex">
                            <button data-toggle="modal" data-target="#loginStoreModal" class="btn btn-primary" type="button" data-dismiss="modal">สำหรับร้านค้า</button>
                        </div>
                    </div>
                    <div row align="center" style="margin-bottom: -25px;">
                            <p style="font-size: 60px; color: #343a40; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-crown"></i></p>
                    </div>
                    <div row align="center">
                        <button data-toggle="modal" data-target="#loginAdminModal" class="btn btn-dark" type="button" data-dismiss="modal">สำหรับแอดมิน</button>
                    </div>
                </div>

            <!-- Modal footer -->
            <br>
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-danger" type="button" data-dismiss="modal">ปิดหน้านี้</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2. End of Login Group Modal -->

    <!-- 2.1 Login Customer Modal -->
    <div class="modal" id="loginCustomerModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เข้าสู่ระบบ (สำหรับลูกค้า)</h2>
            </div>

            <!-- Modal body -->
            <form id="formLoginCustomer" name="formLoginCustomer" method="POST">
                <div class="modal-body">
                    <img src="./icons/loginlogo.png" width="150px" height="150px">
                    <div class="form-group col-sm-9">
                        <label for="customerUsername" style="font-weight: regular;">ชื่อผู้ใช้</label>
                        <input type="text" placeholder="เบอร์โทรศัพท์หรืออีเมลล์" class="form-control" id="customerUsername" name="customerUsername">
                    </div>
                    <div class="form-group col-sm-9">
                        <label for="customerPassword" style="font-weight: regular;">รหัสผ่าน</label>
                        <input type="password" placeholder="รหัสผ่าน" class="form-control" id="customerPassword" name="customerPassword">
                    </div>
                </div>

            <!-- Modal footer -->
            <br>
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-success" type="button" onclick="customerLoginFunction()">เข้าสู่ระบบ</button>
                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#loginGroupModal" data-dismiss="modal">ยกเลิก</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2.1 End of Login Customer Modal -->

    <!-- 2.2 Login Store Modal -->
    <div class="modal" id="loginStoreModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เข้าสู่ระบบ (สำหรับร้านค้า)</h2>
            </div>

            <!-- Modal body -->
            <form id="formLoginStore" name="formLoginStore" method="POST">
                <div class="modal-body">
                    <img src="./icons/loginlogo.png" width="150px" height="150px">
                    <div class="form-group col-sm-9">
                        <label for="storeUsername" style="font-weight: regular;">ชื่อผู้ใช้</label>
                        <input type="text" placeholder="เบอร์โทรศัพท์หรืออีเมลล์" class="form-control" id="storeUsername" name="storeUsername">
                    </div>
                    <div class="form-group col-sm-9">
                        <label for="storePassword" style="font-weight: regular;">รหัสผ่าน</label>
                        <input type="password" placeholder="รหัสผ่าน" class="form-control" id="storePassword" name="storePassword">
                    </div>
                </div>

            <!-- Modal footer -->
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-success" type="button" onclick="storeLoginFunction()">เข้าสู่ระบบ</button>
                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#loginGroupModal" data-dismiss="modal">ยกเลิก</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2.2 End of Login Store Modal -->

    <!-- 2.3 Admin Login Modal -->
    <div class="modal" id="loginAdminModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">
                
            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เข้าสู่ระบบ (สำหรับผู้ดูแลระบบ)</h2>
            </div>
            <form id="formAdminLogin" name="formAdminLogin" method="POST">

            <!-- Modal body -->
                <div class="modal-body">
                    <img src="./icons/loginlogo.png" width="150px" height="150px">
                    <div class="form-group col-sm-9">
                        <label for="adminUsername" style="font-weight: regular;">ชื่อผู้ใช้</label>
                        <input type="text" placeholder="ชื่อผู้ใช้งานผู้ดูแล" class="form-control" id="adminUsername" name="adminUsername">
                    </div>
                    <div class="form-group col-sm-9">
                        <label for="adminPassword" style="font-weight: regular;">รหัสผ่าน</label>
                        <input type="password" placeholder="รหัสผ่าน" class="form-control" id="adminPassword" name="adminPassword">
                    </div>
                </div>

            <!-- Modal footer -->
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-success" type="button" onclick="adminLoginFunction()">เข้าสู่ระบบ</button>
                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#loginGroupModal" data-dismiss="modal">ยกเลิก</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2.3 End of Admin Login Modal -->
    <!-- End of Modal Zone -->

    <nav class="navbar navbar-expand-sm">
        <a class="navbar-brand" style="font-family:'KRR_AengAei.ttf'!important; color: #FFA200;font-size:35px;">
            <i class="fas fa-dolly"></i>ตะกร้าสินค้า
        </a>
        <ul class="navbar-nav mr-auto"></ul>
        <ul class="navbar-nav">
            <?php
                if (@$_SESSION['role'] == "customer") {
                    $strSQL = "SELECT * FROM tbl_customer WHERE customer_code = '".$_SESSION['customer_code']."' ";
                    $result = @$conn->query($strSQL);
                    while($row = $result->fetch_assoc()){
                        echo "
                            <li class=\"nav-item\">
                               <a href=\"CustomerInfo.php\" class=\"nav-link\"><i class=\"fas fa-user\"></i>ข้อมูลส่วนตัว</a>
                            </li>
                            <li class=\"nav-item\">
                                <a style=\"color: #FFA200 !important;,font-size: 14px !important;\" class=\"nav-link\">ยินดีต้อนรับ&nbspคุณ&nbsp :&nbsp " . @$row['customer_fullname'] . "</a>
                            </li>
                            <form id=\"formLogout\" name=\"formLogout\">
                                <li class=\"nav-item\">
                                    <a class=\"nav-link\" style=\"cursor:pointer\" onclick=\"logoutFunction()\"><i class=\"fas fa-sign-out-alt\"></i>ออกจากระบบ</a>
                                </li>
                            </form>
                        ";
                    }
                } else {
                    echo "
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#registerGroupModal\" style=\"cursor:pointer\"><i class=\"fas fa-user-plus\"></i>สมัครสมาชิก</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#loginGroupModal\" style=\"cursor:pointer\"><i class=\"fas fa-sign-in-alt\"></i>เข้าสู่ระบบ</a>
                        </li>
                    ";
                }
            ?>
        </ul>
    </nav>
    
    <div style="padding-top: 5px; background-color: #D8D8D4;">
        <ul id="menu" align="center">
            <li>
                <a style="font-weight: bold;" href="#">ประเภทสินค้า 1</a>
            </li>
            <li>
                |
            </li>
            <li>
                <a style="font-weight: bold;" href="#">ประเภทสินค้า 2</a>
            </li>
            <li>
                |
            </li>
            <li>
                <a style="font-weight: bold;" href="#">ประเภทสินค้า 3</a>
            </li>
            <li>
                |
            </li>
            <li>
                <a style="font-weight: bold;" href="#">ประเภทสินค้า 4</a>
            </li>
        </ul>
    </div>

    <div align="center">
        <div id="rcorners2" style="margin-bottom:30px; padding-bottom:5px; padding-top:5px;">
            <div class="row">
                <div class="col-sm-1">
                    <input class="checkbox" type="checkbox" name="selectAll" value="" stlye="background-color:orange !Important;">
                </div>
                <div class="col-sm-4" align="left">
                    <label>สินค้าในตะกร้า</label>
                </div>
                <div class="col-sm-2" align="left">
                    <label>ราคาต่อชิ้น</label>
                </div>
                <div class="col-sm-1" align="left">
                    <label>จำนวน</label>
                </div>
                <div class="col-sm-2" align="center">
                    <label>ราคารวม</label>
                </div>
                <div class="col-sm-2" align="center">
                    <label>ลบสินค้า</label>
                </div>
            </div>
        </div>

        <div id="rcorners2" style="padding-bottom:10px; padding-top:5px;">
            <div class="row">
                <div class="col-sm-1" style="margin-top:25px;">
                    <input class="checkbox" type="checkbox" name="selectAll" value="" stlye="background-color:orange !Important;">
                </div>
                <div class="col-sm-4" align="left">
                    <img src="./icons/loginlogo.png" width="80px" height="80px" style="margin-right:20px;">
                    <label>สินค้าในตะกร้า</label>
                </div>
                <div class="col-sm-2" align="left" style="margin-top:25px;">
                    <label>2000 บาท</label>
                </div>
                <div class="col-sm-1" align="left" style="margin-top:25px;">
                    <label>10 กล่อง</label>
                </div>
                <div class="col-sm-2" align="center" style="margin-top:25px;">
                    <label>20,000 บาท</label>
                </div>
                <div class="col-sm-2" align="center" style="margin-top:25px;">
                    <a id="b"><i class="fas fa-times" style="font-size:30px; color:red; cursor:pointer;"></i></a>
                </div>
            </div>
        </div>
        <div id="rcorners2" style="padding-bottom:10px; padding-top:5px;">
            <div class="row">
                <div class="col-sm-1" style="margin-top:25px;">
                    <input class="checkbox" type="checkbox" name="selectAll" value="" stlye="background-color:orange !Important;">
                </div>
                <div class="col-sm-4" align="left">
                    <img src="./icons/loginlogo.png" width="80px" height="80px" style="margin-right:20px;">
                    <label>สินค้าในตะกร้า</label>
                </div>
                <div class="col-sm-2" align="left" style="margin-top:25px;">
                    <label>2000 บาท</label>
                </div>
                <div class="col-sm-1" align="left" style="margin-top:25px;">
                    <label>10 กล่อง</label>
                </div>
                <div class="col-sm-2" align="center" style="margin-top:25px;">
                    <label>20,000 บาท</label>
                </div>
                <div class="col-sm-2" align="center" style="margin-top:25px;">
                    <a id="b"><i class="fas fa-times" style="font-size:30px; color:red; cursor:pointer;"></i></a>
                </div>
            </div>
        </div>
        
        <div id="rcorners2" style="margin-top:50px; padding-bottom:5px; padding-top:5px;">
            <div class="row">
                <div class="col-sm-1">
                    <input class="checkbox" type="checkbox" name="selectAll" value="" stlye="background-color:orange !Important;">
                </div>
                <div class="col-sm-5" align="left">
                    <label>เลือกทั้งหมด</label>
                </div>
                <div class="col-sm-2" align="right">
                    <label>รวมค่าสินค้า&nbsp;&nbsp;&nbsp;(&nbsp;1&nbsp;&nbsp;&nbsp;สินค้า)</label>
                </div>
                <div class="col-sm-2" align="center">
                    <b style="font:bold; color:red;">40,000 บาท</b>
                </div>
                <div class="col-sm-2" align="left">
                    <a class="btn btn-warning" style="font-size:14px; color:black; width:180px;" >สั่งซื้อสินค้า</a>
                </div>
            </div>
        </div>
        
    </div>
    
  <!-- Modal ERROR-->
  <div class="modal fade" id="loginfail" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/wrong.png" width="150px" height="150px">
        <br><br>
        <p>ชื่อผู้ใช้หรือรหัสผ่านของคุณผิดพลาด</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

  <!-- Modal SUCCESS-->
  <div class="modal fade" id="loginsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p>เข้าสู่ระบบสำเร็จ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

      <!-- Modal LOGOUT-->
      <div class="modal fade" id="logoutsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/logout.png" width="150px" height="150px">
        <br><br>
        <p>กำลังออกจากระบบ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

</body>

<footer style="background-color: #575757;">
    <div class="container-fluid" style="margin-top: 245px;">
        <nav class="navbar navbar-expand-sm" style="display: flex;justify-content: center;">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <label style="color: #FFA200;font-size: 14px;">&copy;&nbsp;2020 localshopcenter.com</label>
                </li>
            </ul>
        </nav>
    </div>
</footer>
</html>