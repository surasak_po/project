<?php
    include "webservice/setting/Config.php";

    @session_start();
    @session_cache_expire(30);

    if(@$_SESSION['role'] == "admin"){
      echo "<meta  http-equiv='refresh' content='0;URL=1AdminCustomer.php'>";
  }
  if(@$_SESSION['role'] == "customer"){
      echo "<meta  http-equiv='refresh' content='0;URL=Index.php'>";
  }
  if(@$_SESSION['role'] == ""){
      echo "<meta  http-equiv='refresh' content='0;URL=Index.php'>";
  }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>จัดการข้อมูลร้านค้า</title>
    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="./css/sidebar.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="./css/styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }

        @font-face {
            font-family: 'Opun-Regular.ttf';
            src: url('fonts/Opun-Regular.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        .modalfont {
            font-family: 'Opun-Regular.ttf';
            src: url('fonts/Opun-Regular.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }

        body {
            font-family: 'Opun-Regular.ttf' !important;
        }

        .navbar {
            background-color: #575757;
            font-weight: bold;
            font-size: 18px;
            margin-bottom: 0px;
        }

        .navbar a {
            color: #FFA200;
            margin-top: -6px;
        }

        nav ul li a {
            font-size: 14px !important;
            text-decoration: underline;
        }

        #margin {
            margin-top: 10px;
            margin-left: 30px;
        }

        ul#menu li {
            display: inline;
            text-align: center;
            margin-left: 20px;
            margin-right: 20px;
            font-size: 18px;
            color: #575757;
        }

        ul#menu li a {
            color: #575757;
        }

        ul#menu li a:hover {
            color: #FFA200;
            text-decoration: none;
        }

        ul#label li {
            display: inline;
            text-align: center;
            margin-left: 20px;
            margin-right: 20px;
            font-size: 16px;
            color: #575757;
        }

        #hline {
            color: #FFA200 !important;
        }

        /* div a {
            font-size: 14px;
        }

        div a:hover {
            color: black;
            text-decoration: none;
        } */

        #bgblack {
            background-color: rgb(255, 94, 0);
        }

        .nav-link {
            text-decoration: none;
        }

        .nav-link:hover {
            color: black !important;
        }

        .nav-item {
            margin-top: 10px !important;
        }

        .row {
            padding-top: 10px;
        }

        .fas {
            padding-right: 5px;
            margin-right: 5px;
        }
   
    .hovertable:hover {
         background-color:#BDBDBD ; 
    }
    .hovertable{
        padding-top:7px;
        padding-bottom:7px;
        background-color:#858585;
    }
    

    .hovertableproductgroup:hover {
         background-color:#ACE7FF  ; 
    }
    .hovertableproductgroup{
        padding-top:7px;
        padding-bottom:7px;
        background-color:#2CC2FF;
    }
    .modal-header .close {
        display:none;
        }
    </style>
</head>

<script>

function pleaseinserttracking(){
    $("#pleaseinserttracking").modal({backdrop: 'static', keyboard: false})  
      $("#pleaseinserttracking").modal('show');
      setTimeout(function(){$('#pleaseinserttracking').modal('hide')},2000);
    }

    function deletesuccess(){
        $("#deletesuccess").modal({backdrop: 'static', keyboard: false})  
      $("#deletesuccess").modal('show');
      setTimeout(function(){$('#deletesuccess').modal('hide')},2000);
    }
    function alertexception() {
        $("#alertexception").modal({backdrop: 'static', keyboard: false})  
        $("#alertexception").modal('show');
        setTimeout(function(){$('#alertexception').modal('hide')},2000);
    }
 function updatesuccess(){
    $("#updatesuccess").modal({backdrop: 'static', keyboard: false})  
      $("#updatesuccess").modal('show');
      setTimeout(function(){$('#updatesuccess').modal('hide')},1000);
    }
    
    function trackingsuccess(){
    $("#trackingsuccess").modal({backdrop: 'static', keyboard: false})  
      $("#trackingsuccess").modal('show');
      setTimeout(function(){$('#trackingsuccess').modal('hide')},1000);
    }
    function trackingnotfound(){
    $("#trackingnotfound").modal({backdrop: 'static', keyboard: false})  
      $("#trackingnotfound").modal('show');
      setTimeout(function(){$('#trackingnotfound').modal('hide')},2000);
    }
  function logoutsuccess(){
    $("#logoutsuccess").modal({backdrop: 'static', keyboard: false})  
      $("#logoutsuccess").modal('show');
      setTimeout(function(){$('#logoutsuccess').modal('hide')},2000);
    }

    function insertsuccess(){
        $("#insertsuccess").modal({backdrop: 'static', keyboard: false})  
      $("#insertsuccess").modal('show');
      setTimeout(function(){$('#insertsuccess').modal('hide')},1000);
    }
    
function encodeImageFileAsURL(element) {
  var file = element.files[0];
  var reader = new FileReader();
  reader.onloadend = function() {
   // console.log(reader.result);
  }
  reader.readAsDataURL(file);

  return reader;
}
    function cancelUpdateStoreInfo() {
      location.reload();
    }

    function logoutFunction() {
           logoutsuccess();
                   setTimeout(function(){ 
                    document.forms["formLogout"].action = "webservice/Logout.php";
                    document.forms["formLogout"].submit();
                   },2600);
    }
</script>

<body>
    <nav class="navbar navbar-expand-sm">
        <a class="navbar-brand" style="font-family:'KRR_AengAei.ttf'!important; color: #FFA200;font-size:35px;">
            <i class="fas fa-dolly"></i>ผลิตภัณฑ์ชุมชน
        </a>
        <ul class="navbar-nav mr-auto"></ul>
        <ul class="navbar-nav">
            <?php
                echo "
                    <form id=\"formLogout\" name=\"formLogout\">
                        <a class=\"nav-link\" style=\"cursor:pointer\" onclick=\"logoutFunction()\"><i class=\"fas fa-sign-out-alt\"></i>ออกจากระบบ</a>
                    </form>
                ";
            ?>
        </ul>
    </nav>

    <!-- Sidebar  -->
    <div class="wrapper">
        
        <nav id="sidebar">
            <div class="sidebar-header">
                <h4>หน้าจัดการร้านค้า</h4>
                <strong>BS</strong>
            </div>

            <ul class="list-unstyled components">
                <li >
                    <a href="Store.php">
                        <i class="fas fa-briefcase"></i>
                        ข้อมูลร้านค้า
                    </a>
                </li>
           
                <li >
                    <a href="StoreProductView.php">
                        <i class="fas fa-box"></i>
                        เพิ่มสินค้า
                    </a>
                </li>
<li>
                    <a href="StoreMyproduct.php">
                        <i class="fas fa-box"></i>
                      สินค้าของฉัน
                    </a>
                </li>

                <li >
                    <a href="StoreTransportView.php">
                        <i class="fas fa-box"></i>
                        จัดการช่องทางการจัดส่ง
                    </a>
                </li>

                <li class="active">
                    <a href="StoreOrderview.php">
                        <i class="fas fa-box"></i>
                       ส่งสินค้า
                        <span id="badge_order_num" class="badge badge-light"></span>
                    </a>
                </li>
                <li>
                    <a href="StoreSell_list.php">
                        <i class="fas fa-box"></i>
                        รายรับของฉัน
                        <span id="badge_order_num" class="badge badge-light"></span>
                    </a>
                </li>
                <li >
                <a href="StoreWithdraw.php">
                        <i class="fas fa-box"></i>
                        ถอนเงิน
                        <span id="badge_order_num" class="badge badge-light"></span>
                    </a>
                </li>


                
            </ul>
        </nav>

        <script>

setInterval(function(){ 
    var store_code ="<?php echo @$_SESSION['store_code'];?>";

            if(store_code!=""){ //ถ้า login
                $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: 'application/json',
                        async : false,
                        url: "../restapi/getordernumstore.php",
                        data:JSON.stringify({
                            store_code:store_code
                        }),
                        success: function (response) {
                        var json_data = response;
                        var getdata = json_data;   
                document.getElementById("badge_order_num").innerHTML = getdata.ordernum;
                        }
                    }); 
                }
        }, 500);
        </script>

        <!-- Page Content  -->
        <div id="content">

            <div class="container-fluid" style="background-color:#DDDDDD;padding-top:10px">

            <div class="container" style="background-color:White;padding-top:10px">
          
            <div class="container">
                <div id="rcorners">
<!-- เริ่ม -->
<?php 
$strgetorder="SELECT * FROM tbl_order
INNER JOIN tbl_store ON tbl_order.store_code = tbl_store.store_code
INNER JOIN tbl_shipping ON tbl_order.shipping_code = tbl_shipping.shipping_code
INNER JOIN tbl_transport ON tbl_shipping.transport_code = tbl_transport.transport_code
INNER JOIN tbl_customer_address ON tbl_order.customer_address_code = tbl_customer_address.customer_address_code
WHERE tbl_order.store_code = '".$_SESSION['store_code']."' AND order_status = '1'
 ";
     $resultstrgetorder = @$conn->query($strgetorder);
     if(@$resultstrgetorder->num_rows >0){
         while($rowstrgetorder = $resultstrgetorder->fetch_assoc()){

            $order_code =$rowstrgetorder['order_code'];
         
?>
<!-- แบ่งหัว -->
<div class="row">
<br><br>
        <div class="col-sm-12" style="background-color:#C4C4C4;"></div >
</div >
<!-- แบ่งหัว -->
                    <div class="row">
                        <div class="col-sm-4">
                            <img src="image/alert.png" style="margin-left:7px;" width="20px" height="20px">
                            <label style="font-size:14px; margin-left:7px;">เลขใบสั่งซื้อ : <span style="font-weight:bold;"><?php echo $rowstrgetorder['order_code'];?></span></label>
                        </div>
                        <div class="col-sm-4" align="left" >

                        </div>
                        <div class="col-sm-4" align="right" >
                        การจัดส่ง : <span style="font-weight:bold;"><?php echo $rowstrgetorder['transport_name']." ค่าจัดส่ง  ฿".$rowstrgetorder['order_shipping_price'] ?></span>
                        </div>
                    </div>
                    <div class="row" style="margin-top:-10px; margin-bottom:-10px;">
                        <hr width=90% color=#F2F2F2>
                    </div>

<!-- productloop -->
<?php 
$totalprice = $rowstrgetorder['order_shipping_price'];
$strgetorderdetail="SELECT * FROM tbl_order_detail 
INNER JOIN tbl_product_group ON tbl_order_detail.product_group_code = tbl_product_group.product_group_code
INNER JOIN tbl_product ON tbl_product_group.product_code = tbl_product.product_code
WHERE order_code = '$order_code'  ";
     $resultstrgetorderdetail = @$conn->query($strgetorderdetail);
     if(@$resultstrgetorderdetail->num_rows >0){
         while($rowstrgetorderdetail = $resultstrgetorderdetail->fetch_assoc()){
?>
                    <div class="row">
                        <div class="col-sm-2" align="right">
                            <img src="../restapi/product/<?php echo $rowstrgetorderdetail['product_group_picture']; ?>" style="margin-left:50px;" width="120px" height="120px">
                        </div>
                        <div class="col-sm-6" align="left" style="margin-left:20px;">
                            <label style="font-size:20px">ชื่อสินค้า : <?php echo $rowstrgetorderdetail['product_name']; ?></label><br>
                            <label style="font-size:12px">ตัวเลือก : <?php echo $rowstrgetorderdetail['product_group_name']; ?></label><br>
                            <label style="font-size:12px">จำนวน : <?php echo $rowstrgetorderdetail['order_detail_num']." ".$rowstrgetorderdetail['product_group_unit_name']; ?> </label>
                        </div>
                        <div class="col-sm-3" align="right">
                            <label> ฿ <?php 
                            $totalprice = $totalprice + ($rowstrgetorderdetail['order_detail_price']*$rowstrgetorderdetail['order_detail_num']);
                            echo $rowstrgetorderdetail['order_detail_price']*$rowstrgetorderdetail['order_detail_num']; ?></label><span>.00</span>
                        </div>
                    </div>
        
         <?php }}?>
<!-- productloop -->

                <div id="rcorners2">
                    <div class="row">
                        <div class="col-sm-12" align="right">
                            <label style="font-size:14px;">ยอดรวมคำสั่งซื้อทั้งหมด&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                            <label style="font-size:24px; color:orange;">฿ <?php echo $totalprice?></label><span style="font-size:24px; color:orange;">.00</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4" align="left">
                        <label style="font-size:14px;margin-left:15px;">
<!--address  -->
<b style="font-size:16px;">ที่อยู่ในการจัดส่ง :</b><br>
<div style="margin-top:8px;">
                        <?php echo "ชื่อ :".$rowstrgetorder['customer_address_fullname']." เบอร์โทรศัพท์ :".$rowstrgetorder['customer_address_tel']."<br>" ; ?>
                        <?php echo $rowstrgetorder['customer_address_detail']." ตำบล :".$rowstrgetorder['customer_address_subdistrict']
                    ." อำเภอ :".$rowstrgetorder['customer_address_district']." จังหวัด :".$rowstrgetorder['customer_address_province']
                    ." รหัสไปรษณีย์ :".$rowstrgetorder['customer_address_postcode'] ; ?>
</div>
<!--  -->
                        </label>
                        </div>
                        <div class="col-sm-6" align="right">
                            <?php if($rowstrgetorder['order_status']=="1"){ ?>
                            <a  class="btn btn-lg btn-warning " style="font-size:13px;"
                            transport_name ="<?php echo $rowstrgetorder['transport_name']; ?>"
                            order_code ="<?php echo $rowstrgetorder['order_code']; ?>"
                            onclick="inserttracking(this)"
                            >TRACKING</a>
                            <?php } ?>
                        </div>
                        <div class="col-sm-2" align="center">
                        <input type="text" id="inserttracking<?php echo $rowstrgetorder['order_code']; ?>" size="18">
                        </div>
                    </div>
                </div>
         <?php }} ?>

<script>
function inserttracking(obj){
    var order_code = obj.getAttribute("order_code");
    var transport_name = obj.getAttribute("transport_name");
    var inserttracking =document.getElementById("inserttracking"+order_code).value.trim();
    
    console.log(transport_name);

    if(inserttracking==""){
                    pleaseinserttracking();
                    setTimeout(function(){ 
                    },2600);
    }else{
                    $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    async : false,
                    url: "../restapi/1updateordertracking.php",
                    data:JSON.stringify({
                        order_code:order_code,
                        transport_name:transport_name,
                        inserttracking:inserttracking,
                    }),
                    success: function (response) {
                    var json_data = response;
                    console.log(json_data.result);
                        if(json_data.result=="Success"){
                            trackingsuccess();
                            setTimeout(function(){ 
                            location.reload();
                            },1600);
                        }else{
                            trackingnotfound();
                            setTimeout(function(){ 
                            location.reload();
                            },2600);
                        }
                        }
                });
    }
}
</script>
<!-- แบ่งท้าย -->
<div class="row">
<br><br>
        <div class="col-sm-12" style="background-color:#C4C4C4;"></div >
</div >
<!-- แบ่งท้าย -->
<!-- สุดสิ้นลูป -->
<br><br>
            </div>
        </div>
        <!-- ปิด -->
             
            </div>
            <br><br>
        </div>
        </div>
        <!-- END Content  -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
    <!-- End of Sidebar  -->

            <!-- Modal LOGOUT-->
            <div class="modal fade" id="logoutsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/logout.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;" class="modalfont">กำลังออกจากระบบ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

      <!-- Modal UPDATESUCCESS-->
      <div class="modal fade" id="updatesuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p style="color:black" class="modalfont">แก้ไขข้อมูลเรียบร้อย</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

           <!-- Modal alertexception-->
           <div class="modal fade" id="alertexception" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p style="color:black" class="modalfont">เกิดข้อผิดพลาดกรุณาลองอีกครั้ง</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

 <!-- Modal AddataSUCCESS-->
 <div class="modal fade" id="insertsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <span style="color:black" class="modalfont">เพิ่มข้อมูลสำเร็จ</span><span id="insertsuccesshtml"></span>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->


            <!-- Modal DELETESUCCESS-->
 <div class="modal fade" id="deletesuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p style="color:black" class="modalfont">ลบข้อมูลสำเร็จ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->
           <!-- Modal DUPLICATE_TRANSPOT-->
           <div class="modal fade" id="duplicateshipping" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p style="color:black" class="modalfont">ขนส่งที่ท่านเลือก ท่านได้เลือกก่อนหน้านี้แล้ว</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

    <!-- Modal -->
           <!-- Modal choosetransport-->
           <div class="modal fade" id="choosetransport" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p style="color:black" class="modalfont">กรุณาเลือกขนส่ง</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modalchoosetransport -->

</script>
           <!-- Modal pleaseinserttracking-->
           <div class="modal fade" id="pleaseinserttracking" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p style="color:black" class="modalfont">กรุณากรอกหมายเลข Tracking ให้ถูกต้องค่ะ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

        <!-- Modal UPDATESUCCESS-->
        <div class="modal fade" id="trackingsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p style="color:black" class="modalfont">TRACKING เรียบร้อย</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

<!-- Modal UPDATESUCCESS-->
        <div class="modal fade" id="trackingnotfound" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p style="color:black" class="modalfont">หมายเลข TRACKING ของคุณไม่ถูกต้อง</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->
</body>

</html>