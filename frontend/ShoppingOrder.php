<?php
    include "../restapi/setting/config.php";
    @session_start();
    @session_cache_expire(30);

    if(@$_SESSION['role'] = "customer"){
        
    }else if(@$_SESSION['role'] = "admin"){
        echo "<meta  http-equiv='refresh' content='0;URL=1AdminCustomer.php'>";
    }else if(@$_SESSION['role'] = "store"){
        echo "<meta  http-equiv='refresh' content='0;URL=Store.php'>";
    }
    else{
        echo "<meta  http-equiv='refresh' content='0;URL=Index.php'>";
    }
    
    $checkcustomeraddress = "SELECT * FROM tbl_customer_address WHERE customer_code = '".$_SESSION['customer_code']."'  ";
    $resultcheckcustomeraddress = @$conn->query($checkcustomeraddress);
    if($resultcheckcustomeraddress->num_rows > 0){
        $cus_address_have="1";
    }else{
        $cus_address_have="0";
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>สั่งซื้อ</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="./css/styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.omise.co/omise.js"></script>

    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }

        .card {
            background-color: #F88360;  
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }
        .modal-header .close {
        display:none;
        }
    </style>
</head>

<script>
   /* setTimeout(function(){ 
    var customer_code ="<?php // echo @$_SESSION['customer_code'];?>";

        $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/getcartnum.php",
                data:JSON.stringify({
                    customer_code:customer_code
                }),
                success: function (response) {
                var json_data = response;
                var getdata = json_data;   
                 if(getdata.cartnum=="0"){
                    location.replace("index.php");
                 }
                }
            }); 

         
}, 100);*/

function ordersuccessbycreditcard() {
    $("#ordersuccessbycreditcard").modal({backdrop: 'static', keyboard: false})  
        $("#ordersuccessbycreditcard").modal('show');
        setTimeout(function(){$('#ordersuccessbycreditcard').modal('hide')},3000);
    }
function Failedpayment() {
    $("#Failedpayment").modal({backdrop: 'static', keyboard: false})  
        $("#Failedpayment").modal('show');
        setTimeout(function(){$('#Failedpayment').modal('hide')},3000);
    }
function ordersuccess() {
    $("#ordersuccess").modal({backdrop: 'static', keyboard: false})  
        $("#ordersuccess").modal('show');
        setTimeout(function(){$('#ordersuccess').modal('hide')},3000);
    }
function wanttodelete() {
    $("#wanttodelete").modal({backdrop: 'static', keyboard: false})  
        $("#wanttodelete").modal('show');
    }
function Numnotenough() {
    $("#Numnotenough").modal({backdrop: 'static', keyboard: false})  
        $("#Numnotenough").modal('show');
        setTimeout(function(){$('#Numnotenough').modal('hide')},2000);
    }
function hasbanned() {
    $("#hasbanned").modal({backdrop: 'static', keyboard: false})  
        $("#hasbanned").modal('show');
        setTimeout(function(){$('#hasbanned').modal('hide')},3000);
    }
    function loginfail() {
        $("#loginfail").modal({backdrop: 'static', keyboard: false})  
        $("#loginfail").modal('show');
        setTimeout(function(){$('#loginfail').modal('hide')},3000);
    }

    function loginsuccess() {
        $("#loginsuccess").modal({backdrop: 'static', keyboard: false})  
        $("#loginsuccess").modal('show');
        setTimeout(function(){$('#loginsuccess').modal('hide')},3000);
    }

    function logoutsuccess() {
        $("#logoutsuccess").modal({backdrop: 'static', keyboard: false})  
        $("#logoutsuccess").modal('show');
        setTimeout(function(){$('#logoutsuccess').modal('hide')},2000);
    }
    
    function customerLoginFunction() {
        var customerUsername = document.getElementById("customerUsername").value;
        var customerPassword = document.getElementById("customerPassword").value;

        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/logincustomer.php",
            data:JSON.stringify({
                customerUsername:customerUsername,
                customerPassword:customerPassword
            }),
            success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;

                if(apistatus=="Success") {
                loginsuccess();
                setTimeout(function(){ 
                    location.reload();
                },3600);
                } else if(apistatus=="Banned"){
                    
                    hasbanned();
                    setTimeout(function(){ 
                    location.reload();
                    },3600);
                } else{
                    loginfail();
                    document.getElementById("adminUsername").value=""
                    document.getElementById("adminPassword").value=""
                }
            }
        });
    }

    function storeLoginFunction() {
        var storeUsername = document.getElementById("storeUsername").value;
        var storePassword = document.getElementById("storePassword").value;

        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/LoginStore.php",
            data:JSON.stringify({
                storeUsername:storeUsername,
                storePassword:storePassword
            }),
            success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;
                if(apistatus=="Success") {
                loginsuccess();
                setTimeout(function(){ 
                    location.replace("Store.php");
                },3600);
                } else if(apistatus=="Banned"){
                    hasbanned();
                    setTimeout(function(){ 
                    location.reload();
                    },3600);
                } else{
                    loginfail();
                    document.getElementById("adminUsername").value=""
                    document.getElementById("adminPassword").value=""
                }
            }
        });
    }

    function adminLoginFunction() {
        var adminUsername = document.getElementById("adminUsername").value;
        var adminPassword = document.getElementById("adminPassword").value;
    
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/LoginAdmin.php",
            data:JSON.stringify({
                adminUsername:adminUsername,
                adminPassword:adminPassword
            }),
            success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;
                console.log(apistatus);

                if(apistatus=="Success") {
                loginsuccess();
                setTimeout(function(){ 
                    location.replace("Admin.php");
                },3600);
                } else if(apistatus=="Banned"){
                    hasbanned();
                    setTimeout(function(){ 
                    location.reload();
                    },3600);
                } else{
                    loginfail();
                    document.getElementById("adminUsername").value=""
                    document.getElementById("adminPassword").value=""
                }

                
            }
        });
    }
    setTimeout(function(){ 
    var customer_code ="<?php echo @$_SESSION['customer_code'];?>";

        $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/getcartnum.php",
                data:JSON.stringify({
                    customer_code:customer_code
                }),
                success: function (response) {
                var json_data = response;
                var getdata = json_data;   
              
           
                 if(getdata.cartnum=="0"){
                    location.replace("index.php");
                 }
                }
            }); 

         
}, 100);
    function logoutFunction() {
        logoutsuccess();
            setTimeout(function(){ 
            document.forms["formLogout"].action = "../restapi/logout.php";
            document.forms["formLogout"].submit();
        },2600);
    }
</script>

<body>



    <!-- Modal Zone -->
    <!-- 1. Register Group Modal -->
    <div class="modal" id="registerGroupModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เลือกประเภทการสมัครสมาชิก</h2>
            </div>

            <!-- Modal body -->
                <div class="modal-body">
                    <div row align="center" style="margin-bottom: -25px;">
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 50px;">
                            <p style="font-size: 60px; color: #17a2b8; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-users"></i></p>
                        </div>
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 10px;">
                            <p style="font-size: 60px; color: #007bff; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-store"></i></p>
                        </div>
                    </div>
                    <div row align="center">
                        <div class="col-sm-5 d-inline-flex">
                            <a class="btn btn-info" type="button" href="CustomerRegister.php">สำหรับลูกค้า</a>
                        </div>
                        <div class="col-sm-5 d-inline-flex">
                            
                        <a class="btn btn-primary" type="button" href="StoreRegister.php">สำหรับร้านค้า</a>
                        </div>
                    </div>
                </div>

            <!-- Modal footer -->
            <br>
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-danger" type="button" data-dismiss="modal">ปิดหน้านี้</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 1. End of Register Group Modal -->

    <!-- 2. Login Group Modal -->
    <div class="modal" id="loginGroupModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เลือกประเภทการเข้าสู่ระบบ</h2>
            </div>

            <!-- Modal body -->
                <div class="modal-body">
                    <div row align="center" style="margin-bottom: -25px;">
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 50px;">
                            <p style="font-size: 60px; color: #17a2b8; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-users"></i></p>
                        </div>
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 10px;">
                            <p style="font-size: 60px; color: #007bff; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-store"></i></p>
                        </div>
                    </div>
                    <div row align="center" style="margin-bottom: 30px;">
                        <div class="col-sm-5 d-inline-flex">
                            <button data-toggle="modal" data-target="#loginCustomerModal" class="btn btn-info" type="button" data-dismiss="modal">สำหรับลูกค้า</button>
                        </div>
                        <div class="col-sm-5 d-inline-flex">
                            <button data-toggle="modal" data-target="#loginStoreModal" class="btn btn-primary" type="button" data-dismiss="modal">สำหรับร้านค้า</button>
                        </div>
                    </div>
                    <div row align="center" style="margin-bottom: -25px;">
                            <p style="font-size: 60px; color: #343a40; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-crown"></i></p>
                    </div>
                    <div row align="center">
                        <button data-toggle="modal" data-target="#loginAdminModal" class="btn btn-dark" type="button" data-dismiss="modal">สำหรับแอดมิน</button>
                    </div>
                </div>

            <!-- Modal footer -->
            <br>
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-danger" type="button" data-dismiss="modal">ปิดหน้านี้</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2. End of Login Group Modal -->

    <!-- 2.1 Login Customer Modal -->
    <div class="modal" id="loginCustomerModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เข้าสู่ระบบ (สำหรับลูกค้า)</h2>
            </div>

            <!-- Modal body -->
            <form id="formLoginCustomer" name="formLoginCustomer" method="POST">
                <div class="modal-body">
                    <img src="./icons/loginlogo.png" width="150px" height="150px">
                    <div class="form-group col-sm-9">
                        <label for="customerUsername" style="font-weight: regular;">ชื่อผู้ใช้</label>
                        <input type="text" placeholder="เบอร์โทรศัพท์หรืออีเมลล์" class="form-control" id="customerUsername" name="customerUsername">
                    </div>
                    <div class="form-group col-sm-9">
                        <label for="customerPassword" style="font-weight: regular;">รหัสผ่าน</label>
                        <input type="password" placeholder="รหัสผ่าน" class="form-control" id="customerPassword" name="customerPassword">
                    </div>
                </div>

            <!-- Modal footer -->
            <br>
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-success" type="button" onclick="customerLoginFunction()">เข้าสู่ระบบ</button>
                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#loginGroupModal" data-dismiss="modal">ยกเลิก</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2.1 End of Login Customer Modal -->

    <!-- 2.2 Login Store Modal -->
    <div class="modal" id="loginStoreModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เข้าสู่ระบบ (สำหรับร้านค้า)</h2>
            </div>

            <!-- Modal body -->
            <form id="formLoginStore" name="formLoginStore" method="POST">
                <div class="modal-body">
                    <img src="./icons/loginlogo.png" width="150px" height="150px">
                    <div class="form-group col-sm-9">
                        <label for="storeUsername" style="font-weight: regular;">ชื่อผู้ใช้</label>
                        <input type="text" placeholder="เบอร์โทรศัพท์หรืออีเมลล์" class="form-control" id="storeUsername" name="storeUsername">
                    </div>
                    <div class="form-group col-sm-9">
                        <label for="storePassword" style="font-weight: regular;">รหัสผ่าน</label>
                        <input type="password" placeholder="รหัสผ่าน" class="form-control" id="storePassword" name="storePassword">
                    </div>
                </div>

            <!-- Modal footer -->
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-success" type="button" onclick="storeLoginFunction()">เข้าสู่ระบบ</button>
                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#loginGroupModal" data-dismiss="modal">ยกเลิก</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2.2 End of Login Store Modal -->

    <!-- 2.3 Admin Login Modal -->
    <div class="modal" id="loginAdminModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">
                
            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เข้าสู่ระบบ (สำหรับผู้ดูแลระบบ)</h2>
            </div>
            <form id="formAdminLogin" name="formAdminLogin" method="POST">

            <!-- Modal body -->
                <div class="modal-body">
                    <img src="./icons/loginlogo.png" width="150px" height="150px">
                    <div class="form-group col-sm-9">
                        <label for="adminUsername" style="font-weight: regular;">ชื่อผู้ใช้</label>
                        <input type="text" placeholder="ชื่อผู้ใช้งานผู้ดูแล" class="form-control" id="adminUsername" name="adminUsername">
                    </div>
                    <div class="form-group col-sm-9">
                        <label for="adminPassword" style="font-weight: regular;">รหัสผ่าน</label>
                        <input type="password" placeholder="รหัสผ่าน" class="form-control" id="adminPassword" name="adminPassword">
                    </div>
                </div>

            <!-- Modal footer -->
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-success" type="button" onclick="adminLoginFunction()">เข้าสู่ระบบ</button>
                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#loginGroupModal" data-dismiss="modal">ยกเลิก</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2.3 End of Admin Login Modal -->
    <!-- End of Modal Zone -->

    <nav class="navbar navbar-expand-sm">
        <a class="navbar-brand" style="font-family:'KRR_AengAei.ttf'!important; color: #FFA200;font-size:35px;" href="index.php">
            <i class="fas fa-dolly"></i>ผลิตภัณฑ์ชุมชน
        </a>
        <ul class="navbar-nav mr-auto"></ul>
        <ul class="navbar-nav">
            <?php
                if (@$_SESSION['role'] == "customer") {
                    $strSQL = "SELECT * FROM tbl_customer WHERE customer_code = '".$_SESSION['customer_code']."' ";
                    $result = @$conn->query($strSQL);
                    while($row = $result->fetch_assoc()){
                        echo "
                            <li class=\"nav-item\">
                               <a href=\"CustomerInfoNewForm.php\" class=\"nav-link\"><i class=\"fas fa-user\"></i>ข้อมูลส่วนตัว</a>
                            </li>
                            <li class=\"nav-item\">
                                <a style=\"color: #FFA200 !important;,font-size: 14px !important;\" class=\"nav-link\">ยินดีต้อนรับ&nbspคุณ&nbsp :&nbsp " . @$row['customer_fullname'] . "</a>
                            </li>
                            <form id=\"formLogout\" name=\"formLogout\">
                                <li class=\"nav-item\">
                                    <a class=\"nav-link\" style=\"cursor:pointer\" onclick=\"logoutFunction()\"><i class=\"fas fa-sign-out-alt\"></i>ออกจากระบบ</a>
                                </li>
                            </form>
                           
                        ";
                    }
                } else {
                    echo "
              
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#registerGroupModal\" style=\"cursor:pointer\"><i class=\"fas fa-user-plus\"></i>สมัครสมาชิก</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#loginGroupModal\" style=\"cursor:pointer\"><i class=\"fas fa-sign-in-alt\"></i>เข้าสู่ระบบ</a>
                        </li>
                      
                    ";
                }
            ?>
        </ul>
    </nav>
    <script>
function opencart(){
    var customer_code ="<?php echo @$_SESSION['customer_code'];?>";

    if(customer_code==""){
            console.log("ไม่ล้อคอิน");
    }else{
        console.log("ไปหน้าตะกร้า");
        location.replace("index.php");
    }

}
    </script>
   <!-- BODY -->
  

    <div class="row">
            <div class="col-md-1"></div>

            <div class="col-md-10">
   <div class="jumbotron">

   <div align="center">
        <div  style="margin-bottom:30px; padding-bottom:5px; padding-top:5px;">
            <div class="row" style="background-color:white;font-weight:bold;font-size:20px;" >
      
            <div class="col-sm-12" align="left" style="color:#FF6D14"> 
                    <label><i class="fas fa-map-marker-alt"></i>ที่อยู่ในการจัดส่ง</label>
                    <br> <br>
            </div>
         

                <?php 
                     $querycustomeraddress = "SELECT * FROM tbl_customer_address WHERE customer_code = '".$_SESSION['customer_code']."' AND customer_address_status = '1' ";
                     $resultquerycustomeraddress = @$conn->query($querycustomeraddress);
                     if($resultquerycustomeraddress->num_rows > 0){
                     while($rowquerycustomeraddress = $resultquerycustomeraddress->fetch_assoc()){
                ?>
                   <div class="col-sm-4" align="left" style="background-color:white;font-weight:bold;font-size:16px;">
                   <input type="text" class="form-control" id="input_customer_address_code" hidden value="<?php echo $rowquerycustomeraddress['customer_address_code']; ?>">
                    <label>
                    <?php echo "ชื่อ :".$rowquerycustomeraddress['customer_address_fullname']." เบอร์โทรศัพท์ :".$rowquerycustomeraddress['customer_address_tel'] ; ?>
                    <br> <br>
                    </label>
                    </div>
                    <div class="col-sm-6" align="left" style="background-color:white;font-weight:normal;font-size:14px;">
                    <label>
                    <?php echo $rowquerycustomeraddress['customer_address_detail']." ตำบล :".$rowquerycustomeraddress['customer_address_subdistrict']
                    ." อำเภอ :".$rowquerycustomeraddress['customer_address_district']." จังหวัด :".$rowquerycustomeraddress['customer_address_province']
                    ." รหัสไปรษณีย์ :".$rowquerycustomeraddress['customer_address_postcode'] ; ?>
                    <br> <br>
                    </label>
                    </div>
                    <div class="col-sm-2" align="center" style="background-color:white;font-weight:normal;font-size:20px;color:blue;">
                    <label onclick="changeaddress()" style=" cursor: pointer;">
                    
                    เปลี่ยนที่อยู่จัดส่ง
                    </label>
                    </div>
                 <?php }}else{ ?>
              
                 <div class="col-sm-10" align="left" style="background-color:white;font-weight:bold;font-size:16px;">
                
                </div>
                  
                <div class="col-sm-2" align="center" style="background-color:white;font-weight:normal;font-size:20px;color:blue;">
                <span class="nav-link" data-toggle="modal"  data-target="#customeraddaddress" style="cursor:pointer;color:#2DFF1C;font-size:20px;"  onmouseover="this.style.color='black'"  onmouseout="this.style.color='#2DFF1C'">เพิ่มสถานที่จัดส่ง</span>
                    </div>
                 <?php }?>
            </div>
        </div>
    </div>
    
      <script>
      function changeaddress(){
        $("#changeaddress").modal({backdrop: 'static', keyboard: false})  ;
        $("#changeaddress").modal('show');
      }
      </script>
<script>
function promptpay1() {
    $("#promptpay1").modal({backdrop: 'static', keyboard: false})  ;
        $("#promptpay1").modal('show');
        setTimeout(function(){$('#promptpay1').modal('hide')},100000);
    }
</script>

   <div align="center">
        <div id="rcorners2" style="margin-bottom:30px; padding-bottom:5px; padding-top:5px;">
            <div class="row" style="background-color:white;font-weight:bold;font-size:20px;" >
                <div class="col-sm-1">
                    <!-- <input class="checkbox" type="checkbox" name="selectAll" value="" stlye="background-color:orange !Important;"> -->
                </div>
                <div class="col-sm-4" align="left">
                    <label>สั่งซื้อสินค้าแล้ว</label>
                </div>
                <div class="col-sm-2" align="left">
                    <label>ราคาต่อชิ้น</label>
                </div>
                <div class="col-sm-2" align="center">
                    <label>จำนวน</label>
                </div>
                <div class="col-sm-3" align="center">
                    <label>ราคาย่อย</label>
                </div>
               
            </div>
        </div>


 <?php 
 
$totalpricecart=0;
$getstore="SELECT * FROM tbl_cart
INNER JOIN tbl_store ON tbl_cart.store_code = tbl_store.store_code 
INNER JOIN tbl_shipping ON tbl_cart.shipping_code = tbl_shipping.shipping_code
INNER JOIN tbl_transport ON tbl_shipping.transport_code = tbl_transport.transport_code
WHERE customer_code ='".$_SESSION['customer_code']."' ORDER BY cart_code ASC";
  $resultgetstore = @$conn->query($getstore);
  if(@$resultgetstore->num_rows >0){
      while($rowgetstore = $resultgetstore->fetch_assoc()){

 
?> 
<div> 
           <?php $getcartdetail="SELECT * FROM tbl_cart_detail 
            WHERE cart_code ='".$rowgetstore['cart_code']."' AND cart_detail_status='1'";
            $resultgetcartdetail = @$conn->query($getcartdetail);
            if(@$resultgetcartdetail->num_rows >0){ ?>

        <div id="rcorners2" style="padding-bottom:10px; padding-top:5px;">
            <div class="row" style="background-color:#464646;color:white">
                <div class="col-sm-1" >
        
                </div>
                <div class="col-sm-4" align="left" >
                    <label>ร้าน : <?php echo $rowgetstore['store_namestore']; ?></label>
                </div>
            </div>
        </div>
        
            <?php }?>
        <?php
            
            $getcartdetail="SELECT * FROM tbl_cart_detail INNER JOIN tbl_product_group ON tbl_cart_detail.product_group_code = tbl_product_group.product_group_code  
            INNER JOIN tbl_product ON tbl_product_group.product_code = tbl_product.product_code  
            WHERE cart_code ='".$rowgetstore['cart_code']."' AND cart_detail_status='1' ";
            $resultgetcartdetail = @$conn->query($getcartdetail);
            if(@$resultgetcartdetail->num_rows >0){
                
                while($rowgetcartdetail = $resultgetcartdetail->fetch_assoc()){
                 
        ?>
        <!-- bodycart -->
        <div id="rcorners2" style="padding-bottom:10px; padding-top:5px;">
            <div class="row" style="background-color:white">
                <div class="col-sm-1" style="margin-top:25px;">
                </div>
<script>
function updatecartdetailstatus(obj){
    var cart_detail_code = obj.getAttribute("cart_detail_code");
    var cart_detail_status = obj.getAttribute("cart_detail_status");

    console.log(cart_detail_status);
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/updatecartdetailstatus.php",
            data:JSON.stringify({
            cart_detail_code:cart_detail_code,
            cart_detail_status:cart_detail_status
            }),
            success: function (response) {
            var json_data = response;
            var getdata = json_data;
            if(getdata.result=="Success"){
                location.reload();
            }
            }
        });

}
</script>
                <div class="col-sm-4" align="left">
                    <img src="../restapi/product/<?php  echo $rowgetcartdetail['product_group_picture']; ?>" width="80px" height="80px" style="margin-right:20px;">
                    <label style="font-weight:bold;font-size:20px;"><?php echo $rowgetcartdetail['product_name'];?></label>
                    <label>ตัวเลือก <?php echo $rowgetcartdetail['product_group_name'];?></label>
                </div>
                <div class="col-sm-2" align="left" style="margin-top:25px;">
                    <label>฿ <span><?php  echo $rowgetcartdetail['product_group_price']; ?></span></label>
                </div>
                <div class="col-sm-2" align="center" style="margin-top:25px;">
        
    
          <input readonly type="text" id="cart_detail_num" name="cart_detail_num" class="form-control input-number" value="<?php  echo $rowgetcartdetail['cart_detail_num']; ?>" style="text-align:center">
                </div>
                <div class="col-sm-3" align="center" style="margin-top:25px;font-weight:bold;">
                <label>฿ <span><?php
        $totalpricecartsumtransport = 0;
        $gettotalprice="SELECT * FROM tbl_cart_detail 
        INNER JOIN tbl_product_group ON tbl_cart_detail.product_group_code = tbl_product_group.product_group_code  
        INNER JOIN tbl_product ON tbl_product_group.product_code = tbl_product.product_code  
        WHERE cart_code ='".$rowgetstore['cart_code']."' AND cart_detail_status='1' ";
        $resultgettotalprice = @$conn->query($gettotalprice);
        if(@$resultgettotalprice->num_rows >0){
            while($rowgettotalprice = $resultgettotalprice->fetch_assoc()){
                
                $totalpricecartsumtransport = $totalpricecartsumtransport + $rowgettotalprice['product_group_price']*$rowgettotalprice['cart_detail_num'];
            }}
                echo $rowgetcartdetail['product_group_price']* $rowgetcartdetail['cart_detail_num']; ?>
                
                </span><span>.00</span></label>
                
                </div>
                


            </div>
            
        </div>
        <script>
        function deletecartdetail(obj){
            var cart_detail_code=obj.getAttribute("cart_detail_code");
            var product_name = obj.getAttribute("product_name");
            var product_group_name = obj.getAttribute("product_group_name");
            document.getElementById("wanttodeletedetailid").value=cart_detail_code;
            document.getElementById("wanttodeletedetail").innerHTML=" ชื่อสินค้า: "+product_name+" ตัวเลือก: "+product_group_name;
                wanttodelete();
        }
        </script>
        <!-- bodycart -->
        <script>
        function cart_detail_num_minus(obj){
            var product_name = obj.getAttribute("product_name");
            var product_group_name = obj.getAttribute("product_group_name");
            var cart_detail_code = obj.getAttribute("cart_detail_code");
            var cart_detail_num = obj.getAttribute("cart_detail_num");
            if(cart_detail_num=="1"){

                document.getElementById("wanttodeletedetailid").value=cart_detail_code;
                document.getElementById("wanttodeletedetail").innerHTML=" ชื่อสินค้า: "+product_name+" ตัวเลือก: "+product_group_name;
                wanttodelete();
            }else{
                        $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    async : false,
                    url: "../restapi/updatecartdetailminus.php",
                    data:JSON.stringify({
                    cart_detail_code:cart_detail_code,
                    cart_detail_num:cart_detail_num
                    }),
                    success: function (response) {
                    var json_data = response;
                    var getdata = json_data;
                    if(getdata.result=="Success"){
                        location.reload();
                    }
                    }
                     });
            }

        }
        function cart_detail_num_plus(obj){
            var product_group_code = obj.getAttribute("product_group_code");
            var cart_detail_code = obj.getAttribute("cart_detail_code");
            var cart_detail_num = obj.getAttribute("cart_detail_num");
            $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    async : false,
                    url: "../restapi/updatecartdetailplus.php",
                    data:JSON.stringify({
                     product_group_code:product_group_code,
                    cart_detail_code:cart_detail_code,
                    cart_detail_num:cart_detail_num
                    }),
                    success: function (response) {
                    var json_data = response;
                    var getdata = json_data;
                    if(getdata.result=="Success"){
                        location.reload();
                    }else{
                        Numnotenough();
                        setTimeout(function(){ 
                               
                        },2600);
                    }
                    }
                     });
        }
        </script>
 
      <?php }
    
    }?>
      
      
      <?php $getcartdetail="SELECT * FROM tbl_cart_detail 
            WHERE cart_code ='".$rowgetstore['cart_code']."' AND cart_detail_status='1' ";
            $resultgetcartdetail = @$conn->query($getcartdetail);
            if(@$resultgetcartdetail->num_rows >0){ ?>
      <div class="row" style="background-color:white;">
       <!-- เลือกการจัดส่งร้าน -->
       <div class="col-sm-5" align="center" style="margin-top:25px;" ></div>
                <div class="col-sm-2" align="left" style="margin-top:25px;border-left:1px dashed;border-top:1px dashed;border-bottom:1px dashed">
                <span style="color:#00B674"><br>
                Shipping Option: 
                <br><br>
                </span>
                </div>
                <div class="col-sm-2" align="left" style="margin-top:25px;border-top:1px dashed;border-bottom:1px dashed">
                <span style="color:#000000;"><br><?php echo $rowgetstore['transport_name'] ?></span>
                <br><br>
                </div>
                <div class="col-sm-1" align="left" style="margin-top:25px;border-top:1px dashed;border-bottom:1px dashed">
                <span style="color:#2546FF; cursor: pointer;" onclick="changeshippingbycustomer(this)"
                store_code  = "<?php echo $rowgetstore['store_code']; ?>"
                ><br>เปลี่ยน</span>
                <!-- modal -->
         
                <!-- modal -->
                </div>
                <div class="col-sm-2" align="left" style="margin-top:25px;border-top:1px dashed;border-bottom:1px dashed;border-right:1px dashed">
                <span style="color:#000000;font-weight:bold;"><br>฿</span>
                <span style="color:#000000;font-weight:bold;" id="shipping_price" name="shipping_price"><?php echo $rowgetstore['shipping_price']; ?></span>
               
                </div>
<!-- เลือกการจัดส่งร้าน -->
                <div class="col-sm-12" align="center" style="margin-top:25px;" >
                <br>
                </div>
                <div class="col-sm-9" align="center" style="margin-top:25px;" >
                <br>
                </div>
                <div class="col-sm-3" align="left" style="margin-top:25px;" >
               ยอดสั่งซื้อทั้งหมด : <?php
               $totalpricecartsumtransport = $totalpricecartsumtransport+$rowgetstore['shipping_price'];
               $totalpricecart = $totalpricecart+ $totalpricecartsumtransport;
               echo $totalpricecartsumtransport;?>.00
                </div>  
            </div>
            <?php 
            }
      ?>
      <?php 
    }
      ?>

    <?php } ?>
    <script>
                function changeshippingbycustomer(obj) {
                    var store_code = obj.getAttribute("store_code");
                    console.log(store_code);
                    document.getElementById("store_codechangeshipping").value=store_code;
                    checkboxtransport = "<option hidden>กรุณาเลือกขนส่ง</option>";
                    $('#checkboxtransport').html(checkboxtransport);
                $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/getshippingbycustomer.php",
                data:JSON.stringify({
                    store_code:store_code
                }),
                success: function (response) {
    
                    $.each(response, function(index) {
                        checkboxtransport += "<option value="+"  '"+response[index].id+"' "+">"+"ชื่อ :"+response[index].name+"ราคา :"+response[index].price  +"</option>";
                         });
                    document.getElementById("checkboxtransport").disabled = false;
                    $('#checkboxtransport').html(checkboxtransport);
                    
                    
                }
            });
            $("#changeshippingbycustomer").modal({backdrop: 'static', keyboard: false})  
                   $("#changeshippingbycustomer").modal('show');
                }
                </script>
   </div>
<!-- footercart -->
        <div id="rcorners2" style="margin-top:50px; padding-bottom:5px; padding-top:5px;">
        <div class="row" style="background-color:white">
                <div class="col-sm-2" align="center">
                วิธีการชำระเงิน                 
                </div>
                <?php 
               if($cus_address_have=="1"){ 
                ?>
                <div class="col-sm-8" align="left">
                <button id="creditcart" idname="creditcart" class="btn btn-outline-warning d-inline-flex" style="font-size:14px; color:black; margin-bottom:20px; margin-right:5px;" onclick="proGroupSelect(this,this.id)">บัตรเครดิต</button>
                <!-- <button id="ibanking" idname="ibanking" class="btn btn-outline-warning d-inline-flex" style="font-size:14px; color:black; margin-bottom:20px; margin-right:5px;" onclick="proGroupSelect(this,this.id)">iBanking/Mobile Banking</button>  -->
                <button id="promptpay" idname="promptpay" class="btn btn-outline-warning d-inline-flex" style="font-size:14px; color:black; margin-bottom:20px; margin-right:5px;" onclick="proGroupSelect(this,this.id)">PromptPay</button>     
                </div>
                <div class="col-sm-2" align="left">
                </div>
               <?php } ?>

               <?php 
               if($cus_address_have=="0"){ 
                ?>

                <div class="col-sm-8" align="left">
                <button id="creditcart" idname="creditcart" class="btn btn-secondary"  style="cursor: no-drop;font-size:14px; color:black; margin-bottom:20px; margin-right:5px;" >บัตรเครดิต</button>
                <!-- <button id="ibanking" idname="ibanking" class="btn btn-outline-warning d-inline-flex" style="font-size:14px; color:black; margin-bottom:20px; margin-right:5px;" onclick="proGroupSelect(this,this.id)">iBanking/Mobile Banking</button>  -->
                <button id="promptpay" idname="promptpay" class="btn btn-secondary " style="cursor: no-drop;font-size:14px; color:black; margin-bottom:20px; margin-right:5px;" >PromptPay</button>     
                </div>
                <div class="col-sm-2" align="left">
                </div>
            
                <?php } ?>
                
            </div>
<script>
var changeclass="";
 function proGroupSelect(obj,idd) {

        console.log("change img price name");
        var id =obj.getAttribute("idname");


        if(changeclass==""){
            document.getElementById(idd).classList.add('btn-warning');
            document.getElementById(idd).classList.remove('btn-outline-warning');
            changeclass=id;
            
           
        } else {
            document.getElementById(changeclass).classList.add('btn-outline-warning');
            document.getElementById(changeclass).classList.remove('btn-warning');
            document.getElementById(idd).classList.add('btn-warning');
            document.getElementById(idd).classList.remove('btn-outline-warning');
            changeclass=id;
           
        }

        console.log(id);
        if(id=="creditcart"){
            document.getElementById("accept_order").disabled=true;
            document.getElementById("showcreditcart").hidden=false;
            //document.getElementById("showibanking").hidden=true;
        }else{
            document.getElementById("accept_order").disabled=false;
           // document.getElementById("showibanking").hidden=false;
            document.getElementById("showcreditcart").hidden=true;
        }
     
     

        
    } 
</script>
<!-- credit -->
<div class="row" style="background-color:white" hidden id="showcreditcart">
<div class="col-sm-2"  align="left"></div>
                <div class="col-sm-6"  align="left">

                <form id="checkoutForm" method="POST">
                <input type="hidden" name="omiseToken">
                <input type="hidden" name="omiseSource">
                <button type="submit" id="checkoutButton" class="btn btn-success">เพิ่มบัตร + </button>
                </form>

                </div>
             
                <br>  <br> <br>  <br>
</div>
<script>
    OmiseCard.configure({
        publicKey: "pkey_test_5kwbj3tbv09itjm0p8m"
    });
    var customer_code = "<?php echo $_SESSION['customer_code']; ?>"
    var customer_address_code = document.getElementById("input_customer_address_code").value;
    var button = document.querySelector("#checkoutButton");
    var form = document.querySelector("#checkoutForm");
    var totalprice = "<?php echo $totalpricecart; ?>"+"00";
    var shipping_price = document.getElementById("shipping_price").innerHTML;
    button.addEventListener("click", (event) => {
        event.preventDefault();
        OmiseCard.open({
        amount: totalprice,
        currency: "THB",
        defaultPaymentMethod: "credit_card",
        onCreateTokenSuccess: (nonce) => {
            $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: 'application/json',
                        async : false,
                        url: "../restapi/addorder.php",
                        data:JSON.stringify({
                            omise_token:nonce,
                            totalprice:totalprice,
                            shipping_price:shipping_price,
                            customer_code:customer_code,
                            customer_address_code:customer_address_code,
                            payment_type:"credit_card"
                        }),
                        success: function (response) {
                            var json_data = response.result;
                            console.log(json_data);
                            if(json_data=="Success") {
                                
                            ordersuccessbycreditcard()
                            setTimeout(function(){ 
                                location.replace("index.php");
                             },3600);
                                }else{
                            Failedpayment()
                            setTimeout(function(){ 
                                location.replace("index.php");
                             },3600);
                                }
                        
                            }
                        });

           
        }
        });
    });
</script>
<!-- endcredit -->
<!-- ibanking -->
<div class="row" style="background-color:white" hidden id="showibanking">
                <div class="col-sm-2"  >เลือกธนาคาร
                </div>
                <div class="col-sm-10"  align="left">
                <input type="radio" id="male" name="gender" value="male"> <img src="icons/kbank.png" width="30px" height="30px" style="border:1px solid #CBCBCB"> ธนาคารกสิกรไทย
                <br><br><input type="radio" id="male" name="gender" value="male"> <img src="icons/scb.png" width="30px" height="30px" style="border:1px solid #CBCBCB"> ธนาคารไทยพาณิชย์
                <br><br><input type="radio" id="male" name="gender" value="male"> <img src="icons/krungthai.png" width="30px" height="30px" style="border:1px solid #CBCBCB"> ธนาคารกรุงไทย
                <br><br><input type="radio" id="male" name="gender" value="male"> <img src="icons/bkk.png" width="30px" height="30px" style="border:1px solid #CBCBCB"> ธนาคารกรุงเทพ
              
               
                </div>
                <br>  <br> <br>  <br>
</div>
<!-- endibanking -->

            <div class="row" style="background-color:white">
                <div class="col-sm-1">
                    <!-- <input class="checkbox" type="checkbox" name="selectAll" value="" stlye="background-color:orange !Important;" -->
                    
                    
                </div>
                <div class="col-sm-5" align="left">
                    <!-- <label>เลือกทั้งหมด</label> -->
                </div>
                <div class="col-sm-2" align="right">
                  
                </div>
                <div class="col-sm-2" align="center">
                    <b style="font-weight:bold; color:red;">฿ <?php echo $totalpricecart; ?></b><span style="font-weight:bold; color:red;">.00</span>
                </div>
                <div class="col-sm-2" align="left">
                    <button class="btn btn-success" style="font-size:14px; color:white; width:180px;" onclick="acceptorder()" Id="accept_order" name="accept_order" disabled>ยืนยันซื้อสินค้า</button>
                </div>
                <br>  <br>
            </div>
        </div>
        <!-- footercart -->

        <script>
        function acceptorder(){
            var customer_code = "<?php echo $_SESSION['customer_code']; ?>"
            var customer_address_code = document.getElementById("input_customer_address_code").value;
            var ImgPromptpay="";
            $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: 'application/json',
                        async : false,
                        url: "../restapi/addorder.php",
                        data:JSON.stringify({
                            customer_code:customer_code,
                            customer_address_code:customer_address_code,
                            payment_type:"promptpay"
                        }),
                        success: function (response) {
                            var json_data = response.result;
                            $.each(response, function(index) {
                              ImgPromptpay += "<div style=\"background-color:#CAD0C7\" >"
                              ImgPromptpay += "<p>ใบสั่งซื้อหมายเลข : "+response[index].order_code +"</p>"
                              ImgPromptpay += "<p>ราคาทั้งสิ้น : "+response[index].ordertotalprice +"</p>"
                              ImgPromptpay += "<img src="+"  '"+response[index].URL+"' "+">";
                              ImgPromptpay += "<br><br>";
                              ImgPromptpay += "</div>"
                              ImgPromptpay += "<br>";
                             });
                             $('#ImgPromptpay').html(ImgPromptpay);
                            promptpay1();
                            }
                        });


        }
         </script>
    </div>

    </div>
    <div class="col-md-1"></div>
    </div>
    </div>
   
<!-- ENDBODY -->

<div class="modal fade" id="promptpay1" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <div class="row">
        <div class="col-md-12"> 
        <img src="icons/promptpay.png" width="150px" height="100px">
        </div>
        </div>
                <div class="row">
        <div class="col-md-12"> 
        <div id="ImgPromptpay"> </div>
        </div>
        </div> 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal" id="btnOK" onclick="promptpayok()">ตกลง</button> 
        </div>
      </div>
    </div>
  </div>
  
<script>
    function promptpayok(){
        location.replace("index.php");
    }

</script>

  <!-- Modal ERROR-->
  <div class="modal fade" id="loginfail" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/wrong.png" width="150px" height="150px">
        <br><br>
        <p>ชื่อผู้ใช้หรือรหัสผ่านของคุณผิดพลาด</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

  <!-- Modal SUCCESS-->
  <div class="modal fade" id="loginsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p>เข้าสู่ระบบสำเร็จ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

      <!-- Modal LOGOUT-->
      <div class="modal fade" id="logoutsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/logout.png" width="150px" height="150px">
        <br><br>
        <p>กำลังออกจากระบบ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->


  <!-- Modal BANNED-->
  <div class="modal fade" id="hasbanned" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/banned.png" width="150px" height="150px">
        <br><br>
        <p>รหัสผู้ใช้ของท่านโดน แบน กรุณาติดต่อ ผู้ดูแลระบบ ด้วยค่ะ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

    <!-- Modal Numnotenough-->
             <div class="modal fade" id="Numnotenough" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p>สินค้ามีจำนวนไม่เพียงพอ ไม่สามารถเพิ่มจำนวนสินค้าได้</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal Numnotenough-->



   <!-- Modal wanttodelete-->
 <div class="modal fade" id="wanttodelete" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;">คุณแน่ใจว่าต้องการลบหรือไม่?</h4>
        </div>
        <div class="modal-body" align="center" style="font-size:16px;">
        <input type="text" id="wanttodeletedetailid" hidden>
        <p id="wanttodeletedetail" > ท่านต้องการลบสินค้า </p>
        </div>
        <div class="modal-footer d-block" align="center">
           <a id="updateproductgroupBtn" class="btn btn-outline-success btn-sm" 
           onclick="acceptdelete()"
           >ยืนยัน</a>
           <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-danger btn-sm" data-dismiss="modal">ยกเลิก</a>
        </div>
      </div>

    </div>
  </div>
<script>
function acceptdelete(){
    var cart_detail_code = document.getElementById("wanttodeletedetailid").value;

             $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    async : false,
                    url: "../restapi/deletecartdetail.php",
                    data:JSON.stringify({
                    cart_detail_code:cart_detail_code
                    }),
                    success: function (response) {
                    var json_data = response;
                    var getdata = json_data;
                    if(getdata.result=="Success"){
                        location.reload();
                    }
                    }
                     });
}
</script>

  <!-- Modal wanttodelete-->


<!--  changeaddress Modal -->
<div class="modal" id="changeaddress">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">เปลี่ยนที่อยู่จัดส่ง</h2>
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">
                    
                    <?php 
                     $querycustomeraddress = "SELECT * FROM tbl_customer_address WHERE customer_code = '".$_SESSION['customer_code']."'  ";
                     $resultquerycustomeraddress = @$conn->query($querycustomeraddress);
                     if($resultquerycustomeraddress->num_rows > 0){
                     while($rowquerycustomeraddress = $resultquerycustomeraddress->fetch_assoc()){
                ?>
                           <div class="row" style="margin-left: 20px;margin-right: 80px;background-color:#E1E1E1 ;">
                    
                    <div class="col-sm-3"> ตั้งที่อยู่หลัก :
                    <?php if($rowquerycustomeraddress['customer_address_status']=="1"){
                        echo "
                        <label class=\"switch\">
                        <input type=\"checkbox\" checked disabled>
                        <span class=\"slider round\"></span>
                        </label>
                        ";
                    }else{
                        echo "
                        <label class=\"switch\">
                        <input type=\"checkbox\" customer_address_code=".$rowquerycustomeraddress['customer_address_code']." onclick=\"updatecustomeraddressstatus(this)\">
                        <span class=\"slider round\"></span>
                        </label>
                        ";
                    }
                    
                    ?> 
                   
                    </div>
                            <div class="col-sm-9" align="left"style="font-size:12px;background-color:#B6B6B6"> <br>
                            <?php echo "ชื่อ :".$rowquerycustomeraddress['customer_address_fullname']." เบอร์โทรศัพท์ :".$rowquerycustomeraddress['customer_address_tel']."<br>";
                            
                             echo $rowquerycustomeraddress['customer_address_detail']." ตำบล :".$rowquerycustomeraddress['customer_address_subdistrict']
                    ." อำเภอ :".$rowquerycustomeraddress['customer_address_district']." จังหวัด :".$rowquerycustomeraddress['customer_address_province']
                    ." รหัสไปรษณีย์ :".$rowquerycustomeraddress['customer_address_postcode'] ; ?><br><br>
                           </div>
                        
                        </div>
                     <?php }}
                        ?>


                    </div>   
                  
                    </div>   
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                
        </div>
    </div>
    </div>
    <!--  End ofchangeaddress Modal -->
<script>
function updatecustomeraddressstatus(obj){
    var customer_address_code =obj.getAttribute("customer_address_code");
    var customer_code ="<?php echo $_SESSION['customer_code'];?>";
 
                     $.ajax({
                    type: "POST",
                            dataType: 'json',
                            contentType: 'application/json',
                            async : false,
                            url: "../restapi/updatecustomeraddressstatus.php",
                            data:JSON.stringify({
                            customer_code:customer_code,
                            customer_address_code:customer_address_code
                            }),
                            success: function (response) {
                            var json_data = response;
                            var apistatus = json_data.result;
                            console.log(apistatus);
                            if(apistatus=="Success") {
                            
                            location.reload();
                        
                            }
                            }
                            });
                    }
</script>

  
<!--  changeshippingbycustomer Modal -->
<div class="modal" id="changeshippingbycustomer">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">เปลี่ยนการจัดส่ง</h2>
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">
                    <div class="row">
                    <div class="col-sm-4" ></div>
                    <div class="col-sm-4" >
                            <input type="text" id="store_codechangeshipping" hidden>
                            <select id="checkboxtransport" name="checkboxtransport" Disabled onchange="changeshippingbycustomersql()">
                            <option hidden>กรุณาเลือกการจัดส่ง</option>
                            </select> 
                    </div>
                    <div class="col-sm-4" ></div>

                    </div> 
                    </div>   
                    </div>   
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
         </div>
    </div>
    </div>
    <!--  End changeshippingbycustomer Modal -->
<script>
function changeshippingbycustomersql(){

var store_code = document.getElementById("store_codechangeshipping").value;
var shipping_code = document.getElementById("checkboxtransport").value;
var customer_code ="<?php echo @$_SESSION['customer_code'];?>";

console.log(store_code+shipping_code+customer_code);
$.ajax({
                    type: "POST",
                            dataType: 'json',
                            contentType: 'application/json',
                            async : false,
                            url: "../restapi/updateshippingbycustomer.php",
                            data:JSON.stringify({
                                store_code:store_code,
                                shipping_code:shipping_code,
                                customer_code:customer_code
                            }),
                            success: function (response) {
                            var json_data = response;
                            var apistatus = json_data.result;
                            console.log(apistatus);
                            if(apistatus=="Success") {
                            
                            location.reload();
                        
                            }
                            }
                            });
}
</script>

  <!-- Modal SUCCESS-->
  <div class="modal fade" id="ordersuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p>สั่งซื้อสำเร็จ กรุณา ชำระเงินในระยะเวลาที่กำหนดด้วยค่ะ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

    <!-- Modal SUCCESSCreditcard-->
    <div class="modal fade" id="ordersuccessbycreditcard" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p>สั่งซื้อสำเร็จ ขอบคุณที่ใช้บริการค่ะ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- ModalSUCCESSCreditcard -->

<!-- Modal FAILEDPAYMENT-->
      <div class="modal fade" id="Failedpayment" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p>ชำระเงินล้มเหลว กรุณาชำระเงินอีกครั้ง ทีหน้า "สินค้าที่ยังไม่ได้ชำระเงิน"</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal FAILEDPAYMENT-->

  <!--  insertransport Modal -->
<div class="modal" id="customeraddaddress" data-backdrop="static" data-keyboard="false"> 
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">เพิ่มข้อมูลที่อยู่</h2>
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">ชื่อ - นามสกุล : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="customer_address_fullname" name="customer_address_fullname"  >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">เบอรโทรศัพท์ : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" maxlenght="10" class="form-control form-control-sm" id="customer_address_tel" name="customer_address_tel"  >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">จังหวัด : </div>
                            <div class="col-sm-6 d-inline-flex">
                              
                              <select id="customer_address_province" name="customer_address_province" onchange="getprovincecode()">
                                   
                                    <option hidden>กรุณาเลือกจังหวัด</option>
                              <?php
                                   include "../restapi/setting/configthailand.php";
                                   $getprovince ="SELECT * FROM th_province ORDER BY name_th ASC";
                                   $resultgetprovince = @$conn->query($getprovince);
                                   if($resultgetprovince->num_rows > 0){
                                       while($rowgetprovince = $resultgetprovince->fetch_assoc()){
                              ?>
                                     <option value="<?php echo $rowgetprovince['name_th'].":".$rowgetprovince['id'] ?>">จังหวัด<?php echo $rowgetprovince['name_th'];  ?></option>
                              <?php
                                       }}
                              ?>
                              </select> 
                            
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">อำเภอ : </div>
                            <div class="col-sm-6 d-inline-flex">
                                   
                            <select id="customer_address_district" name="customer_address_district" Disabled onchange="getdistrictcode()">
                            <option hidden>กรุณาเลือกอำเภอ</option>
                            </select> 
                            
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">ตำบล : </div>
                            <div class="col-sm-6 d-inline-flex">
                                   
                            <select id="customer_address_subdistrict" name="customer_address_subdistrict" Disabled onchange="getamphurpostcode()">
                            <option hidden>กรุณาเลือกตำบล</option>
                            </select> 
                            
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">รหัสไปรษณีย์ : </div>
                            <div class="col-sm-6 d-inline-flex">
                               
                                <input type="text" class="form-control form-control-sm" id="customer_address_postcode" name="customer_address_postcode"  readonly>
                           
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">รายละเอียดที่อยู่ (บ้านเลขที่ หมู่บ้าน ถนน ซอย) : </div>
                            <div class="col-sm-6 d-inline-flex">
                               
                                <textarea  rows="4" class="form-control form-control-sm" id="customer_address_detail" name="customer_address_detail" >
                                </textarea>
                           
                            </div>
                        </div>

                        </div></div>
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-success btn-sm" data-dismiss="modal" onclick="insertaddress()">ยืนยัน</a>
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    <!-- End of insertransport Modal -->
<script>

function insertaddress(){
        var customer_code="<?php echo $_SESSION['customer_code']?>";
        var customer_address_fullname=document.getElementById("customer_address_fullname").value;
        var customer_address_tel=document.getElementById("customer_address_tel").value;
        
        var customer_address_province = document.getElementById("customer_address_province").value;
        var customer_address_province = customer_address_province.split(":");
        var customer_address_province = customer_address_province[0].trim();

        var customer_address_district = document.getElementById("customer_address_district").value;
        var customer_address_district = customer_address_district.split(":");
        var customer_address_district = customer_address_district[0].trim();

        var customer_address_subdistrict = document.getElementById("customer_address_subdistrict").value;
        var customer_address_subdistrict = customer_address_subdistrict.split(":");
        var customer_address_subdistrict = customer_address_subdistrict[0].trim();

        var customer_address_postcode = document.getElementById("customer_address_postcode").value;
        var customer_address_detail = document.getElementById("customer_address_detail").value.trim();
       

        $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/addcustomeraddress.php",
                data:JSON.stringify({
                    customer_code:customer_code,
                    customer_address_fullname:customer_address_fullname,
                    customer_address_tel:customer_address_tel,
                    customer_address_province:customer_address_province,
                    customer_address_district:customer_address_district,
                    customer_address_subdistrict:customer_address_subdistrict,
                    customer_address_postcode:customer_address_postcode,
                    customer_address_detail:customer_address_detail
                }),
                success: function (response) {
                var json_data = response;
                var getdata = json_data;
                console.log(getdata);
                if(getdata.result=="Success"){
    
                    location.reload();
                 
                }else{
                      emptyaddress()
                            setTimeout(function(){ 
                            location.reload();
                            },2600);
                }
                }
            });
    }
    function getdistrictcode(){
           document.getElementById("customer_address_postcode").value="";
           var districtcode=document.getElementById("customer_address_district").value;
           var districtcode = districtcode.split(":");
           console.log(districtcode[1]);
           customer_address_subdistrict="<option hidden>กรุณาเลือกตำบล</option>";
            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/getsubdistrictlist.php",
                data:JSON.stringify({
                    districtcode :districtcode[1] 
                }),
                success: function (response) {
    
                    $.each(response, function(index) {
                        customer_address_subdistrict += "<option value="+"  '"+response[index].TAMBON_NAME+":"+response[index].POST_CODE+"' "+">"+response[index].TAMBON_NAME+"</option>";
                    });
                    document.getElementById("customer_address_subdistrict").disabled = false;
                    $('#customer_address_subdistrict').html(customer_address_subdistrict);
                }
            });
    }

    function getprovincecode(){
            document.getElementById("customer_address_postcode").value="";
           var provincecode=document.getElementById("customer_address_province").value;
           var provincecode = provincecode.split(":");
           console.log(provincecode[1]);
           var customer_address_district="<option hidden>กรุณาเลือกอำเภอ</option>";
           var customer_address_subdistrict="<option hidden>กรุณาเลือกตำบล</option>";
           $('#customer_address_subdistrict').html(customer_address_subdistrict);
           document.getElementById("customer_address_subdistrict").disabled = true;
            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/getamphurlist.php",
                data:JSON.stringify({
                    PROVINCE_ID :provincecode[1] 
                }),
                success: function (response) {
    
                    $.each(response, function(index) {
                        customer_address_district += "<option value="+"  '"+response[index].AMPHUR_NAME+":"+response[index].AMPHUR_ID+"' "+">"+response[index].AMPHUR_NAME+"</option>";
                    });
                    document.getElementById("customer_address_district").disabled = false;
                    $('#customer_address_district').html(customer_address_district);
                }
            });

    }
    function getamphurpostcode(){
        var postcode=document.getElementById("customer_address_subdistrict").value;
        var postcode = postcode.split(":");
        var postcode = postcode[1];
        console.log(postcode);
        document.getElementById("customer_address_postcode").value=postcode;
    }
    function emptyaddress() {
    $("#emptyaddress").modal({backdrop: 'static', keyboard: false})  
        $("#emptyaddress").modal('show');
        setTimeout(function(){$('#emptyaddress').modal('hide')},2000);
    }
</script>
    

          <!-- Modal DELETESUCCESS-->
          <div class="modal fade" id="emptyaddress" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <p style="color:black" class="modalfont">กรุณากรอกข้อมูลสถานที่จัดส่งให้ครบถ้วน</p>
        </div>
        <div class="modal-footer">
      
        </div>
      </div>
      </div>
  </div> 
  <!-- asdasd -->
</body>


</html>