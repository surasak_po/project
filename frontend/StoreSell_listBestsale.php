<?php
    include "webservice/setting/Config.php";

    @session_start();
    @session_cache_expire(30);

    if(@$_SESSION['role']=="store") {
    } else {
        echo "<meta http-equiv='refresh' content='0 ; URL=Index.php'>";
    }

    $store_code = $_SESSION['store_code'];
    @$start_date = $_GET['start_date'];
    @$end_date = $_GET['end_date'];

    $total_wait_price = 0;
    $total_wait_order = 0;
    $strgetorderwait ="SELECT * FROM tbl_order 
    WHERE  store_code = '$store_code' AND order_status != '3' ";
     $resultstrgetorderwait = @$conn->query($strgetorderwait);
     if($resultstrgetorderwait->num_rows > 0){
        while($rowstrgetorderwait = $resultstrgetorderwait->fetch_assoc()){
            $total_wait_order= $total_wait_order +1;
            $order_code = $rowstrgetorderwait['order_code'];
            $order_shipping_pricewait = $rowstrgetorderwait['order_shipping_price'];

            $strgetpriceall ="SELECT * FROM tbl_order_detail 
            WHERE order_code = '$order_code' ";
            $resultstrgetpriceall = @$conn->query($strgetpriceall);
            if($resultstrgetpriceall->num_rows > 0){
                while($rowstrgetpriceall = $resultstrgetpriceall->fetch_assoc()){
                    $total_wait_price = $total_wait_price+($rowstrgetpriceall['order_detail_price']*$rowstrgetpriceall['order_detail_num']);
                }
              
            }

        }
    }

    $firstdayweek = date("Y-m-d", strtotime('monday this week'));
    $lastdayweek = date("Y-m-d", strtotime('sunday this week'));

    $total_price_week = 0;
    $total_order_week= 0;
    $strgetorderweek ="SELECT * FROM tbl_order 
    WHERE DATE_FORMAT(order_date, '%Y-%m-%d') BETWEEN '$firstdayweek' AND '$lastdayweek'
    AND store_code = '$store_code' AND order_status = '3' ";
     $resultstrgetorderweek = @$conn->query($strgetorderweek);
     if($resultstrgetorderweek->num_rows > 0){
        while($rowstrgetorderweek = $resultstrgetorderweek->fetch_assoc()){
            $total_order_week= $total_order_week+1;
            $order_code = $rowstrgetorderweek['order_code'];
            $order_shipping_priceweek = $rowstrgetorderweek['order_shipping_price'];

            $strgetpriceallweek ="SELECT * FROM tbl_order_detail 
            WHERE order_code = '$order_code' ";
            $resultstrgetpriceallweek = @$conn->query($strgetpriceallweek);
            if($resultstrgetpriceallweek->num_rows > 0){
                while($rowstrgetpriceallweek = $resultstrgetpriceallweek->fetch_assoc()){
                $total_price_week = $total_price_week+($rowstrgetpriceallweek['order_detail_price']*$rowstrgetpriceallweek['order_detail_num']);
                }
             
            }
        }
    }

    $firstdaymonth = date("Y-m-d", strtotime('first day of this month'));
    $lastdaymonth = date("Y-m-d", strtotime('last day of this month'));

    $total_price_month= 0;
    $total_order_month= 0;
    $strgetordermonth ="SELECT * FROM tbl_order 
    WHERE DATE_FORMAT(order_date, '%Y-%m-%d') BETWEEN '$firstdaymonth' AND '$lastdaymonth'
    AND store_code = '$store_code' AND order_status = '3' ";
     $resultstrgetordermonth = @$conn->query($strgetordermonth);
     if($resultstrgetordermonth->num_rows > 0){
        while($rowstrgetordermonth = $resultstrgetordermonth->fetch_assoc()){
            $total_order_month=$total_order_month+1;
            $order_code = $rowstrgetordermonth['order_code'];
            $order_shipping_pricemonth = $rowstrgetordermonth['order_shipping_price'];

            $strgetpriceall ="SELECT * FROM tbl_order_detail 
            WHERE order_code = '$order_code' ";
            $resultstrgetpriceall = @$conn->query($strgetpriceall);
            if($resultstrgetpriceall->num_rows > 0){
                while($rowstrgetpriceall = $resultstrgetpriceall->fetch_assoc()){
                $total_price_month = $total_price_month+($rowstrgetpriceall['order_detail_price']*$rowstrgetpriceall['order_detail_num']);
                }
              
            }
        }
    }

    $total_price_all= 0;
    $total_order_all= 0;
    $strgetorderall ="SELECT * FROM tbl_order 
    WHERE store_code = '$store_code' AND order_status = '3' ";
     $resultstrgetorderall = @$conn->query($strgetorderall);
     if($resultstrgetorderall->num_rows > 0){
        while($rowstrgetorderall = $resultstrgetorderall->fetch_assoc()){
            $total_order_all = $total_order_all+1;
            $order_code = $rowstrgetorderall['order_code'];
            $order_shipping_pricesuccess = $rowstrgetorderall['order_shipping_price'];

            $strgetpriceall ="SELECT * FROM tbl_order_detail 
            WHERE order_code = '$order_code' ";
            $resultstrgetpriceall = @$conn->query($strgetpriceall);
            if($resultstrgetpriceall->num_rows > 0){
                while($rowstrgetpriceall = $resultstrgetpriceall->fetch_assoc()){
                $total_price_all = $total_price_all+($rowstrgetpriceall['order_detail_price']*$rowstrgetpriceall['order_detail_num']);
                }
             
            }
        }
    }


    $numcreditcard= 0;
    $numpromptpay= 0;
    $strgetnumtypepayment ="SELECT * FROM tbl_payment
    INNER JOIN tbl_order ON tbl_payment.order_code = tbl_order.order_code
    WHERE store_code = '$store_code' AND order_status = '3' ";
     $resultstrgetnumtypepayment = @$conn->query($strgetnumtypepayment);
     if($resultstrgetnumtypepayment->num_rows > 0){
        while($rowstrgetnumtypepayment = $resultstrgetnumtypepayment->fetch_assoc()){

            if($rowstrgetnumtypepayment['payment_type']=="promptpay"){
                $numpromptpay = $numpromptpay+1;
            }
            if($rowstrgetnumtypepayment['payment_type']=="credit_card"){
                $numcreditcard = $numcreditcard+1;
            }
        }
    }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>จัดการข้อมูลร้านค้า</title>
    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="./css/sidebar.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="./css/styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }

        @font-face {
            font-family: 'Opun-Regular.ttf';
            src: url('fonts/Opun-Regular.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }

        body {
            font-family: 'Opun-Regular.ttf' !important;
        }

        .navbar {
            background-color: #575757;
            font-weight: bold;
            font-size: 18px;
            margin-bottom: 0px;
        }

        .navbar a {
            color: #FFA200;
            margin-top: -6px;
        }

        nav ul li a {
            font-size: 14px !important;
            text-decoration: underline;
        }

        #margin {
            margin-top: 10px;
            margin-left: 30px;
        }

        ul#menu li {
            display: inline;
            text-align: center;
            margin-left: 20px;
            margin-right: 20px;
            font-size: 18px;
            color: #575757;
        }

        ul#menu li a {
            color: #575757;
        }

        ul#menu li a:hover {
            color: #FFA200;
            text-decoration: none;
        }

        ul#label li {
            display: inline;
            text-align: center;
            margin-left: 20px;
            margin-right: 20px;
            font-size: 16px;
            color: #575757;
        }

        #hline {
            color: #FFA200 !important;
        }

        /* div a {
            font-size: 14px;
        }

        div a:hover {
            color: black;
            text-decoration: none;
        } */

        #bgblack {
            background-color: rgb(255, 94, 0);
        }

        .nav-link {
            text-decoration: none;
        }

        .nav-link:hover {
            color: black !important;
        }

        .nav-item {
            margin-top: 10px !important;
        }

        .row {
            padding-top: 10px;
        }

        .fas {
            padding-right: 5px;
            margin-right: 5px;
        }
   
    .hovertable:hover {
         background-color:#BDBDBD ; 
    }
    .hovertable{
        padding-top:7px;
        padding-bottom:7px;
        background-color:#858585;
    }
    

    .hovertableproductgroup:hover {
         background-color:#ACE7FF  ; 
    }
    .hovertableproductgroup{
        padding-top:7px;
        padding-bottom:7px;
        background-color:#2CC2FF;
    }
    .modal-header .close {
        display:none;
        }
    </style>
</head>

<script>
    function cantdelete(){
        $("#cantdelete").modal({backdrop: 'static', keyboard: false})  
      $("#cantdelete").modal('show');
      setTimeout(function(){$('#cantdelete').modal('hide')},2000);
    }
    function deletesuccess(){
        $("#deletesuccess").modal({backdrop: 'static', keyboard: false})  
      $("#deletesuccess").modal('show');
      setTimeout(function(){$('#deletesuccess').modal('hide')},2000);
    }
    function alertproductgroupname() {
        $("#alertproductgroupname").modal({backdrop: 'static', keyboard: false})  
        $("#alertproductgroupname").modal('show');
        setTimeout(function(){$('#alertproductgroupname').modal('hide')},2000);
    }
    function alertproductname() {
        $("#alertproductname").modal({backdrop: 'static', keyboard: false})  
        $("#alertproductname").modal('show');
        setTimeout(function(){$('#alertproductname').modal('hide')},2000);
    }
    function alertproductgroupname() {
        $("#alertproductgroupname").modal({backdrop: 'static', keyboard: false})  
        $("#alertproductgroupname").modal('show');
        setTimeout(function(){$('#alertproductgroupname').modal('hide')},2000);
    }
    function alertexception() {
        $("#alertexception").modal({backdrop: 'static', keyboard: false})  
        $("#alertexception").modal('show');
        setTimeout(function(){$('#alertexception').modal('hide')},2000);
    }
   function alerttel() {
    $("#alerttel").modal({backdrop: 'static', keyboard: false})  
        $("#alerttel").modal('show');
        setTimeout(function(){$('#alerttel').modal('hide')},2000);
    }
    function alertmail() {
        $("#alertmail").modal({backdrop: 'static', keyboard: false})  
        $("#alertmail").modal('show');
        setTimeout(function(){$('#alertmail').modal('hide')},2000);
    }
 function updatesuccess(){
    $("#updatesuccess").modal({backdrop: 'static', keyboard: false})  
      $("#updatesuccess").modal('show');
      setTimeout(function(){$('#updatesuccess').modal('hide')},1000);
    }

  function logoutsuccess(){
    $("#logoutsuccess").modal({backdrop: 'static', keyboard: false})  
      $("#logoutsuccess").modal('show');
      setTimeout(function(){$('#logoutsuccess').modal('hide')},2000);
    }

    function insertsuccess(){
        $("#insertsuccess").modal({backdrop: 'static', keyboard: false})  
      $("#insertsuccess").modal('show');
      setTimeout(function(){$('#insertsuccess').modal('hide')},1000);
    }
    
function encodeImageFileAsURL(element) {
  var file = element.files[0];
  var reader = new FileReader();
  reader.onloadend = function() {
   // console.log(reader.result);
  }
  reader.readAsDataURL(file);

  return reader;
}
    function cancelUpdateStoreInfo() {
      location.reload();
    }

    function logoutFunction() {
           logoutsuccess();
                   setTimeout(function(){ 
                    document.forms["formLogout"].action = "webservice/Logout.php";
                    document.forms["formLogout"].submit();
                   },2600);
    }
</script>



<body>
    <nav class="navbar navbar-expand-sm">
        <a class="navbar-brand" style="font-family:'KRR_AengAei.ttf'!important; color: #FFA200;font-size:35px;">
            <i class="fas fa-dolly"></i>ผลิตภัณฑ์ชุมชน
        </a>
        <ul class="navbar-nav mr-auto"></ul>
        <ul class="navbar-nav">
            <?php
                echo "
                    <form id=\"formLogout\" name=\"formLogout\">
                        <a class=\"nav-link\" style=\"cursor:pointer\" onclick=\"logoutFunction()\"><i class=\"fas fa-sign-out-alt\"></i>ออกจากระบบ</a>
                    </form>
                ";
            ?>
        </ul>
    </nav>

    <!-- Sidebar  -->
    <div class="wrapper">
        
        <nav id="sidebar">
            <div class="sidebar-header">
                <h4>หน้าจัดการร้านค้า</h4>
                <strong>BS</strong>
            </div>

            <ul class="list-unstyled components">
                <li >
                    <a href="Store.php">
                        <i class="fas fa-briefcase"></i>
                        ข้อมูลร้านค้า
                    </a>
                </li>
           
                <li >
                    <a href="StoreProductView.php">
                        <i class="fas fa-box"></i>
                        เพิ่มสินค้า
                    </a>
                </li>
                <li >
                    <a href="StoreMyproduct.php">
                        <i class="fas fa-box"></i>
                      สินค้าของฉัน
                    </a>
                </li>

                <li>
                    <a href="StoreTransportView.php">
                    <i class="fas fa-box"></i>
                        จัดการช่องทางการจัดส่ง
                    </a>
                </li>
                <li>
                    <a href="StoreOrderview.php">
                        <i class="fas fa-box"></i>
                       ส่งสินค้า
                        <span id="badge_order_num" class="badge badge-light"></span>
                    </a>
                </li>
                <li class="active">
                    <a href="StoreSell_list.php">
                        <i class="fas fa-box"></i>
                        รายรับของฉัน
                        <span id="badge_order_num" class="badge badge-light"></span>
                    </a>
                </li>
                <li >
                <a href="StoreWithdraw.php">
                        <i class="fas fa-box"></i>
                        ถอนเงิน
                        <span id="badge_order_num" class="badge badge-light"></span>
                    </a>
                </li>

                
                
            </ul>
        </nav>

        <script>

setInterval(function(){ 
    var store_code ="<?php echo @$_SESSION['store_code'];?>";

            if(store_code!=""){ //ถ้า login
                $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: 'application/json',
                        async : false,
                        url: "../restapi/getordernumstore.php",
                        data:JSON.stringify({
                            store_code:store_code
                        }),
                        success: function (response) {
                        var json_data = response;
                        var getdata = json_data;   
                document.getElementById("badge_order_num").innerHTML = getdata.ordernum;
                        }
                    }); 
                }
        }, 500);
        
        function prepairgetmonney(){
            window.location.href = "StoreSell_list.php";
        }
        function getmonney(){
            window.location.href = "StoreSell_listSuccess.php";
        }
        
        function bestsale(){
            window.location.href = "StoreSell_listBestsale.php";
        }
        </script>
        <!-- Page Content  -->
        <div id="content">

        <div class="container-fluid" style="background-color:#DDDDDD;padding-top:10px">
        <div class="container" style="background-color:White;padding-top:10px">
        <h4 style="font-weight:bold;">ภาพรวมของฉัน</h4>
        <br>

        <div class="row" style="font-size:20px;font-weight:bold;">
                <div class="col-md-4">
                เตรียมการโอนเงิน
                </div>
                <div class="col-md-4" style="border-left: 4px solid  #C8C8C8;">
                โอนเงินแล้ว
                </div>
        </div>
        <div class="row">
                <div class="col-md-4" >
                <span style="color:#8D8D8D  ">รวม</span><br>
                <span style="color:black;font-size:20px;font-weight:bold">฿<?php echo number_format($total_wait_price).".00";?></span>
                </div>
                <div class="col-md-3" style="border-left: 4px solid  #C8C8C8;">
                <span style="color:#8D8D8D  ">สัปดาห์นี้ </span><br>
                <span style="color:black;font-size:20px;font-weight:bold">฿<?php echo number_format($total_price_week).".00";?></span>
                </div>
                <div class="col-md-3"> 
                <span style="color:#8D8D8D  ">เดือนนี้</span><br>
                <span style="color:black;font-size:20px;font-weight:bold">฿<?php echo number_format($total_price_month).".00";?></span>
                </div>
                <div class="col-md-2">
                <span style="color:#8D8D8D  ">รวม</span><br>
                <span style="color:black;font-size:20px;font-weight:bold">฿<?php echo number_format($total_price_all).".00";?></span>  
                </div>
              
        </div>
        <div class="row">
                <div class="col-md-4">
                <span style="color:#8D8D8D  ">จำนวนใบสั่งซื้อ</span><br>
                <span style="color:black;font-size:20px;font-weight:bold"><?php echo $total_wait_order;?></span>
                </div>
                <div class="col-md-3" style="border-left: 4px solid  #C8C8C8;">
                <span style="color:#8D8D8D  ">จำนวนใบสั่งซื้อ </span><br>
                <span style="color:black;font-size:20px;font-weight:bold"><?php echo $total_order_week;?></span>
                </div>
                <div class="col-md-3"> 
                <span style="color:#8D8D8D  ">จำนวนใบสั่งซื้อ</span><br>
                <span style="color:black;font-size:20px;font-weight:bold"><?php echo $total_order_month;?></span>
                </div>
                <div class="col-md-2">
                <span style="color:#8D8D8D  ">จำนวนใบสั่งซื้อ</span><br>
                <span style="color:black;font-size:20px;font-weight:bold"><?php echo $total_order_all;?></span>
                </div>
              
        </div>
        <br>
        <div class="row" style="border-bottom: 4px solid  #C8C8C8;">
        </div>
        <br>
        <div class="row" style="font-size:20px;font-weight:bold;">
                <div class="col-md-4">
                ช่องทางการชำระเงิน
                </div>
                
        </div>
        <div class="row">
                <div class="col-md-3" >
                <span style="color:#8D8D8D  ">บัตรเครดิต</span><br>
                <span style="color:black;font-size:20px;font-weight:bold"><?php echo $numcreditcard; ?></span>
                </div>
                <div class="col-md-3" >
                <span style="color:#8D8D8D  ">พร้อมเพย์</span><br>
                <span style="color:black;font-size:20px;font-weight:bold"><?php echo $numpromptpay; ?></span>
                </div>
              
        </div>
        </div>   
        <br>
        </div>

        <div class="container-fluid" style="background-color:#DDDDDD;padding-top:10px">
        <div class="container" style="background-color:White;padding-top:10px">
        
        <h4 style="font-weight:bold;">รายละเอียดรายรับของฉัน</h4><br>
        <table >
        <tr>
        <td style="font-size:20px;padding-right:30px;cursor:pointer" onclick="prepairgetmonney()">เตรียมการโอนเงิน</td>
        <td style="font-size:20px;padding-right:30px;cursor:pointer" onclick="getmonney()">โอนเงินแล้ว</td>
        <td style="color:#FF750F ;font-size:20px;padding-right:30px;cursor:pointer" onclick="bestsale()">10อันดับสินค้าขายดี</td>
        </tr>
        <tr>
        </tr>
        </table>
        <div class="row">
                <div class="col-md-12" align="center">

                <?php if($start_date !="" && $end_date !=""){ ?>
                <div class="d-inline ">
                <span id="alertdate" hidden style="color:red;">กรุณาเลือกวันที่ให้ครบด้วยค่ะ</span>
                <span id="alertdate2" hidden style="color:red;">กรุณาเลือกวันที่ให้ถูกต้องด้วยค่ะ</span>
                <br>
                ระหว่างวันที่ : <input type="date" id ="start_date" name ="start_date" value="<?php echo $start_date;?>">  ถึง : <input type="date" id ="end_date" name ="end_date" value="<?php echo $end_date;?>">

                </div>
                <?php }else{  ?>
                    <div class="d-inline ">
                <span id="alertdate" hidden style="color:red;">กรุณาเลือกวันที่ให้ครบด้วยค่ะ</span>
                <span id="alertdate2" hidden style="color:red;">กรุณาเลือกวันที่ให้ถูกต้องด้วยค่ะ</span>
                <br>
                ระหว่างวันที่ : <input type="date" id ="start_date" name ="start_date" >  ถึง : <input type="date" id ="end_date" name ="end_date" >
                </div>
                    <?php }  ?>
                    <button  class="btn btn-success" onclick="searchreport1()">ค้นหา</button>
                    
                </div>
                <div class="col-md-12"  align="center">
                <br>
                <?php if($start_date !="" && $end_date !=""){ ?>
                <p style="font-size:20px;color:black;font-family:'Opun-Regular.ttf'!important;">   ยอดขายตั้งแต่วันที่ <?php echo $start_date; ?>  ถึงวันที่ <?php echo $end_date; ?> <p>
                <?php }?>
             
                </div>
        </div>
        <br><br>
        <table class="table ">
            <tr style="font-weight:bold;" bgcolor="#DDDDDD">
            <td align="center">ลำดับ</td>
            <td align="center">ชื่อสินค้า</td>
            <td align="center">ตัวเลือก</td>
            <td align="center">ราคา</td>
            <td align="center">จำนวนที่ขายได้</td>
            <td align="center">ราคารวม</td>
            </tr>
            <style>
tr#dataproduct{
    background-color: white;
    cursor: pointer;
    height: 30px;

}
tr#dataproduct:hover{
    background-color:#FFDCB9 ;
    cursor: pointer;

}
</style>
<script>
function searchreport1(){
var start_date =document.getElementById("start_date").value;
var end_date =document.getElementById("end_date").value;
console.log(start_date);
if(start_date!="" && end_date==""){
    document.getElementById("end_date").focus();
    document.getElementById("alertdate").hidden=false;
}else if(start_date=="" && end_date!=""){
    document.getElementById("start_date").focus();
    document.getElementById("alertdate").hidden=false;
}else if(start_date > end_date){
    document.getElementById("start_date").focus();
    document.getElementById("end_date").focus();
    document.getElementById("alertdate").hidden=true;
    document.getElementById("alertdate2").hidden=false;
}else{
 window.location.href = "StoreSell_listBestsale.php?start_date=" + start_date+"&end_date="+end_date;

}

}
</script>
<?php  
$no = 1;
$totalprice_bottom = 0;
if($start_date !="" && $end_date !=""){
$strgetorder ="SELECT *,SUM(order_detail_price) as sumtotal,SUM(order_detail_num) as sumnum  FROM tbl_order 
INNER JOIN tbl_order_detail ON tbl_order.order_code = tbl_order_detail.order_code
INNER JOIN tbl_customer ON tbl_order.customer_code = tbl_customer.customer_code
INNER JOIN tbl_product_group ON tbl_order_detail.product_group_code = tbl_product_group.product_group_code
INNER JOIN tbl_product ON tbl_product_group.product_code = tbl_product.product_code
WHERE tbl_product.store_code ='$store_code' AND order_status = '3' 
AND DATE_FORMAT(order_date, '%Y-%m-%d') BETWEEN '$start_date' AND '$end_date'
GROUP BY tbl_product_group.product_group_code
ORDER BY SUM(order_detail_num) DESC
LIMIT 10
";
}else{
    $strgetorder ="SELECT *,SUM(order_detail_price) as sumtotal,SUM(order_detail_num) as sumnum  FROM tbl_order 
    INNER JOIN tbl_order_detail ON tbl_order.order_code = tbl_order_detail.order_code
    INNER JOIN tbl_customer ON tbl_order.customer_code = tbl_customer.customer_code
    INNER JOIN tbl_product_group ON tbl_order_detail.product_group_code = tbl_product_group.product_group_code
    INNER JOIN tbl_product ON tbl_product_group.product_code = tbl_product.product_code
    WHERE tbl_product.store_code ='$store_code' AND order_status = '3' 
    GROUP BY tbl_product_group.product_group_code
    ORDER BY SUM(order_detail_num) DESC
    LIMIT 10
    ";
}

 $resultstrgetorder = @$conn->query($strgetorder);
 if($resultstrgetorder->num_rows > 0){
    while($rowstrgetorder = $resultstrgetorder->fetch_assoc()){
          
    ?>
            <tr id="dataproduct"  onclick="vieworderdetail(this)">
            <td><?php echo $no; $no++;?></td>
            <td><?php echo $rowstrgetorder['product_name']; ?></td>
            <td><?php echo $rowstrgetorder['product_group_name']; ?></td>
             <td align="right"><?php echo number_format($rowstrgetorder['order_detail_price']).".00"; ?></td>
            <td align="right"><?php echo $rowstrgetorder['sumnum']; ?></td>
            <td align="right"><?php echo number_format(($rowstrgetorder['sumnum']*$rowstrgetorder['order_detail_price'])).".00"; $totalprice_bottom =$totalprice_bottom + ($rowstrgetorder['sumnum']*$rowstrgetorder['order_detail_price']); ?></td>
            </tr>
 <?php }}else{?>   
    <tr align="center">
            <td colspan="6">
            <img src="icons/empty_box.png" width="100px;"><br>
            ไม่มีสินค้า
            </td>  
    </tr>
<?php }?>  
        </table>
        <div class="row">
        <div class="col-md-2">
        <table class="table">
            <tr style="font-weight:bold;" bgcolor="#DDDDDD">
            <td>ราคาทั้งสิ้น</td>
            </tr>
            <tr style="font-weight:bold;" >
            <td><?php echo  "฿".$totalprice_bottom.".00";?></td>
            </tr>
        </table>
        </div>
    </div>


        <br><br>
        </div>   
        <br><br>
        </div>

        </div>
        <!-- END Content  -->
     
        <!--  vieworderdetaillist Modal -->
        <div class="modal"  data-backdrop="static" data-keyboard="false" id="order_detail_listmodal">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
              
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                        
                        <table class="table " id="order_detail_list">
                            
                        </table>
                </div>
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                 
            </div>
            </div>
            </div>
    <!--  End of vieworderdetaillist Modal -->

    
            <!-- Modal LOGOUT-->
            <div class="modal fade" id="logoutsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/logout.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;" class="modalfont">กำลังออกจากระบบ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

</body>

</html>