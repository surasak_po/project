<?php
    include "webservice/setting/Config.php";

    @session_start();
    @session_cache_expire(30);

    if(@$_SESSION['role'] == "admin"){
        echo "<meta  http-equiv='refresh' content='0;URL=1AdminCustomer.php'>";
    }
    if(@$_SESSION['role'] == "customer"){
        echo "<meta  http-equiv='refresh' content='0;URL=Index.php'>";
    }
    if(@$_SESSION['role'] == ""){
        echo "<meta  http-equiv='refresh' content='0;URL=Index.php'>";
    }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>จัดการข้อมูลร้านค้า</title>
    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="./css/sidebar.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="./css/styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }

        @font-face {
            font-family: 'Opun-Regular.ttf';
            src: url('fonts/Opun-Regular.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        .modalfont {
            font-family: 'Opun-Regular.ttf';
            src: url('fonts/Opun-Regular.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }

        body {
            font-family: 'Opun-Regular.ttf' !important;
        }

        .navbar {
            background-color: #575757;
            font-weight: bold;
            font-size: 18px;
            margin-bottom: 0px;
        }

        .navbar a {
            color: #FFA200;
            margin-top: -6px;
        }

        nav ul li a {
            font-size: 14px !important;
            text-decoration: underline;
        }

        #margin {
            margin-top: 10px;
            margin-left: 30px;
        }

        ul#menu li {
            display: inline;
            text-align: center;
            margin-left: 20px;
            margin-right: 20px;
            font-size: 18px;
            color: #575757;
        }

        ul#menu li a {
            color: #575757;
        }

        ul#menu li a:hover {
            color: #FFA200;
            text-decoration: none;
        }

        ul#label li {
            display: inline;
            text-align: center;
            margin-left: 20px;
            margin-right: 20px;
            font-size: 16px;
            color: #575757;
        }

        #hline {
            color: #FFA200 !important;
        }

        /* div a {
            font-size: 14px;
        }

        div a:hover {
            color: black;
            text-decoration: none;
        } */

        #bgblack {
            background-color: rgb(255, 94, 0);
        }

        .nav-link {
            text-decoration: none;
        }

        .nav-link:hover {
            color: black !important;
        }

        .nav-item {
            margin-top: 10px !important;
        }

        .row {
            padding-top: 10px;
        }

        .fas {
            padding-right: 5px;
            margin-right: 5px;
        }
   
    .hovertable:hover {
         background-color:#BDBDBD ; 
    }
    .hovertable{
        padding-top:7px;
        padding-bottom:7px;
        background-color:#858585;
    }
    

    .hovertableproductgroup:hover {
         background-color:#ACE7FF  ; 
    }
    .hovertableproductgroup{
        padding-top:7px;
        padding-bottom:7px;
        background-color:#2CC2FF;
    }
    .modal-header .close {
        display:none;
        }
    </style>
</head>

<script>

function choosetransport(){
    $("#choosetransport").modal({backdrop: 'static', keyboard: false})  
      $("#choosetransport").modal('show');
      setTimeout(function(){$('#choosetransport').modal('hide')},1000);
    }
 function duplicateshipping(){
    $("#duplicateshipping").modal({backdrop: 'static', keyboard: false})  
      $("#duplicateshipping").modal('show');
      setTimeout(function(){$('#duplicateshipping').modal('hide')},2000);
    }
    function deletesuccess(){
        $("#deletesuccess").modal({backdrop: 'static', keyboard: false})  
      $("#deletesuccess").modal('show');
      setTimeout(function(){$('#deletesuccess').modal('hide')},2000);
    }
    function cantdelete(){
        $("#cantdelete").modal({backdrop: 'static', keyboard: false})  
      $("#cantdelete").modal('show');
      setTimeout(function(){$('#cantdelete').modal('hide')},2000);
    }
 function updatesuccess(){
    $("#updatesuccess").modal({backdrop: 'static', keyboard: false})  
      $("#updatesuccess").modal('show');
      setTimeout(function(){$('#updatesuccess').modal('hide')},1000);
    }

  function logoutsuccess(){
    $("#logoutsuccess").modal({backdrop: 'static', keyboard: false})  
      $("#logoutsuccess").modal('show');
      setTimeout(function(){$('#logoutsuccess').modal('hide')},2000);
    }

    function insertsuccess(){
        $("#insertsuccess").modal({backdrop: 'static', keyboard: false})  
      $("#insertsuccess").modal('show');
      setTimeout(function(){$('#insertsuccess').modal('hide')},1000);
    }
    
function encodeImageFileAsURL(element) {
  var file = element.files[0];
  var reader = new FileReader();
  reader.onloadend = function() {
   // console.log(reader.result);
  }
  reader.readAsDataURL(file);

  return reader;
}
    function cancelUpdateStoreInfo() {
      location.reload();
    }

    function logoutFunction() {
           logoutsuccess();
                   setTimeout(function(){ 
                    document.forms["formLogout"].action = "webservice/Logout.php";
                    document.forms["formLogout"].submit();
                   },2600);
    }
</script>

<body>
    <nav class="navbar navbar-expand-sm">
        <a class="navbar-brand" style="font-family:'KRR_AengAei.ttf'!important; color: #FFA200;font-size:35px;">
            <i class="fas fa-dolly"></i>ผลิตภัณฑ์ชุมชน
        </a>
        <ul class="navbar-nav mr-auto"></ul>
        <ul class="navbar-nav">
            <?php
                echo "
                    <form id=\"formLogout\" name=\"formLogout\">
                        <a class=\"nav-link\" style=\"cursor:pointer\" onclick=\"logoutFunction()\"><i class=\"fas fa-sign-out-alt\"></i>ออกจากระบบ</a>
                    </form>
                ";
            ?>
        </ul>
    </nav>

    <!-- Sidebar  -->
    <div class="wrapper">
        
        <nav id="sidebar">
            <div class="sidebar-header">
                <h4>หน้าจัดการร้านค้า</h4>
                <strong>BS</strong>
            </div>

            <ul class="list-unstyled components">
                <li >
                    <a href="Store.php">
                        <i class="fas fa-briefcase"></i>
                        ข้อมูลร้านค้า
                    </a>
                </li>
           
                <li >
                    <a href="StoreProductView.php">
                        <i class="fas fa-box"></i>
                        เพิ่มสินค้า
                    </a>
                </li>
<li>
                    <a href="StoreMyproduct.php">
                        <i class="fas fa-box"></i>
                      สินค้าของฉัน
                    </a>
                </li>

                <li class="active">
                    <a href="StoreTransportView.php">
                        <i class="fas fa-box"></i>
                        จัดการช่องทางการจัดส่ง
                    </a>
                </li>
                <li>
                    <a href="StoreOrderview.php">
                        <i class="fas fa-box"></i>
                       ส่งสินค้า
                        <span id="badge_order_num" class="badge badge-light"></span>
                    </a>
                </li>
                <li>
                    <a href="StoreSell_list.php">
                        <i class="fas fa-box"></i>
                        รายรับของฉัน
                        <span id="badge_order_num" class="badge badge-light"></span>
                    </a>
                </li>
                <li >
                <a href="StoreWithdraw.php">
                        <i class="fas fa-box"></i>
                        ถอนเงิน
                        <span id="badge_order_num" class="badge badge-light"></span>
                    </a>
                </li>


                
            </ul>
        </nav>

        <script>

setInterval(function(){ 
    var store_code ="<?php echo @$_SESSION['store_code'];?>";

            if(store_code!=""){ //ถ้า login
                $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: 'application/json',
                        async : false,
                        url: "../restapi/getordernumstore.php",
                        data:JSON.stringify({
                            store_code:store_code
                        }),
                        success: function (response) {
                        var json_data = response;
                        var getdata = json_data;   
                document.getElementById("badge_order_num").innerHTML = getdata.ordernum;
                        }
                    }); 
                }
        }, 500);
        </script>

        <!-- Page Content  -->
        <div id="content">

            <div class="container-fluid" style="background-color:#DDDDDD;padding-top:10px">

            <div class="container" style="background-color:White;padding-top:10px">
        

            <div class="row">
                        <div class="col-md-9">
                        </div>
                        <div class="col-md-3">
                        <label style="text-align: right;" data-toggle="modal" data-target="#addshipping" ><h4 style="cursor:pointer;color:#FF6C1D;"  onmouseover="this.style.color='black'"  onmouseout="this.style.color='#FF6C1D'">เพิ่มช่องทางการจัดส่ง</h4></label>
                        </div>
                </div>

            <table class="table">
                    <thead align="center">
                        <tr style="font-weight:bold;" bgcolor="#DDDDDD">
                            <th>ชื่อขนส่ง</th>
                            <th>ราคา</th>
                            <th id="size">แก้ไข</th>
                            <th id="size">ลบ</th>
                        </tr>
                    </thead>
<style>
tr#dataproduct{
    background-color: white;
    cursor: pointer;
    height: 30px;

}
tr#dataproduct:hover{
    background-color:#FFDCB9 ;
    cursor: pointer;

}
</style>
                    <tbody> 
                        <?php
                      
                            $getshippinglist =  "SELECT * FROM tbl_shipping 
                            INNER JOIN tbl_transport ON tbl_shipping.transport_code = tbl_transport.transport_code
                            WHERE store_code = '".$_SESSION['store_code']."' ";
                            $resultgetshippinglist = @$conn->query($getshippinglist);
                            if($resultgetshippinglist->num_rows > 0){
                                while($rowgetshippinglist = $resultgetshippinglist->fetch_assoc()){
                                  
                        ?>
                        <tr id="dataproduct" align="center">
                            <td><?php echo $rowgetshippinglist['transport_name']; ?> </td>
                            <td><?php echo $rowgetshippinglist['shipping_price']; ?> </td>
                            <td>
                                <ul id="element2">
                                    <a id="b" class="nav-link" data-toggle="modal" data-target="#deleteCustomerModal" style="cursor:pointer; color: orange;"
                                    transport_code="<?php echo $rowgetshippinglist['transport_code'];?>"
                                        onclick="edittransport(this)"
                                    ><i class="fas fa-edit"></i></a>
                                </ul>
                            </td>
                            <td>
                                <ul id="element2">
                                    <a id="b" class="nav-link" data-toggle="modal" data-target="#deleteCustomerModal" style="cursor:pointer; color: red;"
                                    shipping_code="<?php echo $rowgetshippinglist['shipping_code'];?>"
                                        onclick="deleteshipping(this)"
                                    ><i class="fas fa-trash"></i></a>
                                </ul>
                            </td>
                        </tr>
                        <?php
                                }
                            }else{
                                echo "<tr align=\"center\">  
                                <td colspan=\"4\" >ไม่มีข้อมูลช่องทางการจัดส่ง กรุณากด 'เพิ่มช่องทางการจัดส่ง' </td>
                                
                                </tr>";
                            }
                        ?>
                    </tbody>
                </table>

                <br>
            </div>
            <br><br>
        </div>
        
        </div>
        <script>
function deleteshipping(obj){
    var shipping_code = obj.getAttribute("shipping_code");
   
         
            $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/deleteshippingfromstore.php",
            data:JSON.stringify({
                shipping_code:shipping_code
            }),
            success: function (response) {
            var json_data = response;
            var apistatus = json_data.result;

            if(apistatus=="Success") {
                    deletesuccess();
                    setTimeout(function(){ 
                    location.reload();
                    },2600);
                    }else{
                        cantdelete();
                        setTimeout(function(){ 
                        location.reload();
                        },2600);
                    }
          
            }
            });
            
}
        </script>
        <!-- END Content  -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
    <!-- End of Sidebar  -->

            <!-- Modal LOGOUT-->
            <div class="modal fade" id="logoutsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/logout.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;font-family:'Opun-Regular.ttf'!important;font-size:20px;" class="modalfont">กำลังออกจากระบบ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

      <!-- Modal UPDATESUCCESS-->
      <div class="modal fade" id="updatesuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;font-family:'Opun-Regular.ttf'!important;font-size:20px;" class="modalfont">แก้ไขข้อมูลเรียบร้อย</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

           <!-- Modal alertexception-->
           <div class="modal fade" id="alertexception" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;font-family:'Opun-Regular.ttf'!important;font-size:20px;" class="modalfont">เกิดข้อผิดพลาดกรุณาลองอีกครั้ง</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

 <!-- Modal AddataSUCCESS-->
 <div class="modal fade" id="insertsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <span style="color:black;font-family:'Opun-Regular.ttf'!important;font-size:20px;" class="modalfont">เพิ่มข้อมูลสำเร็จ</span><span id="insertsuccesshtml"></span>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->


            <!-- Modal DELETESUCCESS-->
 <div class="modal fade" id="deletesuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;font-family:'Opun-Regular.ttf'!important;font-size:20px;" class="modalfont">ลบข้อมูลสำเร็จ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->
           <!-- Modal DUPLICATE_TRANSPOT-->
           <div class="modal fade" id="duplicateshipping" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;font-family:'Opun-Regular.ttf'!important;font-size:20px;" class="modalfont">ขนส่งที่ท่านเลือก ท่านได้เลือกก่อนหน้านี้แล้ว</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

    <!-- Modal -->
           <!-- Modal choosetransport-->
           <div class="modal fade" id="choosetransport" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;font-family:'Opun-Regular.ttf'!important;font-size:20px;" class="modalfont">กรุณาเลือกขนส่ง</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modalchoosetransport -->

<!--  addshipping Modal -->
<div class="modal" id="addshipping">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">เพิ่มข้อมูลการจัดส่ง</h2>
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">ชื่อขนส่ง : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <select id="transport_code" name="transport_code">
                                <option hidden>กรุณาเลือกขนส่ง</option>
                                <?php  
                                $strgettransport ="SELECT * FROM tbl_transport ORDER BY transport_no ";
                                $resultstrgettransport = @$conn->query($strgettransport);
                                if($resultstrgettransport->num_rows > 0){
                                    while($rowstrgettransport = $resultstrgettransport->fetch_assoc()){
                                ?>

                                <option value="<?php echo $rowstrgettransport['transport_code']; ?>">
                                <?php echo " ชื่อขนส่ง :".$rowstrgettransport['transport_name']; ?>
                                </option>

                                    <?php }} 
                                    ?>
                                </select>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">กำหนดราคาในการจัดส่ง : </div>
                            <div class="col-sm-3 d-inline-flex">
                            <script >
                            function chkNumber(ele)
                            {
                            var vchar = String.fromCharCode(event.keyCode);
                            if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
                            ele.onKeyPress=vchar;
                            }
                        </script>
                               <input maxlength="8" id="shipping_price" name="shipping_price" OnKeyPress="return chkNumber(this)" placeholder="0.00">
                            </div>
                            <div class="col-sm-2" align="left">บาท</div>
                        </div>

                        </div></div>
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-success btn-sm" data-dismiss="modal" onclick="addshipping()">ยืนยัน</a>
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    <!-- End of addshipping Modal -->
<script>
function addshipping(){
    var transport_code = document.getElementById("transport_code").value;
    var shipping_price = document.getElementById("shipping_price").value;
    var store_code ="<?php echo $_SESSION['store_code']?>";
    console.log(transport_code+store_code);
if(transport_code=="กรุณาเลือกขนส่ง"){
    
    choosetransport();
                    setTimeout(function(){ 
                  
                    },1600);

}else{
    $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/addshippingfromstore.php",
                data:JSON.stringify({
                    transport_code:transport_code,
                    shipping_price:shipping_price,
                    store_code:store_code
                }),
                success: function (response) {
                var json_data = response;
                console.log(json_data.result);
                if(json_data.result=="Success"){
                    $('#addshipping').modal('hide');
                    insertsuccess();
                    setTimeout(function(){ 
                    location.reload();
                    },1600);

                }else if(json_data.result=="DUPLICATE_TRANSPORT"){
                    $('#addshipping').modal('hide');
                    duplicateshipping();
                    setTimeout(function(){ 
                    location.reload();
                    },2600);

                }else{
                    $('#addshipping').modal('hide');
                    alertexception();
                    setTimeout(function(){
                        location.reload();
                    },2600);
                    }
                }
            });
}
    

}
</script>
<!-- Modal CANTDELETE-->
<div class="modal fade" id="cantdelete" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;font-family:'Opun-Regular.ttf'!important;font-size:20px;">ไม่สามารถลบข้อมูลได้เนื่องจากมีการใช้ข้อมูลอยู่</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->
</body>

</html>