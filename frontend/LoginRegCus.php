<?php
    include "../restapi/setting/config.php";
    @session_start();
    @session_cache_expire(30);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>เข้าสู่ระบบสำหรับลูกค้า</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="./css/styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }

        .card {
            background-color: #F88360;  
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }

        .card:hover{
            background-color: #F88360;  
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
            border:2px solid #FF5F0F;
        }

        #rcorners {
            border-radius: 3px;
            border: 1.5px solid #D1D1D1;
            padding: 10px;
            width: 80%;
            -webkit-box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
            -moz-box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
            box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
            background-color: white;
        }
        form {
            width: 100%;
            max-width: 500px;
            margin: 0 auto;
            outline: 1px solid lightgrey;
            padding: 10px;
        }

        label, input[type='text'], input[type='password'] {
            font-size: 12pt;
            padding: 8px;
        }

        label {
            color: grey;
        }

        input {
            border: none;
            outline: none;
            border-bottom: 1px solid grey;
        }
        .modal-header .close {
        display:none;
        }
    </style>
    <script>
        function hasbanned() {
        $("#hasbanned").modal({backdrop: 'static', keyboard: false})  
            $("#hasbanned").modal('show');
            setTimeout(function(){$('#hasbanned').modal('hide')},3000);
        }
        function loginfail() {
            $("#loginfail").modal({backdrop: 'static', keyboard: false})  
            $("#loginfail").modal('show');
            setTimeout(function(){$('#loginfail').modal('hide')},3000);
        }

        function loginsuccess() {
            $("#loginsuccess").modal({backdrop: 'static', keyboard: false})  
            $("#loginsuccess").modal('show');
            setTimeout(function(){$('#loginsuccess').modal('hide')},3000);
        }

        function logoutsuccess() {
            $("#logoutsuccess").modal({backdrop: 'static', keyboard: false})  
            $("#logoutsuccess").modal('show');
            setTimeout(function(){$('#logoutsuccess').modal('hide')},2000);
        }
    
        function customerLoginFunction() {
        var customerUsername = document.getElementById("customerUsername").value;
        var customerPassword = document.getElementById("customerPassword").value;

        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/logincustomer.php",
            data:JSON.stringify({
                customerUsername:customerUsername,
                customerPassword:customerPassword
            }),
            success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;

                if(apistatus=="Success") {
                loginsuccess();
                setTimeout(function(){ 
                    location.replace("index.php");
                },3600);
                } else if(apistatus=="Banned"){
                    hasbanned();
                    setTimeout(function(){ 
                    location.reload();
                    },3600);
                } else{
                    loginfail();
                    setTimeout(function(){ 
                    location.reload();
                    },3600);
                }
            }
        });
    }
    </script>
</head>
<body>
    <div>
        <nav class="navbar navbar-expand-sm">
            <a class="navbar-brand" style="font-family:'KRR_AengAei.ttf'!important; color: #FFA200;font-size:35px;" href="index.php">
                <i class="fas fa-dolly"></i>ผลิตภัณฑ์ชุมชน
            </a>
            <ul class="navbar-nav mr-auto"></ul>
            <ul class="navbar-nav">
            </ul>
        </nav>
        <div style="padding-top: 130px; background-color: #FE914E; height:578px;">
            <div class="col-md-12" align="center">
                <div class="container" style="margin-top: -70px;" align="center">
                    <div id="rcorners">
                        <img src="./icons/loginlogo.png" width="120px" height="120px" style="margin-top:30px; margin-bottom:30px;">
                        <h4 style="padding-top:20px; padding-bottom:20px;">เข้าสู่ระบบสำหรับลูกค้า</h4>
                   
                            <label for="customerUsername">ชื่อผู้ใช้</label>
                            <input id="customerUsername" name="customerUsername" type="text"/>
                            <br/>

                            <label for="customerPassword">รหัสผ่าน</label>
                            <input id="customerPassword" name="customerPassword" type="password"/>
                            <br/>
                            <br/>

                            <a class="btn btn-success" onclick="customerLoginFunction()">เข้าสู่ระบบ</a><br><br>
                     
                        เพิ่งเคยเข้ามาใน OTOPMALL ใช่หรือไม่<span style="color:blue;cursor:pointer;" ><a href="CustomerRegister.php">สมัครใหม่</a></span>
                    </div>
         
                </div>
            </div>

        </div>
    </div>


    
  <!-- Modal SUCCESS-->
  <div class="modal fade" id="loginsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p>เข้าสู่ระบบสำเร็จ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

    <!-- Modal ERROR-->
    <div class="modal fade" id="loginfail" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/wrong.png" width="150px" height="150px">
        <br><br>
        <p>ชื่อผู้ใช้หรือรหัสผ่านของคุณผิดพลาด</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->
  <!-- Modal BANNED-->
  <div class="modal fade" id="hasbanned" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/banned.png" width="150px" height="150px">
        <br><br>
        <p>รหัสผู้ใช้ของท่านโดน แบน กรุณาติดต่อ ผู้ดูแลระบบ ด้วยค่ะ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->
</body>

<footer style="background-color: #575757;">
        <nav class="navbar navbar-expand-sm" style="display: flex;justify-content: center;">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <label style="color: #FFA200;font-size: 14px;">&copy;&nbsp;2020 localshopcenter.com</label>
                </li>
            </ul>
        </nav>
</footer>
</html>