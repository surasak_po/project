<?php
     include "../restapi/setting/config.php";
    @session_start();
    @session_cache_expire(30);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ข้อมูลส่วนตัวลูกค้า</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="./css/styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

<!-- Toggle Switch-->

<style>

.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<!-- Toggle Switch -->
    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }

        .row {
            margin-top: 8px;
        }

        p.round {
            border: 3px solid orangered;
            border-radius: 8px;
            height: 40px;
            padding-top: 5px;
            padding-left: 3px;
        }

        div ul#uif li a:hover {
            color: rgb(196, 46, 0) !important;
        }
        .modal-header .close {
        display:none;
        }
    </style>
</head>
<script>

  function alertexception() {
    $("#alertexception").modal({backdrop: 'static', keyboard: false})  
        $("#alertexception").modal('show');
        setTimeout(function(){$('#alertexception').modal('hide')},2000);
    }
   function alerttel() {
    $("#alerttel").modal({backdrop: 'static', keyboard: false})  
        $("#alerttel").modal('show');
        setTimeout(function(){$('#alerttel').modal('hide')},2000);
    }
    function alertmail() {
        $("#alertmail").modal({backdrop: 'static', keyboard: false})  
        $("#alertmail").modal('show');
        setTimeout(function(){$('#alertmail').modal('hide')},2000);
    }
 function deletesuccess(){
    $("#deletesuccess").modal({backdrop: 'static', keyboard: false})  
      $("#deletesuccess").modal('show');
      setTimeout(function(){$('#deletesuccess').modal('hide')},2000);
    }
 function cantdelete(){
    $("#cantdelete").modal({backdrop: 'static', keyboard: false})  
      $("#cantdelete").modal('show');
      setTimeout(function(){$('#cantdelete').modal('hide')},2000);
    }

 function insertsuccess(){
    $("#insertsuccess").modal({backdrop: 'static', keyboard: false})  
      $("#insertsuccess").modal('show');
      setTimeout(function(){$('#insertsuccess').modal('hide')},1000);
    }

function updatecustomeraddressstatus(obj){
    var customer_address_code =obj.getAttribute("customer_address_code");
    var customer_code ="<?php echo $_SESSION['customer_code'];?>";
    //console.log(customer_code+customer_address_code);
                     $.ajax({
                                            type: "POST",
                                            dataType: 'json',
                                            contentType: 'application/json',
                                            async : false,
                                            url: "../restapi/updatecustomeraddressstatus.php",
                                            data:JSON.stringify({
                                                customer_code:customer_code,
                                                customer_address_code:customer_address_code
                                            }),
                                            success: function (response) {
                                                var json_data = response;
                                                var apistatus = json_data.result;
                                                console.log(apistatus);
                                                if(apistatus=="Success") {
                                                    updatesuccess();
                                                    setTimeout(function(){ 
                                                        location.reload();
                                                    },1600);
                                                }
                                            }
                                            });
}

function encodeImageFileAsURL(element) {
  var file = element.files[0];
  var reader = new FileReader();
  reader.onloadend = function() {
   // console.log(reader.result);
  }
  reader.readAsDataURL(file);

  return reader;
}
  function updatesuccess(){
      $("#updatesuccess").modal('show');
      setTimeout(function(){$('#updatesuccess').modal('hide')},1000);
    }
  function logoutsuccess(){
      $("#logoutsuccess").modal('show');
      setTimeout(function(){$('#logoutsuccess').modal('hide')},2000);
    }

    var loadFile = function(event) {
        var output = document.getElementById('showuploadimage');
        output.src = URL.createObjectURL(event.target.files[0]);
    };

    function updateCustomerInfo() { 
        var button = document.getElementById("updateCustomerInfoBtn").innerHTML;
        if(button == "แก้ไข"){
            document.getElementById("customer_profile").disabled = false;
            document.getElementById("customer_tel").disabled = false;
            document.getElementById("customer_email").disabled = false;
            document.getElementById("customer_password").disabled = false;
            document.getElementById("customer_fullname").disabled = false;
            document.getElementById('updateCustomerInfoBtn').classList.remove('btn-outline-warning');
            document.getElementById('updateCustomerInfoBtn').classList.add('btn-outline-success');
            document.getElementById("updateCustomerInfoBtn").innerHTML = "ยืนยัน";
        }
        if(button == "ยืนยัน"){
            if(document.formCustomerInfo.customer_tel.value == "") {
                alert('กรุณากรอกเบอร์โทรศัพท์ของท่าน');
                document.formCustomerInfo.customer_tel.focus();		
                return false;
            } else if (document.formCustomerInfo.customer_email.value == "") {
                alert('กรุณากรอกอีเมลล์ของท่าน');
                document.formCustomerInfo.customer_email.focus();		
                return false;
            } else if (document.formCustomerInfo.customer_password.value == "") {
                alert('กรุณากรอกรหัสผ่านของท่าน');
                document.formCustomerInfo.customer_password.focus();		
                return false;
            } else if (document.formCustomerInfo.customer_fullname.value == "") {
                alert('กรุณากรอกชื่อ - นามสกุลของท่าน');
                document.formCustomerInfo.customer_fullname.focus();		
                return false;
            } else {
                            customer_code="<?php echo $_SESSION['customer_code']; ?>";
                            customer_fullname=document.getElementById("customer_fullname").value;
                            customer_email=document.getElementById("customer_email").value;
                            customer_tel=document.getElementById("customer_tel").value;
                            customer_password=document.getElementById("customer_password").value;
                            customer_profile=document.getElementById("customer_profile").value;
                        
                        if(customer_profile == ""){
                                    $.ajax({
                                            type: "POST",
                                            dataType: 'json',
                                            contentType: 'application/json',
                                            async : false,
                                            url: "../restapi/updatecustomer.php",
                                            data:JSON.stringify({
                                                customer_code:customer_code,
                                                customer_fullname:customer_fullname,
                                                customer_email:customer_email,
                                                customer_tel:customer_tel,
                                                customer_password:customer_password
                                            }),
                                            success: function (response) {
                                                var json_data = response;
                                                var apistatus = json_data.result;
                                                if(apistatus=="Success") {
                                                    updatesuccess();
                                                    setTimeout(function(){ 
                                                        location.reload();
                                                    },1600);
                                                }else if(apistatus=="EMAIL_DUPLICATE"){
                                                    alertmail();
                                                    setTimeout(function(){},2600);
                                                }else if(apistatus=="TEL_DUPLICATE"){
                                                    alerttel();
                                                    setTimeout(function(){ },2600);
                                                }else{
                                                    alertexception();
                                                    setTimeout(function(){ },2600);
                                                }
                                            }
                                            });

                        }else{
                            var base64 = encodeImageFileAsURL(document.getElementById('customer_profile'));
                            base64.onloadend = function() {
                            var imagebase64 = base64.result;

                                                $.ajax({
                                            type: "POST",
                                            dataType: 'json',
                                            contentType: 'application/json',
                                            async : false,
                                            url: "../restapi/updatecustomer.php",
                                            data:JSON.stringify({
                                                customer_code:customer_code,
                                                customer_fullname:customer_fullname,
                                                customer_email:customer_email,
                                                customer_tel:customer_tel,
                                                customer_password:customer_password,
                                                customer_profile:imagebase64
                                            }),
                                            success: function (response) {
                                                var json_data = response;
                                                var apistatus = json_data.result;
                                                if(apistatus=="Success") {
                                                    updatesuccess();
                                                    setTimeout(function(){ 
                                                        location.reload();
                                                    },2600);
                                                }else if(apistatus=="EMAIL_DUPLICATE"){
                                                    alertmail();
                                                    setTimeout(function(){},2600);
                                                }else if(apistatus=="TEL_DUPLICATE"){
                                                    alerttel();
                                                    setTimeout(function(){ },2600);
                                                }else{
                                                    alertexception();
                                                    setTimeout(function(){ },2600);
                                                }
                                            }
                                            });
                           }

                        }

            }
        }
    }

    function cancelUpdateCustomerInfo() {
        location.reload();
    }

    function updateCustomerAddress() { 
        var button = document.getElementById("updateCustomerAddressBtn").innerHTML;
        if(button == "แก้ไข"){
            document.getElementById("customerAddressTel").disabled = false;
            document.getElementById("customerAddressFullname").disabled = false;
            document.getElementById("customerAddressDistrict").disabled = false;
            document.getElementById("customerAddressPostcode").disabled = false;
            document.getElementById("customerAddressProvince").disabled = false;
            document.getElementById("customerAddressDetail").disabled = false;
            document.getElementById('updateCustomerAddressBtn').classList.remove('btn-outline-warning');
            document.getElementById('updateCustomerAddressBtn').classList.add('btn-outline-success');
            document.getElementById("updateCustomerAddressBtn").innerHTML = "ยืนยัน";
        }
        if(button == "ยืนยัน"){
            if(document.formCustomerInfo.customerTel.value == "") {
                alert('กรุณากรอกเบอร์โทรศัพท์ของท่าน');
                document.formCustomerInfo.customerTel.focus();		
                return false;
            } else if (document.formCustomerInfo.customerEmail.value == "") {
                alert('กรุณากรอกอีเมลล์ของท่าน');
                document.formCustomerInfo.customerEmail.focus();		
                return false;
            } else if (document.formCustomerInfo.customerPassword.value == "") {
                alert('กรุณากรอกรหัสผ่านของท่าน');
                document.formCustomerInfo.customerPassword.focus();		
                return false;
            } else if (document.customerInfo.customerFullname.value == "") {
                alert('กรุณากรอกชื่อ - นามสกุลของท่าน');
                document.formCustomerInfo.customerFullname.focus();		
                return false;
            } else {
                document.forms["formCustomerAddress"].action = "webservice/UpdateCustomerAddress.php";
                document.forms["formCustomerAddress"].submit();
            }
        }
    }

    function cancelUpdateCustomerAddress() {
        var button = document.getElementById("updateCustomerAddressBtn").innerHTML;
        if(button == "ยืนยัน"){
            document.getElementById("customerAddressTel").disabled = true;
            document.getElementById("customerAddressFullname").disabled = true;
            document.getElementById("customerAddressDistrict").disabled = true;
            document.getElementById("customerAddressPostcode").disabled = true;
            document.getElementById("customerAddressProvince").disabled = true;
            document.getElementById("customerAddressDetail").disabled = true;
            document.getElementById('updateCustomerAddressBtn').classList.remove('btn-outline-success');
            document.getElementById('updateCustomerAddressBtn').classList.add('btn-outline-warning');
            document.getElementById("updateCustomerAddressBtn").innerHTML = "แก้ไข";
        }
    }

    function logoutFunction() {
                 logoutsuccess();
                   setTimeout(function(){ 
                    document.forms["formLogout"].action = "webservice/Logout.php";
                    document.forms["formLogout"].submit();
                   },2600);
    }
</script>

<body>
    <nav class="navbar navbar-expand-sm">
        <a class="navbar-brand" href="index.php" style="font-family: 'KRR_AengAei.ttf' !important; color: #FFA200;font-size:35px;">
            <i class="fas fa-dolly"></i>ผลิตภัณฑ์ชุมชน
        </a>
        <ul class="navbar-nav mr-auto"></ul>
        <ul class="navbar-nav">
            <?php
            if(@$_SESSION['role'] == "customer") {
                $customerCode = $_SESSION['customer_code'];
                $strSQL = "SELECT * FROM tbl_customer WHERE customer_code = '".$customerCode."' ";
                $result = @$conn->query($strSQL);
                while($row = $result->fetch_assoc()){
                    echo "
                        <li class=\"nav-item\">
                            <a style=\"color: White !important;,font-size: 14px !important;\" class=\"nav-link\"><i class=\"fas fa-user\"></i>ข้อมูลส่วนตัว</a>
                        </li>
                        <li class=\"nav-item\">
                            <a style=\"color: #FFA200 !important;,font-size: 14px !important;\" class=\"nav-link\">ยินดีต้อนรับ&nbspคุณ&nbsp :&nbsp " . @$row['customer_fullname'] . "</a>
                        </li>
                        <form id=\"formLogout\" name=\"formLogout\">
                            <li class=\"nav-item\">
                                <a class=\"nav-link\" style=\"cursor:pointer\" onclick=\"logoutFunction()\"><i class=\"fas fa-sign-out-alt\"></i>ออกจากระบบ</a>
                            </li>
                        </form>
                    ";
                }
            }
            ?>
        </ul>
    </nav>

    <div style="padding-top: 5px; background-color: #D8D8D4;">
        <ul id="menu" align="center">
            <b style="color: Blue;">จัดการข้อมูลส่วนตัว</b>
        </ul>
    </div>

    <div class="container">
        <div>
            <img src="image/banner2.png" width="100%" style="margin-top:-16px;" />
        </div>
        <br><br>
        
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist" id="uif">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#customerInfo">ข้อมูลส่วนตัว</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#customerAddress">ที่อยู่ของฉัน</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#waitingPayment">สินค้าที่ต้องชำระ</a>
            </li>
        </ul>
      

            <div id="customerAddress" class="container tab-pane fade"><br>
            <div class="row"> 
                    <div class="col-md-10"> 
                    <h3>ที่อยู่ของฉัน</h3>
                    </div> 
                    <div class="col-md-2"> 
                    <h3 class="nav-link" data-toggle="modal"  data-target="#customeraddaddress" style="cursor:pointer;color:#2DFF1C;"  onmouseover="this.style.color='black'"  onmouseout="this.style.color='#2DFF1C'">เพิ่ม +</h3>
                    </div>
            </div>
                
                <form name="formCustomerAddress" id="formCustomerAddress">

        <?php
            $querycustomeraddress = "SELECT * FROM tbl_customer_address WHERE customer_code = '".$_SESSION['customer_code']."' ORDER BY customer_address_status DESC";
            $resultquerycustomeraddress = @$conn->query($querycustomeraddress);
            if($resultquerycustomeraddress->num_rows > 0){
            while($rowquerycustomeraddress = $resultquerycustomeraddress->fetch_assoc()){
        ?>
                    <div class="row" style="margin-left: 20px;margin-right: 80px;background-color:#E1E1E1 ;">
                    
                    <div class="col-sm-11"> ตั้งที่อยู่หลัก :
                    <?php if($rowquerycustomeraddress['customer_address_status']=="1"){
                        echo "
                        <label class=\"switch\">
                        <input type=\"checkbox\" checked disabled>
                        <span class=\"slider round\"></span>
                        </label>
                        ";
                    }else{
                        echo "
                        <label class=\"switch\">
                        <input type=\"checkbox\" customer_address_code=".$rowquerycustomeraddress['customer_address_code']." onclick=\"updatecustomeraddressstatus(this)\">
                        <span class=\"slider round\"></span>
                        </label>
                        ";
                    }
                    
                    ?> 
                    </div>
                    <div class="col-sm-1"> 
                    <span style="text-align: left; color:red;cursor: pointer;" 
                    customer_address_code = "<?php echo $rowquerycustomeraddress['customer_address_code']; ?>"
                    onclick="deletecustomeraddress(this)">ลบ</span>  
                       </div>
                       <script>
                           function deletecustomeraddress(obj){

                                var customer_address_code = obj.getAttribute("customer_address_code");
                                console.log(customer_address_code);
                                $.ajax({
                                    type: "POST",
                                    dataType: 'json',
                                    contentType: 'application/json',
                                    async : false,
                                    url: "../restapi/deletecustomeraddress.php",
                                    data:JSON.stringify({
                                        customer_address_code:customer_address_code
                                    }),
                                    success: function (response) {
                                        var json_data = response.result;
                                        if(json_data=="Success") {
                                            deletesuccess()
                                        setTimeout(function(){ 
                                            location.reload();
                                        },2600);
                                            }
                                            if(json_data=="IN_USE") {cantdelete() }
                                        }
                                    });
                                }
                       </script>
                    
                        <div class="col-sm-5">ชื่อ-นามสกุล : </div>
                        <div class="col-sm-7 d-inline-flex"><?php echo $rowquerycustomeraddress['customer_address_fullname']; ?></div>

                        <div class="col-sm-5">เบอร์โทรศัพท์ : </div>
                        <div class="col-sm-7 d-inline-flex"><?php echo $rowquerycustomeraddress['customer_address_tel']; ?></div>

                        <div class="col-sm-5">จังหวัด : </div>
                        <div class="col-sm-7 d-inline-flex"><?php echo $rowquerycustomeraddress['customer_address_province']; ?></div>

                        <div class="col-sm-5">เขต/อำเภอ : </div>
                        <div class="col-sm-7 d-inline-flex"><?php echo $rowquerycustomeraddress['customer_address_district']; ?></div>

                        <div class="col-sm-5">ตำบล : </div>
                        <div class="col-sm-7 d-inline-flex"><?php echo $rowquerycustomeraddress['customer_address_subdistrict']; ?></div>

                        <div class="col-sm-5">รหัสไปรษณีย์ : </div>
                        <div class="col-sm-7 d-inline-flex"><?php echo $rowquerycustomeraddress['customer_address_postcode']; ?></div>

                        <div class="col-sm-5">รายละเอียด (บ้านเลขที่ หมู่ หมู่บ้าน ถนน ซอย) : </div>
                        <div class="col-sm-7 d-inline-flex"><?php echo $rowquerycustomeraddress['customer_address_detail']; ?></div>
                    </div>
        <?php
            }
        }else{

        
        ?>
                <div class="row" style="margin-left: 20px;margin-right: 80px;background-color:#E1E1E1 ;" align="center">
                <div class="col-sm-12">  ไม่พบที่อยู่ ต้องการเพิ่มที่อยู่ กรุณากด "เพิ่ม+" มุมด้านขวา</div>
                      
                    </div>

        <?php
            }
        ?>

                </form>
            </div>

            <div id="waitingPayment" class="container tab-pane"><br>
            <div class="container">
                <div id="rcorners">
<!-- เริ่ม -->
<?php 
$strgetorder="SELECT * FROM tbl_payment
INNER JOIN tbl_order ON tbl_payment.order_code = tbl_order.order_code
INNER JOIN tbl_store ON tbl_order.store_code = tbl_store.store_code
INNER JOIN tbl_shipping ON tbl_order.shipping_code = tbl_shipping.shipping_code
INNER JOIN tbl_transport ON tbl_shipping.transport_code = tbl_transport.transport_code
WHERE tbl_order.customer_code = '".$_SESSION['customer_code']."' AND order_status = '0'
AND  tbl_payment.customer_code = '".$_SESSION['customer_code']."' ";
     $resultstrgetorder = @$conn->query($strgetorder);
     if(@$resultstrgetorder->num_rows >0){
         while($rowstrgetorder = $resultstrgetorder->fetch_assoc()){

            $order_code =$rowstrgetorder['order_code'];
            if($rowstrgetorder['payment_status']== "1"){
                    $updateorder="UPDATE tbl_order SET order_status='1' WHERE order_code = '$order_code' ";
                    $conn->query($updateorder);
            }
            if($rowstrgetorder['payment_status'] == "2" or $rowstrgetorder['payment_status'] == "0" ){
                $updateorder="UPDATE tbl_order SET order_status='0' WHERE order_code = '$order_code' ";
                $conn->query($updateorder);
            }
?>
                    <div class="row">
                        <div class="col-sm-4">
                            <img src="image/alert.png" style="margin-left:7px;" width="20px" height="20px">
                            <label style="font-size:14px; margin-left:7px;">ชื่อร้านค้า : <span style="font-weight:bold;"><?php echo $rowstrgetorder['store_namestore'];?></span></label>
                        </div>
                        <div class="col-sm-4" align="left" >
                        
                        </div>
                        <div class="col-sm-4" align="right" >
                        การจัดส่ง : <span style="font-weight:bold;"><?php echo $rowstrgetorder['transport_name']." ค่าจัดส่ง  ฿".$rowstrgetorder['transport_price'] ?></span>
                        </div>
                    </div>
                    <div class="row" style="margin-top:-10px; margin-bottom:-10px;">
                        <hr width=90% color=#F2F2F2>
                    </div>

<!-- productloop -->
<?php 
$totalprice = $rowstrgetorder['transport_price'];
$strgetorderdetail="SELECT * FROM tbl_order_detail 
INNER JOIN tbl_product_group ON tbl_order_detail.product_group_code = tbl_product_group.product_group_code
INNER JOIN tbl_product ON tbl_product_group.product_code = tbl_product.product_code
WHERE order_code = '$order_code'  ";
     $resultstrgetorderdetail = @$conn->query($strgetorderdetail);
     if(@$resultstrgetorderdetail->num_rows >0){
         while($rowstrgetorderdetail = $resultstrgetorderdetail->fetch_assoc()){
?>
                    <div class="row">
                        <div class="col-sm-2" align="right">
                            <img src="../restapi/product/<?php echo $rowstrgetorderdetail['product_group_picture']; ?>" style="margin-left:50px;" width="120px" height="120px">
                        </div>
                        <div class="col-sm-6" align="left" style="margin-left:20px;">
                            <label style="font-size:20px">ชื่อสินค้า : <?php echo $rowstrgetorderdetail['product_name']; ?></label><br>
                            <label style="font-size:12px">ชื่อหมวด : <?php echo $rowstrgetorderdetail['product_group_name']; ?></label><br>
                            <label style="font-size:12px">จำนวน : <?php echo $rowstrgetorderdetail['order_detail_num']." ".$rowstrgetorderdetail['product_group_unit_name']; ?> </label>
                        </div>
                        <div class="col-sm-3" align="right">
                            <label> ฿ <?php 
                            $totalprice = $totalprice + ($rowstrgetorderdetail['order_detail_price']*$rowstrgetorderdetail['order_detail_num']);
                            echo $rowstrgetorderdetail['order_detail_price']*$rowstrgetorderdetail['order_detail_num']; ?></label>
                        </div>
                    </div>
                </div>
         <?php }}?>
<!-- productloop -->

                <div id="rcorners2">
                    <div class="row">
                        <div class="col-sm-12" align="right">
                            <label style="font-size:14px;">ยอดรวมคำสั่งซื้อทั้งหมด&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                            <label style="font-size:24px; color:orange;">฿ <?php echo $totalprice?></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6" align="left">
                            <label style="font-size:12px; margin-left:15px;">ยืนยันการรับสินค้าภายในวันที่ 27-08-2020 11:21</label>
                        </div>
                        <div class="col-sm-6" align="center">
                            <?php if($rowstrgetorder['order_status']=="1"){ ?>
                            <a href="#" class="btn btn-lg btn-success " style="font-size:13px;">ชำระเงินสำเร็จ</a>
                            <?php } ?>
                            <?php if($rowstrgetorder['order_status']=="0"){ ?>
                            <a href="#" class="btn btn-lg btn-warning " style="font-size:13px;">ชำระเงิน</a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
         <?php }} ?>
<!-- สุดสิ้นลูป -->
                
            </div>
        </div>
    </div>


        <br><br>
        </div>
   
    </div>


            <!-- Modal LOGOUT-->
            <div class="modal fade" id="logoutsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/logout.png" width="150px" height="150px">
        <br><br>
        <p>กำลังออกจากระบบ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->


    <!-- Modal UPDATESUCCESS-->
    <div class="modal fade" id="updatesuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p>แก้ไขข้อมูลเรียบร้อย</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

<!--  insertransport Modal -->
<div class="modal" id="customeraddaddress">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">เพิ่มข้อมูลที่อยู่</h2>
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">ชื่อ - นามสกุล : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="customer_address_fullname" name="customer_address_fullname"  >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">เบอรโทรศัพท์ : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" maxlenght="10" class="form-control form-control-sm" id="customer_address_tel" name="customer_address_tel"  >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">จังหวัด : </div>
                            <div class="col-sm-6 d-inline-flex">
                              
                              <select id="customer_address_province" name="customer_address_province" onchange="getprovincecode()">
                                   
                                    <option hidden>กรุณาเลือกจังหวัด</option>
                              <?php
                                   include "../restapi/setting/configthailand.php";
                                   $getprovince ="SELECT * FROM th_province ORDER BY name_th ASC";
                                   $resultgetprovince = @$conn->query($getprovince);
                                   if($resultgetprovince->num_rows > 0){
                                       while($rowgetprovince = $resultgetprovince->fetch_assoc()){
                              ?>
                                     <option value="<?php echo $rowgetprovince['name_th'].":".$rowgetprovince['id'] ?>">จังหวัด<?php echo $rowgetprovince['name_th'];  ?></option>
                              <?php
                                       }}
                              ?>
                              </select> 
                            
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">อำเภอ : </div>
                            <div class="col-sm-6 d-inline-flex">
                                   
                            <select id="customer_address_district" name="customer_address_district" Disabled onchange="getdistrictcode()">
                            <option hidden>กรุณาเลือกอำเภอ</option>
                            </select> 
                            
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">ตำบล : </div>
                            <div class="col-sm-6 d-inline-flex">
                                   
                            <select id="customer_address_subdistrict" name="customer_address_subdistrict" Disabled onchange="getamphurpostcode()">
                            <option hidden>กรุณาเลือกตำบล</option>
                            </select> 
                            
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">รหัสไปรษณีย์ : </div>
                            <div class="col-sm-6 d-inline-flex">
                               
                                <input type="text" class="form-control form-control-sm" id="customer_address_postcode" name="customer_address_postcode"  readonly>
                           
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">รายละเอียดที่อยู่ (บ้านเลขที่ หมู่บ้าน ถนน ซอย) : </div>
                            <div class="col-sm-6 d-inline-flex">
                               
                                <textarea  rows="4" class="form-control form-control-sm" id="customer_address_detail" name="customer_address_detail" >
                                </textarea>
                           
                            </div>
                        </div>

                        </div></div>
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-success btn-sm" data-dismiss="modal" onclick="insertaddress()">ยืนยัน</a>
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    <!-- End of insertransport Modal -->

    <script>

    function insertaddress(){
        var customer_code="<?php echo $_SESSION['customer_code']?>";
        var customer_address_fullname=document.getElementById("customer_address_fullname").value;
        var customer_address_tel=document.getElementById("customer_address_tel").value;
        
        var customer_address_province = document.getElementById("customer_address_province").value;
        var customer_address_province = customer_address_province.split(":");
        var customer_address_province = customer_address_province[0].trim();

        var customer_address_district = document.getElementById("customer_address_district").value;
        var customer_address_district = customer_address_district.split(":");
        var customer_address_district = customer_address_district[0].trim();

        var customer_address_subdistrict = document.getElementById("customer_address_subdistrict").value;
        var customer_address_subdistrict = customer_address_subdistrict.split(":");
        var customer_address_subdistrict = customer_address_subdistrict[0].trim();

        var customer_address_postcode = document.getElementById("customer_address_postcode").value;
        var customer_address_detail = document.getElementById("customer_address_detail").value.trim();
       

        $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/addcustomeraddress.php",
                data:JSON.stringify({
                    customer_code:customer_code,
                    customer_address_fullname:customer_address_fullname,
                    customer_address_tel:customer_address_tel,
                    customer_address_province:customer_address_province,
                    customer_address_district:customer_address_district,
                    customer_address_subdistrict:customer_address_subdistrict,
                    customer_address_postcode:customer_address_postcode,
                    customer_address_detail:customer_address_detail
                }),
                success: function (response) {
                var json_data = response;
                var getdata = json_data;
                console.log(getdata);
                if(getdata.result=="Success"){
                    insertsuccess();
                    setTimeout(function(){ 
                    location.reload();
                    },1600);
                }
                }
            });
    }
    
    function getdistrictcode(){
           document.getElementById("customer_address_postcode").value="";
           var districtcode=document.getElementById("customer_address_district").value;
           var districtcode = districtcode.split(":");
           console.log(districtcode[1]);
           customer_address_subdistrict="<option hidden>กรุณาเลือกตำบล</option>";
            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/getsubdistrictlist.php",
                data:JSON.stringify({
                    districtcode :districtcode[1] 
                }),
                success: function (response) {
    
                    $.each(response, function(index) {
                        customer_address_subdistrict += "<option value="+"  '"+response[index].TAMBON_NAME+":"+response[index].POST_CODE+"' "+">"+response[index].TAMBON_NAME+"</option>";
                    });
                    document.getElementById("customer_address_subdistrict").disabled = false;
                    $('#customer_address_subdistrict').html(customer_address_subdistrict);
                }
            });
    }

    function getprovincecode(){
            document.getElementById("customer_address_postcode").value="";
           var provincecode=document.getElementById("customer_address_province").value;
           var provincecode = provincecode.split(":");
           console.log(provincecode[1]);
           customer_address_district="<option hidden>กรุณาเลือกอำเภอ</option>";
           customer_address_subdistrict="<option hidden>กรุณาเลือกตำบล</option>";
           $('#customer_address_subdistrict').html(customer_address_subdistrict);
           document.getElementById("customer_address_subdistrict").disabled = true;
            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/getamphurlist.php",
                data:JSON.stringify({
                    PROVINCE_ID :provincecode[1] 
                }),
                success: function (response) {
    
                    $.each(response, function(index) {
                        customer_address_district += "<option value="+"  '"+response[index].AMPHUR_NAME+":"+response[index].AMPHUR_ID+"' "+">"+response[index].AMPHUR_NAME+"</option>";
                    });
                    document.getElementById("customer_address_district").disabled = false;
                    $('#customer_address_district').html(customer_address_district);
                }
            });

    }
    function getamphurpostcode(){
        var postcode=document.getElementById("customer_address_subdistrict").value;
        var postcode = postcode.split(":");
        var postcode = postcode[1];
        console.log(postcode);
        document.getElementById("customer_address_postcode").value=postcode;
    }
    </script>


    <!-- Modal AddataSUCCESS-->
    <div class="modal fade" id="insertsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <span>เพิ่มข้อมูลสำเร็จ</span><span id="insertsuccesshtml"></span>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

            <!-- Modal CANTDELETE-->
            <div class="modal fade" id="cantdelete" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p>ไม่สามารถลบข้อมูลได้เนื่องจากมีการใช้ข้อมูลอยู่</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

     <!-- Modal DELETESUCCESS-->
 <div class="modal fade" id="deletesuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p>ลบข้อมูลสำเร็จ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

         <!-- Modal ALERTTEL-->
         <div class="modal fade" id="alerttel" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p>เบอร์โทรศัพท์ของท่าน ซ้ำ!! ในระบบ กรุณากรอก เบอร์โทรศัพท์ อื่น</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

         <!-- Modal ALERTMAIL-->
         <div class="modal fade" id="alertmail" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p>E-Mail ของท่าน ซ้ำ!! ในระบบ กรุณากรอก E-Mail อื่น</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

           <!-- Modal alertexception-->
           <div class="modal fade" id="alertexception" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p>เกิดข้อผิดพลาดกรุณาลองอีกครั้ง</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

</body>
<footer style="background-color: #575757; ">
<div class="container-fluid">
        <nav class="navbar navbar-expand-sm" style="display: flex;justify-content: center;">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <label style="color: #FFA200;font-size: 14px;">&copy;&nbsp;2020 localshopcenter.com</label>
                </li>
            </ul>
        </nav>
    </div>
</footer>
</html>