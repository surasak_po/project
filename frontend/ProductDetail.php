<?php
    include "../restapi/setting/config.php";
    @session_start();
    @session_cache_expire(30);

    @$product_code = $_GET['product_code'];

    if(@$_SESSION['role'] == "admin"){
        echo "<meta  http-equiv='refresh' content='0;URL=1AdminCustomer.php'>";
    }
    if(@$_SESSION['role'] == "store"){
        echo "<meta  http-equiv='refresh' content='0;URL=Store.php'>";
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ข้อมูลสินค้า</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="./css/styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <style>
        body {
            background-color: #ECECEC;
        }

        @font-face {
            font-family: 'Opun-Regular.ttf';
            src: url('fonts/Opun-Regular.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }

        #rcorners {
            border-radius: 3px;
            border: 1.5px solid #D1D1D1;
            padding: 10px;
            width: 80%;
            -webkit-box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
            -moz-box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
            box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
            background-color: white;
        }

        #rcorners2 {
            border-radius: 10px;
            border: 1.5px solid #D1D1D1;
            padding: 10px;
            width: 85%;
            -webkit-box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
            -moz-box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
            box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
            background-color: white;
        }


        .card {
            background-color: white;  
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }

        .productimg {
            border: 0.5px solid #ddd;
            padding: 5px;
            width: 99px;
            background-color: white;
            -webkit-box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
            -moz-box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
            box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
        }

        .carousel-inner {
            width: 380px;
            background-color: black;
            -webkit-box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
            -moz-box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
            box-shadow: 10px 6px 9px -5px rgba(0,0,0,0.33);
            border-radius: 12px;
            border: 10px solid #ddd;
        }

        .modal-header .close {
        display:none;
        }
    </style>
</head>

<script>

setInterval(function(){ 
    var customer_code ="<?php echo @$_SESSION['customer_code'];?>";

    if(customer_code!=""){ //ถ้า login
        $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/getcartnum.php",
                data:JSON.stringify({
                    customer_code:customer_code
                }),
                success: function (response) {
                var json_data = response;
                var getdata = json_data;   
        document.getElementById("badge_cart_num").innerHTML = getdata.cartnum;
                }
            }); 

            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/getcartinfologin.php",
                data:JSON.stringify({
                    customer_code:customer_code
                }),
                success: function (response) {
                    var json_data = response;
                    var apistatus = json_data.result;

                    if(apistatus=="empty") {
                           
                        strHTML ="<div class=\"container\">"
                        strHTML +="<div class=\"row\">"

                        strHTML +="<div class=\"col-md-1\">"
                        strHTML +="</div>"

                        strHTML +="<div class=\"col-md-8\">"
                        strHTML +="<img src=\"icons/empty-cart.png\"  width=\"250px\" height=\"200px\" >";
                        strHTML +="<p style=\"font-size:16px;text-align:center;font-family:'Opun-Regular.ttf'!important;Padding-left:40px;\" >ยังไม่มีสินค้าในตะกร้า</p>"
                        strHTML +="</div>"

                        strHTML +="<div class=\"col-md-2\">"
                        strHTML +="</div>"

                        strHTML +="</div>"
                        strHTML +="</div>"
                        $('#list-cart').html(strHTML);
                    }else{
      
                    strHTML=" <table class=\"table table-bordered\" style=\"font-size:10px;font-family:'Opun-Regular.ttf'!important;\">";
                    strHTML +="<tbody id=\"cart_list_table\">";
                    strHTML +="<tr>";
                    strHTML +="<th style=\"text-align:center;\"></th>";
                    strHTML +="<th style=\"text-align:center;\"> ชื่อสินค้า</th>";
                    strHTML +="<th style=\"text-align:center;\" width=\"5%\">จำนวน</th>";
                    strHTML +="<th style=\"text-align:center;\" width=\"20%\">ราคาต่อชิ้น</th>"; 
                    strHTML +="<th style=\"text-align:center;\" width=\"10%\">ราคา</th>";
                    strHTML +="<th style=\"text-align:center;\" width=\"5%\"></th>";
                    strHTML +=" </tr>";
                    $.each(response, function(index) {
                        strHTML += "<tr>";
                        strHTML += "<td align=center>"+"<img src=\" ../restapi/product/" +response[index].product_group_picture+ "\"  width=\"50px\" height=\"50px\" >"+"</td>";
                        strHTML += "<td align=center>"+response[index].product_group_name+"</td>";
                        strHTML += "<td align=center>"+response[index].cart_detail_num+"</td>";
                        strHTML += "<td align=center>"+response[index].product_group_price+"</td>";
                        strHTML += "<td align=center>"+response[index].totalprice+".00"+"</td>";
                       strHTML += "<td align=center>"+" <a  style=\"cursor:pointer;font-size:20px;\" onclick='deletecartlogin(this.id)' id=\""+response[index].product_group_code+"\" >ลบ</a>"+"</td>";
                        strHTML += "</tr>";
                    });
                    strHTML +="</tbody>";
                    strHTML +="</table>";
                    strHTML +="<div class=\"row\">  <div class=\"col-md-6\"></div> <div class=\"col-md-6\" style=\"cursor:pointer;\" onclick=\"opencart();\"><div align=\"center\"  align=\"right\" style=\"background-color:#D5AEAE;font-size:16px;font-family:'Opun-Regular.ttf'!important;\"><br>ดูตะกร้าสินค้าของคุณ<br></div></div> </div>";

                  
                    $('#list-cart').html(strHTML);
                    }
                }
            });
    }else{ //ไม่ล้อคอิน
        $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/getcartnumnologin.php",
                data:JSON.stringify({
                }),
                success: function (response) {
                var json_data = response;
                var getdata = json_data; 
                if( getdata.cartnum=="0"){
                    document.getElementById("badge_cart_num").innerHTML ="";
                }else{
                    document.getElementById("badge_cart_num").innerHTML = getdata.cartnum;
                }
             
                }
            });

            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/getcartinfonologin.php",
                data:JSON.stringify({
                }),
                success: function (response) {
                    var json_data = response;
                    var apistatus = json_data.result;

                    if(apistatus=="empty") {
                           
                        strHTML ="<div class=\"container\">"
                        strHTML +="<div class=\"row\">"

                        strHTML +="<div class=\"col-md-1\">"
                        strHTML +="</div>"

                        strHTML +="<div class=\"col-md-8\">"
                        strHTML +="<img src=\"icons/empty-cart.png\"  width=\"250px\" height=\"200px\" >";
                        strHTML +="<p style=\"font-size:16px;text-align:center;font-family:'Opun-Regular.ttf'!important;Padding-left:40px;\" >ยังไม่มีสินค้าในตะกร้า</p>"
                        strHTML +="</div>"

                        strHTML +="<div class=\"col-md-2\">"
                        strHTML +="</div>"

                        strHTML +="</div>"
                        strHTML +="</div>"
                        $('#list-cart').html(strHTML);
                    }else{
                        
                    strHTML=" <table class=\"table table-bordered\" style=\"font-size:10px;font-family:'Opun-Regular.ttf'!important;\">";
                    strHTML +="<tbody id=\"cart_list_table\">";
                    strHTML +="<tr>";
                    strHTML +="<th style=\"text-align:center;\"></th>";
                    strHTML +="<th style=\"text-align:center;\"> ชื่อสินค้า</th>";
                    strHTML +="<th style=\"text-align:center;\" width=\"5%\">จำนวน</th>";
                    strHTML +="<th style=\"text-align:center;\" width=\"20%\">ราคาต่อชิ้น</th>"; 
                    strHTML +="<th style=\"text-align:center;\" width=\"10%\">ราคา</th>";
                    strHTML +="<th style=\"text-align:center;\" width=\"5%\"></th>";
                    strHTML +=" </tr>";
                    $.each(response, function(index) {
                        strHTML += "<tr>";
                        strHTML += "<td align=center>"+"<img src=\" ../restapi/product/" +response[index].product_group_picture+ "\"  width=\"50px\" height=\"50px\" >"+"</td>";
                        strHTML += "<td align=center>"+response[index].product_group_name+"</td>";
                        strHTML += "<td align=center>"+response[index].cart_detail_num+"</td>";
                        strHTML += "<td align=center>"+response[index].product_group_price+"</td>";
                        strHTML += "<td align=center>"+response[index].totalprice+".00"+"</td>";
                       strHTML += "<td align=center>"+" <a style=\"cursor:pointer;\" onclick='deletecartnologin(this.id)' id=\""+response[index].product_group_code+"\" >ลบ</a>"+"</td>";
                        strHTML += "</tr>";
                    });
                    strHTML +="</tbody>";
                    strHTML +="</table>";
                    strHTML +="<div class=\"row\">  <div class=\"col-md-6\"></div> <div class=\"col-md-6\" style=\"cursor:pointer;\" onclick=\"opencart();\"><div align=\"center\"  align=\"right\" style=\"background-color:#D5AEAE;font-size:16px;font-family:'Opun-Regular.ttf'!important;\"><br>ดูตะกร้าสินค้าของคุณ<br></div></div> </div>";

                    $('#list-cart').html(strHTML);
                    }
                }
            });
    }
   
}, 1000);

function deletecartnologin(id) {
   
    $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/deletecartnologin.php",
                data:JSON.stringify({
                id:id
                }),
                success: function (response) {
              
                }
            });
}
function deletecartlogin(id) {
   
   $.ajax({
               type: "POST",
               dataType: 'json',
               contentType: 'application/json',
               async : false,
               url: "../restapi/deletecartlogin.php",
               data:JSON.stringify({
               id:id
               }),
               success: function (response) {
             
               }
           });
}

    var changeclass="";
    function proGroupSelect(id) {
        document.getElementById("flashselectproduct").innerHTML="";
        console.log("change img price name");
        var product_group_code = id;
        if(changeclass==""){
            document.getElementById(id).classList.add('btn-warning');
            document.getElementById(id).classList.remove('btn-outline-warning');
            changeclass=id;
        } else {
            document.getElementById(changeclass).classList.add('btn-outline-warning');
            document.getElementById(changeclass).classList.remove('btn-warning');
            document.getElementById(id).classList.add('btn-warning');
            document.getElementById(id).classList.remove('btn-outline-warning');
            changeclass=id;
        }

        $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/getproductgroupdetail.php",
                data:JSON.stringify({
                    product_group_code:product_group_code
                }),
                success: function (response) {
                var json_data = response;
                var getdata = json_data;
        document.getElementById("image_product_slide").hidden=true;
        document.getElementById("image_product").hidden=false;
        document.getElementById("image_product").src = "../restapi/product/"+getdata.product_group_picture;
        document.getElementById("wide").innerHTML = getdata.product_group_wide+" เซนติเมตร";
        document.getElementById("high").innerHTML = getdata.product_group_high+" เซนติเมตร";
        document.getElementById("long").innerHTML = getdata.product_group_long+" เซนติเมตร";
        document.getElementById("weight").innerHTML = getdata.product_group_weight+" กรัม";
        document.getElementById("product_group_price").innerHTML = "฿ "+getdata.product_group_price;
        document.getElementById("product_group_num").innerHTML = getdata.product_group_num;
        document.getElementById("product_group_unit_name").innerHTML = getdata.product_group_unit_name;
                }
            });
        
    }

    function buyBtn() {

        var customer_code ="<?php echo @$_SESSION['customer_code'];?>";

if(customer_code==""){ //ไม่ได้ล็อคอิน
       
    if(changeclass==""){
        console.log(changeclass);
        document.getElementById("flashselectproduct").innerHTML="กรุณาเลือกสินค้าด้วยค่ะ";
    }else{
        console.log(changeclass);
        $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/addcartnologin.php",
                data:JSON.stringify({
                cart_detail_num:"1",
                product_group_code:changeclass
                }),
                success: function (response) {
                var json_data = response;
                var getdata = json_data;
                if(getdata.result=="Success"){
                    location.replace("LoginRegCus.php");
                }else{
                    console.log("ไม่สำเร็จ");
                Numnotenough();
                setTimeout(function(){ 
                },2600);
                }
                }
        });

    }

}else{ //ล็อคอิน
    
    if(changeclass==""){
        console.log(changeclass);
        document.getElementById("flashselectproduct").innerHTML="กรุณาเลือกสินค้าด้วยค่ะ";
    }else{
        console.log(changeclass);
       
        $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/addcart.php",
                data:JSON.stringify({
                customer_code:customer_code,
                product_group_code:changeclass
                }),
                success: function (response) {
                var json_data = response;
                var getdata = json_data;
                console.log(getdata.result);
             
                if(getdata.result=="Success"){
                   location.replace("ShoppingBasket.php");
                }else{
                Numnotenough();
                setTimeout(function(){ 
                },2600);
                }
                }
        });
    }
  
}

    }
    function Numnotenough() {
    $("#Numnotenough").modal({backdrop: 'static', keyboard: false})  
        $("#Numnotenough").modal('show');
        setTimeout(function(){$('#Numnotenough').modal('hide')},2000);
    }
    function loginfail() {
        $("#loginfail").modal({backdrop: 'static', keyboard: false})  
        $("#loginfail").modal('show');
        setTimeout(function(){$('#loginfail').modal('hide')},3000);
    }

    function loginsuccess() {
        $("#loginsuccess").modal({backdrop: 'static', keyboard: false})  
        $("#loginsuccess").modal('show');
        setTimeout(function(){$('#loginsuccess').modal('hide')},3000);
    }

    function logoutsuccess() {
        $("#logoutsuccess").modal({backdrop: 'static', keyboard: false})  
        $("#logoutsuccess").modal('show');
        setTimeout(function(){$('#logoutsuccess').modal('hide')},2000);
    }
    
    function customerLoginFunction() {
        var customerUsername = document.getElementById("customerUsername").value;
        var customerPassword = document.getElementById("customerPassword").value;

        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/logincustomer.php",
            data:JSON.stringify({
                customerUsername:customerUsername,
                customerPassword:customerPassword
            }),
            success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;
                if(apistatus=="Success") {
              
                   loginsuccess();
                   setTimeout(function(){ 
                    location.reload();
                   },3600);

                }else{
                    loginfail();
                    document.getElementById("customerUsername").value="";
                    document.getElementById("customerPassword").value="";
                }
            }
        });
    }

    function storeLoginFunction() {
        var storeUsername = document.getElementById("storeUsername").value;
        var storePassword = document.getElementById("storePassword").value;

        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/LoginStore.php",
            data:JSON.stringify({
                storeUsername:storeUsername,
                storePassword:storePassword
            }),
            success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;
                if(apistatus=="Success") {
                loginsuccess();
                setTimeout(function(){ 
                    location.replace("Store.php");
                },3600);

                }else{
                    loginfail();
                    document.getElementById("storeUsername").value="";
                    document.getElementById("storePassword").value="";
                }
            }
        });
    }

    function adminLoginFunction() {
        var adminUsername = document.getElementById("adminUsername").value;
        var adminPassword = document.getElementById("adminPassword").value;
    
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/LoginAdmin.php",
            data:JSON.stringify({
                adminUsername:adminUsername,
                adminPassword:adminPassword
            }),
            success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;
                if(apistatus=="Success") {
                loginsuccess();
                setTimeout(function(){ 
                    location.replace("Admin.php");
                },3600);

                }else{
                    loginfail();
                    document.getElementById("adminUsername").value=""
                    document.getElementById("adminPassword").value=""
                }
            }
        });
    }

    function logoutFunction() {
        logoutsuccess();
            setTimeout(function(){ 
            document.forms["formLogout"].action = "../restapi/logout.php";
            document.forms["formLogout"].submit();
        },2600);
    }
</script>

<body>

    <!-- Modal Zone -->
    <!-- 1. Register Group Modal -->
    <div class="modal" id="registerGroupModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เลือกประเภทการสมัครสมาชิก</h2>
            </div>

            <!-- Modal body -->
                <div class="modal-body">
                    <div row align="center" style="margin-bottom: -25px;">
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 50px;">
                            <p style="font-size: 60px; color: #17a2b8; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-users"></i></p>
                        </div>
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 10px;">
                            <p style="font-size: 60px; color: #007bff; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-store"></i></p>
                        </div>
                    </div>
                    <div row align="center">
                        <div class="col-sm-5 d-inline-flex">
                            <a class="btn btn-info" type="button" href="CustomerRegister.php">สำหรับลูกค้า</a>
                        </div>
                        <div class="col-sm-5 d-inline-flex">
                            
                        <a class="btn btn-primary" type="button" href="StoreRegister.php">สำหรับร้านค้า</a>
                        </div>
                    </div>
                </div>

            <!-- Modal footer -->
            <br>
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-danger" type="button" data-dismiss="modal">ปิดหน้านี้</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 1. End of Register Group Modal -->

    <!-- 2. Login Group Modal -->
    <div class="modal" id="loginGroupModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เลือกประเภทการเข้าสู่ระบบ</h2>
            </div>

            <!-- Modal body -->
                <div class="modal-body">
                    <div row align="center" style="margin-bottom: -25px;">
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 50px;">
                            <p style="font-size: 60px; color: #17a2b8; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-users"></i></p>
                        </div>
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 10px;">
                            <p style="font-size: 60px; color: #007bff; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-store"></i></p>
                        </div>
                    </div>
                    <div row align="center" style="margin-bottom: 30px;">
                        <div class="col-sm-5 d-inline-flex">
                            <button data-toggle="modal" data-target="#loginCustomerModal" class="btn btn-info" type="button" data-dismiss="modal">สำหรับลูกค้า</button>
                        </div>
                        <div class="col-sm-5 d-inline-flex">
                            <button data-toggle="modal" data-target="#loginStoreModal" class="btn btn-primary" type="button" data-dismiss="modal">สำหรับร้านค้า</button>
                        </div>
                    </div>
                    <div row align="center" style="margin-bottom: -25px;">
                            <p style="font-size: 60px; color: #343a40; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-crown"></i></p>
                    </div>
                    <div row align="center">
                        <button data-toggle="modal" data-target="#loginAdminModal" class="btn btn-dark" type="button" data-dismiss="modal">สำหรับแอดมิน</button>
                    </div>
                </div>

            <!-- Modal footer -->
            <br>
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-danger" type="button" data-dismiss="modal">ปิดหน้านี้</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2. End of Login Group Modal -->

    <!-- 2.1 Login Customer Modal -->
    <div class="modal" id="loginCustomerModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เข้าสู่ระบบ (สำหรับลูกค้า)</h2>
            </div>

            <!-- Modal body -->
            <form id="formLoginCustomer" name="formLoginCustomer" method="POST">
                <div class="modal-body">
                    <img src="./icons/loginlogo.png" width="150px" height="150px">
                    <div class="form-group col-sm-9">
                        <label for="customerUsername" style="font-weight: regular;">ชื่อผู้ใช้</label>
                        <input type="text" placeholder="เบอร์โทรศัพท์หรืออีเมลล์" class="form-control" id="customerUsername" name="customerUsername">
                    </div>
                    <div class="form-group col-sm-9">
                        <label for="customerPassword" style="font-weight: regular;">รหัสผ่าน</label>
                        <input type="password" placeholder="รหัสผ่าน" class="form-control" id="customerPassword" name="customerPassword">
                    </div>
                </div>

            <!-- Modal footer -->
            <br>
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-success" type="button" onclick="customerLoginFunction()">เข้าสู่ระบบ</button>
                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#loginGroupModal" data-dismiss="modal">ยกเลิก</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2.1 End of Login Customer Modal -->

    <!-- 2.2 Login Store Modal -->
    <div class="modal" id="loginStoreModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เข้าสู่ระบบ (สำหรับร้านค้า)</h2>
            </div>

            <!-- Modal body -->
            <form id="formLoginStore" name="formLoginStore" method="POST">
                <div class="modal-body">
                    <img src="./icons/loginlogo.png" width="150px" height="150px">
                    <div class="form-group col-sm-9">
                        <label for="storeUsername" style="font-weight: regular;">ชื่อผู้ใช้</label>
                        <input type="text" placeholder="เบอร์โทรศัพท์หรืออีเมลล์" class="form-control" id="storeUsername" name="storeUsername">
                    </div>
                    <div class="form-group col-sm-9">
                        <label for="storePassword" style="font-weight: regular;">รหัสผ่าน</label>
                        <input type="password" placeholder="รหัสผ่าน" class="form-control" id="storePassword" name="storePassword">
                    </div>
                </div>

            <!-- Modal footer -->
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-success" type="button" onclick="storeLoginFunction()">เข้าสู่ระบบ</button>
                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#loginGroupModal" data-dismiss="modal">ยกเลิก</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2.2 End of Login Store Modal -->

    <!-- 2.3 Admin Login Modal -->
    <div class="modal" id="loginAdminModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">
                
            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เข้าสู่ระบบ (สำหรับผู้ดูแลระบบ)</h2>
            </div>
            <form id="formAdminLogin" name="formAdminLogin" method="POST">

            <!-- Modal body -->
                <div class="modal-body">
                    <img src="./icons/loginlogo.png" width="150px" height="150px">
                    <div class="form-group col-sm-9">
                        <label for="adminUsername" style="font-weight: regular;">ชื่อผู้ใช้</label>
                        <input type="text" placeholder="ชื่อผู้ใช้งานผู้ดูแล" class="form-control" id="adminUsername" name="adminUsername">
                    </div>
                    <div class="form-group col-sm-9">
                        <label for="adminPassword" style="font-weight: regular;">รหัสผ่าน</label>
                        <input type="password" placeholder="รหัสผ่าน" class="form-control" id="adminPassword" name="adminPassword">
                    </div>
                </div>

            <!-- Modal footer -->
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-success" type="button" onclick="adminLoginFunction()">เข้าสู่ระบบ</button>
                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#loginGroupModal" data-dismiss="modal">ยกเลิก</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2.3 End of Admin Login Modal -->
    <!-- End of Modal Zone -->

    <nav class="navbar navbar-expand-sm">
        <a class="navbar-brand" style="font-family:'Opun-Regular.ttf'!important; color: #FFA200;font-size:35px;" href="index.php">
            <i class="fas fa-dolly"></i>ผลิตภัณฑ์ชุมชน
        </a>
        <ul class="navbar-nav mr-auto"></ul>
        <ul class="navbar-nav">
            <?php
                if (@$_SESSION['role'] == "customer") {
                    $strSQL = "SELECT * FROM tbl_customer WHERE customer_code = '".$_SESSION['customer_code']."' ";
                    $result = @$conn->query($strSQL);
                    while($row = $result->fetch_assoc()){
                        ?>
                    
                            <li class="nav-item">
                               <a href="CustomerInfoNewForm.php" class="nav-link"><i class="fas fa-user"></i>ข้อมูลส่วนตัว</a>
                            </li>
                            <li class="nav-item">
                                <a style="color: #FFA200 !important;,font-size: 14px !important;\" class="nav-link"> <?php echo "ยินดีต้อนรับ&nbspคุณ&nbsp :&nbsp " . @$row['customer_fullname'] . ""; ?> </a>
                            </li>
                            <form id="formLogout\" name="formLogout">
                                <li class="nav-item">
                                    <a class="nav-link" style="cursor:pointer" onclick="logoutFunction()"><i class="fas fa-sign-out-alt"></i>ออกจากระบบ</a>
                                </li>
                            </form>
                            <li class="nav-item">
                            <a class="nav-link"  ><i  style="font-size:25px;" class="cart fas fa-shopping-cart">
                            <div class="list-cart" id="list-cart"></div></i><span id="badge_cart_num" class="badge badge-light"></span></a>
                            
                            </li>
                     
                    <?php }
         } else {   ?>
                
            <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#registerGroupModal" style="cursor:pointer"><i class="fas fa-user-plus"></i>สมัครสมาชิก</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#loginGroupModal" style="cursor:pointer"><i class="fas fa-sign-in-alt"></i>เข้าสู่ระบบ</a>
            </li> 
            <li class="nav-item\">
            <a class="nav-link" ><i  style="font-size:25px;" class="cart fas fa-shopping-cart" >
            <div class="list-cart" id="list-cart"></div>   
           </i>
          
           <span id="badge_cart_num" class="badge badge-light"></span></a>
            </li>
       
<?php    }    ?>

        </ul>
    </nav>

<style>
        .list-cart {

            background-color: #E4E4E4;
            /* height: 250px;
  width: 300px; 
  border: 2px solid red;*/
            right: 0;
            position: absolute;
            transition: 2s all;
            transform: scale(0);
            transform-origin: 100% 0%;
        }

        .cart:hover>.list-cart {
            transform: scale(0.7);
        }
</style>

    <script>
function opencart(){
    var customer_code ="<?php echo @$_SESSION['customer_code'];?>";

    if(customer_code==""){
        location.replace("LoginRegCus.php");
    }else{
        console.log("ไปหน้าตะกร้า");
        location.replace("ShoppingBasket.php");
    }

}
    </script>


<div style="padding-bottom: 10px; background-color: #D8D8D4;">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                        <div class="input-group md-form form-sm form-2 pl-0">
                        <input id="searchname"  class="form-control my-0 py-1 red-border" type="text" placeholder="ค้นหา" aria-label="ค้นหา">
                        <div class="input-group-append">
              <span class="input-group-text red lighten-3" id="basic-text1">
              <i class="fas fa-search text-grey" aria-hidden="true" onclick="searchnameproduct()"></i></span>
                        </div>
                        </div>
                </div>
                <div class="col-md-3"></div>
      </div>
    </div>
<script>
function searchnameproduct(){
    var productname =document.getElementById("searchname").value;
    window.location.href = "index.php?productname=" + productname;
}


</script>
    <div align="center">
        <div id="rcorners2" style="padding-bottom:100px; padding-top:20px;">
            <div class="row">
                <div class="col-sm-4">

  <div id="image_product_slide"  class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">

                    <?php
            $strgetproductimage="SELECT * FROM tbl_image_product WHERE image_product_banned != '1' AND product_code = '$product_code' ORDER BY image_product_no ASC LIMIT 1";
            $resultstrgetproductimage = @$conn->query($strgetproductimage);
            if(@$resultstrgetproductimage->num_rows >0){
            while($rowstrgetproductimage = $resultstrgetproductimage->fetch_assoc()){
                $imagenameproduct = $rowstrgetproductimage['image_product_name']; 
                    ?>
                
           
            <div class="carousel-item active">
            <img class="d-block w-100"  src="../restapi/product/<?php echo $imagenameproduct; ?>" style=" width:250px; height:250px;">
            </div>
             
                        <?php 
                      }}else{
                        ?>
                <div class="carousel-item active">
                            <img class="d-block w-100"  src="../restapi/product/productimgempty.png" style=" width:250px; height:250px;">
                </div>
  <?php 
  }
   ?>

                        <?php
            $strgetproductimage="SELECT * FROM tbl_image_product WHERE image_product_banned != '1' AND product_code = '$product_code' ORDER BY image_product_no ASC ";
            $resultstrgetproductimage = @$conn->query($strgetproductimage);
            if(@$resultstrgetproductimage->num_rows >0){
            while($rowstrgetproductimage = $resultstrgetproductimage->fetch_assoc()){
                $imagenameproduct = $rowstrgetproductimage['image_product_name']; 
                    ?>
                
           
            <div class="carousel-item ">
            <img class="d-block w-100" src="../restapi/product/<?php echo $imagenameproduct; ?>" style=" width:250px; height:250px;">
            </div>
             
                        <?php 
                      }}
                        ?>

                        
     </div>
     <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
        
    <img hidden class="productimg" id="image_product"  style=" width:380px; height:250px;"> 
                </div>

                <?php 
                  $store_picture;
                  $store_namestore;
                  $strgetproduct="SELECT * FROM tbl_product 
                  INNER JOIN tbl_store ON tbl_product.store_code = tbl_store.store_code
                  WHERE product_code ='$product_code' ";
                  $resultstrgetproduct = @$conn->query($strgetproduct);
                  if(@$resultstrgetproduct->num_rows >0){
                      while($rowstrgetproduct = $resultstrgetproduct->fetch_assoc()){

                        $product_detail=$rowstrgetproduct['product_detail'];
                        if("1"==$rowstrgetproduct['product_banned']){
                            echo "โดนแบน";
                        }
                        if("3"==$rowstrgetproduct['store_status']){
                            echo "โดนแบน";
                        }

                        $product_name=$rowstrgetproduct['product_name'];
                        $store_namestore=$rowstrgetproduct['store_namestore'];
                        $store_address=$rowstrgetproduct['store_address'];
                        $store_picture=$rowstrgetproduct['store_picture'];
                      }}

                    $product_group_name_code[]= array();

        
                     
                        
                ?>

                <div class="col-sm-6" align="left">
                    <h3>
                    <?php echo $product_name; ?>
                    </h3><br>
                    <h5 id="product_group_price">
                    <?php
            $strgetmin="SELECT MIN(product_group_price) AS product_group_price FROM tbl_product_group WHERE product_code = '$product_code' ";
            $resultstrgetmin = @$conn->query($strgetmin);
            while($rowstrgetmin = $resultstrgetmin->fetch_assoc()){
                $strmin=$rowstrgetmin['product_group_price'];
                echo "฿ ".$strmin;
            }
            $strgetmax="SELECT MAX(product_group_price) AS product_group_price FROM tbl_product_group WHERE product_code = '$product_code' ";
            $resultstrgetmax = @$conn->query($strgetmax);
            while($rowstrgetmax = $resultstrgetmax->fetch_assoc()){
                $strmax=$rowstrgetmax['product_group_price'];
                if($strmin==$strmax){
                            //NOT SHOW
                }else{
                    echo "-".$strmax;
                }
            }

                    ?>
                    </h5><br>
                    <?php
                    $sumnumberofproduct=0;
                               $strgetproductgroup="SELECT * FROM tbl_product_group WHERE product_code = '$product_code' ";
                               $resultstrgetproductgroup = @$conn->query($strgetproductgroup);
                               if(@$resultstrgetproductgroup->num_rows >0){
                                   while($rowstrgetproductgroup = $resultstrgetproductgroup->fetch_assoc()){
                                    $sumnumberofproduct = $sumnumberofproduct + $rowstrgetproductgroup['product_group_num'];
                         
                    if($rowstrgetproductgroup['product_group_num']>0){
                        echo "<button id=\"".$rowstrgetproductgroup['product_group_code']."\" class=\"btn btn-outline-warning d-inline-flex\" style=\"font-size:14px; color:black; margin-bottom:20px; margin-right:5px;\" onclick=\"proGroupSelect(this.id)\">".$rowstrgetproductgroup['product_group_name']."</button>";
                    }else{
                        echo "<button Disabled class=\"btn btn-outline-warning d-inline-flex\" style=\"font-size:14px; color:black; margin-bottom:20px; margin-right:5px;\" >".$rowstrgetproductgroup['product_group_name']."</button>";
                    }
                        }} 
                        ?>
                        <h7 id="flashselectproduct" style="color:red"></h7>
                    <h5> จำนวนคงเหลือ

                   <span id="product_group_num"><?php echo $sumnumberofproduct; ?> </span>
                    
                     <span id="product_group_unit_name">ชิ้น</span>
                     </h5><br>
                     <a id="buyBtn" href="#" class="btn btn-primary d-inline-flex" style="font-size:12px; color:white; margin-right: 10px;" onclick="buyBtn()">ซื้อสินค้า</a>
                    <a id="addCartBtn" href="#" class="btn btn-success d-inline-flex" style="font-size:12px; color:white; margin-right: 10px;" onclick="addCartBtn()">หยิบใส่ตะกร้า</a>
                    <br> <br>
<script>
    function addCartBtn() {
        
        var customer_code ="<?php echo @$_SESSION['customer_code'];?>";

        if(customer_code==""){ //ไม่ได้ล็อคอิน
               
            if(changeclass==""){
                console.log(changeclass);
                document.getElementById("flashselectproduct").innerHTML="กรุณาเลือกสินค้าด้วยค่ะ";
            }else{
                console.log(changeclass);
                $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: 'application/json',
                        async : false,
                        url: "../restapi/addcartnologin.php",
                        data:JSON.stringify({
                        cart_detail_num:"1",
                        product_group_code:changeclass
                        }),
                        success: function (response) {
                        var json_data = response;
                        var getdata = json_data;
                        if(getdata.result=="Success"){
                                console.log("สำเร็จ");
                        }else{
                            console.log("ไม่สำเร็จ");
                        Numnotenough();
                        setTimeout(function(){ 
                        },2600);
                        }
                        }
                });

            }

        }else{ //ล็อคอิน
            
            if(changeclass==""){
                console.log(changeclass);
                document.getElementById("flashselectproduct").innerHTML="กรุณาเลือกสินค้าด้วยค่ะ";
            }else{
                console.log(changeclass);
               
                $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: 'application/json',
                        async : false,
                        url: "../restapi/addcart.php",
                        data:JSON.stringify({
                        customer_code:customer_code,
                        product_group_code:changeclass
                        }),
                        success: function (response) {
                        var json_data = response;
                        var getdata = json_data;
                        if(getdata.result=="Success"){
                   
                        }else{
                        Numnotenough();
                        setTimeout(function(){ 
                        },2600);
                        }
                        }
                });
            }
          
        }

    }
</script>


                    <div class="row" >
                        <div class="col-md-3" >
                        รายละเอียด :
                        </div>
                        <div class="col-md-8" >
                        <?php echo $product_detail;?>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-md-3" >
                        กว้าง :
                        </div>
                        <div class="col-md-8" >
                        <span id="wide">-</span>
                        </div>
                    </div>
                    <div class="row" >
                    <div class="col-md-3" >
                        ยาว :
                        </div>
                        <div class="col-md-8" >
                        <span id="long">-</span>
                        </div>
                    </div>
                    <div class="row" >
                    <div class="col-md-3" >
                        สูง :
                        </div>
                        <div class="col-md-8" >
                        <span id="high">-</span>
                        </div>
                    </div>
                    <div class="row" >
                    <div class="col-md-3" >
                        น้ำหนัก :
                        </div>
                        <div class="col-md-8" >
                        <span id="weight">-</span>
                        </div>
                    </div>
                   
                       </div>
            </div>
        </div>
    </div>


    <div align="center">
        <div id="rcorners2" style="padding-bottom:100px; padding-top:20px;">
         <div class="row">
                <div class="col-md-1" align="center">
                <span id="weight">
                <img src="../restapi/profilestore/<?php echo $store_picture;?>" width="80px" height="80px">
                </span>
                </div>
                <div class="col-md-11" align="left" style="font-size:20px;font-weight:bold;">
                <span id="weight">ร้าน : <?php echo  $store_namestore;?></span>
                </div>

                <div class="col-md-1" align="center">
                <span id="weight">
                
                </span>
                </div>
                <div class="col-md-11" align="left" style="font-size:20px;font-weight:bold;">
                <span id="weight">ที่อยู่ : <?php echo  $store_address;?></span>
                </div>
         </div>
        </div>
    </div>

      <div align="center">
        <div id="rcorners2" style="padding-bottom:100px; padding-top:20px;">
         <div class="row">
                 <div class="col-md-2" align="left">
                </div>
                <div class="col-md-8" align="left" style="background-color:#ECECEC ;">
                <span style="font-size:20px;font-weight:bold;">
                รีวิว
                   <div class="col-md-12" align="left">
                <?php
                    $strgetreview="SELECT * FROM tbl_order_detail
                    INNER JOIN tbl_product_group ON tbl_order_detail.product_group_code = tbl_product_group.product_group_code
                    INNER JOIN tbl_order ON tbl_order_detail.order_code = tbl_order.order_code
                    INNER JOIN tbl_customer ON tbl_order.customer_code = tbl_customer.customer_code
                    WHERE tbl_product_group.product_code = '$product_code' AND order_detail_review != '' ";
                    $resultstrgetreview = @$conn->query($strgetreview);
                    if(@$resultstrgetreview->num_rows >0){
                    while($rowstrstrgetreview = $resultstrgetreview->fetch_assoc()){
                   
                ?>
                         <div class="row" >
                           <div class="col-md-3" align="left">
                           </div>
                            <div class="col-md-6" align="left" style="background-color:white;">
                             <span style="font-size:20px;font-weight:bold;">
                             <img class="rounded-circle"style="border:1px solid black" src="../restapi/profile/<?php echo $rowstrstrgetreview['customer_profile'];?>" width="50px" height="50px">
                             คุณ <?php echo $rowstrstrgetreview['customer_fullname'];?>
                             </span>
                           </div>
                           <div class="col-md-3" align="left">
                           </div>
                         </div>
                            <div class="row" >
                           <div class="col-md-3" align="left">
                           </div>
                            <div class="col-md-6" align="left" style="background-color:#FFC591;">
                           <?php echo $rowstrstrgetreview['order_detail_review'];?>
                           </div>
                           <div class="col-md-3" align="left">
                           </div>
                         </div>
                 <?php
                    }}else{?>
                          <div class="row" >
                           <div class="col-md-3" align="left">
                           </div>
                            <div class="col-md-6" align="center" >
                             <span style="font-size:20px;font-weight:bold;">
                           ไม่พบรีวิว
                             </span>
                           </div>
                           <div class="col-md-3" align="left">
                           </div>
                         </div>
                         

                  <?php     }
                ?>

                </span>
                </div>
                 <div class="col-md-2" align="left">
                </div>
              
                </div>
         </div>
        </div>
    </div>
    
  <!-- Modal ERROR-->
  <div class="modal fade" id="loginfail" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/wrong.png" width="150px" height="150px">
        <br><br>
        <p>ชื่อผู้ใช้หรือรหัสผ่านของคุณผิดพลาด</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

  <!-- Modal SUCCESS-->
  <div class="modal fade" id="loginsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p>เข้าสู่ระบบสำเร็จ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->



      <!-- Modal LOGOUT-->
      <div class="modal fade" id="logoutsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/logout.png" width="150px" height="150px">
        <br><br>
        <p>กำลังออกจากระบบ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

    <!-- Modal Numnotenough-->
    <div class="modal fade" id="Numnotenough" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
    
        <br><br>
        <p>สินค้ามีจำนวนไม่เพียงพอ ไม่สามารถเพิ่มจำนวนสินค้าได้</p>
        </div>
        <div class="modal-footer">
           <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK"  onclick="okNumnotenough()">ตกลง</button>  -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal Numnotenough-->
</body>

<footer style="background-color: #575757;">
    <div class="container-fluid" style="margin-top: 157px;">
        <nav class="navbar navbar-expand-sm" style="display: flex;justify-content: center;">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <label style="color: #FFA200;font-size: 14px;">&copy;&nbsp;2020 localshopcenter.com</label>
                </li>
            </ul>
        </nav>
    </div>
</footer>
</html>