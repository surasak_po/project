<?php
    include "webservice/setting/Config.php";

    @session_start();
    @session_cache_expire(30);

    if(@$_SESSION['role'] == "admin"){
        echo "<meta  http-equiv='refresh' content='0;URL=1AdminCustomer.php'>";
    }
    if(@$_SESSION['role'] == "customer"){
        echo "<meta  http-equiv='refresh' content='0;URL=Index.php'>";
    }
    if(@$_SESSION['role'] == ""){
        echo "<meta  http-equiv='refresh' content='0;URL=Index.php'>";
    }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>จัดการข้อมูลร้านค้า</title>
    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="./css/sidebar.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="./css/styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }

        @font-face {
            font-family: 'Opun-Regular.ttf';
            src: url('fonts/Opun-Regular.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }

        body {
            font-family: 'Opun-Regular.ttf' !important;
        }

        .navbar {
            background-color: #575757;
            font-weight: bold;
            font-size: 18px;
            margin-bottom: 0px;
        }

        .navbar a {
            color: #FFA200;
            margin-top: -6px;
        }

        nav ul li a {
            font-size: 14px !important;
            text-decoration: underline;
        }

        #margin {
            margin-top: 10px;
            margin-left: 30px;
        }

        ul#menu li {
            display: inline;
            text-align: center;
            margin-left: 20px;
            margin-right: 20px;
            font-size: 18px;
            color: #575757;
        }

        ul#menu li a {
            color: #575757;
        }

        ul#menu li a:hover {
            color: #FFA200;
            text-decoration: none;
        }

        ul#label li {
            display: inline;
            text-align: center;
            margin-left: 20px;
            margin-right: 20px;
            font-size: 16px;
            color: #575757;
        }

        #hline {
            color: #FFA200 !important;
        }

        /* div a {
            font-size: 14px;
        }

        div a:hover {
            color: black;
            text-decoration: none;
        } */

        #bgblack {
            background-color: rgb(255, 94, 0);
        }

        .nav-link {
            text-decoration: none;
        }

        .nav-link:hover {
            color: black !important;
        }

        .nav-item {
            margin-top: 10px !important;
        }

        .row {
            padding-top: 10px;
        }

        .fas {
            padding-right: 5px;
            margin-right: 5px;
        }
   
    .hovertable:hover {
         background-color:#EFEFEF ; 
    }
    .hovertable{
        padding-top:7px;
        padding-bottom:7px;
        background-color:#D3D1D0;
    }
    

    .hovertableproductgroup:hover {
         background-color:#FFBD9C ; 
    }
    .hovertableproductgroup{
        padding-top:7px;
        padding-bottom:7px;
        background-color:#FFFFFF;
    }
    .modal-header .close {
        display:none;
        }
    </style>
</head>

<script>
    function cantdelete(){
        $("#cantdelete").modal({backdrop: 'static', keyboard: false})  
      $("#cantdelete").modal('show');
      setTimeout(function(){$('#cantdelete').modal('hide')},2000);
    }
    function deletesuccess(){
        $("#deletesuccess").modal({backdrop: 'static', keyboard: false})  
      $("#deletesuccess").modal('show');
      setTimeout(function(){$('#deletesuccess').modal('hide')},2000);
    }
    function alertproductgroupname() {
        $("#alertproductgroupname").modal({backdrop: 'static', keyboard: false})  
        $("#alertproductgroupname").modal('show');
        setTimeout(function(){$('#alertproductgroupname').modal('hide')},2000);
    }
    function alertproductname() {
        $("#alertproductname").modal({backdrop: 'static', keyboard: false})  
        $("#alertproductname").modal('show');
        setTimeout(function(){$('#alertproductname').modal('hide')},2000);
    }
    function alertproductgroupname() {
        $("#alertproductgroupname").modal({backdrop: 'static', keyboard: false})  
        $("#alertproductgroupname").modal('show');
        setTimeout(function(){$('#alertproductgroupname').modal('hide')},2000);
    }
    function alertexception() {
        $("#alertexception").modal({backdrop: 'static', keyboard: false})  
        $("#alertexception").modal('show');
        setTimeout(function(){$('#alertexception').modal('hide')},2000);
    }
   function alerttel() {
    $("#alerttel").modal({backdrop: 'static', keyboard: false})  
        $("#alerttel").modal('show');
        setTimeout(function(){$('#alerttel').modal('hide')},2000);
    }
    function alertmail() {
        $("#alertmail").modal({backdrop: 'static', keyboard: false})  
        $("#alertmail").modal('show');
        setTimeout(function(){$('#alertmail').modal('hide')},2000);
    }
 function updatesuccess(){
    $("#updatesuccess").modal({backdrop: 'static', keyboard: false})  
      $("#updatesuccess").modal('show');
      setTimeout(function(){$('#updatesuccess').modal('hide')},1000);
    }

  function logoutsuccess(){
    $("#logoutsuccess").modal({backdrop: 'static', keyboard: false})  
      $("#logoutsuccess").modal('show');
      setTimeout(function(){$('#logoutsuccess').modal('hide')},2000);
    }

    function insertsuccess(){
        $("#insertsuccess").modal({backdrop: 'static', keyboard: false})  
      $("#insertsuccess").modal('show');
      setTimeout(function(){$('#insertsuccess').modal('hide')},1000);
    }
    
function encodeImageFileAsURL(element) {
  var file = element.files[0];
  var reader = new FileReader();
  reader.onloadend = function() {
   // console.log(reader.result);
  }
  reader.readAsDataURL(file);

  return reader;
}
    function cancelUpdateStoreInfo() {
      location.reload();
    }

    function logoutFunction() {
           logoutsuccess();
                   setTimeout(function(){ 
                    document.forms["formLogout"].action = "webservice/Logout.php";
                    document.forms["formLogout"].submit();
                   },2600);
    }
</script>

<body>
    <nav class="navbar navbar-expand-sm">
        <a class="navbar-brand" style="font-family:'KRR_AengAei.ttf'!important; color: #FFA200;font-size:35px;">
            <i class="fas fa-dolly"></i>ผลิตภัณฑ์ชุมชน
        </a>
        <ul class="navbar-nav mr-auto"></ul>
        <ul class="navbar-nav">
            <?php
                echo "
                    <form id=\"formLogout\" name=\"formLogout\">
                        <a class=\"nav-link\" style=\"cursor:pointer\" onclick=\"logoutFunction()\"><i class=\"fas fa-sign-out-alt\"></i>ออกจากระบบ</a>
                    </form>
                ";
            ?>
        </ul>
    </nav>

    <!-- Sidebar  -->
    <div class="wrapper">
        
        <nav id="sidebar">
            <div class="sidebar-header">
                <h4>หน้าจัดการร้านค้า</h4>
                <strong>BS</strong>
            </div>

            <ul class="list-unstyled components">
                <li >
                    <a href="Store.php">
                        <i class="fas fa-briefcase"></i>
                        ข้อมูลร้านค้า
                    </a>
                </li>
           
                <li class="active">
                    <a href="StoreProductView.php">
                        <i class="fas fa-box"></i>
                        เพิ่มสินค้า
                    </a>
                </li>

                <li>
                    <a href="StoreMyproduct.php">
                        <i class="fas fa-box"></i>
                      สินค้าของฉัน
                    </a>
                </li>

                <li>
                    <a href="StoreTransportView.php">
                    <i class="fas fa-box"></i>
                        จัดการช่องทางการจัดส่ง
                    </a>
                </li>
                <li>
                    <a href="StoreOrderview.php">
                        <i class="fas fa-box"></i>
                       ส่งสินค้า
                        <span id="badge_order_num" class="badge badge-light"></span>
                    </a>
                </li>
                <li>
                    <a href="StoreSell_list.php">
                        <i class="fas fa-box"></i>
                        รายรับของฉัน
                        <span id="badge_order_num" class="badge badge-light"></span>
                    </a>
                </li>
                <li >
                <a href="StoreWithdraw.php">
                        <i class="fas fa-box"></i>
                        ถอนเงิน
                        <span id="badge_order_num" class="badge badge-light"></span>
                    </a>
                </li>

                
                
            </ul>
        </nav>

        <script>

setInterval(function(){ 
    var store_code ="<?php echo @$_SESSION['store_code'];?>";

            if(store_code!=""){ //ถ้า login
                $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: 'application/json',
                        async : false,
                        url: "../restapi/getordernumstore.php",
                        data:JSON.stringify({
                            store_code:store_code
                        }),
                        success: function (response) {
                        var json_data = response;
                        var getdata = json_data;   
                document.getElementById("badge_order_num").innerHTML = getdata.ordernum;
                        }
                    }); 
                }
        }, 500);
        </script>
        <!-- Page Content  -->
        <div id="content">

        <div class="container-fluid" style="background-color:#DDDDDD;padding-top:10px">
        <div class="container" style="background-color:White;padding-top:10px">


            <div class="container-fluid">

                <div class="row">
                        <div class="col-md-10">
                        </div>
                        <div class="col-md-2">
                        <?php  
                      
                        $strgetcheckaddproduct="SELECT * FROM tbl_store WHERE store_code = '".$_SESSION['store_code']."' ";
                        $resultstrgetcheckaddproduct = @$conn->query($strgetcheckaddproduct);
                        if($resultstrgetcheckaddproduct->num_rows > 0){
                            while($rowstrgetcheckaddproduct = $resultstrgetcheckaddproduct->fetch_assoc()){ 
                                    $store_status = $rowstrgetcheckaddproduct['store_status'];
                            }}
                            $store_status2="0";
                            $strgetcheckaddproduct2="SELECT * FROM tbl_shipping WHERE store_code = '".$_SESSION['store_code']."' ";
                            $resultstrgetcheckaddproduct2 = @$conn->query($strgetcheckaddproduct2);
                            if($resultstrgetcheckaddproduct2->num_rows > 0){
                               
                                        $store_status2 = "1";
                                }
                        ?>
                        <?php
                        if($store_status=="1" && $store_status2 == "1"){ ?>
                        <label style="text-align: right;" data-toggle="modal" data-target="#addproduct" ><h4 class="btn btn-success">เพิ่มสินค้า</h4></label> 
                        <?php }else{?>
                            <label style="text-align: right;"  disabled><h4 style="cursor:no-drop;" class="btn btn-secondary" disabled>เพิ่มสินค้า</h4></label>
                            <?php if($store_status2 == "0"){?>
                            <label style="color:red;">กรุณาเพิ่มช่องทางการจัดส่ง</label>
                            <?php }?>
                            <?php if($store_status != "1"){?>
                            <label style="color:red;">กรุณารอการยืนยันร้านค้า</label>
                            <?php }?>
                        <?php }
                        ?>
                      
                   </div>
                </div>


                <div class="row">
                        <div class="col-md-12"> 
                        <table class="table table-lg">
                      
                        <?php  
                         $no = 0;
                        $strgetlistproduct ="SELECT * FROM tbl_product 
                        INNER JOIN tbl_product_type ON tbl_product.product_type_code=tbl_product_type.product_type_code
                        WHERE store_code = '".$_SESSION['store_code']."'
                        ORDER BY product_no";
                        $resultstrgetlistproduct = @$conn->query($strgetlistproduct);
                        if($resultstrgetlistproduct->num_rows > 0){
                            while($rowstrgetlistproduct = $resultstrgetlistproduct->fetch_assoc()){
                                $no++;
                                $countimage_product_count=0;
                                $countimgproduct ="SELECT COUNT(image_product_no) As image_product_count  FROM tbl_image_product WHERE product_code ='".$rowstrgetlistproduct['product_code']."' ";
                                $resultcountimgproductt = @$conn->query($countimgproduct);
                                if($resultcountimgproductt->num_rows > 0){
                                    while($rowcountimgproduct = $resultcountimgproductt->fetch_assoc()){
                                        $countimage_product_count=$rowcountimgproduct['image_product_count'];
                                    }
                                }

                                $countproductgroupcount=0;
                                $countproductgroup ="SELECT COUNT(product_group_no) As product_group_count  FROM tbl_product_group WHERE product_code ='".$rowstrgetlistproduct['product_code']."' ";
                                $resultcountproductgroup = @$conn->query($countproductgroup);
                                if($resultcountproductgroup->num_rows > 0){
                                    while($rowcountproductgroup = $resultcountproductgroup->fetch_assoc()){
                                        $countproductgroupcount=$rowcountproductgroup['product_group_count'];
                                    }
                                }
        
                        ?>
                          <div class="container" style="background-color:#E3E3E3 ;border-radius: 10px;">
                        <div class="row">
                        <div class="hovertable col-md-10" style="border: 4px solid #5A5A5A;color:#FF5F0F;font-size:18px; cursor: pointer;" 
                        product_code="<?php echo $rowstrgetlistproduct['product_code'];?>"
                        onclick="viewproduct(this)"
                        data-toggle="modal" data-target="#viewproduct"
                        >
                        <?php
                        echo $no." : "."<b style=\"color:black\" >  ชื่อสินค้า : </b>".$rowstrgetlistproduct['product_name']."<b style=\"color:black\" > รูปภาพ : </b>".$countimage_product_count.
                        " รูป "." <b style=\"color:black\" > ตัวเลือกสินค้า : </b>".$countproductgroupcount." ชิ้น " . "<b style=\"color:black\" > ประเภทสินค้า : </b>".$rowstrgetlistproduct['product_type_name'] ;
                        ?>
                        </div>
                       <div class="hovertable col-md-2" style="border-top: 4px solid #5A5A5A;border-right: 4px solid #5A5A5A;border-bottom: 4px solid #5A5A5A;  color:#FF0000  ;font-size:18px; cursor: pointer;" 
                       product_code="<?php echo $rowstrgetlistproduct['product_code'];?>"
                       onclick="deleteproduct(this)"
                       >
                        ลบสินค้า</div>
                        </div>
                        <!-- ENDPRODUCT -->

                        <!-- IMAGE -->
                        <div class="container-fluid">
                        <div class="row">
                        <div class=" col-md-12 btn btn-success" style="margin-bottom:10px;" data-toggle="modal" data-target="#addproductimage" 
                        product_code="<?php echo $rowstrgetlistproduct['product_code'];?>"
                        product_name="<?php echo $rowstrgetlistproduct['product_name'];?>"
                        onclick="addproductimage(this)">เพิ่มรูปสินค้า</div>
                       <script>
                       function deleteproduct(obj){
                                var product_code = obj.getAttribute("product_code");
                                $.ajax({
                                type: "POST",
                                dataType: 'json',
                                contentType: 'application/json',
                                async : false,
                                url: "../restapi/deleteproduct.php",
                                data:JSON.stringify({
                                product_code:product_code
                                }),
                                success: function (response) {
                                var json_data = response;
                                var apistatus = json_data.result;
                                    console.log(apistatus);
                                if(apistatus=="Success") {
                                        deletesuccess();
                                        setTimeout(function(){ 
                                        location.reload();
                                        },2600);
                                        }else{
                                                cantdelete()
                                                setTimeout(function(){ 
                                                    location.reload();
                                                },2600);
                                                    }
                            
                                }
                                });

                            }
                        function addproductimage(obj){
                            var product_code = obj.getAttribute("product_code");
                            var product_name = obj.getAttribute("product_name");
                            
                            document.getElementById("addproductimageproductcode").value=product_code;
                            document.getElementById("addproductimageproductname").value=product_name;
                        }
                       </script>
                       
                        <?php  
                         $getimgproduct ="SELECT *  FROM tbl_image_product WHERE product_code ='".$rowstrgetlistproduct['product_code']."' ";
                         $resultgetimgproduct = @$conn->query($getimgproduct);
                         if($resultgetimgproduct->num_rows > 0){
                             while($rowgetimgproduct = $resultgetimgproduct->fetch_assoc()){
                        ?>
                        <div class="col-md-2" style="border-style: solid;background-color:white; padding-bottom:20px;" align="center">
                        <label style="color:red;font-size:18px; cursor: pointer;margin-left: 140px;"
                        image_product_code ="<?php echo $rowgetimgproduct['image_product_code']; ?>"
                        onclick="deleteimageproduct(this)"
                        >X</label>
                        <img  src="../restapi/product/<?php echo $rowgetimgproduct['image_product_name'];  ?>" width="130px" height="130px" style="cursor: pointer;">
                        <br><br>
                        </div>
                             <?php 
                             }} 
                             ?>
                        </div>
                        </div>
            <!-- endimgproduct -->
<script>
function deleteimageproduct(obj){
    var image_product_code = obj.getAttribute("image_product_code");
    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: 'application/json',
                        async : false,
                        url: "../restapi/deleteimageproduct.php",
                        data:JSON.stringify({
                            image_product_code:image_product_code
                        }),
                        success: function (response) {
                            var json_data = response.result;
                            if(json_data=="Success") {
                             deletesuccess()
                             setTimeout(function(){ 
                                location.reload();
                             },2600);
                                }
                        
                            }
                        });
}
</script>

            <!-- productgroup -->
                        <div class="container-fluid">
                        <div class="row">
                        
                        <div class=" col-md-12 btn btn-success" style="margin-bottom:10px;"
                        data-toggle="modal" data-target="#addproductgroup"
                        product_code="<?php echo $rowstrgetlistproduct['product_code'];?>"
                        product_name="<?php echo $rowstrgetlistproduct['product_name'];?>"
                        onclick="addproductgroup(this)"
                        >เพิ่มตัวเลือกสินค้า</div>
                        
                        <script>
                        function addproductgroup(obj){
                        document.getElementById("productgroup_name_add").value="";
                        document.getElementById("productgroup_width_add").value="";
                        document.getElementById("productgroup_long_add").value="";
                        document.getElementById("productgroup_high_add").value="";
                        document.getElementById("productgroup_weight_add").value="";
                     
                        document.getElementById("productgroup_price_add").value="";
                        document.getElementById("productgroup_num_add").value="";

                        var product_code = obj.getAttribute("product_code");
                        var product_name = obj.getAttribute("product_name");
                        document.getElementById("productgroup_codeproduct_add").value=product_code;
                        document.getElementById("productgroup_nameproduct_add").value=product_name;
                        }
                        </script>
                        <?php  
                         $getproductgroup ="SELECT *  FROM tbl_product_group WHERE product_code ='".$rowstrgetlistproduct['product_code']."' ";
                         $resultgetproductgroup = @$conn->query($getproductgroup);
                         if($resultgetproductgroup->num_rows > 0){
                             while($rowgetproductgroup = $resultgetproductgroup->fetch_assoc()){
                        ?>

                      

                        <div class="hovertableproductgroup col-md-10" style="color:#FF5F0F;font-size:18px; cursor: pointer;" 
                        product_group_code="<?php echo $rowgetproductgroup['product_group_code'];?>"
                        onclick="viewproductgroup(this)"
                        data-toggle="modal" data-target="#viewproductgroup">         
                        <?php
                        echo "<b style=\"color:black\" > ชื่อตัวเลือกสินค้า : </b>".$rowgetproductgroup['product_group_name']."<b style=\"color:black\" > ราคา : </b>".$rowgetproductgroup['product_group_price'].
                        " บาท"."<b style=\"color:black\" > จำนวนคงเหลือ : </b>".$rowgetproductgroup['product_group_num']." ".$rowgetproductgroup['product_group_unit_name'];
                        ?>
                        
                        </div>

                        <div class="hovertableproductgroup col-md-2" style="color:#FF0000  ;font-size:18px; cursor: pointer;" 
                        product_group_code="<?php echo $rowgetproductgroup['product_group_code'];?>"
                        onclick="deleteproductgroup(this)"
                        >
                        ลบตัวเลือกสินค้า</div>

                      
                        <?php 
                        }} 
                             ?>
                           
                        </div>
                        <br>
                        </div>
                        </div>
                         <br> 
                     <?php 
                     }}?>
                        </table>

                        </div>
                </div>
                
            </div>
           
            </div>
            <br><br>
            </div>
        </div>
        <!-- END Content  -->
        <script>
  function deleteproductgroup(obj){
            var product_group_code = obj.getAttribute("product_group_code");
            $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/deleteproductgroup.php",
            data:JSON.stringify({
            product_group_code:product_group_code
            }),
            success: function (response) {
            var json_data = response;
            var apistatus = json_data.result;

            if(apistatus=="Success") {
                  
                    deletesuccess();
                    setTimeout(function(){ 
                    location.reload();
                    },2600);
                    }else{
                             cantdelete();
                             setTimeout(function(){ 
                                location.reload();
                             },2600);
                                }
          
            }
            });

         }


          function viewproduct(obj){
            document.getElementById('updateproductBtn').classList.add('btn-outline-warning');
            document.getElementById("updateproductBtn").innerHTML = "แก้ไข";
            document.getElementById("product_name").disabled =true;
            document.getElementById("product_detail").disabled = true;
            document.getElementById("product_type_name").disabled = true;
            var product_code = obj.getAttribute("product_code");
            $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/getproductdetail.php",
            data:JSON.stringify({
            product_code:product_code
            }),
            success: function (response) {
            var json_data = response;
            var getdata = json_data;
            var selectdata='';
            // console.log(getdata);
            document.getElementById("product_code").value = getdata.product_code;
            document.getElementById("product_name").value = getdata.product_name;
            document.getElementById("product_detail").value = getdata.product_detail;
            document.getElementById("product_type_name").value = getdata.product_type_code;
            }
            });

                            }

            function viewproductgroup(obj){

            document.getElementById('updateproductgroupBtn').classList.add('btn-outline-warning');
            document.getElementById("updateproductgroupBtn").innerHTML = "แก้ไข";
            document.getElementById("product_name").disabled =true;
            document.getElementById("product_detail").disabled = true;
            document.getElementById("product_type_name").disabled = true;
            var product_group_code = obj.getAttribute("product_group_code");

                         $.ajax({
                            type: "POST",
                            dataType: 'json',
                            contentType: 'application/json',
                            async : false,
                            url: "../restapi/getproductgroupdetail.php",
                            data:JSON.stringify({
                            product_group_code:product_group_code
                            }),
                            success: function (response) {
                            var json_data = response;
    
                            document.getElementById("productgroup_code").value = json_data.product_group_code;
                            document.getElementById("productgroup_name").value = json_data.product_group_name;
                            document.getElementById("productgroup_productname").value = json_data.product_code;
                            document.getElementById("productgroup_image").src ="../restapi/product/"+ json_data.product_group_picture;
                            document.getElementById("productgroup_width").value = json_data.product_group_wide;
                            document.getElementById("productgroup_long").value = json_data.product_group_long;
                            document.getElementById("productgroup_high").value = json_data.product_group_high;
                            document.getElementById("productgroup_weight").value = json_data.product_group_weight;
                            document.getElementById("productgroup_unit_add").value = json_data.product_group_unit_name;
                            document.getElementById("productgroup_price").value = json_data.product_group_price;
                            document.getElementById("productgroup_num").value = json_data.product_group_num;
                          
                                            }
                                        });

                            }
    </script>


    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
    <!-- End of Sidebar  -->

            <!-- Modal LOGOUT-->
            <div class="modal fade" id="logoutsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/logout.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;" class="modalfont">กำลังออกจากระบบ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

      <!-- Modal UPDATESUCCESS-->
      <div class="modal fade" id="updatesuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;" class="modalfont">แก้ไขข้อมูลเรียบร้อย</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

      <!-- Modal ALERTTEL-->
      <div class="modal fade" id="alerttel" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;" class="modalfont">เบอร์โทรศัพท์ของท่าน ซ้ำ!! ในระบบ กรุณากรอก เบอร์โทรศัพท์ อื่น</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

         <!-- Modal ALERTMAIL-->
         <div class="modal fade" id="alertmail" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;" class="modalfont">E-Mail ของท่าน ซ้ำ!! ในระบบ กรุณากรอก E-Mail อื่น</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

           <!-- Modal PRODUCTNAME-->
           <div class="modal fade" id="alertproductname" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;" class="modalfont">ชื่อสินค้าซ้ำกรุณาใช้ชื่ออื่นค่ะ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

        <!-- Modal PRODUCTNAME-->
        <div class="modal fade" id="alertproductgroupname" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;" class="modalfont">ชื่อตัวเลือกสินค้าซ้ำกรุณาใช้ชื่ออื่นค่ะ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

             <!-- Modal PRODUCTNAME-->
             <div class="modal fade" id="alertproductgroupname" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;" class="modalfont">ชื่อตัวเลือกสินค้าซ้ำกรุณาใช้ชื่ออื่นค่ะ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

           <!-- Modal alertexception-->
           <div class="modal fade" id="alertexception" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;" class="modalfont">เกิดข้อผิดพลาดกรุณาลองอีกครั้ง</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

 <!-- Modal AddataSUCCESS-->
 <div class="modal fade" id="insertsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <span class="modalfont">เพิ่มข้อมูลสำเร็จ</span><span id="insertsuccesshtml"></span>
        </div>
        <div style="color:black;" class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

<!--  viewproduct Modal -->
<div class="modal" id="viewproduct">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">ดูข้อมูลสินค้า</h2>
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">ชื่อสินค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input hidden type="text" class="form-control form-control-sm" id="product_code" name="product_code" >
                                <input type="text" class="form-control form-control-sm" id="product_name" name="product_name" disabled placeholder="ชื่อสินค้า">
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">รายละเอียด : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <textarea rows="10" class="form-control form-control-sm" id="product_detail" name="product_detail"  disabled placeholder="รายละเอียด">
                                </textarea>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">ประเภทสินค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <select id="product_type_name" name="product_type_name" disabled>
                                <?php 
                                $getlistproducttype ="SELECT * FROM tbl_product_type";
                                $resultgetlistproducttype = @$conn->query($getlistproducttype);
                                if($resultgetlistproducttype->num_rows > 0){
                                    while($rowgetlistproducttype = $resultgetlistproducttype->fetch_assoc()){
                                ?>
                                <option value="<?php  echo $rowgetlistproducttype['product_type_code']?>">  
                                <?php  echo $rowgetlistproducttype['product_type_name']?>
                                </option>
                                <?php 
                                    }
                                }
                                ?>
                                </select>
                            </div>
                        </div>
                   
                        </div>
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                       
                        <a id="updateproductBtn" class="btn btn-outline-warning btn-sm" onclick="updateproduct()">แก้ไข</a>
                        
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    <!--  End of viewproduct Modal -->
    <script>
    function updateproduct(){
            var button = document.getElementById("updateproductBtn").innerHTML;
            document.getElementById("product_name").disabled = false;
            document.getElementById("product_detail").disabled = false;
            document.getElementById("product_type_name").disabled = false;
            document.getElementById('updateproductBtn').classList.remove('btn-outline-warning');
            document.getElementById('updateproductBtn').classList.add('btn-outline-success');
            document.getElementById("updateproductBtn").innerHTML = "ยืนยัน";
            if(button == "ยืนยัน"){
            var product_code = document.getElementById("product_code").value;
            var product_name = document.getElementById("product_name").value;
            var product_detail = document.getElementById("product_detail").value;
            var product_type_code = document.getElementById("product_type_name").value;
            var store_code = "<?php echo $_SESSION['store_code']?>";
           // console.log(product_code+product_name+product_detail+product_type_code);
          
                $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/updateproduct.php",
                data:JSON.stringify({
                    store_code:store_code,
                    product_code:product_code,
                    product_name:product_name,
                    product_detail:product_detail,
                    product_type_code:product_type_code
                }),
                success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;
                console.log(apistatus);
                if(apistatus=="Success") {
                    $('#viewproduct').modal('hide');
                    updatesuccess();
                    setTimeout(function(){ 
                    location.reload();
                    },1600);
                    }else if(apistatus=="PRODUCT_NAME_DUPLICATE"){
                    $('#viewproduct').modal('hide');
                    alertproductname();
                    setTimeout(function(){
                        location.reload();
                    },2600);
                    }else{
                    $('#viewproduct').modal('hide');
                    alertexception();
                    setTimeout(function(){
                        location.reload();
                    },2600);
                    }

                }
            });

            }
    }
    </script>

 <!--  addproduct Modal -->
<div class="modal" id="addproduct">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">เพิ่มสินค้า</h2>
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">ชื่อสินค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input type="text" class="form-control form-control-sm" id="add_product_name" name="add_product_name" placeholder="ชื่อสินค้า">
                            </div>
                            <br>
                            <div class="col-sm-12" align="center">
                                <p hidden id="add_product_name_req" name="add_product_name_req" style="color:red; font-size:14px;">กรุณากรอกชื่อสินค้าด้วยค่ะ</p>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">รายละเอียด : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <textarea rows="10"  class="form-control form-control-sm" id="add_product_detail" name="add_product_detail" placeholder="รายละเอียด"></textarea>
                            </div>
                            <br>
                            <div class="col-sm-12" align="center">
                                <p hidden id="add_product_detail_req" name="add_product_detail_req" style="color:red; font-size:14px;">กรุณากรอกรายละเอียดสินค้าด้วยค่ะ</p>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">ประเภทสินค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <select id="add_product_type_code" name="add_product_type_code">
                                <?php 
                                $getlistproducttype ="SELECT * FROM tbl_product_type";
                                $resultgetlistproducttype = @$conn->query($getlistproducttype);
                                if($resultgetlistproducttype->num_rows > 0){
                                    while($rowgetlistproducttype = $resultgetlistproducttype->fetch_assoc()){
                                ?>
                                <option value="<?php  echo $rowgetlistproducttype['product_type_code']?>">  
                                <?php  echo $rowgetlistproducttype['product_type_name']?>
                                </option>
                                <?php 
                                    }
                                }
                                ?>
                                </select>
                            </div>
                        </div>
                   
                        </div>
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                       
                        <a id="updateproductBtn" class="btn btn-xl btn-outline-success btn-sm" onclick="addproduct()">ยืนยัน</a>
                        
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    <!--  End of addproduct Modal -->
    <script>
    function addproduct(){
        var store_code = "<?php echo $_SESSION['store_code']?>";
        var add_product_type_code = document.getElementById("add_product_type_code").value;
        var add_product_detail = document.getElementById("add_product_detail").value;
        var add_product_name = document.getElementById("add_product_name").value;
        
        if(add_product_name == "") {
            document.getElementById("add_product_name_req").hidden=false;
        } else {
            document.getElementById("add_product_name_req").hidden=true;
        }

        if(add_product_detail == "") {
            document.getElementById("add_product_detail_req").hidden=false;
        } else {
            document.getElementById("add_product_detail_req").hidden=true;
        }

        if(add_product_name != "" && add_product_detail != ""){
        $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/addproduct.php",
                data:JSON.stringify({
                    store_code:store_code,
                    product_type_code:add_product_type_code,
                    product_detail:add_product_detail,
                    product_name:add_product_name
                }),
                success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;
                console.log(apistatus);
                if(apistatus=="Success") {
                    $('#addproduct').modal('hide');
                    insertsuccess();
                    setTimeout(function(){ 
                    location.reload();
                    },1600);
                    }else if(apistatus=="PRODUCT_NAME_DUPLICATE"){
                    $('#addproduct').modal('hide');
                    alertproductname();
                    setTimeout(function(){
                        location.reload();
                    },2600);
                    }else{
                    $('#addproduct').modal('hide');
                    alertexception();
                    setTimeout(function(){
                        location.reload();
                    },2600);
                    }

                }
            });
        }
            
    }
    </script>

        <!--  viewproductgroup Modal -->
<div class="modal" id="viewproductgroup">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">ข้อมูลตัวเลือกสินค้า</h2>
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">ชื่อตัวเลือกสินค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input hidden type="text" class="form-control form-control-sm" id="productgroup_code" name="productgroup_code"  >
                                <input type="text" class="form-control form-control-sm" id="productgroup_name" name="productgroup_name"  disabled>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">ของสินค้า : </div>
                            <div class="col-sm-6 d-inline-flex">

               <select class="form-control " id="productgroup_productname" name="productgroup_productname" style="height:40px;" disabled>
                <?php
                $getproductlistgroup = "SELECT * FROM tbl_product WHERE store_code = '".$_SESSION['store_code']."' ORDER BY product_no ";
                $resultgetproductlistgroup = @$conn->query($getproductlistgroup);
                if($resultgetproductlistgroup->num_rows > 0){
                    while($rowgetproductlistgroup = $resultgetproductlistgroup->fetch_assoc()){
                ?>
                <option value="<?php echo $rowgetproductlistgroup['product_code'] ?>" ><?php echo $rowgetproductlistgroup['product_name'] ?></option>
                    <?php }}?>

                </select>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">รูปตัวเลือกสินค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                            <img id="productgroup_image" name="productgroup_image" width="100px" height="100px">
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-sm-4"></div>
                            <div class="col-sm-6 d-inline-flex">
                            <input type="file" onchange="loadFileproductgroup(event)" accept="image/*" id="uploadproductgroup_image" name="uploadproductgroup_image" disabled>
                            </div>
                        </div>
                        <script>
                            var loadFileproductgroup = function(event) {
                                var output = document.getElementById('productgroup_image');
                                output.src = URL.createObjectURL(event.target.files[0]);
                            };
                           
                            function chkNumber(ele)
                            {
                            var vchar = String.fromCharCode(event.keyCode);
                            if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
                            ele.onKeyPress=vchar;
                            }
                        </script>
                            
                        
                        <div class="row" >
                            <div class="col-sm-4">กว้าง : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input OnKeyPress="return chkNumber(this)" placeholder="0.00" maxlength="12" type="text" class="form-control form-control-sm" id="productgroup_width" name="productgroup_width"  disabled>
                            </div>
                            <div class="col-sm-2">เซนติเมตร</div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">ยาว : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input  OnKeyPress="return chkNumber(this)" placeholder="0.00" maxlength="12" type="text" class="form-control form-control-sm" id="productgroup_long" name="productgroup_long"  disabled>
                            </div>
                            <div class="col-sm-2">เซนติเมตร</div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">สูง : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input OnKeyPress="return chkNumber(this)" placeholder="0.00"  maxlength="12"  type="text" class="form-control form-control-sm" id="productgroup_high" name="productgroup_high"  disabled>
                            </div>
                            <div class="col-sm-2">เซนติเมตร</div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">น้ำหนัก : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input  OnKeyPress="return chkNumber(this)" placeholder="0.00"  maxlength="12"  type="text" class="form-control form-control-sm" id="productgroup_weight" name="productgroup_weight"  disabled>
                            </div>
                            <div class="col-sm-2">กรัม</div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">หน่วยนับ : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <!-- <input type="text" class="form-control form-control-sm" id="productgroup_unit" name="productgroup_unit"  disabled> -->
                            
                                <select style=" height:40px;" class="form-control form-control-sm" id="productgroup_unit_add" name="productgroup_unit_add" disabled>
                            <option value="ชิ้น" hidden selected>ชิ้น</option>
                                <?php 
                                $strnametype="SELECT * FROM tbl_nametype ORDER BY nametype_name ASC";
                                     $resultstrnametype = @$conn->query($strnametype);
                                     if(@$resultstrnametype->num_rows >0){
                                         while($rowstrnametype = $resultstrnametype->fetch_assoc()){
                                ?>
                                <option value="<?php echo $rowstrnametype['nametype_name'];?>"><?php echo $rowstrnametype['nametype_name'];?></option>
                        <?php }}?>
                            </select>
                            
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">ราคา : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input  OnKeyPress="return chkNumber(this)" placeholder="0.00" maxlength="12"  type="text" class="form-control form-control-sm" id="productgroup_price" name="productgroup_price"  disabled>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">คงเหลือในคลัง : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input  OnKeyPress="return chkNumber(this)" placeholder="0.00" maxlength="12"  type="number" class="form-control form-control-sm" id="productgroup_num" name="productgroup_num"  disabled>
                            </div>
                        </div>
                    

                        </div>
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="updateproductgroupBtn" class="btn btn-outline-warning btn-sm" onclick="updateproductgroup()">แก้ไข</a>
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    <!--  End of viewproductgroup Modal -->

    <script>
    function updateproductgroup(){
            var button = document.getElementById("updateproductgroupBtn").innerHTML;
            document.getElementById("productgroup_name").disabled = false;
            document.getElementById("productgroup_productname").disabled = false;
            document.getElementById("uploadproductgroup_image").disabled = false;
            document.getElementById("productgroup_width").disabled = false;
            document.getElementById("productgroup_long").disabled = false;
            document.getElementById("productgroup_high").disabled = false;
            document.getElementById("productgroup_weight").disabled = false;
            document.getElementById("productgroup_unit_add").disabled = false; 
            document.getElementById("productgroup_price").disabled = false;
            document.getElementById("productgroup_num").disabled = false;
        
            document.getElementById('updateproductgroupBtn').classList.remove('btn-outline-warning');
            document.getElementById('updateproductgroupBtn').classList.add('btn-outline-success');
            document.getElementById("updateproductgroupBtn").innerHTML = "ยืนยัน";

            if(button == "ยืนยัน"){
           var product_group_code = document.getElementById("productgroup_code").value;
           var product_group_name = document.getElementById("productgroup_name").value;
           var product_code = document.getElementById("productgroup_productname").value;
           var product_group_image = document.getElementById("uploadproductgroup_image").value;
           var product_group_wide = document.getElementById("productgroup_width").value;
           var product_group_long = document.getElementById("productgroup_long").value;
           var product_group_high = document.getElementById("productgroup_high").value;
           var product_group_weight = document.getElementById("productgroup_weight").value;
           var product_group_unit_name = document.getElementById("productgroup_unit_add").value;
           var product_group_price = document.getElementById("productgroup_price").value;
           var product_group_num = document.getElementById("productgroup_num").value;
      
            if(document.getElementById("uploadproductgroup_image").value==""){

                        $.ajax({
                                            type: "POST",
                                            dataType: 'json',
                                            contentType: 'application/json',
                                            async : false,
                                            url: "../restapi/updateproductgroup.php",
                                            data:JSON.stringify({
                                                product_group_code:product_group_code,
                                                product_group_name:product_group_name,
                                                product_code:product_code,
                                                product_group_wide:product_group_wide,
                                                product_group_long:product_group_long,
                                                product_group_high:product_group_high,
                                                product_group_weight:product_group_weight,
                                                product_group_unit_name:product_group_unit_name,
                                                product_group_price:product_group_price,
                                                product_group_num:product_group_num
                                            }),
                                            success: function (response) {
                                                var json_data = response;
                                                var apistatus = json_data.result;
                                                if(apistatus=="Success") {
                                                    $('#viewproductgroup').modal('hide');
                                                    updatesuccess();
                                                    setTimeout(function(){ 
                                                        location.reload();
                                                    },1600);
                                                }else if(apistatus=="PRODUCT_GROUP_NAME_DUPLICATE"){
                                                    $('#viewproductgroup').modal('hide');
                                                    alertproductgroupname();
                                                    setTimeout(function(){
                                                        location.reload();
                                                    },2600);
                                                 }else{
                                                    $('#viewproductgroup').modal('hide');
                                                    alertexception();
                                                    setTimeout(function(){
                                                        location.reload();
                                                    },2600);
                                                }

                                            

                                    }
                            });


        }else{
            var base64 = encodeImageFileAsURL(document.getElementById('uploadproductgroup_image'));
                            base64.onloadend = function() {
                            var product_group_picture = base64.result;

                            $.ajax({
                                            type: "POST",
                                            dataType: 'json',
                                            contentType: 'application/json',
                                            async : false,
                                            url: "../restapi/updateproductgroup.php",
                                            data:JSON.stringify({
                                                product_group_code:product_group_code,
                                                product_group_name:product_group_name,
                                                product_group_picture:product_group_picture,
                                                product_code:product_code,
                                                product_group_wide:product_group_wide,
                                                product_group_long:product_group_long,
                                                product_group_high:product_group_high,
                                                product_group_weight:product_group_weight,
                                                product_group_unit_name:product_group_unit_name,
                                                product_group_price:product_group_price,
                                                product_group_num:product_group_num
                                            }),
                                            success: function (response) {
                                                var json_data = response;
                                                var apistatus = json_data.result;
                                                if(apistatus=="Success") {
                                                    $('#viewproductgroup').modal('hide');
                                                    updatesuccess();
                                                    setTimeout(function(){ 
                                                        location.reload();
                                                    },1600);
                                                }else if(apistatus=="PRODUCT_GROUP_NAME_DUPLICATE"){
                                                    $('#viewproductgroup').modal('hide');
                                                    alertproductgroupname();
                                                    setTimeout(function(){
                                                        location.reload();
                                                    },2600);
                                                 }else{
                                                    $('#viewproductgroup').modal('hide');
                                                    alertexception();
                                                    setTimeout(function(){
                                                        location.reload();
                                                    },2600);
                                                }

                                            

                                    }
                            });

                            }

            
        }
                                
           

            

    }
    
    }
    </script>

    <!-- 4.1 add producttype Modal -->
<div class="modal" id="addproductimage">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">เพิ่มรูปสินค้า</h2>
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">
                        <div class="row" >
                            <div class="col-sm-4">ชื่อสินค้า : </div><br>
                            <div class="col-sm-6 d-inline-flex">
                            <input type="text" class="form-control form-control-sm" id="addproductimageproductcode" name="addproductimageproductcode"  hidden>
                            <input type="text" class="form-control form-control-sm" id="addproductimageproductname" name="addproductimageproductname"  readonly>
                            </div>
                        </div>
                        <div class="row" >
                        <div class="col-sm-4">อัพโหลดรูป : </div><br>
                            <div class="col-sm-6 d-inline-flex">
                            <img id="product_image" name="product_image" width="150px" height="150px">
                            </div>
                        </div>
                        <div class="row" >
                        <div class="col-sm-4">อัพโหลดรูป : </div><br>
                            <div class="col-sm-6 d-inline-flex">
                            <input type="file" onchange="loadFileproductimage(event)" accept="image/*" id="uploadproduct_image" name="uploadproduct_image" >
                            </div>
                        </div>
                        <script>
                            var loadFileproductimage = function(event) {
                                var output = document.getElementById('product_image');
                                output.src = URL.createObjectURL(event.target.files[0]);
                            };
                        </script>
                </div>
                </div>   
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-success btn-sm" data-dismiss="modal" onclick="addproductimagebystore()">ยืนยัน</a>
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    <!-- 4.1 End ofadd producttype Modal -->
    <script>
       
        function addproductimagebystore(){
            
            var product_code = document.getElementById("addproductimageproductcode").value;
        
            var base64 = encodeImageFileAsURL(document.getElementById('uploadproduct_image'));
                            base64.onloadend = function() {
                            var product_image = base64.result;
                            $.ajax({
                                            type: "POST",
                                            dataType: 'json',
                                            contentType: 'application/json',
                                            async : false,
                                            url: "../restapi/addimageproduct.php",
                                            data:JSON.stringify({
                                                product_code:product_code,
                                                image_product_name:product_image
                                            }),
                                            success: function (response) {
                                                var json_data = response;
                                                var apistatus = json_data.result;
          
                                                if(apistatus=="Success") {
                                                    $('#addproductimage').modal('hide');
                                                    insertsuccess();
                                                    setTimeout(function(){ 
                                                        location.reload();
                                                    },1600);
                                                }else{
                                                    $('#addproductimage').modal('hide');
                                                    alertexception();
                                                    setTimeout(function(){
                                                        location.reload();
                                                    },2600);
                                                }

                                            

                                    }
                            });


                            }
                            

        }
    </script>

            <!-- Modal DELETESUCCESS-->
 <div class="modal fade" id="deletesuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;font-family:'Opun-Regular.ttf'!important;font-size:20px;" class="modalfont">ลบข้อมูลสำเร็จ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

      <!--  addproductgroup Modal -->
      <div class="modal" id="addproductgroup">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">เพิ่มตัวเลือกสินค้า</h2>
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">

                    <div class="row" >
                            <div class="col-sm-4">ของสินค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                              <input hidden type="text" class="form-control form-control-sm" id="productgroup_codeproduct_add" name="productgroup_codeproduct_add"  >
                              <input readonly type="text" class="form-control form-control-sm" id="productgroup_nameproduct_add" name="productgroup_nameproduct_add"  >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">ชื่อตัวเลือกสินค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                              
                                <input type="text" class="form-control form-control-sm" id="productgroup_name_add" name="productgroup_name_add"  >
                            </div>
                        </div>

                        <div class="row" >
                            <div class="col-sm-4">รูปตัวเลือกสินค้า : </div>
                            <div class="col-sm-6 d-inline-flex">
                            <img id="productgroup_image_add" name="productgroup_image_add" width="150px" height="150px">
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-sm-4"></div>
                            <div class="col-sm-6 d-inline-flex">
                            <input type="file" onchange="loadFileproductgroupadd(event)" accept="image/*" id="uploadproductgroup_image_add" name="uploadproductgroup_image_add" >
                            </div>
                        </div>
                        <script>
                            var loadFileproductgroupadd = function(event) {
                                var output = document.getElementById('productgroup_image_add');
                                output.src = URL.createObjectURL(event.target.files[0]);
                            };
                        </script>
                        <div class="row" >
                            <div class="col-sm-4">กว้าง : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input OnKeyPress="return chkNumber(this)" placeholder="0.00" type="text" class="form-control form-control-sm" id="productgroup_width_add" name="productgroup_width_add"  >
                            </div>
                            <div class="col-sm-2">เซนติเมตร</div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">ยาว : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input OnKeyPress="return chkNumber(this)" placeholder="0.00" type="text" class="form-control form-control-sm" id="productgroup_long_add" name="productgroup_long_add"  >
                            </div>
                            <div class="col-sm-2">เซนติเมตร</div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">สูง : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input OnKeyPress="return chkNumber(this)" placeholder="0.00" type="text" class="form-control form-control-sm" id="productgroup_high_add" name="productgroup_high_add"  >
                            </div>
                            <div class="col-sm-2">เซนติเมตร</div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">น้ำหนัก : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input OnKeyPress="return chkNumber(this)" placeholder="0.00" type="text" class="form-control form-control-sm" id="productgroup_weight_add" name="productgroup_weight_add"  >
                            </div>
                            <div class="col-sm-2">กรัม</div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">หน่วยนับ : </div>
                            <div class="col-sm-6 d-inline-flex">
                            

                            <select style=" height:40px;" class="form-control form-control-sm" id="productgroup_unit_add" name="productgroup_unit_add">
                            <option value="ชิ้น" hidden selected>ชิ้น</option>
                                <?php 
                                $strnametype="SELECT * FROM tbl_nametype ORDER BY nametype_name ASC";
                                     $resultstrnametype = @$conn->query($strnametype);
                                     if(@$resultstrnametype->num_rows >0){
                                         while($rowstrnametype = $resultstrnametype->fetch_assoc()){
                                ?>
                                <option value="<?php echo $rowstrnametype['nametype_name'];?>"><?php echo $rowstrnametype['nametype_name'];?></option>
                        <?php }}?>
                            </select>

                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">ราคา : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input OnKeyPress="return chkNumber(this)" placeholder="0.00" type="text" class="form-control form-control-sm" id="productgroup_price_add" name="productgroup_price_add"  >
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-4">คงเหลือในคลัง : </div>
                            <div class="col-sm-6 d-inline-flex">
                                <input OnKeyPress="return chkNumber(this)" placeholder="0.00" type="text" class="form-control form-control-sm" id="productgroup_num_add" name="productgroup_num_add"  >
                            </div>
                        </div>
                    

                        </div>
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="updateproductgroupBtn" class="btn btn-outline-success btn-sm" onclick="addproductgroupbystore()">ยืนยัน</a>
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    <!--  End of addproductgroup Modal -->
<script>
function addproductgroupbystore(){
    var productgroup_codeproduct_add = document.getElementById("productgroup_codeproduct_add").value;
    var productgroup_name_add = document.getElementById("productgroup_name_add").value;
    var productgroup_width_add = document.getElementById("productgroup_width_add").value;
    var productgroup_long_add = document.getElementById("productgroup_long_add").value;
    var productgroup_high_add = document.getElementById("productgroup_high_add").value;
    var productgroup_weight_add = document.getElementById("productgroup_weight_add").value;
    var productgroup_unit_add = document.getElementById("productgroup_unit_add").value;
    var productgroup_price_add = document.getElementById("productgroup_price_add").value;
    var productgroup_num_add = document.getElementById("productgroup_num_add").value;

        if(document.getElementById("uploadproductgroup_image_add").value==""){

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    async : false,
                    url: "../restapi/addproductgroup.php",
                    data:JSON.stringify({
                        product_group_name:productgroup_name_add,
                        product_group_wide:productgroup_width_add,
                        product_group_long:productgroup_long_add,
                        product_group_high:productgroup_high_add,
                        product_group_weight:productgroup_weight_add,
                        product_group_unit_name:productgroup_unit_add,
                        product_group_price:productgroup_price_add,
                        product_group_num:productgroup_num_add,
                        product_code:productgroup_codeproduct_add
                    }),
                    success: function (response) {
                    var json_data = response;
                    var apistatus = json_data.result;
                    console.log(apistatus);
                    if(apistatus=="Success") {
                        $('#addproductgroup').modal('hide');
                        insertsuccess();
                        setTimeout(function(){ 
                        location.reload();
                        },1600);
                        }else if(apistatus=="PRODUCTGROUP_NAME_DUPLICATE"){
                        $('#addproductgroup').modal('hide');
                        alertproductname();
                        setTimeout(function(){
                            location.reload();
                        },2600);
                        }else{
                        $('#addproductgroup').modal('hide');
                        alertexception();
                        setTimeout(function(){
                            location.reload();
                        },2600);
                        }

                    }
                });

        }else{
            var base64 = encodeImageFileAsURL(document.getElementById('uploadproductgroup_image_add'));
                            base64.onloadend = function() {
                            var product_group_picture = base64.result;

                            $.ajax({
                                type: "POST",
                                dataType: 'json',
                                contentType: 'application/json',
                                async : false,
                                url: "../restapi/addproductgroup.php",
                                data:JSON.stringify({
                                    product_group_picture:product_group_picture,
                                    product_group_name:productgroup_name_add,
                                    product_group_wide:productgroup_width_add,
                                    product_group_long:productgroup_long_add,
                                    product_group_high:productgroup_high_add,
                                    product_group_weight:productgroup_weight_add,
                                    product_group_unit_name:productgroup_unit_add,
                                    product_group_price:productgroup_price_add,
                                    product_group_num:productgroup_num_add,
                                    product_code:productgroup_codeproduct_add
                                }),
                                success: function (response) {
                                var json_data = response;
                                var apistatus = json_data.result;
                                console.log(apistatus);
                                if(apistatus=="Success") {
                                    $('#addproductgroup').modal('hide');
                                    insertsuccess();
                                    setTimeout(function(){ 
                                    location.reload();
                                    },1600);
                                    }else if(apistatus=="PRODUCTGROUP_NAME_DUPLICATE"){
                                    $('#addproductgroup').modal('hide');
                                    alertproductname();
                                    setTimeout(function(){
                                        location.reload();
                                    },2600);
                                    }else{
                                    $('#addproductgroup').modal('hide');
                                    alertexception();
                                    setTimeout(function(){
                                        location.reload();
                                    },2600);
                                    }

                                }
                            });
                            
                            }

        }


      
}
</script>
 <!-- Modal CANTDELETE-->
 <div class="modal fade" id="cantdelete" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/alert.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;font-family:'Opun-Regular.ttf'!important;font-size:20px;">ไม่สามารถลบข้อมูลได้เนื่องจากมีการใช้ข้อมูลอยู่</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

</body>

</html>