<?php
    include "webservice/setting/Config.php";

    @session_start();
    @session_cache_expire(30);

    if(@$_SESSION['role'] == "admin"){
        echo "<meta  http-equiv='refresh' content='0;URL=1AdminCustomer.php'>";
    }
    if(@$_SESSION['role'] == "customer"){
        echo "<meta  http-equiv='refresh' content='0;URL=Index.php'>";
    }
    if(@$_SESSION['role'] == ""){
        echo "<meta  http-equiv='refresh' content='0;URL=Index.php'>";
    }
    
    $store_code = $_SESSION['store_code'];
    $store_money = 0;
    $store_bank_brand = "";
    $store_omise_receive_code="";
    $srtgetmonney="SELECT * FROM tbl_store WHERE store_code ='$store_code' ";
    $resultsrtgetmonney = @$conn->query($srtgetmonney);
    if($resultsrtgetmonney->num_rows > 0){
       while($rowsrtgetmonney = $resultsrtgetmonney->fetch_assoc()){
        $store_money = $rowsrtgetmonney['store_income'];
        $store_bank_brand = $rowsrtgetmonney['store_bank_brand'];
        $store_omise_receive_code = $rowsrtgetmonney['store_omise_receive_code'];
       }
    }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>จัดการข้อมูลร้านค้า</title>
    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="./css/sidebar.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="./css/styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }

        @font-face {
            font-family: 'Opun-Regular.ttf';
            src: url('fonts/Opun-Regular.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }

        body {
            font-family: 'Opun-Regular.ttf' !important;
        }

        .navbar {
            background-color: #575757;
            font-weight: bold;
            font-size: 18px;
            margin-bottom: 0px;
        }

        .navbar a {
            color: #FFA200;
            margin-top: -6px;
        }

        nav ul li a {
            font-size: 14px !important;
            text-decoration: underline;
        }

        #margin {
            margin-top: 10px;
            margin-left: 30px;
        }

        ul#menu li {
            display: inline;
            text-align: center;
            margin-left: 20px;
            margin-right: 20px;
            font-size: 18px;
            color: #575757;
        }

        ul#menu li a {
            color: #575757;
        }

        ul#menu li a:hover {
            color: #FFA200;
            text-decoration: none;
        }

        ul#label li {
            display: inline;
            text-align: center;
            margin-left: 20px;
            margin-right: 20px;
            font-size: 16px;
            color: #575757;
        }

        #hline {
            color: #FFA200 !important;
        }

        /* div a {
            font-size: 14px;
        }

        div a:hover {
            color: black;
            text-decoration: none;
        } */

        #bgblack {
            background-color: rgb(255, 94, 0);
        }

        .nav-link {
            text-decoration: none;
        }

        .nav-link:hover {
            color: black !important;
        }

        .nav-item {
            margin-top: 10px !important;
        }

        .row {
            padding-top: 10px;
        }

        .fas {
            padding-right: 5px;
            margin-right: 5px;
        }
   
    .hovertable:hover {
         background-color:#BDBDBD ; 
    }
    .hovertable{
        padding-top:7px;
        padding-bottom:7px;
        background-color:#858585;
    }
    

    .hovertableproductgroup:hover {
         background-color:#ACE7FF  ; 
    }
    .hovertableproductgroup{
        padding-top:7px;
        padding-bottom:7px;
        background-color:#2CC2FF;
    }
    .modal-header .close {
        display:none;
        }
    </style>
</head>

<script>

setTimeout(function(){
    var store_code ="<?php echo @$_SESSION['store_code'];?>";

    if(store_code!=""){ //ถ้า login
    $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/1updatewithdrawstatus.php",
            data:JSON.stringify({
                store_code:store_code
            }),
            success: function (response) {
            var json_data = response;
            var getdata = json_daFta;   
    document.getElementById("badge_order_num").innerHTML = getdata.ordernum;
            }
        }); 
    }
}, 100);
function Notenough() {
    $("#Notenough").modal({backdrop: 'static', keyboard: false})  
        $("#Notenough").modal('show');
        setTimeout(function(){$('#Notenough').modal('hide')},2000);
    }

function failwithdraw() {
    $("#failwithdraw").modal({backdrop: 'static', keyboard: false})  
        $("#failwithdraw").modal('show');
        setTimeout(function(){$('#failwithdraw').modal('hide')},2000);
    }

    function cantdelete(){
        $("#cantdelete").modal({backdrop: 'static', keyboard: false})  
      $("#cantdelete").modal('show');
      setTimeout(function(){$('#cantdelete').modal('hide')},2000);
    }
    function deletesuccess(){
        $("#deletesuccess").modal({backdrop: 'static', keyboard: false})  
      $("#deletesuccess").modal('show');
      setTimeout(function(){$('#deletesuccess').modal('hide')},2000);
    }
    function alertproductgroupname() {
        $("#alertproductgroupname").modal({backdrop: 'static', keyboard: false})  
        $("#alertproductgroupname").modal('show');
        setTimeout(function(){$('#alertproductgroupname').modal('hide')},2000);
    }
    function alertproductname() {
        $("#alertproductname").modal({backdrop: 'static', keyboard: false})  
        $("#alertproductname").modal('show');
        setTimeout(function(){$('#alertproductname').modal('hide')},2000);
    }
    function alertproductgroupname() {
        $("#alertproductgroupname").modal({backdrop: 'static', keyboard: false})  
        $("#alertproductgroupname").modal('show');
        setTimeout(function(){$('#alertproductgroupname').modal('hide')},2000);
    }
    function alertexception() {
        $("#alertexception").modal({backdrop: 'static', keyboard: false})  
        $("#alertexception").modal('show');
        setTimeout(function(){$('#alertexception').modal('hide')},2000);
    }
   function alerttel() {
    $("#alerttel").modal({backdrop: 'static', keyboard: false})  
        $("#alerttel").modal('show');
        setTimeout(function(){$('#alerttel').modal('hide')},2000);
    }
    function alertmail() {
        $("#alertmail").modal({backdrop: 'static', keyboard: false})  
        $("#alertmail").modal('show');
        setTimeout(function(){$('#alertmail').modal('hide')},2000);
    }
 function updatesuccess(){
    $("#updatesuccess").modal({backdrop: 'static', keyboard: false})  
      $("#updatesuccess").modal('show');
      setTimeout(function(){$('#updatesuccess').modal('hide')},1000);
    }

  function logoutsuccess(){
    $("#logoutsuccess").modal({backdrop: 'static', keyboard: false})  
      $("#logoutsuccess").modal('show');
      setTimeout(function(){$('#logoutsuccess').modal('hide')},2000);
    }

    function insertsuccess(){
        $("#insertsuccess").modal({backdrop: 'static', keyboard: false})  
      $("#insertsuccess").modal('show');
      setTimeout(function(){$('#insertsuccess').modal('hide')},1000);
    }
    
function encodeImageFileAsURL(element) {
  var file = element.files[0];
  var reader = new FileReader();
  reader.onloadend = function() {
   // console.log(reader.result);
  }
  reader.readAsDataURL(file);

  return reader;
}
    function cancelUpdateStoreInfo() {
      location.reload();
    }

    function logoutFunction() {
           logoutsuccess();
                   setTimeout(function(){ 
                    document.forms["formLogout"].action = "webservice/Logout.php";
                    document.forms["formLogout"].submit();
                   },2600);
    }
</script>



<body>
    <nav class="navbar navbar-expand-sm">
        <a class="navbar-brand" style="font-family:'KRR_AengAei.ttf'!important; color: #FFA200;font-size:35px;">
            <i class="fas fa-dolly"></i>ผลิตภัณฑ์ชุมชน
        </a>
        <ul class="navbar-nav mr-auto"></ul>
        <ul class="navbar-nav">
            <?php
                echo "
                    <form id=\"formLogout\" name=\"formLogout\">
                        <a class=\"nav-link\" style=\"cursor:pointer\" onclick=\"logoutFunction()\"><i class=\"fas fa-sign-out-alt\"></i>ออกจากระบบ</a>
                    </form>
                ";
            ?>
        </ul>
    </nav>

    <!-- Sidebar  -->
    <div class="wrapper">
        
        <nav id="sidebar">
            <div class="sidebar-header">
                <h4>หน้าจัดการร้านค้า</h4>
                <strong>BS</strong>
            </div>

            <ul class="list-unstyled components">
                <li >
                    <a href="Store.php">
                        <i class="fas fa-briefcase"></i>
                        ข้อมูลร้านค้า
                    </a>
                </li>
           
                <li >
                    <a href="StoreProductView.php">
                        <i class="fas fa-box"></i>
                        เพิ่มสินค้า
                    </a>
                </li>
                <li >
                    <a href="StoreMyproduct.php">
                        <i class="fas fa-box"></i>
                      สินค้าของฉัน
                    </a>
                </li>

                <li>
                    <a href="StoreTransportView.php">
                    <i class="fas fa-box"></i>
                        จัดการช่องทางการจัดส่ง
                    </a>
                </li>
                <li>
                    <a href="StoreOrderview.php">
                        <i class="fas fa-box"></i>
                       ส่งสินค้า
                        <span id="badge_order_num" class="badge badge-light"></span>
                    </a>
                </li>
                <li >
                    <a href="StoreSell_list.php">
                        <i class="fas fa-box"></i>
                        รายรับของฉัน
                        <span id="badge_order_num" class="badge badge-light"></span>
                    </a>
                </li>
                <li class="active">
                <a href="StoreWithdraw.php">
                        <i class="fas fa-box"></i>
                        ถอนเงิน
                        <span id="badge_order_num" class="badge badge-light"></span>
                    </a>
                </li>

                
                
            </ul>
        </nav>

<script>

setTimeout(function(){ 
    var store_code ="<?php echo @$_SESSION['store_code'];?>";

            if(store_code!=""){ //ถ้า login
                $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: 'application/json',
                        async : false,
                        url: "../restapi/getordernumstore.php",
                        data:JSON.stringify({
                            store_code:store_code
                        }),
                        success: function (response) {
                        var json_data = response;
                        var getdata = json_data;   
                document.getElementById("badge_order_num").innerHTML = getdata.ordernum;
                        }
                    }); 
                }
        }, 500);
        setTimeout(function(){ 
    var store_omise_receive_code ="<?php echo $store_omise_receive_code;?>";

     
                $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: 'application/json',
                        async : false,
                        url: "../restapi/getstatusbankofstore.php",
                        data:JSON.stringify({
                      store_omise_receive_code:store_omise_receive_code
                        }),
                        success: function (response) {
                           
                    var json_data = response;
                    var apistatus = json_data.result;
                  
                    if(apistatus=="Success") {
                document.getElementById("statuscheck").innerHTML = "ตรวจสอบแล้ว";
                    }else{
                document.getElementById("statuscheck").innerHTML = "กำลังตรวจสอบ";
                    }

              
                        }
                    }); 
                
        }, 500);



        function prepairgetmonney(){
            window.location.href = "StoreSell_list.php";
        }
        function getmonney(){
            window.location.href = "StoreSell_listSuccess.php";
        }
        </script>
        <!-- Page Content  -->
        <div id="content">

        <div class="container-fluid" style="background-color:#DDDDDD;padding-top:10px">
        <div class="container" style="background-color:White;padding-top:10px">
        <h4 style="font-weight:bold;">ภาพรวมยอดเงิน</h4>
        <br>

        <div class="row" style="font-size:20px;font-weight:bold;">
                <div class="col-md-6">
                ยอดเงิน
                </div>
                <div class="col-md-6">
                บัญชีธนาคารของฉัน
                </div>
        </div>
        <div class="row">
                <div class="col-md-6">
                <span style="color:black;font-size:26px;font-weight:bold;">
                <?php
                    echo "฿".$store_money;
                ?>
                </span>
                <span>
                <?php if($store_money<=0){ ?>
                <label class="btn " style="text-align: right;background-color:#B4B4B4" ><h4 style="cursor:pointer;color:white;"  >ถอนเงิน</h4></label>
                <?php }else{?>
                <label class="btn " style="text-align: right;background-color:#FF8800" data-toggle="modal" data-target="#withdraw" ><h4 style="cursor:pointer;color:white;"  >ถอนเงิน</h4></label>
                <?php }?>
                </span>
                <br>
               
                </div>
                <div class="col-md-6">
                <div style="font-size:16px;font-weight:bold;">
                <?php 
                if($store_bank_brand=="bbl"){
                    echo "<img src=\"icons/bbl.png\" width=\"50px\" height=\"50px\"> ธนาคารกรุงเทพ (BBL)";
                }
                if($store_bank_brand=="kbank"){
                    echo "<img src=\"icons/kbank.png\" width=\"50px\" height=\"50px\"> ธนาคารกสิกรไทย (KBANK)";
                }
                if($store_bank_brand=="ktb"){
                    echo "<img src=\"icons/krungthai.png\" width=\"50px\" height=\"50px\"> ธนาคารกรุงไทย (KTB)";
                }
                if($store_bank_brand=="tmb"){
                    echo "<img src=\"icons/tmb.png\" width=\"50px\" height=\"50px\"> ธนาคารทหารไทย (TMB)";
                }
                if($store_bank_brand=="scb"){
                    echo "<img src=\"icons/scb.png\" width=\"50px\" height=\"50px\"> ธนาคารไทยพาณิชย์ (SCB)";
                }
                ?>
                </div>
                <div style="font-size:16px;font-weight:bold;">
                สถานะ : <span id="statuscheck" style="background-color:#B4B4B4;"></span>
                </div>
                </div>

              
        </div>
        <br>
        </div>   
        <br>
        </div>

        <div class="container-fluid" style="background-color:#DDDDDD;padding-top:10px">
        <div class="container" style="background-color:White;padding-top:10px">
        
      
        <table class="table ">
            <tr style="font-weight:bold;" bgcolor="#DDDDDD">
            <td>จำนวนเงิน</td>
            <td>วันที่เบิก</td>
            <td align="center">สถานะ</td>
            </tr>

<?php   $getwithdraw =  "SELECT * FROM tbl_withdraw_money WHERE store_code = '$store_code' ORDER BY withdraw_money_date DESC ";
                $resultgetwithdraw = @$conn->query($getwithdraw);
                if($resultgetwithdraw->num_rows > 0){
                    while($rowgetwithdraw = $resultgetwithdraw->fetch_assoc()){

    ?>
          <tr >
                <td>
                
                <?php echo $rowgetwithdraw['withdraw_money_num']; ?> 
                
                </td>
                <td>
                <?php echo $rowgetwithdraw['withdraw_money_date']; ?> 
                </td>

                <td>
                 <?php 
                 if($rowgetwithdraw['withdraw_money_status']=="0"){
                 ?> 
                <p style="color:#DA5318 ;">กำลังโอน</p>
                <?php 
                 }
                 ?> 
                <?php 
                 if($rowgetwithdraw['withdraw_money_status']=="1"){
                 ?> 
                <p style="color:#11A92A  ;">โอนสำเร็จ</p>
                <?php 
                 }
                 ?> 
                </td>
             
            </tr>
            <?php
                    }

                }
            ?>

        </table>



        <br><br>
        </div>   
        <br><br>
        </div>

        </div>
        <!-- END Content  -->
     

<!--  withdraw Modal -->
<div class="modal" id="withdraw" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;" align="center">เพิ่มข้อมูลการจัดส่ง</h2>
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                    <div class="form-group">
                  
                    <div class="row">
                    <div class="col-sm-10 d-inline-flex">
                    
                  
                    </div>
                    </div>
                        <div class="row">
                            <div class="col-sm-4" style="font-size:20px">เลือกจำนวนเงิน : <br>
                            <b style="color:red;">ขั้นต่ำ 50 บาท</b>
                            </div>
                            <div class="col-sm-4 d-inline-flex">
                                <select id="withdraw_price" name="withdraw_price">
                                    <option value="50" selected>50.00 บาท</option>
                                    <option value="100">100.00 บาท</option>
                                    <option value="300">300.00 บาท</option>
                                    <option value="500">500.00 บาท</option>
                                    <option value="1000">1000.00 บาท</option>
                                    <option value="3000">3000.00 บาท</option>
                                    <option value="5000">5000.00 บาท</option>
                                    <option value="10000">10000.00 บาท</option>
                                </select>
                            </div>
                        
                        </div>

                        </div></div>
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-success btn-sm" data-dismiss="modal" onclick="withdraw()">ยืนยัน</a>
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    <!-- End of withdraw Modal -->
<script>
function withdraw(){
    withdraw_price = document.getElementById("withdraw_price").value;
    withdraw_price = parseInt(withdraw_price);
    store_code="<?php echo $_SESSION['store_code']; ?>";
    if(withdraw_price<50){

        failwithdraw()
        setTimeout(function(){ 
            location.reload();
        },2600);
    }else{
                 $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: 'application/json',
                        async : false,
                        url: "../restapi/requestwithdrawmoney.php",
                        data:JSON.stringify({
                            store_code:store_code,
                            withdraw_price:withdraw_price
                        }),
                        success: function (response) {
                        var json_data = response;
                        var getdata = json_data;
                        if(getdata.result=="Success"){
                            location.reload();
                        }else{
                            Notenough()
                            setTimeout(function(){ 
                            location.reload();
                            },2600);
                           
                        }         
                        }
                }); 
    }
    
    
    
}

</script>

    <!-- Modal DELETESUCCESS-->
            <div class="modal fade" id="failwithdraw" role="dialog" data-backdrop="static">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <p style="color:black" class="modalfont">กรุณาเบิก ขั้นต่ำ 50 บาทค่ะ</p>
        </div>
        <div class="modal-footer">
      
        </div>
      </div>
     
    </div>
  </div> <!-- asdasd -->


    
            <!-- Modal LOGOUT-->
            <div class="modal fade" id="logoutsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/logout.png" width="150px" height="150px">
        <br><br>
        <p style="color:black;" class="modalfont">กำลังออกจากระบบ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->
 <!-- Modal DELETESUCCESS-->
 <div class="modal fade" id="Notenough" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <p style="color:black" class="modalfont">ยอดเงินในร้านของท่านไม่พอค่ะ</p>
        </div>
        <div class="modal-footer">
      
        </div>
      </div>
      </div>
  </div> 
  <!-- asdasd -->

</body>

</html>