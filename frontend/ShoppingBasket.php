<?php
    include "../restapi/setting/config.php";
    @session_start();
    @session_cache_expire(30);

    if(@$_SESSION['role'] = "customer"){
        
    }else if(@$_SESSION['role'] = "admin"){
        echo "<meta  http-equiv='refresh' content='0;URL=1AdminCustomer.php'>";
    }else if(@$_SESSION['role'] = "store"){
        echo "<meta  http-equiv='refresh' content='0;URL=Store.php'>";
    }
    else{
        echo "<meta  http-equiv='refresh' content='0;URL=Index.php'>";
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>รถเข็น</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="./css/styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }

        .card {
            background-color: #F88360;  
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }
        .modal-header .close {
        display:none;
        }
    </style>
</head>

<script>
    setTimeout(function(){ 
    var customer_code ="<?php echo @$_SESSION['customer_code'];?>";

        $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "../restapi/getcartnum.php",
                data:JSON.stringify({
                    customer_code:customer_code
                }),
                success: function (response) {
                var json_data = response;
                var getdata = json_data;   
                 if(getdata.cartnum=="0"){
                    location.replace("index.php");
                 }
                }
            }); 

         
}, 100);

function wanttodelete() {
    
    $("#wanttodelete").modal({backdrop: 'static', keyboard: false})  
        $("#wanttodelete").modal('show');
    }
function Numnotenough() {
    $("#Numnotenough").modal({backdrop: 'static', keyboard: false})  
        $("#Numnotenough").modal('show');
        setTimeout(function(){$('#Numnotenough').modal('hide')},2000);
    }
    function Numnotenough1() {
    $("#Numnotenough1").modal({backdrop: 'static', keyboard: false})  
        $("#Numnotenough1").modal('show');
     
    }
function hasbanned() {
    $("#hasbanned").modal({backdrop: 'static', keyboard: false})  
        $("#hasbanned").modal('show');
        setTimeout(function(){$('#hasbanned').modal('hide')},3000);
    }
    function loginfail() {
        $("#loginfail").modal({backdrop: 'static', keyboard: false})  
        $("#loginfail").modal('show');
        setTimeout(function(){$('#loginfail').modal('hide')},3000);
    }

    function loginsuccess() {
        $("#loginsuccess").modal({backdrop: 'static', keyboard: false})  
        $("#loginsuccess").modal('show');
        setTimeout(function(){$('#loginsuccess').modal('hide')},3000);
    }

    function logoutsuccess() {
        $("#logoutsuccess").modal({backdrop: 'static', keyboard: false})  
        $("#logoutsuccess").modal('show');
        setTimeout(function(){$('#logoutsuccess').modal('hide')},2000);
    }

    function customerLoginFunction() {
        var customerUsername = document.getElementById("customerUsername").value;
        var customerPassword = document.getElementById("customerPassword").value;

        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/logincustomer.php",
            data:JSON.stringify({
                customerUsername:customerUsername,
                customerPassword:customerPassword
            }),
            success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;

                if(apistatus=="Success") {
                loginsuccess();
                setTimeout(function(){ 
                    location.reload();
                },3600);
                } else if(apistatus=="Banned"){
                    
                    hasbanned();
                    setTimeout(function(){ 
                    location.reload();
                    },3600);
                } else{
                    loginfail();
                    document.getElementById("adminUsername").value=""
                    document.getElementById("adminPassword").value=""
                }
            }
        });
    }

    function storeLoginFunction() {
        var storeUsername = document.getElementById("storeUsername").value;
        var storePassword = document.getElementById("storePassword").value;

        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/LoginStore.php",
            data:JSON.stringify({
                storeUsername:storeUsername,
                storePassword:storePassword
            }),
            success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;
                if(apistatus=="Success") {
                loginsuccess();
                setTimeout(function(){ 
                    location.replace("Store.php");
                },3600);
                } else if(apistatus=="Banned"){
                    hasbanned();
                    setTimeout(function(){ 
                    location.reload();
                    },3600);
                } else{
                    loginfail();
                    document.getElementById("adminUsername").value=""
                    document.getElementById("adminPassword").value=""
                }
            }
        });
    }

    function adminLoginFunction() {
        var adminUsername = document.getElementById("adminUsername").value;
        var adminPassword = document.getElementById("adminPassword").value;
    
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/LoginAdmin.php",
            data:JSON.stringify({
                adminUsername:adminUsername,
                adminPassword:adminPassword
            }),
            success: function (response) {
                var json_data = response;
                var apistatus = json_data.result;
                console.log(apistatus);

                if(apistatus=="Success") {
                loginsuccess();
                setTimeout(function(){ 
                    location.replace("Admin.php");
                },3600);
                } else if(apistatus=="Banned"){
                    hasbanned();
                    setTimeout(function(){ 
                    location.reload();
                    },3600);
                } else{
                    loginfail();
                    document.getElementById("adminUsername").value=""
                    document.getElementById("adminPassword").value=""
                }

                
            }
        });
    }

    function logoutFunction() {
        logoutsuccess();
            setTimeout(function(){ 
            document.forms["formLogout"].action = "../restapi/logout.php";
            document.forms["formLogout"].submit();
        },2600);
    }
</script>

<body>

    <!-- Modal Zone -->
    <!-- 1. Register Group Modal -->
    <div class="modal" id="registerGroupModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เลือกประเภทการสมัครสมาชิก</h2>
            </div>

            <!-- Modal body -->
                <div class="modal-body">
                    <div row align="center" style="margin-bottom: -25px;">
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 50px;">
                            <p style="font-size: 60px; color: #17a2b8; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-users"></i></p>
                        </div>
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 10px;">
                            <p style="font-size: 60px; color: #007bff; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-store"></i></p>
                        </div>
                    </div>
                    <div row align="center">
                        <div class="col-sm-5 d-inline-flex">
                            <a class="btn btn-info" type="button" href="CustomerRegister.php">สำหรับลูกค้า</a>
                        </div>
                        <div class="col-sm-5 d-inline-flex">
                            
                        <a class="btn btn-primary" type="button" href="StoreRegister.php">สำหรับร้านค้า</a>
                        </div>
                    </div>
                </div>

            <!-- Modal footer -->
            <br>
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-danger" type="button" data-dismiss="modal">ปิดหน้านี้</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 1. End of Register Group Modal -->

    <!-- 2. Login Group Modal -->
    <div class="modal" id="loginGroupModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เลือกประเภทการเข้าสู่ระบบ</h2>
            </div>

            <!-- Modal body -->
                <div class="modal-body">
                    <div row align="center" style="margin-bottom: -25px;">
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 50px;">
                            <p style="font-size: 60px; color: #17a2b8; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-users"></i></p>
                        </div>
                        <div class="col-sm-5 d-inline-flex" style="margin-left: 10px;">
                            <p style="font-size: 60px; color: #007bff; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-store"></i></p>
                        </div>
                    </div>
                    <div row align="center" style="margin-bottom: 30px;">
                        <div class="col-sm-5 d-inline-flex">
                            <button data-toggle="modal" data-target="#loginCustomerModal" class="btn btn-info" type="button" data-dismiss="modal">สำหรับลูกค้า</button>
                        </div>
                        <div class="col-sm-5 d-inline-flex">
                            <button data-toggle="modal" data-target="#loginStoreModal" class="btn btn-primary" type="button" data-dismiss="modal">สำหรับร้านค้า</button>
                        </div>
                    </div>
                    <div row align="center" style="margin-bottom: -25px;">
                            <p style="font-size: 60px; color: #343a40; text-shadow: 2px 2px 2px #000000;"><i class="fas fa-crown"></i></p>
                    </div>
                    <div row align="center">
                        <button data-toggle="modal" data-target="#loginAdminModal" class="btn btn-dark" type="button" data-dismiss="modal">สำหรับแอดมิน</button>
                    </div>
                </div>

            <!-- Modal footer -->
            <br>
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-danger" type="button" data-dismiss="modal">ปิดหน้านี้</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2. End of Login Group Modal -->

    <!-- 2.1 Login Customer Modal -->
    <div class="modal" id="loginCustomerModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เข้าสู่ระบบ (สำหรับลูกค้า)</h2>
            </div>

            <!-- Modal body -->
            <form id="formLoginCustomer" name="formLoginCustomer" method="POST">
                <div class="modal-body">
                    <img src="./icons/loginlogo.png" width="150px" height="150px">
                    <div class="form-group col-sm-9">
                        <label for="customerUsername" style="font-weight: regular;">ชื่อผู้ใช้</label>
                        <input type="text" placeholder="เบอร์โทรศัพท์หรืออีเมลล์" class="form-control" id="customerUsername" name="customerUsername">
                    </div>
                    <div class="form-group col-sm-9">
                        <label for="customerPassword" style="font-weight: regular;">รหัสผ่าน</label>
                        <input type="password" placeholder="รหัสผ่าน" class="form-control" id="customerPassword" name="customerPassword">
                    </div>
                </div>

            <!-- Modal footer -->
            <br>
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-success" type="button" onclick="customerLoginFunction()">เข้าสู่ระบบ</button>
                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#loginGroupModal" data-dismiss="modal">ยกเลิก</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2.1 End of Login Customer Modal -->

    <!-- 2.2 Login Store Modal -->
    <div class="modal" id="loginStoreModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">

            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เข้าสู่ระบบ (สำหรับร้านค้า)</h2>
            </div>

            <!-- Modal body -->
            <form id="formLoginStore" name="formLoginStore" method="POST">
                <div class="modal-body">
                    <img src="./icons/loginlogo.png" width="150px" height="150px">
                    <div class="form-group col-sm-9">
                        <label for="storeUsername" style="font-weight: regular;">ชื่อผู้ใช้</label>
                        <input type="text" placeholder="เบอร์โทรศัพท์หรืออีเมลล์" class="form-control" id="storeUsername" name="storeUsername">
                    </div>
                    <div class="form-group col-sm-9">
                        <label for="storePassword" style="font-weight: regular;">รหัสผ่าน</label>
                        <input type="password" placeholder="รหัสผ่าน" class="form-control" id="storePassword" name="storePassword">
                    </div>
                </div>

            <!-- Modal footer -->
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-success" type="button" onclick="storeLoginFunction()">เข้าสู่ระบบ</button>
                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#loginGroupModal" data-dismiss="modal">ยกเลิก</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2.2 End of Login Store Modal -->

    <!-- 2.3 Admin Login Modal -->
    <div class="modal" id="loginAdminModal">
    <div class="modal-dialog">
        <div class="modal-content" align="center">
                
            <!-- Modal Header -->
            <div class="modal-header d-block">
                <h2 class="modal-title" style="font-weight: bold;">เข้าสู่ระบบ (สำหรับผู้ดูแลระบบ)</h2>
            </div>
            <form id="formAdminLogin" name="formAdminLogin" method="POST">

            <!-- Modal body -->
                <div class="modal-body">
                    <img src="./icons/loginlogo.png" width="150px" height="150px">
                    <div class="form-group col-sm-9">
                        <label for="adminUsername" style="font-weight: regular;">ชื่อผู้ใช้</label>
                        <input type="text" placeholder="ชื่อผู้ใช้งานผู้ดูแล" class="form-control" id="adminUsername" name="adminUsername">
                    </div>
                    <div class="form-group col-sm-9">
                        <label for="adminPassword" style="font-weight: regular;">รหัสผ่าน</label>
                        <input type="password" placeholder="รหัสผ่าน" class="form-control" id="adminPassword" name="adminPassword">
                    </div>
                </div>

            <!-- Modal footer -->
                <div class="modal-footer d-block" align="center">
                    <button class="btn btn-success" type="button" onclick="adminLoginFunction()">เข้าสู่ระบบ</button>
                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#loginGroupModal" data-dismiss="modal">ยกเลิก</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <!-- 2.3 End of Admin Login Modal -->
    <!-- End of Modal Zone -->

    <nav class="navbar navbar-expand-sm">
        <a class="navbar-brand" style="font-family:'KRR_AengAei.ttf'!important; color: #FFA200;font-size:35px;" href="index.php">
            <i class="fas fa-dolly"></i>ผลิตภัณฑ์ชุมชน
        </a>
        <ul class="navbar-nav mr-auto"></ul>
        <ul class="navbar-nav">
            <?php
                if (@$_SESSION['role'] == "customer") {
                    $strSQL = "SELECT * FROM tbl_customer WHERE customer_code = '".$_SESSION['customer_code']."' ";
                    $result = @$conn->query($strSQL);
                    while($row = $result->fetch_assoc()){
                        echo "
                            <li class=\"nav-item\">
                               <a href=\"CustomerInfoNewForm.php\" class=\"nav-link\"><i class=\"fas fa-user\"></i>ข้อมูลส่วนตัว</a>
                            </li>
                            <li class=\"nav-item\">
                                <a style=\"color: #FFA200 !important;,font-size: 14px !important;\" class=\"nav-link\">ยินดีต้อนรับ&nbspคุณ&nbsp :&nbsp " . @$row['customer_fullname'] . "</a>
                            </li>
                            <form id=\"formLogout\" name=\"formLogout\">
                                <li class=\"nav-item\">
                                    <a class=\"nav-link\" style=\"cursor:pointer\" onclick=\"logoutFunction()\"><i class=\"fas fa-sign-out-alt\"></i>ออกจากระบบ</a>
                                </li>
                            </form>
                       
                        ";
                    }
                } else {
                    echo "
              
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#registerGroupModal\" style=\"cursor:pointer\"><i class=\"fas fa-user-plus\"></i>สมัครสมาชิก</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#loginGroupModal\" style=\"cursor:pointer\"><i class=\"fas fa-sign-in-alt\"></i>เข้าสู่ระบบ</a>
                       
                    ";
                }
            ?>
        </ul>
    </nav>
    <script>
function opencart(){
    var customer_code ="<?php echo @$_SESSION['customer_code'];?>";

    if(customer_code==""){
            console.log("ไม่ล้อคอิน");
    }else{
        console.log("ไปหน้าตะกร้า");
        location.replace("index.php");
    }

}
    </script>
   <!-- BODY -->
  

    <div class="row">
            <div class="col-md-1"></div>

            <div class="col-md-10">
   <div class="jumbotron">

   <div align="center">
        <div id="rcorners2" style="margin-bottom:30px; padding-bottom:5px; padding-top:5px;">
            <div class="row" style="background-color:white;font-weight:bold;font-size:20px;" >
                <div class="col-sm-1">
                    <!-- <input class="checkbox" type="checkbox" name="selectAll" value="" stlye="background-color:orange !Important;"> -->
                </div>
                <div class="col-sm-4" align="left">
                    <label>สินค้าในตะกร้า</label>
                </div>
                <div class="col-sm-2" align="left">
                    <label>ราคาต่อชิ้น</label>
                </div>
                <div class="col-sm-2" align="center">
                    <label>จำนวน</label>
                </div>
                <div class="col-sm-2" align="center">
                    <label>ราคารวม</label>
                </div>
                <div class="col-sm-1" align="center">
                    <label>ลบสินค้า</label>
                </div>
            </div>
        </div>


 <?php 
$getstore="SELECT * FROM tbl_cart 
INNER JOIN tbl_store ON tbl_cart.store_code = tbl_store.store_code WHERE customer_code ='".$_SESSION['customer_code']."' ";
  $resultgetstore = @$conn->query($getstore);
  if(@$resultgetstore->num_rows >0){
      while($rowgetstore = $resultgetstore->fetch_assoc()){
     
?> 
<div> 
        <div id="rcorners2" style="padding-bottom:10px; padding-top:5px;">
            <div class="row" style="background-color:#464646;color:white">
                <div class="col-sm-1" >
        
                </div>
                <div class="col-sm-4" align="left" >
                    <label>ร้าน : <?php echo $rowgetstore['store_namestore']; ?></label>
                </div>
            </div>
        </div>
        
        <?php
       
            $getcartdetail="SELECT * FROM tbl_cart_detail 
            INNER JOIN tbl_product_group ON tbl_cart_detail.product_group_code = tbl_product_group.product_group_code  
            INNER JOIN tbl_product ON tbl_product_group.product_code = tbl_product.product_code  
            WHERE cart_code ='".$rowgetstore['cart_code']."' ";
            $resultgetcartdetail = @$conn->query($getcartdetail);
            if(@$resultgetcartdetail->num_rows >0){
                while($rowgetcartdetail = $resultgetcartdetail->fetch_assoc()){


                    if($rowgetcartdetail['product_banned']=="1"){
                        $cart_detail_code = $rowgetcartdetail['cart_detail_code'];
                        $strDelete="DELETE FROM tbl_cart_detail WHERE cart_detail_code ='".$cart_detail_code."' ";
                        $conn->query($strDelete);
                    }
        ?>
        <!-- bodycart -->
        <div id="rcorners2" style="padding-bottom:10px; padding-top:5px;">
            <div class="row" style="background-color:white">
                <div class="col-sm-1" style="margin-top:25px;">

                    <?php if($rowgetcartdetail['cart_detail_status']=="0"){
                     ?>
                    <input class="checkbox" type="checkbox" name="selectAll" value="" stlye="background-color:orange !Important;"
                    cart_detail_status="0"
                    cart_detail_code="<?php echo $rowgetcartdetail['cart_detail_code'];?>"
                    onclick="updatecartdetailstatus(this)">
                    <?php  }else{
                    ?>
                    <input class="checkbox" type="checkbox" name="selectAll" value="" stlye="background-color:orange !Important;"
                    Checked
                    cart_detail_status="1"
                    cart_detail_code="<?php echo $rowgetcartdetail['cart_detail_code'];?>"
                    onclick="updatecartdetailstatus(this)">
                    <?php }?>

                </div>
<script>
function updatecartdetailstatus(obj){
    var cart_detail_code = obj.getAttribute("cart_detail_code");
    var cart_detail_status = obj.getAttribute("cart_detail_status");

    console.log(cart_detail_status);
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            async : false,
            url: "../restapi/updatecartdetailstatus.php",
            data:JSON.stringify({
            cart_detail_code:cart_detail_code,
            cart_detail_status:cart_detail_status
            }),
            success: function (response) {
            var json_data = response;
            var getdata = json_data;
            if(getdata.result=="Success"){
                location.reload();
            }
            }
        });

}
</script>
                <div class="col-sm-4" align="left">
                    <img src="../restapi/product/<?php  echo $rowgetcartdetail['product_group_picture']; ?>" width="80px" height="80px" style="margin-right:20px;">
                    <label style="font-weight:bold;font-size:20px;"><?php echo $rowgetcartdetail['product_name'];?></label>
                    <label>ตัวเลือก <?php echo $rowgetcartdetail['product_group_name'];?></label>
                </div>
                <div class="col-sm-2" align="left" style="margin-top:25px;">
                    <label>฿ <span><?php  echo $rowgetcartdetail['product_group_price']; ?></span></label>
                </div>
                <div class="col-sm-2" align="left" style="margin-top:25px;">

               <div class="input-group">
<script>
function chkNumber(ele)
    {
    var vchar = String.fromCharCode(event.keyCode);
    if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
    ele.onKeyPress=vchar;
    }
function acceptnumberoforder(xxx){
    var cart_detail_code = xxx.split("m");
    cart_detail_code = cart_detail_code[1].trim();
                                
    var cart_detail_num =  document.getElementById(xxx).value;

    cart_detail_num = parseInt(cart_detail_num);
    console.log(cart_detail_num);
    if(cart_detail_num==0){
    document.getElementById("wanttodeletedetailid").value=cart_detail_code;
    wanttodelete();
    }else{

        $.ajax({
        type: "POST",
        dataType: 'json',
        contentType: 'application/json',
        async : false,
        url: "../restapi/updatecartdetailbytype.php",
        data:JSON.stringify({
        cart_detail_code:cart_detail_code,
        cart_detail_num:cart_detail_num
        }),
        success: function (response) {
        var json_data = response;
        var getdata = json_data;
        if(getdata.result=="Success"){
        location.reload();
        }else{
        Numnotenough1();
        }
        }
        });

    }
                             
}        
                </script>
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number" id="cart_detail_num_minus" name="cart_detail_num_minus"
              product_group_name="<?php echo $rowgetcartdetail['product_group_name'];?>"
              product_name="<?php echo $rowgetcartdetail['product_name'];?>"
              cart_detail_code="<?php echo $rowgetcartdetail['cart_detail_code'];?>"
              cart_detail_num="<?php  echo $rowgetcartdetail['cart_detail_num']; ?>"
              onclick="cart_detail_num_minus(this)">
                  <span class="fas fa-minus"></span>
              </button>
          </span>

          <input OnKeyPress="return chkNumber(this)" maxlength="3"  onchange="acceptnumberoforder(this.id)" cart_detail_code="<?php echo $rowgetcartdetail['cart_detail_code'];?>" type="text" id="cart_detail_num<?php echo $rowgetcartdetail['cart_detail_code'];?>" name="cart_detail_num<?php echo $rowgetcartdetail['cart_detail_code'];?>" class="form-control input-number" value="<?php  echo $rowgetcartdetail['cart_detail_num']; ?>" style="text-align:center">
          
          <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-number" id="cart_detail_num_plus" name="cart_detail_num_plus"
              product_group_code="<?php echo $rowgetcartdetail['product_group_code'];?>"
              cart_detail_code="<?php echo $rowgetcartdetail['cart_detail_code'];?>"
              cart_detail_num="<?php  echo $rowgetcartdetail['cart_detail_num']; ?>"
              onclick="cart_detail_num_plus(this)">
                  <span class="fas fa-plus"></span>
              </button>
          </span>
            </div>
                </div>
                <div class="col-sm-2" align="center" style="margin-top:25px;">
                <label>฿ <span><?php
                
       
        $totalpricecart=0;
        $gettotalprice="SELECT * FROM tbl_cart_detail INNER JOIN tbl_product_group ON tbl_cart_detail.product_group_code = tbl_product_group.product_group_code  
        INNER JOIN tbl_product ON tbl_product_group.product_code = tbl_product.product_code  
        WHERE cart_code ='".$rowgetstore['cart_code']."' AND cart_detail_status='1' ";
        $resultgettotalprice = @$conn->query($gettotalprice);
        if(@$resultgettotalprice->num_rows >0){
            while($rowgettotalprice = $resultgettotalprice->fetch_assoc()){
             
                $totalpricecart = $totalpricecart + $rowgettotalprice['product_group_price']*$rowgettotalprice['cart_detail_num'];
               
            }}
          
                echo $rowgetcartdetail['product_group_price']*$rowgetcartdetail['cart_detail_num']; ?></span>.00</label>
                </div>
                <div class="col-sm-1" align="center" style="margin-top:25px;">
                    <a id="b"><i class="fas fa-times" style="font-size:30px; color:red; cursor:pointer;"
                    product_group_name="<?php echo $rowgetcartdetail['product_group_name'];?>"
                    product_name="<?php echo $rowgetcartdetail['product_name'];?>"
                    cart_detail_code="<?php echo $rowgetcartdetail['cart_detail_code'];?>"
                    onclick="deletecartdetail(this)"
                    ></i></a>
                </div>
            </div>
        </div>
        <script>
        function deletecartdetail(obj){
            var cart_detail_code=obj.getAttribute("cart_detail_code");
            var product_name = obj.getAttribute("product_name");
            var product_group_name = obj.getAttribute("product_group_name");
            document.getElementById("wanttodeletedetailid").value=cart_detail_code;
                wanttodelete();
        }
        </script>
        <!-- bodycart -->
        <script>
        function cart_detail_num_minus(obj){
            var product_name = obj.getAttribute("product_name");
            var product_group_name = obj.getAttribute("product_group_name");
            var cart_detail_code = obj.getAttribute("cart_detail_code");
            var cart_detail_num = obj.getAttribute("cart_detail_num");
            if(cart_detail_num=="1"){

                document.getElementById("wanttodeletedetailid").value=cart_detail_code;
             
                wanttodelete();
            }else{
                        $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    async : false,
                    url: "../restapi/updatecartdetailminus.php",
                    data:JSON.stringify({
                    cart_detail_code:cart_detail_code,
                    cart_detail_num:cart_detail_num
                    }),
                    success: function (response) {
                    var json_data = response;
                    var getdata = json_data;
                    if(getdata.result=="Success"){
                        location.reload();
                    }
                    }
                     });
            }

        }
        function cart_detail_num_plus(obj){
            var product_group_code = obj.getAttribute("product_group_code");
            var cart_detail_code = obj.getAttribute("cart_detail_code");
            var cart_detail_num = obj.getAttribute("cart_detail_num");
            $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    async : false,
                    url: "../restapi/updatecartdetailplus.php",
                    data:JSON.stringify({
                     product_group_code:product_group_code,
                    cart_detail_code:cart_detail_code,
                    cart_detail_num:cart_detail_num
                    }),
                    success: function (response) {
                    var json_data = response;
                    var getdata = json_data;
                    if(getdata.result=="Success"){
                        location.reload();
                    }else{
                        Numnotenough();
                        setTimeout(function(){ 
                               
                        },2600);
                    }
                    }
                     });
        }
        </script>
 

      <?php  }
        @$sumtotalpricecart = $sumtotalpricecart + $totalpricecart;
    }else{
           $store_codedelete = $rowgetstore['store_code'];
           $delete = "DELETE FROM tbl_cart WHERE store_code = '$store_codedelete' ";
           if($conn->query($delete)===TRUE){
               echo "<meta  http-equiv='refresh' content='0;URL=ShoppingBasket.php'>";
           }
      } //endcartdetail
      
    }}
      
      ?>
      <br><br>
   </div>
<!-- footercart -->
        <div id="rcorners2" style="margin-top:50px; padding-bottom:5px; padding-top:5px;">
            <div class="row" style="background-color:white">
                <div class="col-sm-1">
                    <!-- <input class="checkbox" type="checkbox" name="selectAll" value="" stlye="background-color:orange !Important;" -->
                    
                    
                </div>
                <div class="col-sm-5" align="left">
                    <!-- <label>เลือกทั้งหมด</label> -->
                </div>
                <div class="col-sm-2" align="right">
                    <label>รวมค่าสินค้า&nbsp;&nbsp;&nbsp;(&nbsp;1&nbsp;&nbsp;&nbsp;สินค้า)</label>
                </div>
                <div class="col-sm-2" align="center">
                    <b style="font-weight:bold; color:red;">฿ <?php
                  
                    echo $sumtotalpricecart; ?></b><span style="font-weight:bold; color:red;">.00</span>
                </div>
                <div class="col-sm-2" align="left">
                     <?php
                     $numofcheck =0;
                     $customer_code = $_SESSION['customer_code'];
                     $strgetno="SELECT * FROM tbl_cart
                     INNER JOIN tbl_cart_detail ON tbl_cart.cart_code = tbl_cart_detail.cart_code
                     WHERE customer_code='".$customer_code."' ";
                     $Resultstrgetno = @$conn->query($strgetno);
                     if($Resultstrgetno->num_rows > 0){
                         while($rowstrgetno = $Resultstrgetno->fetch_assoc()){
                        $numofcheck += $rowstrgetno['cart_detail_status'];
                         }
                    }
                     if($numofcheck==0){
                     ?>
                   <a class="btn btn-secondary" style="font-size:14px; color:black; width:180px;cursor: no-drop;" Disabled>สั่งซื้อสินค้า</a>
                     <?php }else{ ?>
                        <a class="btn btn-warning" style="font-size:14px; color:black; width:180px;" href="ShoppingOrder.php">สั่งซื้อสินค้า</a>
                     <?php }?>
                </div>
                <br>  <br>
            </div>
        </div>
        <!-- footercart -->

    </div>

    </div>
    <div class="col-md-1"></div>
    </div>
    </div>
   
<!-- ENDBODY -->

  <!-- Modal ERROR-->
  <div class="modal fade" id="loginfail" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/wrong.png" width="150px" height="150px">
        <br><br>
        <p>ชื่อผู้ใช้หรือรหัสผ่านของคุณผิดพลาด</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

  <!-- Modal SUCCESS-->
  <div class="modal fade" id="loginsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/correct.png" width="150px" height="150px">
        <br><br>
        <p>เข้าสู่ระบบสำเร็จ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

      <!-- Modal LOGOUT-->
      <div class="modal fade" id="logoutsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/logout.png" width="150px" height="150px">
        <br><br>
        <p>กำลังออกจากระบบ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->


  <!-- Modal BANNED-->
  <div class="modal fade" id="hasbanned" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/banned.png" width="150px" height="150px">
        <br><br>
        <p>รหัสผู้ใช้ของท่านโดน แบน กรุณาติดต่อ ผู้ดูแลระบบ ด้วยค่ะ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->

    <!-- Modal Numnotenough-->
             <div class="modal fade" id="Numnotenough" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
    
        <br><br>
        <p>สินค้ามีจำนวนไม่เพียงพอ ไม่สามารถเพิ่มจำนวนสินค้าได้</p>
        </div>
        <div class="modal-footer">
           <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK"  onclick="okNumnotenough()">ตกลง</button>  -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal Numnotenough-->

      <!-- Modal Numnotenough1-->
      <div class="modal fade" id="Numnotenough1" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"  style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
    
        <br><br>
        <p>สินค้ามีจำนวนไม่เพียงพอ ไม่สามารถเพิ่มจำนวนสินค้าได้</p>
        </div>
        <div class="modal-footer">
           <button type="button" class="btn btn-success" data-dismiss="modal" id="btnOK"  onclick="okNumnotenough()">ตกลง</button> 
        </div>
      </div>

    </div>
  </div>
  <!-- Modal Numnotenough1-->

   <!-- Modal wanttodelete-->
 <div class="modal fade" id="wanttodelete" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;">คุณแน่ใจว่าต้องการลบหรือไม่?</h4>
        </div>
        <div class="modal-body" align="center" style="font-size:16px;">
        <input type="text" id="wanttodeletedetailid" hidden>
        <p id="wanttodeletedetail" > ท่านต้องการลบสินค้า </p>
        </div>
        <div class="modal-footer d-block" align="center">
           <a id="updateproductgroupBtn" class="btn btn-outline-success btn-sm" 
           onclick="acceptdelete()"
           >ยืนยัน</a>
           <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-danger btn-sm" data-dismiss="modal"    onclick="canceldelete()">ยกเลิก</a>
        </div>
      </div>

    </div>
  </div>
<script>
function acceptdelete(){
    var cart_detail_code = document.getElementById("wanttodeletedetailid").value;

             $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    async : false,
                    url: "../restapi/deletecartdetail.php",
                    data:JSON.stringify({
                    cart_detail_code:cart_detail_code
                    }),
                    success: function (response) {
                    var json_data = response;
                    var getdata = json_data;
                    if(getdata.result=="Success"){
                        location.reload();
                    }
                    }
                     });
}
function canceldelete(){
    location.reload();
}
function okNumnotenough(){
    location.reload();
}
</script>

  <!-- Modal wanttodelete-->




</body>


</html>