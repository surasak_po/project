<?php
   include "../restapi/setting/config.php";

    @session_start();
    @session_cache_expire(30);

    if(@$_SESSION['role']=="admin") {
    } else {
        echo "<meta http-equiv='refresh' content='0 ; URL=Index.php'>";
    }
    
    @$start_date = $_GET['start_date'];
    @$end_date = $_GET['end_date'];
    @$store_id = $_GET['store_id'];

 
$strgetstorefromget = "SELECT * FROM tbl_store WHERE store_code = '$store_id' ";
$resultstrgetstorefromget = @$conn->query($strgetstorefromget);
if($resultstrgetstorefromget->num_rows > 0){
    while($rowstrgetstorefromget = $resultstrgetstorefromget->fetch_assoc()){
        @$storename = $rowstrgetstorefromget['store_namestore'];
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="./css/styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }

        .card {
            background-color: #F88360;  
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }

        table, td {
            border: 2px solid grey;
        }

        th {
            border: 2px solid pink;
        }

        #left {
            text-align: left;
        }

        #size {
            width: 1px;
        }

        #element1 {
            display: flex;
            justify-content: space-between;
            margin-bottom: -10px;
        }

        #element2 {
            display: flex;
            justify-content: space-between;
            margin-bottom: -10px;
            margin-left: -15px;
        }

        #b :hover {
            color: black!important;
        }
        .modal-header .close {
        display:none;
        }
        #menuleft{
            color: black !important;
        }
        #menuleft:hover{
            color: #FF8811 !important;
        }
    </style>
</head>

<script>


    function logoutFunction() {

        logoutsuccess();
                   setTimeout(function(){ 
                    document.forms["formLogout"].action = "webservice/Logout.php";
                    document.forms["formLogout"].submit();
                   },2600);
        
    }
</script>

<body>
   

    <nav class="navbar navbar-expand-sm">
        <a class="navbar-brand" style="font-family:'KRR_AengAei.ttf'!important; color: #FFA200;font-size:35px;">
            <i class="fas fa-dolly"></i>ผู้ดูแลระบบ
        </a>
        <ul class="navbar-nav mr-auto"></ul>
        <ul class="navbar-nav">
            <?php
                echo "
                    <form id=\"formLogout\" name=\"formLogout\">
                        <a class=\"nav-link\" style=\"cursor:pointer\" onclick=\"logoutFunction()\"><i class=\"fas fa-sign-out-alt\"></i>ออกจากระบบ</a>
                    </form>
                ";
            ?>
        </ul>
    </nav>
<!-- เปิด -->


    <div class="container-fluid" style="background-color:#ECECEC ;">
            <div class="row">
           
           
                <div class="col-sm-2" align="left" style="background-color:#ECECEC ;"> <br>
                <ul style=" list-style-type: none;">
                <li style="margin-bottom:15px;"> <i class="fas fa-user" style="color:#3AE100;"></i><span style="font-weight:bold;">จัดการข้อมูล</span>
                <ul style=" list-style-type: none;">
                <li style="margin-bottom:5px;"><a href="1AdminCustomer.php" id="menuleft">ลูกค้า</a></li>
                <li style="margin-bottom:5px;"><a href="1AdminStore.php" id="menuleft">ร้านค้า</a></li>   
                <li style="margin-bottom:5px;"><a href="1AdminProductType.php" id="menuleft">ประเภทสินค้า</a></li>
                <li style="margin-bottom:5px;"><a href="1AdminProduct.php"  id="menuleft">สินค้า</a></li>
                <li style="margin-bottom:5px;"><a href="1AdminProductImage.php" id="menuleft">รูปสินค้า</a></li>
                <li style="margin-bottom:5px;"><a href="1AdminProductGroup.php"  id="menuleft">ตัวเลือกสินค้า</a></li>
                <li style="margin-bottom:5px;"><a href="1AdminTransport.php" id="menuleft">ขนส่ง</a></li>
                <li style="margin-bottom:5px;"><a href="1AdminCustomerAddress.php" id="menuleft">สถานที่จัดส่ง</a></li>
                </ul>
                </li>
             
                <li style="margin-bottom:15px;"> <i class="fas fa-money-check-alt" style="color:Blue;"></i><span style="font-weight:bold;">การขาย</span>
                <ul style=" list-style-type: none;">
                <li style="margin-bottom:5px;"><a href="AdminIncome.php" id="menuleft">รายรับของระบบ</a></li>
                <li style="margin-bottom:5px;margin-top:5px;color:#FF8811;"><a href="AdminSaleReport.php" >รายงานการขาย</a></li>
                </ul>
                </li>
             
                </ul>
            </div>
                <div class="col-sm-10" align="center" style="background-color:white;">
               

<div class="container-fluid" >
<br>

<?php if(@$storename!=""){
    echo "<h3 style=\"margin-bottom: 30px;\" align=\"center\">รายงานการขายร้าน :".$storename."</h3>";
}?>
<?php if(@$storename==""){
    echo "<h3 style=\"margin-bottom: 30px;\" align=\"center\">รายงานการขาย</h3>";
}?>


<?php if($start_date !="" && $end_date !=""){ ?>
     <p style="font-size:20px;">   ยอดขายตั้งแต่วันที่ <?php echo $start_date; ?>  ถึงวันที่ <?php echo $end_date; ?> <p>
<?php }?>

</div>
            
<!-- ค้นหา -->
<div class="row">
    <div class="col-md-2">
    </div>
    <div class="col-md-8" align="center">
<!-- <form method="GET" id="searchreport" name="searchreport"> -->
<?php if($start_date !="" && $end_date !=""){ ?>
<div class="d-inline ">
<span id="alertdate" hidden style="color:red;">กรุณาเลือกวันที่ให้ครบด้วยค่ะ</span>
<span id="alertdate2" hidden style="color:red;">กรุณาเลือกวันที่ให้ถูกต้องด้วยค่ะ</span>
<br>
ระหว่างวันที่ : <input type="date" id ="start_date" name ="start_date" value="<?php echo $start_date;?>">  ถึง : <input type="date" id ="end_date" name ="end_date" value="<?php echo $end_date;?>">

</div>
<?php }else{  ?>
    <div class="d-inline ">
<span id="alertdate" hidden style="color:red;">กรุณาเลือกวันที่ให้ครบด้วยค่ะ</span>
<span id="alertdate2" hidden style="color:red;">กรุณาเลือกวันที่ให้ถูกต้องด้วยค่ะ</span>
<br>
ระหว่างวันที่ : <input type="date" id ="start_date" name ="start_date" >  ถึง : <input type="date" id ="end_date" name ="end_date" >
</div>
    <?php }  ?>
<!--  -->
<br><br>
<div class="d-inline ">
ร้าน :
<select id="store_id" name="store_id">
<option value="">ทั้งหมด</option>
<?php 
$strgetstore = "SELECT * FROM tbl_store ";
$resultstrgetstore = @$conn->query($strgetstore);
if($resultstrgetstore->num_rows > 0){
    while($rowstrgetstore = $resultstrgetstore->fetch_assoc()){
        
?>
<?php  if($store_id==$rowstrgetstore['store_code']){ ?>
<option value="<?php echo $rowstrgetstore['store_code'];?>" hidden selected><?php echo $rowstrgetstore['store_namestore'];?></option>
<?php  }
?>
<option value="<?php echo $rowstrgetstore['store_code'];?>"><?php echo $rowstrgetstore['store_namestore'];?></option>
<?php
        }
    } ?>
</select>
</div>
<!--  -->
<br><br>
<div class="d-inline ">
<button  class="btn btn-success" onclick="searchreport1()">ค้นหา</button>
<a  class="btn btn-outline-dark"  href="AdminSaleReport.php">รีเซ๊ท</a>
</div>
<!-- </form> -->

    </div>
    <div class="col-md-2">
    </div>
</div>
<script>
 function logoutsuccess(){
        $("#logoutsuccess").modal({backdrop: 'static', keyboard: false})  
      $("#logoutsuccess").modal('show');
      setTimeout(function(){$('#logoutsuccess').modal('hide')},2000);
    }
function searchreport1(){

    var store_id =document.getElementById("store_id").value;
    var start_date =document.getElementById("start_date").value;
    var end_date =document.getElementById("end_date").value;
    console.log(start_date);
    if(start_date!="" && end_date==""){
        document.getElementById("end_date").focus();
        document.getElementById("alertdate").hidden=false;
    }else if(start_date=="" && end_date!=""){
        document.getElementById("start_date").focus();
        document.getElementById("alertdate").hidden=false;
    }else if(start_date > end_date){
        document.getElementById("start_date").focus();
        document.getElementById("end_date").focus();
        document.getElementById("alertdate").hidden=true;
        document.getElementById("alertdate2").hidden=false;
    }else{
    //document.forms["searchreport"].action="StoreReport.php";
     window.location.href = "AdminSaleReport.php?start_date=" + start_date+"&end_date="+end_date +"&store_id="+store_id;
    //document.forms["searchreport"].submit();*/
    }
   
}
</script>
<!-- ค้นหา -->
<br><br>

<!-- แสดงผล -->
<?php 

if($start_date !="" && $end_date !=""){
    $strgetdate ="SELECT DISTINCT DATE_FORMAT(order_date, '%Y-%m-%d') as date_change FROM 
tbl_order WHERE order_status='3'  AND store_code LIKE '%$store_id%'
AND DATE_FORMAT(order_date, '%Y-%m-%d') BETWEEN '$start_date' AND '$end_date'
ORDER BY order_date ASC ";
}else{
    $strgetdate ="SELECT DISTINCT DATE_FORMAT(order_date, '%Y-%m-%d') as date_change FROM 
    tbl_order WHERE order_status='3' AND store_code LIKE '%$store_id%'  ORDER BY order_date ASC ";
}


 $resultstrgetdate = @$conn->query($strgetdate);
 if($resultstrgetdate->num_rows > 0){
    $getno = 0;
    $gettotalprice = 0;
     while($rowstrgetdate = $resultstrgetdate->fetch_assoc()){
         $getdate =$rowstrgetdate['date_change']; 
       
?>
<div style="font-size:20px;font-weight:bold;" align="left">ยอดขาย วันที่ : <?php echo $getdate; ?></div>
        <table class="table table-dark">
            <tr align="center">
            <td align="center">ลำดับ</td>
            <td align="center">หมายเลขใบสั่งซื้อ</td>
            <td align="center">ชื่อสินค้า</td>
            <td>ตัวเลือกสินค้า</td>
            <td align="center">ราคา</td>
            <td>จำนวน</td>
            <td align="center">ราคารวม</td>
            </tr>

            <?php 
            $strgetdata ="SELECT * FROM tbl_order 
            INNER JOIN tbl_order_detail ON tbl_order.order_code = tbl_order_detail.order_code
            INNER JOIN tbl_product_group ON tbl_order_detail.product_group_code = tbl_product_group.product_group_code
            INNER JOIN tbl_product ON tbl_product_group.product_code = tbl_product.product_code
            WHERE DATE_FORMAT(order_date, '%Y-%m-%d') = '$getdate' AND order_status='3'  AND tbl_order.store_code LIKE '%$store_id%' ";

            $resultstrgetdata = @$conn->query($strgetdata);
            if($resultstrgetdata->num_rows > 0){
                $no = 1;
                $getnoin = 0;
                $gettotalpricein = 0;
                while($rowstrgetdata = $resultstrgetdata->fetch_assoc()){
            ?>
            <tr>
            <td><?php echo $no;?></td>
            <td><?php echo $rowstrgetdata['order_code'];?></td>
            <td><?php echo $rowstrgetdata['product_name'];?></td>
            <td><?php echo $rowstrgetdata['product_group_name'];?></td>
            <td align="right"><?php echo number_format($rowstrgetdata['order_detail_price']).".00";?></td>
            <td align="right"><?php echo $rowstrgetdata['order_detail_num'];?></td>
            <td align="right"><?php 
            $gettotalprice = $gettotalprice + ($rowstrgetdata['order_detail_price']*$rowstrgetdata['order_detail_num']);
            $gettotalpricein = $gettotalpricein + ($rowstrgetdata['order_detail_price']*$rowstrgetdata['order_detail_num']);
            echo number_format($rowstrgetdata['order_detail_price']*$rowstrgetdata['order_detail_num']).".00";
            
            ?></td>
            </tr>
         

            <?php 
            $no++;
            
            }
            $getno = ($getno + $no) - 1;
            $getnoin = ($getnoin + $no) - 1;
        }
            ?>
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>รวมทั้งสิ้น </td>
            <td align="right"><?php echo $getnoin;?> ชิ้น</td>
            </tr>
            <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>รวมเป็นเงิน</td>
            <td align="right"><?php echo "฿".number_format($gettotalpricein).".00";?></td>
            </tr>

        </table>
<?php 
        }
?>

<div class="row">
<div class="col-md-8"></div>
    <div class="col-md-4">
         <table class="table table-dark">
         <tr>
            <td >รวมจำนวนทั้งสิ้น</td>
            <td align="right"><?php echo $getno;?> ชิ้น</td>
        </tr>
        <tr>
        <td >รวมราคาทั้งสิ้น</td>
        <td align="right"><?php echo "฿".number_format($gettotalprice).".00";?></td>
        </tr>
         </table>
         </div>
</div>
<?php 
        }else{
?>
    <div align="center" style="font-size:26px;font-weight:bold;">ไม่มีข้อมูลการการขาย ค่ะ</div>
<?php 
        }
?>
<!-- แสดงผล -->





               


<!-- ปิด 3  -->
</div> 
    </div> 
    </div>


          <!-- Modal LOGOUT-->
          <div class="modal fade" id="logoutsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/logout.png" width="150px" height="150px">
        <br><br>
        <p>กำลังออกจากระบบ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->


  

</body>

<footer style="background-color: #575757;">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-sm" style="display: flex;justify-content: center;">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <label style="color: #FFA200;font-size: 14px;">&copy;&nbsp;2020 localshopcenter.com</label>
                </li>
            </ul>
        </nav>
    </div>
</footer>
</html>