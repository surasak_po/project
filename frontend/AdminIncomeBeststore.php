<?php
   include "../restapi/setting/config.php";

    @session_start();
    @session_cache_expire(30);

    if(@$_SESSION['role']=="admin") {
    } else {
        echo "<meta http-equiv='refresh' content='0 ; URL=Index.php'>";
    }

    @$start_date = $_GET['start_date'];
    @$end_date = $_GET['end_date'];

  $total_wait_price = 0;
    $total_wait_order = 0;
    $strgetorderwait ="SELECT * FROM tbl_order 
    WHERE  order_status != '3' ";
     $resultstrgetorderwait = @$conn->query($strgetorderwait);
     if($resultstrgetorderwait->num_rows > 0){
        while($rowstrgetorderwait = $resultstrgetorderwait->fetch_assoc()){
            $total_wait_order= $total_wait_order +1;
            $order_code = $rowstrgetorderwait['order_code'];
            $order_shipping_pricewait = $rowstrgetorderwait['order_shipping_price'];

            $strgetpriceall ="SELECT * FROM tbl_order_detail 
            WHERE order_code = '$order_code' ";
            $resultstrgetpriceall = @$conn->query($strgetpriceall);
            if($resultstrgetpriceall->num_rows > 0){
                while($rowstrgetpriceall = $resultstrgetpriceall->fetch_assoc()){
                    $total_wait_price = $total_wait_price+($rowstrgetpriceall['order_detail_price']*$rowstrgetpriceall['order_detail_num']);
                }
             
            }

        }
    }

    $firstdayweek = date("Y-m-d", strtotime('monday this week'));
    $lastdayweek = date("Y-m-d", strtotime('sunday this week'));

    $total_price_week = 0;
    $total_order_week= 0;
    $strgetorderweek ="SELECT * FROM tbl_order 
    WHERE DATE_FORMAT(order_date, '%Y-%m-%d') BETWEEN '$firstdayweek' AND '$lastdayweek'
    AND order_status = '3' ";
     $resultstrgetorderweek = @$conn->query($strgetorderweek);
     if($resultstrgetorderweek->num_rows > 0){
        while($rowstrgetorderweek = $resultstrgetorderweek->fetch_assoc()){
            $total_order_week= $total_order_week+1;
            $order_code = $rowstrgetorderweek['order_code'];
            $order_shipping_priceweek = $rowstrgetorderweek['order_shipping_price'];

            $strgetpriceallweek ="SELECT * FROM tbl_order_detail 
            WHERE order_code = '$order_code' ";
            $resultstrgetpriceallweek = @$conn->query($strgetpriceallweek);
            if($resultstrgetpriceallweek->num_rows > 0){
                while($rowstrgetpriceallweek = $resultstrgetpriceallweek->fetch_assoc()){
                $total_price_week = $total_price_week+($rowstrgetpriceallweek['order_detail_price']*$rowstrgetpriceallweek['order_detail_num']);
                }
              
            }
        }
    }

    $firstdaymonth = date("Y-m-d", strtotime('first day of this month'));
    $lastdaymonth = date("Y-m-d", strtotime('last day of this month'));

    $total_price_month= 0;
    $total_order_month= 0;
    $strgetordermonth ="SELECT * FROM tbl_order 
    WHERE DATE_FORMAT(order_date, '%Y-%m-%d') BETWEEN '$firstdaymonth' AND '$lastdaymonth'
    AND order_status = '3' ";
     $resultstrgetordermonth = @$conn->query($strgetordermonth);
     if($resultstrgetordermonth->num_rows > 0){
        while($rowstrgetordermonth = $resultstrgetordermonth->fetch_assoc()){
            $total_order_month=$total_order_month+1;
            $order_code = $rowstrgetordermonth['order_code'];
            $order_shipping_pricemonth = $rowstrgetordermonth['order_shipping_price'];

            $strgetpriceall ="SELECT * FROM tbl_order_detail 
            WHERE order_code = '$order_code' ";
            $resultstrgetpriceall = @$conn->query($strgetpriceall);
            if($resultstrgetpriceall->num_rows > 0){
                while($rowstrgetpriceall = $resultstrgetpriceall->fetch_assoc()){
                $total_price_month = $total_price_month+($rowstrgetpriceall['order_detail_price']*$rowstrgetpriceall['order_detail_num']);
                }
               
            }
        }
    }

    $total_price_all= 0;
    $total_order_all= 0;
    $strgetorderall ="SELECT * FROM tbl_order 
    WHERE order_status = '3' ";
     $resultstrgetorderall = @$conn->query($strgetorderall);
     if($resultstrgetorderall->num_rows > 0){
        while($rowstrgetorderall = $resultstrgetorderall->fetch_assoc()){
            $total_order_all = $total_order_all+1;
            $order_code = $rowstrgetorderall['order_code'];
            $order_shipping_pricesuccess = $rowstrgetorderall['order_shipping_price'];

            $strgetpriceall ="SELECT * FROM tbl_order_detail 
            WHERE order_code = '$order_code' ";
            $resultstrgetpriceall = @$conn->query($strgetpriceall);
            if($resultstrgetpriceall->num_rows > 0){
                while($rowstrgetpriceall = $resultstrgetpriceall->fetch_assoc()){
                $total_price_all = $total_price_all+($rowstrgetpriceall['order_detail_price']*$rowstrgetpriceall['order_detail_num']);
                }
             
            }
        }
    }


    $numcreditcard= 0;
    $numpromptpay= 0;
    $strgetnumtypepayment ="SELECT * FROM tbl_payment
    INNER JOIN tbl_order ON tbl_payment.order_code = tbl_order.order_code
    WHERE order_status = '3' ";
     $resultstrgetnumtypepayment = @$conn->query($strgetnumtypepayment);
     if($resultstrgetnumtypepayment->num_rows > 0){
        while($rowstrgetnumtypepayment = $resultstrgetnumtypepayment->fetch_assoc()){

            if($rowstrgetnumtypepayment['payment_type']=="promptpay"){
                $numpromptpay = $numpromptpay+1;
            }
            if($rowstrgetnumtypepayment['payment_type']=="credit_card"){
                $numcreditcard = $numcreditcard+1;
            }
        }
    }

    $numkerry= 0;
    $numj_t= 0;
    $strgetnumshipping ="SELECT * FROM tbl_order
    INNER JOIN tbl_shipping ON tbl_order.shipping_code = tbl_shipping.shipping_code
    INNER JOIN tbl_transport ON tbl_shipping.transport_code = tbl_transport.transport_code
    WHERE order_status = '3' ";
     $resultstrgetnumshipping = @$conn->query($strgetnumshipping);
     if($resultstrgetnumshipping->num_rows > 0){
        while($rowstrgetnumshipping = $resultstrgetnumshipping->fetch_assoc()){

            if($rowstrgetnumshipping['transport_name']=="Kerry-Express-th"){
                $numkerry = $numkerry+1;
            }
            if($rowstrgetnumshipping['transport_name']=="J&T-Express-th"){
                $numj_t = $numj_t+1;
            }
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="./css/styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }

        .card {
            background-color: #F88360;  
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }

        #left {
            text-align: left;
        }

        #size {
            width: 1px;
        }

        #element1 {
            display: flex;
            justify-content: space-between;
            margin-bottom: -10px;
        }

        #element2 {
            display: flex;
            justify-content: space-between;
            margin-bottom: -10px;
            margin-left: -15px;
        }

        #b :hover {
            color: black!important;
        }
        .modal-header .close {
        display:none;
        }
        #menuleft{
            color: black !important;
        }
        #menuleft:hover{
            color: #FF8811 !important;
        }
    </style>
</head>

<script>


    function logoutFunction() {

        logoutsuccess();
                   setTimeout(function(){ 
                    document.forms["formLogout"].action = "webservice/Logout.php";
                    document.forms["formLogout"].submit();
                   },2600);
        
    }
    function AdminIncome(){
            window.location.href = "AdminIncome.php";
        }
        function AdminIncomeSuccess(){
            window.location.href = "AdminIncomeSuccess.php";
        }
        
        function adminbestsale(){
            window.location.href = "AdminIncomeBestsale.php";
        }
        function adminbeststore(){
            window.location.href = "AdminIncomeBeststore.php";
        }
        function logoutsuccess(){
        $("#logoutsuccess").modal({backdrop: 'static', keyboard: false})  
      $("#logoutsuccess").modal('show');
      setTimeout(function(){$('#logoutsuccess').modal('hide')},2000);
    }
</script>

<body>
   

    <nav class="navbar navbar-expand-sm">
        <a class="navbar-brand" style="font-family:'KRR_AengAei.ttf'!important; color: #FFA200;font-size:35px;">
            <i class="fas fa-dolly"></i>ผู้ดูแลระบบ
        </a>
        <ul class="navbar-nav mr-auto"></ul>
        <ul class="navbar-nav">
            <?php
                echo "
                    <form id=\"formLogout\" name=\"formLogout\">
                        <a class=\"nav-link\" style=\"cursor:pointer\" onclick=\"logoutFunction()\"><i class=\"fas fa-sign-out-alt\"></i>ออกจากระบบ</a>
                    </form>
                ";
            ?>
        </ul>
    </nav>
<!-- เปิด -->


    <div class="container-fluid" style="background-color:#ECECEC ;">
            <div class="row">
           
         <div class="col-sm-2" align="left" style="background-color:#ECECEC ;"> <br>
                <ul style=" list-style-type: none;">
                <li style="margin-bottom:15px;"> <i class="fas fa-user" style="color:#3AE100;"></i><span style="font-weight:bold;">จัดการข้อมูล</span>
                <ul style=" list-style-type: none;">
                <li style="margin-bottom:5px;"><a href="1AdminCustomer.php" id="menuleft">ลูกค้า</a></li>
                <li style="margin-bottom:5px;"><a href="1AdminStore.php" id="menuleft">ร้านค้า</a></li>   
                <li style="margin-bottom:5px;"><a href="1AdminProductType.php" id="menuleft">ประเภทสินค้า</a></li>
                <li style="margin-bottom:5px;"><a href="1AdminProduct.php"  id="menuleft">สินค้า</a></li>
                <li style="margin-bottom:5px;"><a href="1AdminProductImage.php" id="menuleft">รูปสินค้า</a></li>
                <li style="margin-bottom:5px;"><a href="1AdminProductGroup.php"  id="menuleft">ตัวเลือกสินค้า</a></li>
                <li style="margin-bottom:5px;"><a href="1AdminTransport.php" id="menuleft">ขนส่ง</a></li>
                <li style="margin-bottom:5px;"><a href="1AdminCustomerAddress.php" id="menuleft">สถานที่จัดส่ง</a></li>
                </ul>
                </li>
             
                <li style="margin-bottom:15px;"> <i class="fas fa-money-check-alt" style="color:Blue;"></i><span style="font-weight:bold;">การขาย</span>
                <ul style=" list-style-type: none;">
                <li style="margin-bottom:5px;margin-top:5px;color:#FF8811;"><a href="AdminIncome.php" >รายรับของระบบ</a></li>
                    <li style="margin-bottom:5px;"><a href="AdminSaleReport.php" id="menuleft">รายงานการขาย</a></li>
                </ul>
                </li>
             
                </ul>
            </div>
<!-- เปิด -->
<div class="col-sm-10"  style="background-color:#DDDDDD;">        <br> 
<div class="container" style="background-color:White;padding-top:10px">
<br>

<h4  align="left" style="font-weight:bold;">ภาพรวมของฉัน</h4>
        <br>
        <div class="row" style="font-size:20px;font-weight:bold;">
                <div class="col-md-4">
                เตรียมการโอนเงิน
                </div>
                <div class="col-md-4" style="border-left: 4px solid  #C8C8C8;">
                โอนเงินแล้ว
                </div>
        </div>
        <div class="row">
                <div class="col-md-4" >
                <span style="color:#8D8D8D  ">รวม</span><br>
                <span style="color:black;font-size:20px;font-weight:bold">฿<?php echo number_format($total_wait_price).".00";?></span>
                </div>
                <div class="col-md-3" style="border-left: 4px solid  #C8C8C8;">
                <span style="color:#8D8D8D  ">สัปดาห์นี้ </span><br>
                <span style="color:black;font-size:20px;font-weight:bold">฿<?php echo number_format($total_price_week).".00";?></span>
                </div>
                <div class="col-md-3"> 
                <span style="color:#8D8D8D  ">เดือนนี้</span><br>
                <span style="color:black;font-size:20px;font-weight:bold">฿<?php echo number_format($total_price_month).".00";?></span>
                </div>
                <div class="col-md-2">
                <span style="color:#8D8D8D  ">รวม</span><br>
                <span style="color:black;font-size:20px;font-weight:bold">฿<?php echo number_format($total_price_all).".00";?></span>  
                </div>
              
        </div>
        <div class="row">
                <div class="col-md-4">
                <span style="color:#8D8D8D  ">จำนวนใบสั่งซื้อ</span><br>
                <span style="color:black;font-size:20px;font-weight:bold"><?php echo $total_wait_order;?></span>
                </div>
                <div class="col-md-3" style="border-left: 4px solid  #C8C8C8;">
                <span style="color:#8D8D8D  ">จำนวนใบสั่งซื้อ </span><br>
                <span style="color:black;font-size:20px;font-weight:bold"><?php echo $total_order_week;?></span>
                </div>
                <div class="col-md-3"> 
                <span style="color:#8D8D8D  ">จำนวนใบสั่งซื้อ</span><br>
                <span style="color:black;font-size:20px;font-weight:bold"><?php echo $total_order_month;?></span>
                </div>
                <div class="col-md-2">
                <span style="color:#8D8D8D  ">จำนวนใบสั่งซื้อ</span><br>
                <span style="color:black;font-size:20px;font-weight:bold"><?php echo $total_order_all;?></span>
                </div>
              
        </div>
        <br>
        <div class="row" style="border-bottom: 4px solid  #C8C8C8;">
        </div>
        <br>
        <div class="row" style="font-size:20px;font-weight:bold;">
                <div class="col-md-4">
                ช่องทางการชำระเงิน
                </div>
                <div class="col-md-4" style="border-left: 4px solid  #C8C8C8;">
                ช่องทางการจัดส่ง
                </div>
                
        </div>
        <div class="row">
                <div class="col-md-2" >
                <span style="color:#8D8D8D  ">บัตรเครดิต</span><br>
                <span style="color:black;font-size:20px;font-weight:bold"><?php echo $numcreditcard; ?></span>
                </div>
                <div class="col-md-2" >
                <span style="color:#8D8D8D  ">พร้อมเพย์</span><br>
                <span style="color:black;font-size:20px;font-weight:bold"><?php echo $numpromptpay; ?></span>
                </div>
                <div class="col-md-2" style="border-left: 4px solid  #C8C8C8;">
                <span style="color:#8D8D8D  ">Kerry-Express-th</span><br>
                <span style="color:black;font-size:20px;font-weight:bold"><?php echo $numkerry; ?></span>
               
                </div>
                <div class="col-md-2">
                <span style="color:#8D8D8D  ">J&T-Express-th</span><br>
                <span style="color:black;font-size:20px;font-weight:bold"><?php echo $numj_t; ?></span>
                </div>
        </div>
        <br> 
<!-- ปิด 3  -->
</div>
<br> 
<div class="container" style="background-color:White;padding-top:10px" align="left">
<h4 style="font-weight:bold;">รายละเอียดรายรับของฉัน</h4><br>
<table>
        <tr>
        <td style="font-size:20px;padding-right:30px;cursor:pointer" onclick="AdminIncome()">เตรียมการโอนเงิน</td>
        <td style="font-size:20px;padding-right:30px;cursor:pointer" onclick="AdminIncomeSuccess()">โอนเงินแล้ว</td>
        <td style="font-size:20px;padding-right:30px;cursor:pointer" onclick="adminbestsale()">10อันดับสินค้าขายดี</td>
        <td style="color:#FF750F ;font-size:20px;padding-right:30px;cursor:pointer" onclick="adminbeststore()">ร้านค้าขายดี</td>
        </tr>
        <tr>
        </tr>
</table>
<div class="row">
                <div class="col-md-12" align="center">

                <?php if($start_date !="" && $end_date !=""){ ?>
                <div class="d-inline ">
                <span id="alertdate" hidden style="color:red;">กรุณาเลือกวันที่ให้ครบด้วยค่ะ</span>
                <span id="alertdate2" hidden style="color:red;">กรุณาเลือกวันที่ให้ถูกต้องด้วยค่ะ</span>
                <br>
                ระหว่างวันที่ : <input type="date" id ="start_date" name ="start_date" value="<?php echo $start_date;?>">  ถึง : <input type="date" id ="end_date" name ="end_date" value="<?php echo $end_date;?>">

                </div>
                <?php }else{  ?>
                    <div class="d-inline ">
                <span id="alertdate" hidden style="color:red;">กรุณาเลือกวันที่ให้ครบด้วยค่ะ</span>
                <span id="alertdate2" hidden style="color:red;">กรุณาเลือกวันที่ให้ถูกต้องด้วยค่ะ</span>
                <br>
                ระหว่างวันที่ : <input type="date" id ="start_date" name ="start_date" >  ถึง : <input type="date" id ="end_date" name ="end_date" >
                </div>
                    <?php }  ?>
                    <button  class="btn btn-success" onclick="searchreport1()">ค้นหา</button>
                    
                </div>
                <div class="col-md-12"  align="center">
                <br>
                <?php if($start_date !="" && $end_date !=""){ ?>
                <p style="font-size:20px;color:black;font-family:'Opun-Regular.ttf'!important;">   ยอดขายตั้งแต่วันที่ <?php echo $start_date; ?>  ถึงวันที่ <?php echo $end_date; ?> <p>
                <?php }?>
             
                </div>
        </div>
        <br><br>
<table class="table ">
            <tr style="font-weight:bold;" bgcolor="#DDDDDD">
            <td align="center">ลำดับ</td>
            <td align="center">ชื่อร้าน</td>
            <td align="center">จำนวนการสั่งซื้อ</td>
            <td align="center">รอโอน</td>
            <td align="center">สำเร็จ</td>
            <td align="center">ยอดขายรวม</td>
            </tr>
            <style>
tr#dataproduct{
    background-color: white;
    cursor: pointer;
    height: 30px;

}
tr#dataproduct:hover{
    background-color:#FFDCB9 ;
    cursor: pointer;

}
</style>
<script>
function searchreport1(){
var start_date =document.getElementById("start_date").value;
var end_date =document.getElementById("end_date").value;
console.log(start_date);
if(start_date!="" && end_date==""){
    document.getElementById("end_date").focus();
    document.getElementById("alertdate").hidden=false;
}else if(start_date=="" && end_date!=""){
    document.getElementById("start_date").focus();
    document.getElementById("alertdate").hidden=false;
}else if(start_date > end_date){
    document.getElementById("start_date").focus();
    document.getElementById("end_date").focus();
    document.getElementById("alertdate").hidden=true;
    document.getElementById("alertdate2").hidden=false;
}else{
 window.location.href = "AdminIncomeBeststore.php?start_date=" + start_date+"&end_date="+end_date;

}

}
</script>
<?php  
$no = 1;

if($start_date !="" && $end_date !=""){
$strgetorder ="SELECT *,SUM(order_detail_price) as sumtotal FROM tbl_order 
INNER JOIN tbl_order_detail ON tbl_order.order_code = tbl_order_detail.order_code
INNER JOIN tbl_store ON tbl_order.store_code = tbl_store.store_code
WHERE  order_status = '3' 
AND DATE_FORMAT(order_date, '%Y-%m-%d') BETWEEN '$start_date' AND '$end_date'
GROUP BY tbl_store.store_namestore
ORDER BY SUM(order_detail_price) DESC
LIMIT 10
";
}else{
    $strgetorder ="SELECT *,SUM(order_detail_price*order_detail_num) as sumtotal,COUNT(order_no) as sumorder FROM tbl_order 
    INNER JOIN tbl_order_detail ON tbl_order.order_code = tbl_order_detail.order_code
    INNER JOIN tbl_store ON tbl_order.store_code = tbl_store.store_code
    WHERE  order_status = '3' 
    GROUP BY tbl_store.store_namestore
    ORDER BY SUM(order_detail_price) DESC
    LIMIT 10
    ";
}
 $resultstrgetorder = @$conn->query($strgetorder);
 if($resultstrgetorder->num_rows > 0){
    while($rowstrgetorder = $resultstrgetorder->fetch_assoc()){
               
    ?>
            <tr id="dataproduct"  >
            <td><?php echo $no; $no++;?></td>
            <td><?php echo $rowstrgetorder['store_namestore']; ?></td>
            <td align="right">
            <?php 
                $numorderall = 0;
                if($start_date !="" && $end_date !=""){
                    $strgetorderall ="SELECT COUNT(order_no) As sumorderall FROM tbl_order 
                    WHERE store_code = '".$rowstrgetorder['store_code']."' 
                    AND DATE_FORMAT(order_date, '%Y-%m-%d') BETWEEN '$start_date' AND '$end_date'
                    ";
                }else{
                    $strgetorderall ="SELECT COUNT(order_no) As sumorderall FROM tbl_order 
                    WHERE store_code = '".$rowstrgetorder['store_code']."' ";
                }
              
                
                $resultstrgetorderall = @$conn->query($strgetorderall);
                 if($resultstrgetorderall->num_rows > 0){
                    while($rowstrgetorderall = $resultstrgetorderall->fetch_assoc()){
                        $numorderall = $rowstrgetorderall['sumorderall'];
                    }
                }
                echo $numorderall;
            ?>
            </td>
            <td align="right">
            <?php 
                $numorderall = 0;
                if($start_date !="" && $end_date !=""){
                    $strgetorderall ="SELECT COUNT(order_no) As sumorderall FROM tbl_order 
                    WHERE store_code = '".$rowstrgetorder['store_code']."' AND order_status != '3' 
                    AND DATE_FORMAT(order_date, '%Y-%m-%d') BETWEEN '$start_date' AND '$end_date'
                    ";
                }else{
                    $strgetorderall ="SELECT COUNT(order_no) As sumorderall FROM tbl_order 
                    WHERE store_code = '".$rowstrgetorder['store_code']."' AND order_status != '3' ";
                }
              
              
                 $resultstrgetorderall = @$conn->query($strgetorderall);
                 if($resultstrgetorderall->num_rows > 0){
                    while($rowstrgetorderall = $resultstrgetorderall->fetch_assoc()){
                        $numorderall = $rowstrgetorderall['sumorderall'];
                    }
                }
                echo $numorderall;
            ?>
            </td>
            <td align="right">
            <?php 
                $numorderall = 0;
                if($start_date !="" && $end_date !=""){
                    $strgetorderall ="SELECT COUNT(order_no) As sumorderall FROM tbl_order 
                    WHERE store_code = '".$rowstrgetorder['store_code']."' AND order_status = '3' 
                    AND DATE_FORMAT(order_date, '%Y-%m-%d') BETWEEN '$start_date' AND '$end_date'
                    ";
                }else{
                    $strgetorderall ="SELECT COUNT(order_no) As sumorderall FROM tbl_order 
                    WHERE store_code = '".$rowstrgetorder['store_code']."' AND order_status = '3' ";
                }
              
                 $resultstrgetorderall = @$conn->query($strgetorderall);
                 if($resultstrgetorderall->num_rows > 0){
                    while($rowstrgetorderall = $resultstrgetorderall->fetch_assoc()){
                        $numorderall = $rowstrgetorderall['sumorderall'];
                    }
                }
                echo $numorderall;
            ?>
            </td>
            <td align="right">
            <?php 
               echo number_format($rowstrgetorder['sumtotal']).".00";
            ?>
            </td>
            </tr>
 <?php }}else{?>   
    <tr align="center">
            <td colspan="6">
            <img src="icons/empty_box.png" width="100px;"><br>
            ไม่มีร้านค้า
            </td>  
    </tr>
<?php }?>  
        </table>
        <br>
</div> 
<br> 
</div> 
</div>


          <!-- Modal LOGOUT-->
          <div class="modal fade" id="logoutsuccess" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center" style="font-size:30px;font-weight:bold;"></h4>
        </div>
        <div class="modal-body" align="center" style="font-size:25px;">
        <img src="image/logout.png" width="150px" height="150px">
        <br><br>
        <p>กำลังออกจากระบบ</p>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="btnOK">OK</button> -->
        </div>
      </div>

    </div>
  </div>
  <!-- Modal -->


     <!--  vieworderdetaillist Modal -->
     <div class="modal"  data-backdrop="static" data-keyboard="false" id="order_detail_listmodal">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header d-block">
              
            </div>
            <!-- Modal body -->
                <div class="modal-body" style="font-size: 16px;" >
                        
                        <table class="table " id="order_detail_list">
                            
                        </table>
                </div>
                <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center">
                        <a id="cancelUpdateCustomerBtn" class="btn btn-xl btn-outline-danger btn-sm" data-dismiss="modal">ปิดหน้าต่าง</a>
                        </div>
                 
            </div>
            </div>
            </div>
    <!--  End of vieworderdetaillist Modal -->

</body>

<footer style="background-color: #575757;">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-sm" style="display: flex;justify-content: center;">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <label style="color: #FFA200;font-size: 14px;">&copy;&nbsp;2020 localshopcenter.com</label>
                </li>
            </ul>
        </nav>
    </div>
</footer>
</html>