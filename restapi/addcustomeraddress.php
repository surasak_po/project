<?php

include "setting/config.php ";

@header("content-type:application/json;charset=utf-8");
@header("Access-Control-Allow-Origin: *");
@header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');

    if($_SERVER["REQUEST_METHOD"]=="POST"){
        
        $content = @file_get_contents('php://input'); 
        $json_data = @json_decode($content, true);

        @$customer_address_fullname = trim($json_data['customer_address_fullname']);
        @$customer_address_tel = trim($json_data['customer_address_tel']);
        @$customer_address_province = trim($json_data['customer_address_province']);
        @$customer_address_district = trim($json_data['customer_address_district']);
        @$customer_address_subdistrict = trim($json_data['customer_address_subdistrict']);
        @$customer_address_postcode = trim($json_data['customer_address_postcode']);
        @$customer_address_detail = trim($json_data['customer_address_detail']);
        @$customer_code = trim($json_data['customer_code']);


        if($customer_address_fullname=="" or $customer_address_tel=="" or $customer_address_detail==""){

                    echo json_encode(array("result"=>"empty"));
        }else{
        $StrGetNo = "SELECT MAX(customer_address_no) As customer_address_no FROM tbl_customer_address";
                $ResultGetNo = @$conn->query($StrGetNo);
                if($ResultGetNo->num_rows > 0){
                    while($row_get_no = $ResultGetNo->fetch_assoc()){

                        $customer_address_no = $row_get_no['customer_address_no'];
                        
                        $customer_address_code = sprintf("CUSADDRESS%07s",($customer_address_no+1))."-".rand(0,9);                         
                    }
                
                }else{
                            $customer_address_code = sprintf("CUSADDRESS%07s",1)."-".rand(0,9);

                }

                $InsertSQL="INSERT INTO tbl_customer_address(customer_address_code,customer_address_fullname,customer_address_tel,
                customer_address_province,customer_address_district,customer_address_subdistrict,customer_address_postcode,customer_address_detail,customer_code) 
                VALUES ('".$customer_address_code."','".$customer_address_fullname."','".$customer_address_tel."','".$customer_address_province."',
                '".$customer_address_district."','".$customer_address_subdistrict."','".$customer_address_postcode."','".$customer_address_detail."','".$customer_code."')";     
                                                        

                if ($conn->query($InsertSQL) === TRUE) {
                   
                    $checkhave = "SELECT * FROM tbl_customer_address WHERE customer_code = '".$customer_code."' ";
                    $Resultcheckhave = @$conn->query($checkhave);
                    if($Resultcheckhave->num_rows == 1){
                        $UpdateSQL="UPDATE tbl_customer_address SET customer_address_status = '1' WHERE customer_code = '".$customer_code."' ";    
                        $conn->query($UpdateSQL);
                    }
                    echo json_encode(array("result"=>"Success"));
                }

        }



               
    }

?>