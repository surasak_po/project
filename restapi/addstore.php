<?php
require_once 'omise-php/lib/Omise.php';
include "setting/config.php ";
define('UPLOAD_DIR1', 'profilestore/');
define('UPLOAD_DIR2', 'cardstore/');
@header("content-type:application/json;charset=utf-8");
@header("Access-Control-Allow-Origin: *");
@header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');

define('OMISE_PUBLIC_KEY', 'pkey_test_5kwbj3tbv09itjm0p8m');
define('OMISE_SECRET_KEY', 'skey_test_5k7gu0smng7hk1pb84j');


function createrecipientOmise($store_bank_code,$store_nameinbank,$store_bank_brand,$store_email)
{
    $recipient = OmiseRecipient::create(array(
        'name' => $store_nameinbank,
        'description' => 'Tester account',
        'email' =>$store_email,
        'type' => 'individual',
        'tax_id' => '',
        'bank_account' => array(
          'brand' =>$store_bank_brand,
          'number' =>$store_bank_code,
          'name' => $store_nameinbank
        )
      ));

    return $recipient;
}
function uploadcardstore($cardpicture,$storecode)
{
    $store_cardpicture = explode(',',$cardpicture);
    $image_type_aux = explode("image/", $store_cardpicture[0]);
    $image_type = $image_type_aux[1];
    $image_base64 = base64_decode($store_cardpicture[1]);
    $fileName = $storecode.'card' . '.png';
    $file = UPLOAD_DIR2 . $fileName;   
    file_put_contents($file, $image_base64);
    return $fileName;
}
function uploadprofile($profile,$storecode)
{
    $store_picture = explode(',',$profile);
    $image_type_aux = explode("image/", $store_picture[0]);
    $image_type = $image_type_aux[1];
    $image_base64 = base64_decode($store_picture[1]);
    $fileName = $storecode . '.png';
    $file = UPLOAD_DIR1 . $fileName;   
    file_put_contents($file, $image_base64);
    return $fileName;
}

    if($_SERVER["REQUEST_METHOD"]=="POST"){
        
        $content = @file_get_contents('php://input'); 
        $json_data = @json_decode($content, true);

        @$store_fullname = trim($json_data['store_fullname']);
        @$store_namestore = trim($json_data['store_namestore']);
        @$store_email = trim($json_data['store_email']);
        @$store_tel = trim($json_data['store_tel']);
        @$store_password = trim($json_data['store_password']);
        @$store_cardpicture = trim($json_data['store_cardpicture']);
        @$store_picture = trim($json_data['store_picture']);
        @$store_bank_code = trim($json_data['store_bank_code']);
        @$store_bank_brand = trim($json_data['store_bank_brand']);
        @$store_nameinbank = trim($json_data['store_nameinbank']);
        @$store_address = trim($json_data['store_address']);
  


            $CheckEmailDuplicate="SELECT * FROM tbl_store WHERE store_email='".$store_email."' ";
            $ResultCheckEmailDuplicate=@$conn->query($CheckEmailDuplicate);
            if($ResultCheckEmailDuplicate->num_rows > 0){
                echo json_encode(array("result"=>"EMAIL_DUPLICATE"));
            }else{

                        $CheckTelDuplicate="SELECT * FROM tbl_store WHERE store_tel='".$store_tel."' ";
                        $ResultCheckTelDuplicate=@$conn->query($CheckTelDuplicate);
                        if($ResultCheckTelDuplicate->num_rows > 0){
                            echo json_encode(array("result"=>"TEL_DUPLICATE"));
                        }else{


                                $CheckNameDuplicate="SELECT * FROM tbl_store WHERE store_namestore='".$store_namestore."' ";
                                $ResultCheckNameDuplicate=@$conn->query($CheckNameDuplicate);
                                if($ResultCheckNameDuplicate->num_rows > 0){
                                    echo json_encode(array("result"=>"NAME_STORE_DUPLICATE"));
                                }else{
        

                                        $StrGetNo = "SELECT MAX(store_no) As store_no FROM tbl_store";
                                        $ResultGetNo = @$conn->query($StrGetNo);
                                        if($ResultGetNo->num_rows > 0){
                                            while($row_get_no = $ResultGetNo->fetch_assoc()){
                                                $store_no = $row_get_no['store_no'];
                                                $store_code = sprintf("STORE%07s",($store_no+1))."-".rand(0,9);

                                          
                                                        if(@$store_picture==""){
                                                           
                                                            $fileName = uploadcardstore($store_cardpicture,$store_code);
                                                            $recipient = createrecipientOmise($store_bank_code,$store_nameinbank,$store_bank_brand,$store_email);

                                                            $InsertSQL="INSERT INTO tbl_store(store_code,store_fullname,store_namestore,store_email,store_address,store_tel,
                                                            store_password,store_cardpicture,store_bank_code,store_bank_brand,store_nameinbank,store_omise_receive_code) 
                                                            VALUES ('".$store_code."','".$store_fullname."','".$store_namestore."','".$store_email."','".$store_address."'
                                                            ,'".$store_tel."',
                                                            '".$store_password."','".$fileName."','".$store_bank_code."','".$store_bank_brand."','".$store_nameinbank."',
                                                            '".$recipient['id']."')";     
                                                        
                                                            if ($conn->query($InsertSQL) === TRUE) {
                                                                echo json_encode(array("result"=>"Success"));
                                                            }

                                                        }else{

                                                            $fileName1 = uploadcardstore($store_cardpicture,$store_code);
                                                            $fileName2 = uploadprofile($store_picture,$store_code);
                                                            $recipient = createrecipientOmise($store_bank_code,$store_nameinbank,$store_bank_brand,$store_email);

                                                            
                                                            $InsertSQL="INSERT INTO tbl_store(store_code,store_fullname,store_namestore,store_email,store_address,store_tel,
                                                            store_password,store_cardpicture,store_bank_code,store_bank_brand,store_nameinbank,store_omise_receive_code,
                                                            store_picture) VALUES ('".$store_code."','".$store_fullname."','".$store_namestore."','".$store_email."',
                                                            '".$store_address."','".$store_tel."','".$store_password."','".$fileName1."','".$store_bank_code."',
                                                            '".$store_bank_brand."','".$store_nameinbank."',
                                                            '".$recipient['id']."','".$fileName2."' )";     
                                                        
                                                            if ($conn->query($InsertSQL) === TRUE) {
                                                                echo json_encode(array("result"=>"Success"));
                                                            }

                                                        }
                                            }   

                                        }else{
                                                            $store_code = sprintf("STORE%07s",1)."-".rand(0,9);

                                                            if(@$store_picture==""){
                                                                        
                                                                $fileName = uploadcardstore($store_cardpicture,$store_code);
                                                                $recipient = createrecipientOmise($store_bank_code,$store_nameinbank,$store_bank_brand,$store_email);

                                                                $InsertSQL="INSERT INTO tbl_store(store_code,store_fullname,store_namestore,store_email,store_address,store_tel,
                                                                store_password,store_cardpicture,store_bank_code,store_bank_brand,store_nameinbank,store_omise_receive_code) 
                                                                VALUES ('".$store_code."','".$store_fullname."','".$store_namestore."','".$store_email."','".$store_address."'
                                                                ,'".$store_tel."',
                                                                '".$store_password."','".$fileName."','".$store_bank_code."','".$store_bank_brand."','".$store_nameinbank."',
                                                                '".$recipient['id']."')";     
                                                            
                                                                if ($conn->query($InsertSQL) === TRUE) {
                                                                    echo json_encode(array("result"=>"Success"));
                                                                }

                                                            }else{
                                                                $fileName1 = uploadcardstore($store_cardpicture,$store_code);
                                                                $fileName2 = uploadprofile($store_picture,$store_code);
                                                                $recipient = createrecipientOmise($store_bank_code,$store_nameinbank,$store_bank_brand,$store_email);

                                                                
                                                                $InsertSQL="INSERT INTO tbl_store(store_code,store_fullname,store_namestore,store_email,store_address,store_tel,
                                                                store_password,store_cardpicture,store_bank_code,store_bank_brand,store_nameinbank,store_omise_receive_code
                                                                ,store_picture) 
                                                                VALUES ('".$store_code."','".$store_fullname."','".$store_namestore."','".$store_email."','".$store_address."'
                                                                ,'".$store_tel."',
                                                                '".$store_password."','".$fileName1."','".$store_bank_code."','".$store_bank_brand."','".$store_nameinbank."',
                                                                '".$recipient['id']."','".$fileName2."' )";     
                                                            
                                                                if ($conn->query($InsertSQL) === TRUE) {
                                                                    echo json_encode(array("result"=>"Success"));
                                                                }

                                                            }

                                        }

                            }

                        }

            }

}
?>