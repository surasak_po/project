<?php
include "setting/config.php ";
@header("content-type:application/json;charset=utf-8");
@header("Access-Control-Allow-Origin: *");
@header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');

    if($_SERVER["REQUEST_METHOD"]=="POST"){
        
        $content = @file_get_contents('php://input'); 
        $json_data = @json_decode($content, true);

        @$product_type_name = trim($json_data['product_type_name']);

        $CheckProductTypeNameDuplicate="SELECT * FROM tbl_product_type 
        WHERE product_type_name='".$product_type_name."' ";
        $ResultCheckProductTypeNameDuplicate=@$conn->query($CheckProductTypeNameDuplicate);
        if($ResultCheckProductTypeNameDuplicate->num_rows > 0){
            echo json_encode(array("result"=>"PRODUCT_TYPE_NAME_DUPLICATE"));
        }else{

                $StrGetNo = "SELECT MAX(product_type_no) As product_type_no FROM tbl_product_type";
                $ResultGetNo = @$conn->query($StrGetNo);
                if($ResultGetNo->num_rows > 0){
                    while($row_get_no = $ResultGetNo->fetch_assoc()){
                        $product_type_no = $row_get_no['product_type_no'];
                        
                        $product_type_code = sprintf("PROTYPE%07s",($product_type_no+1))."-".rand(0,9);

                           $InsertSQL="INSERT INTO tbl_product_type(product_type_code,product_type_name) 
                           VALUES ('".$product_type_code."','".$product_type_name."')";     
                                                                
                           if ($conn->query($InsertSQL) === TRUE) {   

                            echo json_encode(array("result"=>"Success"));
                            }
                                       
                    }
                
            }else{

                $product_type_code = sprintf("PROTYPE%07s",1)."-".rand(0,9);

                $InsertSQL="INSERT INTO tbl_product_type(product_type_code,product_type_name) 
                VALUES ('".$product_type_code."','".$product_type_name."')";     
                                    
                if ($conn->query($InsertSQL) === TRUE) {   

                    echo json_encode(array("result"=>"Success"));
                }

                }

        }

}

?>