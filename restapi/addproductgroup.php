<?php
include "setting/config.php ";
define('UPLOAD_DIR', 'product/');
@header("content-type:application/json;charset=utf-8");
@header("Access-Control-Allow-Origin: *");
@header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');

    if($_SERVER["REQUEST_METHOD"]=="POST"){
        
        $content = @file_get_contents('php://input'); 
        $json_data = @json_decode($content, true);

        @$product_group_name  = trim($json_data['product_group_name']);
        @$product_group_picture = trim($json_data['product_group_picture']);
        @$product_group_wide = trim($json_data['product_group_wide']);
        @$product_group_long = trim($json_data['product_group_long']);
        @$product_group_high = trim($json_data['product_group_high']);
        @$product_group_weight = trim($json_data['product_group_weight']);
        @$product_group_unit_name = trim($json_data['product_group_unit_name']);
        @$product_group_price = trim($json_data['product_group_price']);
        @$product_group_num = trim($json_data['product_group_num']);
        @$product_code = trim($json_data['product_code']);

    $CheckNameDuplicate="SELECT * FROM tbl_product_group WHERE product_group_name='".$product_group_name."' 
    AND product_code ='".$product_code."' ";
    $ResultCheckNameDuplicate=@$conn->query($CheckNameDuplicate);
    if($ResultCheckNameDuplicate->num_rows > 0){
        echo json_encode(array("result"=>"PRODUCTGROUP_NAME_DUPLICATE"));
    }else{

        $StrGetNo = "SELECT MAX(product_group_no) As product_group_no FROM tbl_product_group";
        $ResultGetNo = @$conn->query($StrGetNo);
        if($ResultGetNo->num_rows > 0){
            while($row_get_no = $ResultGetNo->fetch_assoc()){
                $product_group_no = $row_get_no['product_group_no'];
                $product_group_code = sprintf("PROGROUP%07s",($product_group_no+1))."-".rand(0,9);

                if($product_group_picture==""){

                            $InsertSQL="INSERT INTO tbl_product_group(product_group_code,product_group_name,product_group_wide,product_group_long,
                            product_group_high,product_group_weight,product_group_unit_name,product_group_price,product_group_num,product_code) 
                            VALUES ('".$product_group_code."','".$product_group_name."','".$product_group_wide."','".$product_group_long."',
                            '".$product_group_high."','".$product_group_weight."','".$product_group_unit_name."','".$product_group_price."',
                            '".$product_group_num."','".$product_code."')";     
                                                
                            if ($conn->query($InsertSQL) === TRUE) {
                                echo json_encode(array("result"=>"Success"));
                            }

                }else{

                            $product_group_picture = explode(',',trim($json_data['product_group_picture']));
                            $image_type_aux = explode("image/", $product_group_picture[0]);
                            $image_type = $image_type_aux[1];
                            $image_base64 = base64_decode($product_group_picture[1]);
                            $fileName = $product_group_code . '.png';
                            $file = UPLOAD_DIR . $fileName;    
                            file_put_contents($file, $image_base64);

                                        $InsertSQL="INSERT INTO tbl_product_group(product_group_code,product_group_name,product_group_wide,product_group_long,
                                        product_group_high,product_group_weight,product_group_unit_name,product_group_price,product_group_num,product_code,product_group_picture) 
                                        VALUES ('".$product_group_code."','".$product_group_name."','".$product_group_wide."','".$product_group_long."',
                                        '".$product_group_high."','".$product_group_weight."','".$product_group_unit_name."','".$product_group_price."',
                                        '".$product_group_num."','".$product_code."','".$fileName."')";     
                                                            
                                        if ($conn->query($InsertSQL) === TRUE) {
                                            echo json_encode(array("result"=>"Success"));
                                        }

                }
   
            }

        }else{
            $product_group_code = sprintf("PROGROUP%07s",1)."-".rand(0,9);

                        if($product_group_picture==""){

                            $InsertSQL="INSERT INTO tbl_product_group(product_group_code,product_group_name,product_group_wide,product_group_long,
                            product_group_high,product_group_weight,product_group_unit_name,product_group_price,product_group_num,product_code) 
                            VALUES ('".$product_group_code."','".$product_group_name."','".$product_group_wide."','".$product_group_long."',
                            '".$product_group_high."','".$product_group_weight."','".$product_group_unit_name."','".$product_group_price."',
                            '".$product_group_num."','".$product_code."')";     
                                                
                            if ($conn->query($InsertSQL) === TRUE) {
                                echo json_encode(array("result"=>"Success"));
                            }

                        }else{

                            $product_group_picture = explode(',',trim($json_data['product_group_picture']));
                            $image_type_aux = explode("image/", $product_group_picture[0]);
                            $image_type = $image_type_aux[1];
                            $image_base64 = base64_decode($product_group_picture[1]);
                            $fileName = $product_group_code . '.png';
                            $file = UPLOAD_DIR . $fileName;    
                            file_put_contents($file, $image_base64);
                            
                            $InsertSQL="INSERT INTO tbl_product_group(product_group_code,product_group_name,product_group_wide,product_group_long,
                            product_group_high,product_group_weight,product_group_unit_name,product_group_price,product_group_num,product_code,product_group_picture) 
                            VALUES ('".$product_group_code."','".$product_group_name."','".$product_group_wide."','".$product_group_long."',
                            '".$product_group_high."','".$product_group_weight."','".$product_group_unit_name."','".$product_group_price."',
                            '".$product_group_num."','".$product_code."','".$fileName."')";     
                                                
                            if ($conn->query($InsertSQL) === TRUE) {
                                echo json_encode(array("result"=>"Success"));
                            
                            }

                }
        }

    }



}

?>