<?php
include "setting/config.php ";
define('UPLOAD_DIR', 'profile/');
@header("content-type:application/json;charset=utf-8");
@header("Access-Control-Allow-Origin: *");
@header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');


    if($_SERVER["REQUEST_METHOD"]=="POST"){
        
        $content = @file_get_contents('php://input'); 
        $json_data = @json_decode($content, true);

        @$customer_fullname = trim($json_data['customer_fullname']);
        @$customer_email = trim($json_data['customer_email']);
        @$customer_tel = trim($json_data['customer_tel']);
        @$customer_password = trim($json_data['customer_password']);
        @$customer_profile = trim($json_data['customer_profile']);

    $CheckEmailDuplicate="SELECT * FROM tbl_customer WHERE customer_email='".$customer_email."' ";
    $ResultCheckEmailDuplicate=@$conn->query($CheckEmailDuplicate);
    if($ResultCheckEmailDuplicate->num_rows > 0){
        echo json_encode(array("result"=>"EMAIL_DUPLICATE"));
    }else{

        $CheckTelDuplicate="SELECT * FROM tbl_customer WHERE customer_tel='".$customer_tel."' ";
        $ResultCheckTelDuplicate=@$conn->query($CheckTelDuplicate);
        if($ResultCheckTelDuplicate->num_rows > 0){
            echo json_encode(array("result"=>"TEL_DUPLICATE"));
        }else{

                $StrGetNo = "SELECT MAX(customer_no) As customer_no FROM tbl_customer";
                $ResultGetNo = @$conn->query($StrGetNo);
                if($ResultGetNo->num_rows > 0){
                    while($row_get_no = $ResultGetNo->fetch_assoc()){
                        $customer_no = $row_get_no['customer_no'];
                        $customer_code = sprintf("CUS%07s",($customer_no+1))."-".rand(0,9);
                
                        if(@$customer_profile==""){

                            $InsertSQL="INSERT INTO tbl_customer(customer_code,customer_fullname,customer_email
                            ,customer_tel,customer_password) VALUES ('".$customer_code."','".$customer_fullname."'
                            ,'".$customer_email."','".$customer_tel."','".$customer_password."')";     
                                                
                            if ($conn->query($InsertSQL) === TRUE) {
                                echo json_encode(array("result"=>"Success"));
                            }

                        }else{
                            $customer_profile = explode(',',trim($json_data['customer_profile']));
                            $image_type_aux = explode("image/", $customer_profile[0]);
                            $image_type = $image_type_aux[1];
                            $image_base64 = base64_decode($customer_profile[1]);
                            $fileName = $customer_code . '.png';
                            $file = UPLOAD_DIR . $fileName;    
    
                                 file_put_contents($file, $image_base64);
                                     
                                                $InsertSQL="INSERT INTO tbl_customer(customer_code,customer_fullname,customer_email,
                                                customer_tel,customer_profile,customer_password) VALUES ('".$customer_code."',
                                                '".$customer_fullname."','".$customer_email."','".$customer_tel."','".$fileName."',
                                                '".$customer_password."')";     
                                                                    
                                                if ($conn->query($InsertSQL) === TRUE) {
                                                    echo json_encode(array("result"=>"Success"));
                                                }
    
                                 }
                  
                            }
          
            }else{
                    $customer_code = sprintf("CUS%07s",1)."-".rand(0,9);

                    if(@$customer_profile==""){

                            $InsertSQL="INSERT INTO tbl_customer(customer_code,customer_fullname,customer_email,
                            customer_tel,customer_password) VALUES ('".$customer_code."','".$customer_fullname."',
                            '".$customer_email."','".$customer_tel."','".$customer_password."')";     
                                                
                            if ($conn->query($InsertSQL) === TRUE) {
                                echo json_encode(array("result"=>"Success"));
                            }

                    }else{
                                $customer_profile = explode(',',trim($json_data['customer_profile']));
                                $image_type_aux = explode("image/", $customer_profile[0]);
                                $image_type = $image_type_aux[1];
                                $image_base64 = base64_decode($customer_profile[1]);
                                $fileName = $customer_code . '.png';
                                $file = UPLOAD_DIR . $fileName;
                                file_put_contents($file, $image_base64);

                                        echo json_encode(array("result"=>"Success"));
                                        $InsertSQL="INSERT INTO tbl_customer(customer_code,customer_fullname,
                                        customer_email,customer_tel,customer_profile,
                                        customer_password) VALUES ('".$customer_code."','".$customer_fullname."',
                                        '".$customer_email."','".$customer_tel."','".$fileName."','".$customer_password."')";     
                                                        
                                if ($conn->query($InsertSQL) === TRUE) {   
            
                                    echo json_encode(array("result"=>"Success"));
            
                                }

                    }
                
             }
  
        }
    }

}

?>