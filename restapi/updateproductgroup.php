<?php
///**** BY Surasak.po 7/29/2020  */
///include connection
include "setting/config.php ";
//header
define('UPLOAD_DIR', 'product/');
@header("content-type:application/json;charset=utf-8");
@header("Access-Control-Allow-Origin: *");
@header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');

    if($_SERVER["REQUEST_METHOD"]=="POST"){
        
        $content = @file_get_contents('php://input'); 
        $json_data = @json_decode($content, true);

        @$product_group_code  = trim($json_data['product_group_code']);
        @$product_group_name  = trim($json_data['product_group_name']);
        @$product_group_picture = trim($json_data['product_group_picture']);
        @$product_group_wide = trim($json_data['product_group_wide']);
        @$product_group_long = trim($json_data['product_group_long']);
        @$product_group_high = trim($json_data['product_group_high']);
        @$product_group_weight = trim($json_data['product_group_weight']);
        @$product_group_unit_name = trim($json_data['product_group_unit_name']);
        @$product_group_price = trim($json_data['product_group_price']);
        @$product_group_num = trim($json_data['product_group_num']);
        @$product_code = trim($json_data['product_code']);

    }else{
        echo json_encode(array("result"=>"USE REQUEST METHOD POST!!!"));

    }

//process
if($_SERVER["REQUEST_METHOD"]=="POST"){

    $CheckNameDuplicate="SELECT * FROM tbl_product_group WHERE product_group_name='".$product_group_name."' AND product_group_code != '".$product_group_code."' ";
    $ResultCheckNameDuplicate=@$conn->query($CheckNameDuplicate);
    if($ResultCheckNameDuplicate->num_rows > 0){
        echo json_encode(array("result"=>"PRODUCT_GROUP_NAME_DUPLICATE"));
    }else{
        
        if($product_group_picture==""){

            $UpdateSQL="UPDATE tbl_product_group SET 
            product_group_name = '".$product_group_name."',product_group_wide = '".$product_group_wide."' ,product_group_long = '".$product_group_long."',
            product_group_high = '".$product_group_high."' ,product_group_weight = '".$product_group_weight."',product_group_unit_name = '".$product_group_unit_name."',
            product_group_price = '".$product_group_price."',product_group_num = '".$product_group_num."',product_code = '".$product_code."' 
            WHERE product_group_code = '".$product_group_code."' ";    

            if ($conn->query($UpdateSQL) === TRUE) {
                                
                echo json_encode(array("result"=>"Success"));
            }else{
                echo json_encode(array("result"=>"Failed".$conn->error));
            }

        }else{
            $product_group_picture = explode(',',trim($json_data['product_group_picture']));
            $image_type_aux = explode("image/", $product_group_picture[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($product_group_picture[1]);
            $fileName = $product_group_code.'.png';
            $file = UPLOAD_DIR . $fileName;
            file_put_contents($file, $image_base64);

            $UpdateSQL="UPDATE tbl_product_group SET 
            product_group_name = '".$product_group_name."',product_group_wide = '".$product_group_wide."',product_group_long = '".$product_group_long."',
            product_group_high = '".$product_group_high."',product_group_weight = '".$product_group_weight."',product_group_unit_name = '".$product_group_unit_name."',
            product_group_price = '".$product_group_price."',product_group_num = '".$product_group_num."',product_code = '".$product_code."',product_group_picture = '".$fileName."'
            WHERE product_group_code = '".$product_group_code."' ";    

            if ($conn->query($UpdateSQL) === TRUE) {
                                
                echo json_encode(array("result"=>"Success"));
            }else{
                echo json_encode(array("result"=>"Failed"));
            }

        }

    }

}

 // log
    /*$ip = $_SERVER['REMOTE_ADDR'];   //ดึงค่า ip address ออกมา
    $date = @date("d/m/Y H:i:s");   //วันที่ส่งข้อมูล 
    $objFopen= @fopen("deletebid.log","a+");    //ถ้าไม่มีไฟล์ให้สร้าง ถ้ามีให้เขียนทับ
    $str1="\n";
    $str = $date." |-> IP:".$ip." |->คำสั่ง ".@$delete_id."\n".$str1;
   
    @fwrite($objFopen,$str);
    @fclose($objFopen);*/
?>