<?php
   
    include "setting/Config.php";

    @header("content-type:application/json;charset=utf-8");
    @header("Access-Control-Allow-Origin: *");
    @header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');

    if($_SERVER["REQUEST_METHOD"]=="POST") {
        $content = @file_get_contents('php://input'); 
        $json_data = @json_decode($content, true);
        @$customerUsername = trim($json_data['customerUsername']);
        @$customerPassword = trim($json_data['customerPassword']);
    }

    if($customerUsername == "" OR $customerPassword == "") {
        echo json_encode(array("result"=>"Null"));
    } else {
        $strSQL = "SELECT * FROM tbl_customer WHERE customer_tel = '".$customerUsername."' OR customer_email = '".$customerUsername."'" ;
        $result = @$conn->query($strSQL);
        if($result->num_rows > 0){
            while ($row = $result->fetch_assoc()) { 
                $password = $row['customer_password'];
                if($customerPassword == $password) {

                    if($row['customer_banned']=="1"){
                        
                        echo json_encode(array("result"=>"Banned"));

                    }else{
                    @session_start();

                         ///////start addcart
                    if(!empty($_SESSION["cart_item"])){
                        foreach($_SESSION["cart_item"] as $k => $v) {

                            $strgetstorecode = "SELECT * FROM tbl_product_group 
                            INNER JOIN tbl_product ON tbl_product_group.product_code = tbl_product.product_code
                            WHERE product_group_code = '".$k."' " ;
                            $resultstrgetstorecode = @$conn->query($strgetstorecode);
                            if($resultstrgetstorecode->num_rows > 0){

                                while ($rowstrgetstorecode = $resultstrgetstorecode->fetch_assoc()) { 
                                $store_code = $rowstrgetstorecode['store_code'];

                                }

                                    $strcheckcart = "SELECT * FROM tbl_cart WHERE store_code = '".$store_code."' AND customer_code = '".$row['customer_code']."' " ;
                                    $resultstrcheckcart = @$conn->query($strcheckcart);
                                    if($resultstrcheckcart->num_rows > 0){
                                         //ถ้าตะกร้าร้านนี้มี
                                        while ($rowstrcheckcart = $resultstrcheckcart->fetch_assoc()) { 
  
                                        $cart_code = $rowstrcheckcart['cart_code'];

                                        }
                                            
                                            //เช็คในรายละเอียดตะกร้าว่ามีสินค้านี้ไหม
                                            $strcheckcartdetail = " SELECT * FROM tbl_cart_detail WHERE cart_code = '".$cart_code."' AND product_group_code = '".$k."' ";
                                            $resultstrcheckcartdetail = @$conn->query($strcheckcartdetail);
                                            if($resultstrcheckcartdetail->num_rows > 0){
                                                //ถ้ามี
                                                while ($rowstrcheckcartdetail = $resultstrcheckcartdetail->fetch_assoc()) {

                                                    $cart_detail_code = $rowstrcheckcartdetail['cart_detail_code'];
                                                    $cart_detail_num = $rowstrcheckcartdetail['cart_detail_num'];
                                                }
                                                $cart_detail_num = $cart_detail_num + $_SESSION["cart_item"][$k]["cart_detail_num"];
                                                $strupdatecartdetail = "UPDATE tbl_cart_detail SET cart_detail_num = $cart_detail_num WHERE cart_detail_code = '".$cart_detail_code."' ";
                                                $conn->query($strupdatecartdetail);
                                                //
                                            }else{
                                                //ไม่มีรายละเอียดตะกร้า
                                                $strcartdetailno = "SELECT MAX(cart_detail_no) As cart_detail_no FROM tbl_cart_detail";
                                                $Resultstrcartdetailno = @$conn->query($strcartdetailno);
                                                if($Resultstrcartdetailno->num_rows > 0){

                                                    while($rowstrcartdetailno = $Resultstrcartdetailno->fetch_assoc()){
                                                        $cart_detail_no = $rowstrcartdetailno['cart_detail_no'];
                                                        $cart_detail_code = sprintf("CARTDETAIL%07s",($cart_detail_no+1))."-".rand(0,9);    
                                                
                                                    }
                                                }else{
                                                        $cart_detail_code = sprintf("CARTDETAIL%07s",1)."-".rand(0,9);
                                                }
                                                //
                                                $cart_detail_num = $_SESSION["cart_item"][$k]["cart_detail_num"];
                                                $strinsertcartdetail = "INSERT INTO tbl_cart_detail(cart_detail_code,cart_code,product_group_code,cart_detail_num) VALUES ('".$cart_detail_code."','".$cart_code."','".$k."','$cart_detail_num')";     
                                                $conn->query($strinsertcartdetail);
                                            }
                                            

                                    }else{
                                        //ถ้าตะกร้าร้านนี้ไม่มี
                                        $Resultstrcartno = @$conn->query($strcartno);
                                        if($Resultstrcartno->num_rows > 0){
                                            while($rowstrcartno = $Resultstrcartno->fetch_assoc()){
                        
                                                $cart_no = $rowstrcartno['cart_detail_no'];
                                                $cart_code = sprintf("CART%07s",($cart_no+1))."-".rand(0,9);
                        
                                            }
                                        }else{
                                                $cart_code = sprintf("CART%07s",1)."-".rand(0,9);
                                        }

                                        $getshippingcode="SELECT * FROM tbl_shipping WHERE store_code = '".$store_code."' LIMIT 1";
                                        $Resultgetshippingcode=@$conn->query($getshippingcode);
                                        if($Resultgetshippingcode->num_rows > 0){
                                            while($rowgetshippingcode = $Resultgetshippingcode->fetch_assoc()){
                                                $shipping_code=$rowgetshippingcode['shipping_code'];
                                            }
                                        }

                                        $strinsertcart = "INSERT INTO tbl_cart(cart_code,customer_code,store_code,shipping_code) VALUES ('".$cart_code."','".$row['customer_code']."','".$store_code."','".$shipping_code."')";     
                                        if ($conn->query($strinsertcart) === TRUE) {

                                            $strcartdetailno = "SELECT MAX(cart_detail_no) As cart_detail_no FROM tbl_cart_detail";
                                            $Resultstrcartdetailno = @$conn->query($strcartdetailno);
                                            if($Resultstrcartdetailno->num_rows > 0){

                                                while($rowstrcartdetailno = $Resultstrcartdetailno->fetch_assoc()){
                                                    $cart_detail_no = $rowstrcartdetailno['cart_detail_no'];
                                                    $cart_detail_code = sprintf("CARTDETAIL%07s",($cart_detail_no+1))."-".rand(0,9);    
                                            
                                                }
                                            }else{
                                                    $cart_detail_code = sprintf("CARTDETAIL%07s",1)."-".rand(0,9);
                                            }
                                            //
                                            $cart_detail_num = $_SESSION["cart_item"][$k]["cart_detail_num"];
                                            $strinsertcartdetail = "INSERT INTO tbl_cart_detail(cart_detail_code,cart_code,product_group_code,cart_detail_num) VALUES ('".$cart_detail_code."','".$cart_code."','".$k."','$cart_detail_num')";     
                                            $conn->query($strinsertcartdetail);

                                        }                         
                                        //
                                    }

                            }

                           

                        }
                    }
                        ///////end addcart
                        
                    @session_destroy();
                    @session_start();
                    $_SESSION['customer_code'] = $row['customer_code'];
                    $_SESSION['role'] = "customer";
                    echo json_encode(array("result"=>"Success"));
                    }

                } else {
                    echo json_encode(array("result"=>"WrongPassword"));
                }
            }
        } else {
            echo json_encode(array("result"=>"NotFound"));
        }
    }
?>