<?php
    /* BY Surasak.po 7/29/2020 */
    include "setting/Config.php";
    
    @header("content-type:application/json;charset=utf-8");
    @header("Access-Control-Allow-Origin: *");
    @header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');

    if($_SERVER["REQUEST_METHOD"]=="POST") {
        $content = @file_get_contents('php://input'); 
        $json_data = @json_decode($content, true);
        @$storeUsername = trim($json_data['storeUsername']);
        @$storePassword = trim($json_data['storePassword']);
    }

    if($storeUsername == "" OR $storePassword == "") {
        echo json_encode(array("result"=>"Null"));
    } else {
        $strSQL = "SELECT * FROM tbl_store WHERE store_tel = '".$storeUsername."' OR store_email = '".$storeUsername."'" ;
        $result = @$conn->query($strSQL);
        if($result->num_rows > 0){
            while ($row = $result->fetch_assoc()) { 
                $password = $row['store_password'];
                if($storePassword == $password) {

                    if($row['store_status']=="3"){
                        
                        echo json_encode(array("result"=>"Banned"));

                    }else{
                        @session_start();
                        @session_destroy();
                        @session_start();
                        $_SESSION['store_code'] = $row['store_code'];
                        $_SESSION['role'] = "store";
                        echo json_encode(array("result"=>"Success"));
                    }

                } else {
                    echo json_encode(array("result"=>"WrongPassword"));
                }
            }
        } else {
            echo json_encode(array("result"=>"NotFound"));
        }
    }
?>