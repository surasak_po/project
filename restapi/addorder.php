<?php
/**** BY Surasak.po  */
require_once 'omise-php/lib/Omise.php';
include "setting/config.php ";

define('UPLOAD_DIR', 'profile/');
@header("content-type:application/json;charset=utf-8");
@header("Access-Control-Allow-Origin: *");
@header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');

define('OMISE_PUBLIC_KEY', 'pkey_test_5kwbj3tbv09itjm0p8m');
define('OMISE_SECRET_KEY', 'skey_test_5k7gu0smng7hk1pb84j');


    if($_SERVER["REQUEST_METHOD"]=="POST"){
        
        $content = @file_get_contents('php://input'); 
        $json_data = @json_decode($content, true);

        @$customer_code = trim($json_data['customer_code']);
        @$customer_address_code = trim($json_data['customer_address_code']);
        @$omise_token = trim($json_data['omise_token']);
        @$totalprice = trim($json_data['totalprice']);
        @$shipping_price = trim($json_data['shipping_price']);
        @$payment_type = trim($json_data['payment_type']);
        
        @$payment_token="";
        @$paymentcredit_status = "";
        if($payment_type=="credit_card"){
            $add = OmiseCharge::create(array(
                'amount' => $totalprice,
                'currency' => 'thb',
                'card' => $omise_token
            ));

            if($add['status']=="successful"){
                $paymentcredit_status = "1";

                $payment_token = $add['id'];
            }else{
                $paymentcredit_status = "0";
            }  
        }

        if(@$paymentcredit_status=="1"){
    $strcheckcart="SELECT * FROM tbl_cart 
    INNER JOIN tbl_shipping ON tbl_cart.shipping_code = tbl_shipping.shipping_code
    WHERE customer_code ='".$customer_code."' ";
    $Resultstrcheckcart=@$conn->query($strcheckcart);
    if($Resultstrcheckcart->num_rows > 0){
        while($rowstrcheckcart = $Resultstrcheckcart->fetch_assoc()){

                $cart_code =  $rowstrcheckcart['cart_code']; 
                $store_code = $rowstrcheckcart['store_code'];
                $shipping_code = $rowstrcheckcart['shipping_code'];
                $shipping_price = $rowstrcheckcart['shipping_price'];
                $order_code = "";

          
                $strorderno = "SELECT MAX(order_no) As order_no FROM tbl_order";
                $Resultstrorderno = @$conn->query($strorderno);
                if($Resultstrorderno->num_rows > 0){
                    while($rowstrorderno = $Resultstrorderno->fetch_assoc()){
                        $order_no = $rowstrorderno['order_no'];
                        $order_code = sprintf("ORDER%07s",($order_no+1))."-".rand(0,9);
                    }
                }else{
                        $order_code = sprintf("ORDER%07s",1)."-".rand(0,9);
                }
              
                @date_default_timezone_set('Asia/Bangkok');
                $today = date("Y-m-d H:i:s");
                $insertorder = "INSERT INTO tbl_order(order_code,order_date,customer_code,store_code,shipping_code,customer_address_code,order_shipping_price,order_status) VALUES ('".$order_code."','".$today."','".$customer_code."','".$store_code."','".$shipping_code."','".$customer_address_code."','$shipping_price','1') ";
                $conn->query($insertorder);
                
                $getcartdetail="SELECT * FROM tbl_cart_detail 
                INNER JOIN tbl_product_group ON tbl_cart_detail.product_group_code = tbl_product_group.product_group_code
                WHERE cart_code ='".$cart_code."' AND cart_detail_status = '1' ";
                $Resultgetcartdetail=@$conn->query($getcartdetail);
                if($Resultgetcartdetail->num_rows > 0){
                    while($rowgetcartdetail = $Resultgetcartdetail->fetch_assoc()){
                        
                        $cart_detail_code=$rowgetcartdetail['cart_detail_code'];
                        $order_detail_num=$rowgetcartdetail['cart_detail_num'];
                        $order_detail_price=$rowgetcartdetail['product_group_price'];
                        $product_group_code=$rowgetcartdetail['product_group_code'];
                        $order_detail_code = "";

                        $strorderdetailno = "SELECT MAX(order_detail_no) As order_detail_no FROM tbl_order_detail";
                        $Resultstrorderdetailno = @$conn->query($strorderdetailno);
                        if($Resultstrorderdetailno->num_rows > 0){
                            while($rowstrorderdetailno = $Resultstrorderdetailno->fetch_assoc()){
                                $order_detail_no = $rowstrorderdetailno['order_detail_no'];
                                $order_detail_code = sprintf("ORDERDETAIL%07s",($order_detail_no+1))."-".rand(0,9);
                            }
                        }else{
                                $order_detail_code = sprintf("ORDERDETAIL%07s",1)."-".rand(0,9);
                        }
                        
                        $insertorderdetail = "INSERT INTO tbl_order_detail(order_detail_code,order_detail_num,order_detail_price,product_group_code,order_code) VALUES ('".$order_detail_code."','".$order_detail_num."','".$order_detail_price."','".$product_group_code."','".$order_code."') ";
                        
                        if($conn->query($insertorderdetail)===TRUE){
                            $updatestock ="UPDATE tbl_product_group SET product_group_num = product_group_num - '$order_detail_num' WHERE  product_group_code = '".$product_group_code."' ";
                            if ($conn->query($updatestock) === TRUE) {
                                
                                $deletecartdetail = "DELETE FROM tbl_cart_detail WHERE cart_detail_code ='".$cart_detail_code."' ";
                                if ($conn->query($deletecartdetail) === TRUE) {
    
                                      
                                }
                            }
                        }
                       
                    }
     
                }else{
                    $deleteorder = "DELETE FROM tbl_order WHERE order_code ='".$order_code."' ";
                    $conn->query($deleteorder);
                 
                }

     
                $strpaymentno = "SELECT MAX(payment_no) As payment_no FROM tbl_payment";
                $Resultstrpaymentno = @$conn->query($strpaymentno);
                if($Resultstrpaymentno->num_rows > 0){
                    while($rowstrpaymentno = $Resultstrpaymentno->fetch_assoc()){
                        $payment_no = $rowstrpaymentno['payment_no'];
                        $payment_code = sprintf("PAYMENT%07s",($payment_no+1))."-".rand(0,9);
                    }
                }else{
                        $payment_code = sprintf("PAYMENT%07s",1)."-".rand(0,9);
                }

                $paymentinsert = "INSERT INTO tbl_payment(payment_code,payment_token,payment_status,payment_type,customer_code,order_code) VALUES ('".$payment_code."','".$payment_token."','1','credit_card','".$customer_code."','".$order_code."') ";  
                $conn->query($paymentinsert);
            
        }
    }
            echo json_encode(array("result"=>"Success"));

        }
        if(@$paymentcredit_status=="0"){
            $strcheckcart="SELECT * FROM tbl_cart 
            INNER JOIN tbl_shipping ON tbl_cart.shipping_code = tbl_shipping.shipping_code
            WHERE customer_code ='".$customer_code."' ";
    $Resultstrcheckcart=@$conn->query($strcheckcart);
    if($Resultstrcheckcart->num_rows > 0){
        while($rowstrcheckcart = $Resultstrcheckcart->fetch_assoc()){

                $cart_code =  $rowstrcheckcart['cart_code']; 
                $store_code = $rowstrcheckcart['store_code'];
                $shipping_code = $rowstrcheckcart['shipping_code'];
                $shipping_price = $rowstrcheckcart['shipping_price'];
                $order_code = "";

                $strorderno = "SELECT MAX(order_no) As order_no FROM tbl_order";
                $Resultstrorderno = @$conn->query($strorderno);
                if($Resultstrorderno->num_rows > 0){
                    while($rowstrorderno = $Resultstrorderno->fetch_assoc()){
                        $order_no = $rowstrorderno['order_no'];
                        $order_code = sprintf("ORDER%07s",($order_no+1))."-".rand(0,9);
                    }
                }else{
                        $order_code = sprintf("ORDER%07s",1)."-".rand(0,9);
                }
           
                @date_default_timezone_set('Asia/Bangkok');
                $today = date("Y-m-d H:i:s");
                $insertorder = "INSERT INTO tbl_order(order_code,order_date,customer_code,store_code,shipping_code,customer_address_code,order_shipping_price,order_status) VALUES ('".$order_code."','".$today."','".$customer_code."','".$store_code."','".$shipping_code."','".$customer_address_code."','$shipping_price','1') ";
                $conn->query($insertorder);
                
                $getcartdetail="SELECT * FROM tbl_cart_detail 
                INNER JOIN tbl_product_group ON tbl_cart_detail.product_group_code = tbl_product_group.product_group_code
                WHERE cart_code ='".$cart_code."' AND cart_detail_status = '1' ";
                $Resultgetcartdetail=@$conn->query($getcartdetail);
                if($Resultgetcartdetail->num_rows > 0){
                    while($rowgetcartdetail = $Resultgetcartdetail->fetch_assoc()){
                        
                        $cart_detail_code=$rowgetcartdetail['cart_detail_code'];
                        $order_detail_num=$rowgetcartdetail['cart_detail_num'];
                        $order_detail_price=$rowgetcartdetail['product_group_price'];
                        $product_group_code=$rowgetcartdetail['product_group_code'];
                        $order_detail_code = "";

                        $strorderdetailno = "SELECT MAX(order_detail_no) As order_detail_no FROM tbl_order_detail";
                        $Resultstrorderdetailno = @$conn->query($strorderdetailno);
                        if($Resultstrorderdetailno->num_rows > 0){
                            while($rowstrorderdetailno = $Resultstrorderdetailno->fetch_assoc()){
                                $order_detail_no = $rowstrorderdetailno['order_detail_no'];
                                $order_detail_code = sprintf("ORDERDETAIL%07s",($order_detail_no+1))."-".rand(0,9);
                            }
                        }else{
                                $order_detail_code = sprintf("ORDERDETAIL%07s",1)."-".rand(0,9);
                        }
                        
                        $insertorderdetail = "INSERT INTO tbl_order_detail(order_detail_code,order_detail_num,order_detail_price,product_group_code,order_code) VALUES ('".$order_detail_code."','".$order_detail_num."','".$order_detail_price."','".$product_group_code."','".$order_code."') ";
                        
                        if($conn->query($insertorderdetail)===TRUE){
                            $updatestock ="UPDATE tbl_product_group SET product_group_num = product_group_num - '$order_detail_num' WHERE  product_group_code = '".$product_group_code."' ";
                            if ($conn->query($updatestock) === TRUE) {
                                
                                $deletecartdetail = "DELETE FROM tbl_cart_detail WHERE cart_detail_code ='".$cart_detail_code."' ";
                                if ($conn->query($deletecartdetail) === TRUE) {
    
                                      
                                }
                            }
                        }
                       
                    }
     
                }else{
                    $deleteorder = "DELETE FROM tbl_order WHERE order_code ='".$order_code."' ";
                    $conn->query($deleteorder);
                 
                }

           
                $strpaymentno = "SELECT MAX(payment_no) As payment_no FROM tbl_payment";
                $Resultstrpaymentno = @$conn->query($strpaymentno);
                if($Resultstrpaymentno->num_rows > 0){
                    while($rowstrpaymentno = $Resultstrpaymentno->fetch_assoc()){
                        $payment_no = $rowstrpaymentno['payment_no'];
                        $payment_code = sprintf("PAYMENT%07s",($payment_no+1))."-".rand(0,9);
                    }
                }else{
                        $payment_code = sprintf("PAYMENT%07s",1)."-".rand(0,9);
                }

                $paymentinsert = "INSERT INTO tbl_payment(payment_code,payment_token,payment_status,payment_type,customer_code,order_code) VALUES ('".$payment_code."','-','0','credit_card','".$customer_code."','".$order_code."') ";  
                $conn->query($paymentinsert);
        

        }
    }
            echo json_encode(array("result"=>"FAILED"));

        }

    if($payment_type=="promptpay"){
    $newArr = [];
    $strcheckcart="SELECT * FROM tbl_cart 
    INNER JOIN tbl_shipping ON tbl_cart.shipping_code = tbl_shipping.shipping_code
    WHERE customer_code ='".$customer_code."' ";
    $Resultstrcheckcart=@$conn->query($strcheckcart);
    if($Resultstrcheckcart->num_rows > 0){
        while($rowstrcheckcart = $Resultstrcheckcart->fetch_assoc()){

                $cart_code =  $rowstrcheckcart['cart_code']; 
                $store_code = $rowstrcheckcart['store_code'];
                $shipping_code = $rowstrcheckcart['shipping_code'];
                $shipping_price = $rowstrcheckcart['shipping_price'];
                $order_code = "";

                $strorderno = "SELECT MAX(order_no) As order_no FROM tbl_order";
                $Resultstrorderno = @$conn->query($strorderno);
                if($Resultstrorderno->num_rows > 0){
                    while($rowstrorderno = $Resultstrorderno->fetch_assoc()){
                        $order_no = $rowstrorderno['order_no'];
                        $order_code = sprintf("ORDER%07s",($order_no+1))."-".rand(0,9);
                    }
                }else{
                        $order_code = sprintf("ORDER%07s",1)."-".rand(0,9);
                }
                @date_default_timezone_set('Asia/Bangkok');
                $today = date("Y-m-d H:i:s");
                $insertorder = "INSERT INTO tbl_order(order_code,order_date,customer_code,store_code,shipping_code,customer_address_code,order_shipping_price,order_status) VALUES ('".$order_code."','".$today."','".$customer_code."','".$store_code."','".$shipping_code."','".$customer_address_code."','$shipping_price','1') ";
                $conn->query($insertorder);
                
                $ordertotalprice = 0;
                $getcartdetail="SELECT * FROM tbl_cart_detail 
                INNER JOIN tbl_product_group ON tbl_cart_detail.product_group_code = tbl_product_group.product_group_code
                WHERE cart_code ='".$cart_code."' AND cart_detail_status = '1' ";
                $Resultgetcartdetail=@$conn->query($getcartdetail);
                if($Resultgetcartdetail->num_rows > 0){
                    while($rowgetcartdetail = $Resultgetcartdetail->fetch_assoc()){
                        
                        $cart_detail_code=$rowgetcartdetail['cart_detail_code'];
                        $order_detail_num=$rowgetcartdetail['cart_detail_num'];
                        $order_detail_price=$rowgetcartdetail['product_group_price'];
                        $ordertotalprice = $ordertotalprice + $order_detail_price*$order_detail_num;
                        $product_group_code=$rowgetcartdetail['product_group_code'];
                        $order_detail_code = "";

                        $strorderdetailno = "SELECT MAX(order_detail_no) As order_detail_no FROM tbl_order_detail";
                        $Resultstrorderdetailno = @$conn->query($strorderdetailno);
                        if($Resultstrorderdetailno->num_rows > 0){
                            while($rowstrorderdetailno = $Resultstrorderdetailno->fetch_assoc()){
                                $order_detail_no = $rowstrorderdetailno['order_detail_no'];
                                $order_detail_code = sprintf("ORDERDETAIL%07s",($order_detail_no+1))."-".rand(0,9);
                            }
                        }else{
                                $order_detail_code = sprintf("ORDERDETAIL%07s",1)."-".rand(0,9);
                        }
                        
                        $insertorderdetail = "INSERT INTO tbl_order_detail(order_detail_code,order_detail_num,order_detail_price,product_group_code,order_code) VALUES ('".$order_detail_code."','".$order_detail_num."','".$order_detail_price."','".$product_group_code."','".$order_code."') ";
                        
                        if($conn->query($insertorderdetail)===TRUE){
                            $updatestock ="UPDATE tbl_product_group SET product_group_num = product_group_num - '$order_detail_num' WHERE  product_group_code = '".$product_group_code."' ";
                            if ($conn->query($updatestock) === TRUE) {
                                
                                $deletecartdetail = "DELETE FROM tbl_cart_detail WHERE cart_detail_code ='".$cart_detail_code."' ";
                                if ($conn->query($deletecartdetail) === TRUE) {
    
                                      
                                }
                            }
                        }
                       
                    }
     
                }else{
                    $deleteorder = "DELETE FROM tbl_order WHERE order_code ='".$order_code."' ";
                    $conn->query($deleteorder);
                 
                }

                $strpaymentno = "SELECT MAX(payment_no) As payment_no FROM tbl_payment";
                $Resultstrpaymentno = @$conn->query($strpaymentno);
                if($Resultstrpaymentno->num_rows > 0){
                    while($rowstrpaymentno = $Resultstrpaymentno->fetch_assoc()){
                        $payment_no = $rowstrpaymentno['payment_no'];
                        $payment_code = sprintf("PAYMENT%07s",($payment_no+1))."-".rand(0,9);
                    }
                }else{
                        $payment_code = sprintf("PAYMENT%07s",1)."-".rand(0,9);
                }

                $shipping_price = $rowstrcheckcart['shipping_price'];
                $ordertotalprice = $ordertotalprice + $shipping_price;
                $ordertotalprice = (int)$ordertotalprice;
                $ordertotalprice = $ordertotalprice*100;
             
                $source = OmiseSource::create(array(
                    'amount' => $ordertotalprice,
                    'currency' => 'thb',
                    'type' => 'promptpay'
                  ));
    
                  $charge = OmiseCharge::create(array(
                    'amount' => $ordertotalprice,
                    'currency' => 'thb',
                    'return_uri' => '',
                    'source' => $source['id']
                  ));
    
    
                  $payment_token =  $charge['id'];
                  $chargeurl =  $charge['source']['scannable_code']['image']['download_uri'];
                 
                  
                $paymentinsert = "INSERT INTO tbl_payment(payment_code,payment_token,payment_status,payment_type,customer_code,order_code) VALUES ('".$payment_code."','".$payment_token."','0','promptpay','".$customer_code."','".$order_code."') ";  
                
                if($conn->query($paymentinsert)===TRUE){
                   
                    $newArr[] = array(
                        "order_code"=>$order_code,
                        "ordertotalprice"=>"฿".($ordertotalprice/100).".00",
                        "URL"=>$chargeurl
                    );
                }
         
        } 
        echo json_encode($newArr);
        }
    

     }   

    }
?>